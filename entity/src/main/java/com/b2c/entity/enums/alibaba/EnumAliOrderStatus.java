package com.b2c.entity.enums.alibaba;


/**
 * 描述：
 *
 * @author qlp
 * @date 2019-09-16 20:49
 */
public enum EnumAliOrderStatus {
    waitbuyerpay("等待买家付款", "waitbuyerpay"),
    waitsellersend("等待卖家发货", "waitsellersend"),
    waitbuyerreceive("等待买家确认收货", "waitbuyerreceive"),
    paid_but_not_fund("已支付，未到账", "paid_but_not_fund"),
    confirm_goods("已收货", "confirm_goods"),
    waitsellerconfirm("等待卖家确认订单", "waitsellerconfirm"),
    waitbuyerconfirm("等待买家确认订单", "waitbuyerconfirm"),
    confirm_goods_but_not_fund("已收货，未到账", "confirm_goods_but_not_fund"),
    confirm_goods_and_has_subsidy("已收货，已贴现", "confirm_goods_and_has_subsidy"),
    send_goods_but_not_fund("已发货，未到账", "send_goods_but_not_fund"),
    waitlogisticstakein("等待物流揽件", "waitlogisticstakein"),
    waitbuyersign("等待买家签收", "waitbuyersign"),
    signinsuccess("买家已签收", "signinsuccess"),
    signinfailed("签收失败", "signinfailed"),
    waitselleract("等待卖家操作", "waitselleract"),
    waitbuyerconfirmaction("等待买家确认操作", "waitbuyerconfirmaction"),
    waitsellerpush("等待卖家推进", "waitsellerpush"),
    cancel("交易关闭", "cancel"),
    success("交易成功", "success");



    // 成员变量
    private String name;
    private String index;

    // 构造方法
    private EnumAliOrderStatus(String name, String index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(String index) {
        for (EnumAliOrderStatus c : EnumAliOrderStatus.values()) {
            if (c.getIndex().equals(index)) {
                return c.name;
            }
        }
        return null;
    }

    public static String getIndex(String name) {
        for (EnumAliOrderStatus c : EnumAliOrderStatus.values()) {
            if (c.toString().equals(name)) {
                return c.getIndex();
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }
}
