package com.b2c.entity.enums.erp;

/**
 * 描述：
 * 退货订单状态
 *
 * @author qlp
 * @date 2019-10-21 15:14
 */
public enum EnumOrderReturnStatus {
    WaitSend("待买家发货", 0),
    WaitReceive("买家已发货", 1),
    Received("已签收", 2),
    ReturnToSupplier("已退回供应商",4),
    ExchangeComplete("换货已完成",5),
    StockIn("已入库",6),
    BackBuyer("已退回买家",7),
    Bad("计损",11),
    Complete("已完成",9),
    NotFound("未收到",88),
    CANCEL("已取消",99);

    private String name;
    private int index;

    // 构造方法
    private EnumOrderReturnStatus(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (EnumOrderReturnStatus c : EnumOrderReturnStatus.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
