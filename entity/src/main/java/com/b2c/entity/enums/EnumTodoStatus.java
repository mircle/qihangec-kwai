package com.b2c.entity.enums;

public enum EnumTodoStatus {
    WAITTODO("待处理", 0),
    HASTODO("处理中", 1),
    COMPLETED("已完成", 99);

    private String name;
    private int index;

    // 构造方法
    private EnumTodoStatus(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (EnumTodoStatus c : EnumTodoStatus.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
