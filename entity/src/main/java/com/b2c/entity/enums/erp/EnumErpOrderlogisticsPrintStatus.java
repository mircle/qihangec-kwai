package com.b2c.entity.enums.erp;

/**
 * 描述：
 * 物流面单打印状态
 *
 * @author qlp
 * @date 2019-03-27 16:39
 */
public enum EnumErpOrderlogisticsPrintStatus {
    NotPrint("无快递单", 0),
    Printed("有快递单", 1);

    private String name;
    private int index;

    // 构造方法
    private EnumErpOrderlogisticsPrintStatus(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (EnumErpOrderlogisticsPrintStatus c : EnumErpOrderlogisticsPrintStatus.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
