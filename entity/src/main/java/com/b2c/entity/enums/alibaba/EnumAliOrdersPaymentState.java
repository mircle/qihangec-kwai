package com.b2c.entity.enums.alibaba;

public enum EnumAliOrdersPaymentState {
    waitbuyerpay("等待买家付款", 1),
    waitsellersend("等待卖家发货", 2),
    waitlogisticstakein("等待物流公司揽件", 200),
    waitbuyerreceive("等待买家收货", 3),
    waitbuyersign("等待买家签收", 400),
    signinsuccess("买家已签收", 500),
    confirm_goods("已收货", 4),
    success("交易成功", 5),
    cancel("交易取消", 0),
    terminated("交易终止", 7);

    private String name;
    private int index;

    // 构造方法
    private EnumAliOrdersPaymentState(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (EnumAliOrdersPaymentState c : values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
