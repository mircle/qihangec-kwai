package com.b2c.entity.enums;

/**
 * 描述：
 * 订单状态枚举
 *
 * @author qlp
 * @date 2019-02-21 11:34
 */
public enum OrderTypeEnums {
    //订单状态0已取消1待支付2支付成功（待发货）3已发货（待收货）4已收货5已完成（过了售后期限）
    PT("云购订单", 0),
//    PT_YUSHOU("云购预售订单", 8),
    Purchase("采购订单", 99),
    //    YZ("有赞订单", 1),
//    ALI("阿里订单", 2),
    PiFa("批发订单", 3),
    //    TMALL("天猫订单", 4),
    DaiFa("直播订单", 9);
    // 成员变量
    private String name;
    private int index;

    // 构造方法
    private OrderTypeEnums(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (OrderTypeEnums c : OrderTypeEnums.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    public static Integer getIndex(String name) {
        for (OrderTypeEnums c : OrderTypeEnums.values()) {
            if (c.toString().equals(name)) {
                return c.getIndex();
            }
        }
        return 0;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

}
