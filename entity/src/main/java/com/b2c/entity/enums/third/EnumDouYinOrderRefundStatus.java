package com.b2c.entity.enums.third;

public enum EnumDouYinOrderRefundStatus {
    //erp 订单状态0:待提交,1:待审核,2:待发货3:已发货,4:已收货,5:已完成,6已退货
    //0	开始
    //1	待确认 (用户下单未付款 或者 货到付款订单商家未确认)
    //2	备货中 (用户已付款) 此状态商户才可执行发货操作 (货到付款的订单, 商家需要先确认订单才会进入此状态)
    //3	已发货 (商家出库、已发货)
    //4	已取消    //1.用户未支付并取消订单 //2.或超时未支付后系统自动取消订单//3.或货到付款订单用户拒收
    //5	已完成
    //在线支付订单: 商家发货后, 用户收货、拒收或者15天无物流

    //货到付款订单: 用户确认收货
    //6	(退货) 退货中-用户申请
    //7	(退货) 退货中-商家同意退货
    //8	(退货) 退货中-客服仲裁
    //9	(退货) 已关闭-退货失败
    //10	(退货) 退货中-客服同意
    //11	(退货) 退货中-用户填写完物流
    //12	(退货) 已关闭-商户同意
    //13	(退货) 退货中-再次客服仲裁
    //14	(退货) 已关闭-客服同意
    //15	(退货) 取消退货申请
    //注:	商家未发货用户才可退款, 订单会进入退款相关状态
    //
    //商家确认发货之后用户只可操作退货, 进入退货相关状态, 不会进入退款状态
    //16	(退款) 申请退款中 (商家还未发货, 用户申请退款)
    //17	(退款) 商户同意退款
    //18	(退款) 订单退款仲裁中
    //19	(退款) 退款仲裁支持用户
    //20	(退款) 退款仲裁支持商家
    //21	(退款) 订单退款成功 (即用户在商家出库前可发起退款, 最终退款成功)
    //22	(退款) 售后退款成功 (即用户在退款后, 正常走完退货流程, 退款成功)
    //23	(退货) 退货中-再次用户申请
    //24	(退货) 已关闭-退货成功
    //25	备货中-用户取消
    //26	备货中-退款商家拒绝
    //27	退货中-商家拒绝退货
    //28	退货失败
    //29	退货中-商家再次拒绝
    //30	退款中-退款申请
    //31	退款申请取消
    //32	退款成功-商家同意
    //33	退款中-商家拒绝
    //34	退款中-客服仲裁
    //35	退款中-客服同意
    //36	退款中-退款失败
    //37	已关闭-退款失败
    //38	退款中-线下退款成功
    //39	退款中-退款成功
/*    WAIT_SEND_GOODS("待发货", 2),
    SEND_GOODS("已发货",3),
    CANCEL("已取消", 4),
    TRADE_FINISHED("已完成",  5),
    TRADE_CLOSED("退货中-用户申请",  6),
    TRADE_CLOSED_7("退货中-商家同意退货",7),
    TRADE_CLOSED_8("退货中-客服仲裁",8),
    TRADE_CLOSED_9("已关闭-退货失败",9),
    TRADE_CLOSED_10("退货中-客服同意",10),
    TRADE_CLOSED_11("退货中-用户填写完物流",11),
    TRADE_CLOSED_12("已关闭-商户同意",12),
    TRADE_CLOSED_13("退货中-再次客服仲裁",13),
    TRADE_CLOSED_14("已关闭-客服同意",14),
    TRADE_CLOSED_15("取消退货申请",15),
    TRADE_CLOSED_16("申请退款中 (商家还未发货, 用户申请退款)",16),
    TRADE_CLOSED_17("商户同意退款",17),
    TRADE_CLOSED_18("订单退款仲裁中",18),
    TRADE_CLOSED_19("退款仲裁支持用户",19),
    TRADE_CLOSED_20("退款仲裁支持商家",20),
    TRADE_CLOSED_21("订单退款成功",21),
    TRADE_CLOSED_22("售后退款成功",22),
    TRADE_CLOSED_23("退货中-再次用户申请",23),
    TRADE_CLOSED_24("已关闭-退货成功",24),
    TRADE_CLOSED_25("备货中-用户取消",25),
    TRADE_CLOSED_26("备货中-退款商家拒绝",26),
    TRADE_CLOSED_27("退货中-商家拒绝退货",27),
    TRADE_CLOSED_28("退货失败",28),
    TRADE_CLOSED_29("退货中-商家再次拒绝",29),
    TRADE_CLOSED_30("退款中-退款申请",30),
    TRADE_CLOSED_31("退款申请取消",31),
    TRADE_CLOSED_32("退款成功-商家同意",32),
    TRADE_CLOSED_33("退款中-商家拒绝",33),
    TRADE_CLOSED_34("退款中-客服仲裁",34),
    TRADE_CLOSED_35("退款中-客服同意",35),
    TRADE_CLOSED_36("退款中-退款失败",36),
    TRADE_CLOSED_37("已关闭-退款失败",37),
    TRADE_CLOSED_38("退款中-线下退款成功",38),
    TRADE_CLOSED_39("退款中-退款成功",39);*/

    //退货售后状态，枚举为6(待商家同意),7(待买家退货),11(待商家二次同意),12(售后成功),13(换货待买家收货),14(换货成功),27(商家一次拒绝),28(售后失败),29(商家二次拒绝)

    REFUND_STATUS_6("待商家同意",6),
    REFUND_STATUS_7("待买家退货", 7),
    REFUND_STATUS_11("买家已退货，待商家处理",  11),
    REFUND_STATUS_12("售后成功",  12),
    REFUND_STATUS_13("换货待买家收货",13),
    REFUND_STATUS_14("换货成功",14),
    REFUND_STATUS_27("商家一次拒绝",27),
    REFUND_STATUS_28("售后失败",28),
    REFUND_STATUS_29("商家二次拒绝",29);

    // 成员变量
    private String name;
    private Integer index;

    // 构造方法
    private EnumDouYinOrderRefundStatus(String name,  Integer index) {
        this.name = name;
        this.index = index;
    }

    public static String getName(Integer index) {
        for (EnumDouYinOrderRefundStatus c : EnumDouYinOrderRefundStatus.values()) {
            if (c.getIndex()==index) {
//                System.out.println(c.name);
                return c.name;
            }
        }
        return null;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }
}
