package com.b2c.entity.enums;

/**
 * 系统配置类型
 */
public enum VariableEnums {
    PARMS_TOKEN("公众号token"),
    PARMS_SEND_NUMBER("短信每日最大发送次数"),
    PARMS_SALE_MONEY("会员升级费用"),
    PARMS_WAP_ADVERT("华衣WAP广告语配置"),
    PARMS_ALIBABA_TOKEN("阿里巴巴Token"),
    PARMS_SALESMAN_MIN_WITHDRAW("业务员提成最小可提现金额"),
    PARMS_YESTER_TITLE("刷单商品标题"),

    URL_WAP_INDEX("WAP网站首页地址"),
    URL_WMS_INDEX("WMS网站首页地址"),
    URL_MANAGE_INDEX("MANAGE网站首页地址"),
    URL_DATACENTER_INDEX("DataCenter网站首页地址"),
    URL_BUYER_INDEX("采购系统网站首页地址"),
    URL_WMS_INDEX_IN("华衣仓库本地"),


    DEFAULT_SEND_ADDRESS("默认发货地址"),

    MSG_ZFCG("支付成功"),
    MSG_DDFH("订单发货"),
    MSG_TKCG("退款成功");
    protected final String key;
    protected final String description;

    VariableEnums(String description) {
        this.key = name();
        this.description = description;
    }

    public String getKey() {
        return key;
    }

    public String getDescription() {
        return description;
    }

    // public static void main(String[] args) {
    //     System.out.println(VariableEnums.PARMS_TOKEN.getDescription());
    // }
}
