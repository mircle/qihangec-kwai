package com.b2c.entity.enums;

/**
 * 描述：
 * 订单状态枚举
 *
 * @author qlp
 * @date 2019-02-21 11:34
 */
public enum OrderStateEnums {
    //订单状态0已取消1待支付2支付成功（待发货）3已发货（待收货）4已收货5已完成（过了售后期限）
    Cancel("已取消", 0),
    WaitPay("待支付", 1),
    WaitSend("待发货", 2),//待发货
    Delivered("已发货", 3),//待收货
    Received("已收货", 4),//已收货
    Completed("已完成", 5),//过了售后期限
    REFUND("退款中", 6),//退款中
    FAIL("交易失败", 7),//交易失败
    VERIFY("待审核", 8);//审核中

    // 成员变量
    private String name;
    private int index;

    // 构造方法
    private OrderStateEnums(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (OrderStateEnums c : OrderStateEnums.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
