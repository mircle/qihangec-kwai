package com.b2c.entity.enums.third;

public enum  EnumKwaiOrderStatus {
    WZZT("未知状态", 0),
    WaitPay("待付款", 10),
    WaitSend("待发货", 30),//待发货
    Delivered("已发货", 40),//待收货
    Received("已收货", 50),//已收货
    Completed("已完成", 70),//已完成
    FAIL("订单失败",80);//已退款

    // 成员变量
    private String name;
    private Integer index;

    // 构造方法
    private EnumKwaiOrderStatus(String name,  Integer index) {
        this.name = name;
        this.index = index;
    }

    public static String getName(Integer index) {
        for (EnumKwaiOrderStatus c : EnumKwaiOrderStatus.values()) {
            if (c.getIndex()==index) {
//                System.out.println(c.name);
                return c.name;
            }
        }
        return null;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }
}
