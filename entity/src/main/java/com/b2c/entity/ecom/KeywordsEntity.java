package com.b2c.entity.ecom;

public class KeywordsEntity {
    private Long id;
    private String keyword;
    private String category;
    private String category2;
    private String source;
    private String platform;
    private String remark;
    private String createTime;
    // private Integer minSouSuoReDu;
    // private Integer maxSouSuoReDu;
    private Integer sousuoredu;
    private Integer dianjiredu;
    private Float dianjilv;
    private Float zhifulv;
    private Float shichangchujia;
    private String includeDate;

    
    public Integer getSousuoredu() {
        return sousuoredu;
    }
    public void setSousuoredu(Integer sousuoredu) {
        this.sousuoredu = sousuoredu;
    }
    public Integer getDianjiredu() {
        return dianjiredu;
    }
    public void setDianjiredu(Integer dianjiredu) {
        this.dianjiredu = dianjiredu;
    }
    public Float getDianjilv() {
        return dianjilv;
    }
    public void setDianjilv(Float dianjilv) {
        this.dianjilv = dianjilv;
    }
    public Float getZhifulv() {
        return zhifulv;
    }
    public void setZhifulv(Float zhifulv) {
        this.zhifulv = zhifulv;
    }
    public Float getShichangchujia() {
        return shichangchujia;
    }
    public void setShichangchujia(Float shichangchujia) {
        this.shichangchujia = shichangchujia;
    }
    public String getIncludeDate() {
        return includeDate;
    }
    public void setIncludeDate(String includeDate) {
        this.includeDate = includeDate;
    }
    // public Integer getMinSouSuoReDu() {
    //     return minSouSuoReDu;
    // }
    // public void setMinSouSuoReDu(Integer minSouSuoReDu) {
    //     this.minSouSuoReDu = minSouSuoReDu;
    // }
    // public Integer getMaxSouSuoReDu() {
    //     return maxSouSuoReDu;
    // }
    // public void setMaxSouSuoReDu(Integer maxSouSuoReDu) {
    //     this.maxSouSuoReDu = maxSouSuoReDu;
    // }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getKeyword() {
        return keyword;
    }
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public String getSource() {
        return source;
    }
    public void setSource(String source) {
        this.source = source;
    }
    public String getPlatform() {
        return platform;
    }
    public void setPlatform(String platform) {
        this.platform = platform;
    }
    public String getCreateTime() {
        return createTime;
    }
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
    public String getRemark() {
        return remark;
    }
    public void setRemark(String remark) {
        this.remark = remark;
    }
    public String getCategory2() {
        return category2;
    }
    public void setCategory2(String category2) {
        this.category2 = category2;
    }
    
}
