package com.b2c.entity.ecom;

public class KeywordDataEntity {
    private Long id;
    private Long keywordId;
    private Integer rank;
    private Integer goodsCount;
    private Integer sousuorenqi;
    private Integer sousuoredu;
    private Integer dianjirenqi;
    private Integer dianjiredu;
    private Float dianjilv;
    private Float zhifulv;
    private Float jingzhengzhishu;
    private Float chengjiaozhishu;
    private Float shichangchujia;
    private String includeDate;
    private Integer isDelete;
    private String createTime;
    private String modifyTime;
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Long getKeywordId() {
        return keywordId;
    }
    public void setKeywordId(Long keywordId) {
        this.keywordId = keywordId;
    }
    public Integer getRank() {
        return rank;
    }
    public void setRank(Integer rank) {
        this.rank = rank;
    }
    public Integer getGoodsCount() {
        return goodsCount;
    }
    public void setGoodsCount(Integer goodsCount) {
        this.goodsCount = goodsCount;
    }
    public Integer getSousuorenqi() {
        return sousuorenqi;
    }
    public void setSousuorenqi(Integer sousuorenqi) {
        this.sousuorenqi = sousuorenqi;
    }
    public Integer getSousuoredu() {
        return sousuoredu;
    }
    public void setSousuoredu(Integer sousuoredu) {
        this.sousuoredu = sousuoredu;
    }
    public Integer getDianjirenqi() {
        return dianjirenqi;
    }
    public void setDianjirenqi(Integer dianjirenqi) {
        this.dianjirenqi = dianjirenqi;
    }
    public Integer getDianjiredu() {
        return dianjiredu;
    }
    public void setDianjiredu(Integer dianjiredu) {
        this.dianjiredu = dianjiredu;
    }
    public Float getDianjilv() {
        return dianjilv;
    }
    public void setDianjilv(Float dianjilv) {
        this.dianjilv = dianjilv;
    }
    public Float getZhifulv() {
        return zhifulv;
    }
    public void setZhifulv(Float zhifulv) {
        this.zhifulv = zhifulv;
    }
    public Float getJingzhengzhishu() {
        return jingzhengzhishu;
    }
    public void setJingzhengzhishu(Float jingzhengzhishu) {
        this.jingzhengzhishu = jingzhengzhishu;
    }
    public Float getChengjiaozhishu() {
        return chengjiaozhishu;
    }
    public void setChengjiaozhishu(Float chengjiaozhishu) {
        this.chengjiaozhishu = chengjiaozhishu;
    }
    public Float getShichangchujia() {
        return shichangchujia;
    }
    public void setShichangchujia(Float shichangchujia) {
        this.shichangchujia = shichangchujia;
    }
    public String getIncludeDate() {
        return includeDate;
    }
    public void setIncludeDate(String includeDate) {
        this.includeDate = includeDate;
    }
    public Integer getIsDelete() {
        return isDelete;
    }
    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }
    public String getCreateTime() {
        return createTime;
    }
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
    public String getModifyTime() {
        return modifyTime;
    }
    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    

}
