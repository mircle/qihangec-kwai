package com.b2c.entity.vo.wms;

import java.io.Serializable;

/**
 * 采购单商品列表
 */
public class ErpPurchaseGoodVo implements Serializable {
    private Long erpInvoiceInfoId;
    private Integer specId;//商品规格ID
    private Integer goodsId;//商品id
    private String goodsNumber;//商品编号
    private String specNumber;//规格编号
    private Long quantity;//数量
    private Double  price;//单价
    private Double disRate;//折扣率
    private Double disAmount;//折扣额
    private Double amount;//购货金额
    private String description;//备注

    public Integer getSpecId() {
        return specId;
    }

    public void setSpecId(Integer specId) {
        this.specId = specId;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getDisRate() {
        return disRate;
    }

    public void setDisRate(Double disRate) {
        this.disRate = disRate;
    }

    public Double getDisAmount() {
        return disAmount;
    }

    public void setDisAmount(Double disAmount) {
        this.disAmount = disAmount;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(String goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public String getSpecNumber() {
        return specNumber;
    }

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    public Long getErpInvoiceInfoId() {
        return erpInvoiceInfoId;
    }

    public void setErpInvoiceInfoId(Long erpInvoiceInfoId) {
        this.erpInvoiceInfoId = erpInvoiceInfoId;
    }
}
