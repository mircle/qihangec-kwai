package com.b2c.entity.vo;

/**
 * 描述：
 * erp系统订单明细
 *
 * @author qlp
 * @date 2019-09-17 11:55
 */
public class OrderItemScanCodeVo {


    private String sN;//sku编码
    private Integer qty;//数量

    public String getsN() {
        return sN;
    }

    public void setsN(String sN) {
        this.sN = sN;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }
}
