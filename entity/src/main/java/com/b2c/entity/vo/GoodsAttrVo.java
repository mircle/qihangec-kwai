package com.b2c.entity.vo;

/**
 * 描述：
 *
 * @author qlp
 * @date 2018-12-25 4:17 PM
 */
public class GoodsAttrVo {
    private Integer id;
    private String name;  //属性名
    private Integer valId;
    private String val;  //属性值

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getValId() {
        return valId;
    }

    public void setValId(Integer valId) {
        this.valId = valId;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }
}
