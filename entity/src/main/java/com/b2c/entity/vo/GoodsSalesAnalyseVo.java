package com.b2c.entity.vo;

public class GoodsSalesAnalyseVo {
    private Long goodsId;
    private String name;
    private String number;
    private String image;
    private Integer total;
	private Double amount;
	private Integer DAY0;
    private Integer DAY1;
	private Integer DAY2;
    private Integer DAY3;
    private Integer DAY7;
    private Integer DAY15;
    private Integer DAY30;
    private Integer returnTotal;
    
    
	public Long getGoodsId() {
		return goodsId;
	}
	public void setGoodsId(Long goodsId) {
		this.goodsId = goodsId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public Integer getDAY1() {
		return DAY1;
	}
	public void setDAY1(Integer dAY1) {
		DAY1 = dAY1;
	}
	public Integer getDAY3() {
		return DAY3;
	}
	public void setDAY3(Integer dAY3) {
		DAY3 = dAY3;
	}
	public Integer getDAY7() {
		return DAY7;
	}
	public void setDAY7(Integer dAY7) {
		DAY7 = dAY7;
	}
	public Integer getDAY15() {
		return DAY15;
	}
	public void setDAY15(Integer dAY15) {
		DAY15 = dAY15;
	}
	public Integer getDAY30() {
		return DAY30;
	}
	public void setDAY30(Integer dAY30) {
		DAY30 = dAY30;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public Integer getReturnTotal() {
		return returnTotal;
	}
	public void setReturnTotal(Integer returnTotal) {
		this.returnTotal = returnTotal;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Integer getDAY0() {
		return DAY0;
	}
	public void setDAY0(Integer dAY0) {
		DAY0 = dAY0;
	}
	public Integer getDAY2() {
		return DAY2;
	}
	public void setDAY2(Integer dAY2) {
		DAY2 = dAY2;
	}

    
}
