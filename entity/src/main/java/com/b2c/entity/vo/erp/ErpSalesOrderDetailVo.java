package com.b2c.entity.vo.erp;

import com.b2c.entity.erp.ErpSalesOrderEntity;
import com.b2c.entity.erp.ErpSalesOrderItemEntity;

import java.util.List;

public class ErpSalesOrderDetailVo extends ErpSalesOrderEntity {
    private Long orderId;//订单id
    private List<ErpSalesOrderItemEntity> items;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public List<ErpSalesOrderItemEntity> getItems() {
        return items;
    }

    public void setItems(List<ErpSalesOrderItemEntity> items) {
        this.items = items;
    }
}
