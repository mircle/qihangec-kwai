package com.b2c.entity.vo;

/**
 * @Description: pbd add 2019/3/19 10:19
 */
public class SysRoleMenuVo {
    /**
     * 菜单id
     */
    private int id;
    /**
     * 父级菜单id
     */
    private int pid;
    /**
     * 菜单名称
     */
    private String name;
    /**
     * 菜单链接
     */
    private String url;
    /**
     * 菜单状态
     */
    private int state;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }
}
