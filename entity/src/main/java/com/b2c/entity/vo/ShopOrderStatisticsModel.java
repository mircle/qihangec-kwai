package com.b2c.entity.vo;

import java.math.BigDecimal;

public class ShopOrderStatisticsModel {
    private Integer todayOrder;//今日订单数
    private Integer todayValidOrder;//今日有效订单
    private BigDecimal todayValidOrderAmount;//今日有效订单金额
    private Integer todaySend;//今日发货订单数
    private Integer waitSend;//待发货订单数
    private Integer waitReceving;//待签收
    private Integer hasReceving;//已签收

    //售后状态 1：无售后或售后关闭，2：售后处理中，3：退款中，4： 退款成功 5：全部
    private Integer waitRefund;//售后处理中
    private Integer hasRefund;//退款成功


    public ShopOrderStatisticsModel(Integer todayOrder,Integer todayValidOrder, Integer todaySend, Integer waitSend, Integer waitReceving,
            Integer hasReceving, Integer waitRefund, Integer hasRefund) {
        this.todayOrder = todayOrder;
        this.todayValidOrder = todayValidOrder;
        this.todaySend = todaySend;
        this.waitSend = waitSend;
        this.waitReceving = waitReceving;
        this.hasReceving = hasReceving;
        this.waitRefund = waitRefund;
        this.hasRefund = hasRefund;
    }

    public ShopOrderStatisticsModel(Integer todayOrder, Integer todaySend, Integer waitSend, Integer waitReceving,
            Integer hasReceving, Integer waitRefund, Integer hasRefund) {
        this.todayOrder = todayOrder;
        this.todaySend = todaySend;
        this.waitSend = waitSend;
        this.waitReceving = waitReceving;
        this.hasReceving = hasReceving;
        this.waitRefund = waitRefund;
        this.hasRefund = hasRefund;
    }
    
    
    public Integer getTodayOrder() {
        return todayOrder;
    }
    public void setTodayOrder(Integer todayOrder) {
        this.todayOrder = todayOrder;
    }
    public Integer getTodaySend() {
        return todaySend;
    }
    public void setTodaySend(Integer todaySend) {
        this.todaySend = todaySend;
    }
    public Integer getWaitSend() {
        return waitSend;
    }
    public void setWaitSend(Integer waitSend) {
        this.waitSend = waitSend;
    }
    public Integer getWaitReceving() {
        return waitReceving;
    }
    public void setWaitReceving(Integer waitReceving) {
        this.waitReceving = waitReceving;
    }
    public Integer getHasReceving() {
        return hasReceving;
    }
    public void setHasReceving(Integer hasReceving) {
        this.hasReceving = hasReceving;
    }
    public Integer getWaitRefund() {
        return waitRefund;
    }
    public void setWaitRefund(Integer waitRefund) {
        this.waitRefund = waitRefund;
    }
    public Integer getHasRefund() {
        return hasRefund;
    }
    public void setHasRefund(Integer hasRefund) {
        this.hasRefund = hasRefund;
    }

    public Integer getTodayValidOrder() {
        return todayValidOrder;
    }

    public void setTodayValidOrder(Integer todayValidOrder) {
        this.todayValidOrder = todayValidOrder;
    }

    public BigDecimal getTodayValidOrderAmount() {
        return todayValidOrderAmount;
    }

    public void setTodayValidOrderAmount(BigDecimal todayValidOrderAmount) {
        this.todayValidOrderAmount = todayValidOrderAmount;
    }



    
}
