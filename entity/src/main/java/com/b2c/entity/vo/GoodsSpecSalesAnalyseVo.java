package com.b2c.entity.vo;

public class GoodsSpecSalesAnalyseVo {
    private Integer goodsId;
    private Integer specId;
    private String colorValue;
    private String sizeValue;
    private Integer sales;
    
    public Integer getGoodsId() {
        return goodsId;
    }
    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }
    public Integer getSpecId() {
        return specId;
    }
    public void setSpecId(Integer specId) {
        this.specId = specId;
    }
    public String getColorValue() {
        return colorValue;
    }
    public void setColorValue(String colorValue) {
        this.colorValue = colorValue;
    }
    public String getSizeValue() {
        return sizeValue;
    }
    public void setSizeValue(String sizeValue) {
        this.sizeValue = sizeValue;
    }
    public Integer getSales() {
        return sales;
    }
    public void setSales(Integer sales) {
        this.sales = sales;
    }

    
}
