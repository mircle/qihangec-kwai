package com.b2c.entity.vo.erp;

import com.b2c.entity.erp.ErpSalesOrderRefundEntity;

import java.util.List;

public class ErpSalesOrderRefundDetailVo extends ErpSalesOrderRefundEntity {

    private List<ErpSalesOrderRefundItemVo> items;

    public List<ErpSalesOrderRefundItemVo> getItems() {
        return items;
    }

    public void setItems(List<ErpSalesOrderRefundItemVo> items) {
        this.items = items;
    }
}
