package com.b2c.entity.vo;

import java.math.BigDecimal;

/**
 * 店铺订单统计
 */
public class ShopOrderStatisticsVo {
    private Integer todayOrder =0;//今日订单
    private Integer todayValidOrder =0;//今日有效订单
    private BigDecimal todayValidOrderAmout=new BigDecimal(0.0);//今日有效订单金额
    private Integer waitSendOrder=0;//待发货订单
    private Integer waitAuditReturn=0;//待确认退货

    
    public ShopOrderStatisticsVo(){}
    
    public ShopOrderStatisticsVo(Integer todayOrder, Integer todayValidOrder, BigDecimal todayValidOrderAmout,
            Integer waitSendOrder, Integer waitAuditReturn) {
        this.todayOrder = todayOrder;
        this.todayValidOrder = todayValidOrder;
        this.todayValidOrderAmout = todayValidOrderAmout;
        this.waitSendOrder = waitSendOrder;
        this.waitAuditReturn = waitAuditReturn;
    }

    public Integer getTodayValidOrder() {
        return todayValidOrder;
    }

    public void setTodayValidOrder(Integer todayValidOrder) {
        this.todayValidOrder = todayValidOrder;
    }

    public BigDecimal getTodayValidOrderAmout() {
        return todayValidOrderAmout;
    }

    public void setTodayValidOrderAmout(BigDecimal todayValidOrderAmout) {
        this.todayValidOrderAmout = todayValidOrderAmout;
    }


    public Integer getTodayOrder() {
        return todayOrder;
    }

    public void setTodayOrder(Integer todayOrder) {
        this.todayOrder = todayOrder;
    }



    public Integer getWaitAuditReturn() {
        return waitAuditReturn;
    }

    public void setWaitAuditReturn(Integer waitAuditReturn) {
        this.waitAuditReturn = waitAuditReturn;
    }

    public Integer getWaitSendOrder() {
        return waitSendOrder;
    }

    public void setWaitSendOrder(Integer waitSendOrder) {
        this.waitSendOrder = waitSendOrder;
    }
}
