package com.b2c.entity.vo;

/**
 * @Description: pbd add 2019/3/25 17:35
 */
public class ManageLoginLogVo {
    private int id;
    /**
     * 登录ip
     */
    private String loginIp;
    /**
     * 登录时间
     */
    private String loginTime;
    /**
     * 姓名
     */
    private String trueName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTrueName() {
        return trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }

    public String getLoginIp() {
        return loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }

    public String getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(String loginTime) {
        this.loginTime = loginTime;
    }
}
