package com.b2c.entity.vo;

/**
 * @Description: pbd add 2019/4/10 15:35
 */
public class GoodIdVo {
    private Integer goodsId;

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }
}
