package com.b2c.entity.vo;

import com.b2c.entity.OrderItemEntity;
import com.b2c.entity.OrderLogsEntity;
import com.b2c.entity.vo.goods.OrderGoodsDetailVo;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Description:订单总览列表 ly add 2019/1/9 14:22
 */
public class OrderVo {

    private Long id;  //订单id
    private Integer userId;  //用户id
    private String orderNum;  //订单号
    private Integer goodsNum;  //订单总数
    private String buyerName;//买家昵称
    private String goodsDetail;//订单商品详情
    private List<OrderItemEntity> orderItems;
    //商品详情
    private BigDecimal paymentPrice;  //支付价格
    private BigDecimal orderTotalPrice;  //订单总价
    private Integer paymentMethod;  //支付方式
    private Long paymentTime;  //下单时间
    private String mobile;      //用户手机号
//    private String mjMobile;  //买家手机号
//    private String tjrMobile;  //推荐人手机号

    private Integer salesmanId;//导购师Id
    private String salesmanName;
    private String salesmanMobile;
    private Integer developerId;//业务员Id
    private String developerName;
    private String developerMobile;


    private int state;  //订单状态
    private String consignee; //收货人姓名
    private String consigneeMobile; //收货人手机号
    private String province;  //收货地址
    private String city;  //收货地址
    private String area;  //收货地址
    private String address;  //收货地址
    private List<OrderLogsEntity> logs;  //订单日志
    private BigDecimal goodsTotalPrice; //商品总价
    private String paymentResultTime;//支付时间
    private Integer afterSaleState; //售后状态
    private String afterCreateOn; //售后申请时间
    private String afterFinishOn; //售后退款时间
    private Integer afterId;//退款id
    private String orderCancelNum;  //退货编号
    private BigDecimal afterTotalPrice;  //退货订单总价
    private String comment;  //退款原因
    private Integer paymentState;//支付状态
    //    private String logistics;  //物流信息
    private String send_company;//物流公司
    private String send_company_code;//物流公司code
    private String send_code;//物流单号
    private OrderGoodsDetailVo goodsAfterDetail;  //退货商品详情
    private int sendStatus;//发货状态（0待出库1已出库2已发货）
    private Integer type;//订单类型来源
//    private String logisticsCompany;//物流公司
//    private String logisticsCode;//物流单号
    private BigDecimal freight;//物流费用
    private Long createOn;          //订单创建时间
    private Integer auditStatus;
    private BigDecimal paidPrice;//已支付金额
    private Long aliOrderId;
    private Integer buyerOrderType;
    private Integer shopId;
    private String sellerMemo;

    public String getSellerMemo() {
        return sellerMemo;
    }

    public void setSellerMemo(String sellerMemo) {
        this.sellerMemo = sellerMemo;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getBuyerOrderType() {
        return buyerOrderType;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setBuyerOrderType(Integer buyerOrderType) {
        this.buyerOrderType = buyerOrderType;
    }

    public BigDecimal getOrderTotalPrice() {
        return orderTotalPrice;
    }

    public void setOrderTotalPrice(BigDecimal orderTotalPrice) {
        this.orderTotalPrice = orderTotalPrice;
    }

    public Long getAliOrderId() {
        return aliOrderId;
    }

    public void setAliOrderId(Long aliOrderId) {
        this.aliOrderId = aliOrderId;
    }

    public BigDecimal getPaidPrice() {
        return paidPrice;
    }

    public void setPaidPrice(BigDecimal paidPrice) {
        this.paidPrice = paidPrice;
    }

    public String getSend_company_code() {
        return send_company_code;
    }

    public void setSend_company_code(String send_company_code) {
        this.send_company_code = send_company_code;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public String getSalesmanMobile() {
        return salesmanMobile;
    }

    public void setSalesmanMobile(String salesmanMobile) {
        this.salesmanMobile = salesmanMobile;
    }

    public String getDeveloperName() {
        return developerName;
    }

    public void setDeveloperName(String developerName) {
        this.developerName = developerName;
    }

    public String getDeveloperMobile() {
        return developerMobile;
    }

    public void setDeveloperMobile(String developerMobile) {
        this.developerMobile = developerMobile;
    }

    public Integer getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(Integer salesmanId) {
        this.salesmanId = salesmanId;
    }

    public Integer getDeveloperId() {
        return developerId;
    }

    public void setDeveloperId(Integer developerId) {
        this.developerId = developerId;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public Integer getPaymentState() {
        return paymentState;
    }

    public void setPaymentState(Integer paymentState) {
        this.paymentState = paymentState;
    }

//    public String getLogisticsCompany() {
//        return logisticsCompany;
//    }
//
//    public void setLogisticsCompany(String logisticsCompany) {
//        this.logisticsCompany = logisticsCompany;
//    }
//
//    public String getLogisticsCode() {
//        return logisticsCode;
//    }
//
//    public void setLogisticsCode(String logisticsCode) {
//        this.logisticsCode = logisticsCode;
//    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }


    public Long getCreateOn() {
        return createOn;
    }

    public void setCreateOn(Long createOn) {
        this.createOn = createOn;
    }

    public String getSend_company() {
        return send_company;
    }

    public void setSend_company(String send_company) {
        this.send_company = send_company;
    }

    public String getSend_code() {
        return send_code;
    }

    public void setSend_code(String send_code) {
        this.send_code = send_code;
    }

    public int getSendStatus() {
        return sendStatus;
    }

    public void setSendStatus(int sendStatus) {
        this.sendStatus = sendStatus;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public Integer getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(Integer goodsNum) {
        this.goodsNum = goodsNum;
    }

    public String getGoodsDetail() {
        return goodsDetail;
    }

    public void setGoodsDetail(String goodsDetail) {
        this.goodsDetail = goodsDetail;
    }

    public List<OrderItemEntity> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItemEntity> orderItems) {
        this.orderItems = orderItems;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }


    public Integer getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(Integer paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public BigDecimal getPaymentPrice() {
        return paymentPrice;
    }

    public void setPaymentPrice(BigDecimal paymentPrice) {
        this.paymentPrice = paymentPrice;
    }

    public Long getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(Long paymentTime) {
        this.paymentTime = paymentTime;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public String getConsigneeMobile() {
        return consigneeMobile;
    }

    public void setConsigneeMobile(String consigneeMobile) {
        this.consigneeMobile = consigneeMobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<OrderLogsEntity> getLogs() {
        return logs;
    }

    public void setLogs(List<OrderLogsEntity> logs) {
        this.logs = logs;
    }

    public BigDecimal getGoodsTotalPrice() {
        return goodsTotalPrice;
    }

    public void setGoodsTotalPrice(BigDecimal goodsTotalPrice) {
        this.goodsTotalPrice = goodsTotalPrice;
    }

    public String getPaymentResultTime() {
        return paymentResultTime;
    }

    public void setPaymentResultTime(String paymentResultTime) {
        this.paymentResultTime = paymentResultTime;
    }

    public Integer getAfterSaleState() {
        return afterSaleState;
    }

    public void setAfterSaleState(Integer afterSaleState) {
        this.afterSaleState = afterSaleState;
    }

    public Integer getAfterId() {
        return afterId;
    }

    public void setAfterId(Integer afterId) {
        this.afterId = afterId;
    }

    public String getAfterCreateOn() {
        return afterCreateOn;
    }

    public void setAfterCreateOn(String afterCreateOn) {
        this.afterCreateOn = afterCreateOn;
    }

    public String getOrderCancelNum() {
        return orderCancelNum;
    }

    public void setOrderCancelNum(String orderCancelNum) {
        this.orderCancelNum = orderCancelNum;
    }

    public BigDecimal getAfterTotalPrice() {
        return afterTotalPrice;
    }

    public void setAfterTotalPrice(BigDecimal afterTotalPrice) {
        this.afterTotalPrice = afterTotalPrice;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getAfterFinishOn() {
        return afterFinishOn;
    }

    public void setAfterFinishOn(String afterFinishOn) {
        this.afterFinishOn = afterFinishOn;
    }

    public OrderGoodsDetailVo getGoodsAfterDetail() {
        return goodsAfterDetail;
    }

    public void setGoodsAfterDetail(OrderGoodsDetailVo goodsAfterDetail) {
        this.goodsAfterDetail = goodsAfterDetail;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public BigDecimal getFreight() {
        return freight;
    }

    public void setFreight(BigDecimal freight) {
        this.freight = freight;
    }
}
