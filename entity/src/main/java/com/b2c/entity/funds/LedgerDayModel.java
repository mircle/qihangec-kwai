package com.b2c.entity.funds;

import java.math.BigDecimal;

public class LedgerDayModel {
    private String date;
    private BigDecimal purYF;
    private BigDecimal purSF;
    private BigDecimal purRefundYS;
    private BigDecimal purRefundSS;
    private BigDecimal saleAmount;
    private BigDecimal refundAmount;
    private BigDecimal yongjin;
    private BigDecimal yingxiao;
    private BigDecimal saleFeeOther;

    
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public BigDecimal getPurYF() {
        return purYF;
    }
    public void setPurYF(BigDecimal purYF) {
        this.purYF = purYF;
    }
    public BigDecimal getPurSF() {
        return purSF;
    }
    public void setPurSF(BigDecimal purSF) {
        this.purSF = purSF;
    }

    public BigDecimal getSaleAmount() {
        return saleAmount;
    }
    public void setSaleAmount(BigDecimal saleAmount) {
        this.saleAmount = saleAmount;
    }
    public BigDecimal getRefundAmount() {
        return refundAmount;
    }
    public void setRefundAmount(BigDecimal refundAmount) {
        this.refundAmount = refundAmount;
    }
    public BigDecimal getYongjin() {
        return yongjin;
    }
    public void setYongjin(BigDecimal yongjin) {
        this.yongjin = yongjin;
    }
    public BigDecimal getYingxiao() {
        return yingxiao;
    }
    public void setYingxiao(BigDecimal yingxiao) {
        this.yingxiao = yingxiao;
    }
    public BigDecimal getSaleFeeOther() {
        return saleFeeOther;
    }
    public void setSaleFeeOther(BigDecimal saleFeeOther) {
        this.saleFeeOther = saleFeeOther;
    }
    public BigDecimal getPurRefundYS() {
        return purRefundYS;
    }
    public void setPurRefundYS(BigDecimal purRefundYS) {
        this.purRefundYS = purRefundYS;
    }
    public BigDecimal getPurRefundSS() {
        return purRefundSS;
    }
    public void setPurRefundSS(BigDecimal purRefundSS) {
        this.purRefundSS = purRefundSS;
    }

    
}
