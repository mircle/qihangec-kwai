package com.b2c.entity;

/**
 * @Description: 订单日志
 * ly add 2019/1/10 11:00
 */
public class OrderLogsEntity {
    private int id; //用户id
    private Integer orderId;  //
    private String type;  //操作类型（0订单流转1客户投诉2订单售后）',
    private String comment;  //操作内容
    private String createOn;
    private String createBy;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCreateOn() {
        return createOn;
    }

    public void setCreateOn(String createOn) {
        this.createOn = createOn;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }
}
