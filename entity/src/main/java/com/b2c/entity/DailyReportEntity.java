package com.b2c.entity;

import java.math.BigDecimal;

public class DailyReportEntity {
    /**
     * 日期
     */
    private String tjdate;

    /**
     * 报损总数
     */
    private Long zkc;

    /**
     * 采购入库
     */
    private Long cgrk;

    /**
     * 销售出库
     */
    private Long xsck;

    /**
     * 退货入库
     */
    private Long thrk;

    /**
     * 采购退货
     */
    private Long cgth;

    /**
     * 采购总价
     */
    private BigDecimal cgzj;

    /**
     * 报损数
     */
    private Long bstj;

    public String getTjdate() {
        return tjdate;
    }

    public void setTjdate(String tjdate) {
        this.tjdate = tjdate;
    }

    public Long getZkc() {
        return zkc;
    }

    public void setZkc(Long zkc) {
        this.zkc = zkc;
    }

    public Long getCgrk() {
        return cgrk;
    }

    public void setCgrk(Long cgrk) {
        this.cgrk = cgrk;
    }

    public Long getXsck() {
        return xsck;
    }

    public void setXsck(Long xsck) {
        this.xsck = xsck;
    }

    public Long getThrk() {
        return thrk;
    }

    public void setThrk(Long thrk) {
        this.thrk = thrk;
    }

    public Long getCgth() {
        return cgth;
    }

    public void setCgth(Long cgth) {
        this.cgth = cgth;
    }

    public BigDecimal getCgzj() {
        return cgzj;
    }

    public void setCgzj(BigDecimal cgzj) {
        this.cgzj = cgzj;
    }

    public Long getBstj() {
        return bstj;
    }

    public void setBstj(Long bstj) {
        this.bstj = bstj;
    }
}
