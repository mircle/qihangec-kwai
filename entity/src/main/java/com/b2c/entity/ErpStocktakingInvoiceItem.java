package com.b2c.entity;

/**
 * 库存盘点单item
 */
public class ErpStocktakingInvoiceItem {
	private Long id;//` BIGINT(20) NOT NULL AUTO_INCREMENT,
	private Long iid;//` BIGINT(20) NOT NULL COMMENT '单据id',
    private String billNo;//` VARCHAR(30) NOT NULL DEFAULT '' COMMENT '单据编号',
    private Long goods_id;//` INT(11) NOT NULL,
	private String goods_number;//` VARCHAR(30) NOT NULL,
	private Long goods_spec_id;//` INT(11) NOT NULL,
	private String goods_spec_number;//` VARCHAR(30) NOT NULL,
    private String colorValue;
    private String sizeValue;
	private Long locationId;//` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '仓位id',
	private String locationName;
    private Long currentQty;//` BIGINT(20) NOT NULL COMMENT '当前库存',
    private Long countedQty;//` BIGINT(20) NULL DEFAULT NULL COMMENT '盘点数量',
    private String description;//` VARCHAR(255) NULL DEFAULT NULL COMMENT '备注',
    private Integer isDelete;//` TINYINT(1) NULL DEFAULT '0' COMMENT '1删除  0正常',
    private Long createTime;//` BIGINT(20) NULL DEFAULT NULL COMMENT '创建时间',
    private Long modifyTime;//` BIGINT(20) NULL DEFAULT NULL COMMENT '更新时间',

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getSizeValue() {
        return sizeValue;
    }

    public void setSizeValue(String sizeValue) {
        this.sizeValue = sizeValue;
    }

    public String getColorValue() {
        return colorValue;
    }

    public void setColorValue(String colorValue) {
        this.colorValue = colorValue;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIid() {
        return iid;
    }

    public void setIid(Long iid) {
        this.iid = iid;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public Long getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(Long goods_id) {
        this.goods_id = goods_id;
    }

    public String getGoods_number() {
        return goods_number;
    }

    public void setGoods_number(String goods_number) {
        this.goods_number = goods_number;
    }

    public Long getGoods_spec_id() {
        return goods_spec_id;
    }

    public void setGoods_spec_id(Long goods_spec_id) {
        this.goods_spec_id = goods_spec_id;
    }

    public String getGoods_spec_number() {
        return goods_spec_number;
    }

    public void setGoods_spec_number(String goods_spec_number) {
        this.goods_spec_number = goods_spec_number;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    public Long getCurrentQty() {
        return currentQty;
    }

    public void setCurrentQty(Long currentQty) {
        this.currentQty = currentQty;
    }

    public Long getCountedQty() {
        return countedQty;
    }

    public void setCountedQty(Long countedQty) {
        this.countedQty = countedQty;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Long modifyTime) {
        this.modifyTime = modifyTime;
    }
}
