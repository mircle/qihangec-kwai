package com.b2c.entity.shop;

public class ShopTrafficGoodsEntity {
    private Long id;
     private Integer shopId;
     private Integer shopType;
     private String goodsId;
     private String goodsNumber;
    private String goodsName;
    private String goodsImg;

    private Integer shows;
    private Integer paidShows;
    private Integer visits;
    private Integer paidVisits;
    private Integer views;
    
    private Integer orders;
    private Integer orderUsers;
    private Integer paidOrders;
    private Double orderAmount;
    private Float CVR;
    private String cvrTag;
    private Integer collects;
    private Integer paidCollects;
    private Integer carts;
    private Integer paidCarts;
    private Integer chats;
    private Integer paidChats;
    
    private String date;
    private String remark;
    private String createOn;
    private String modifyOn;

    //全站、搜索、场景
    private Integer paidShowCJ;
    private Integer paidVisitCJ;
    private Integer paidOrderCJ;
    private Integer paidShowSS;
    private Integer paidVisitSS;
    private Integer paidOrderSS;
    private Integer paidShowQZ;
    private Integer paidVisitQZ;
    private Integer paidOrderQZ;

    
    
    public Integer getPaidShowCJ() {
        return paidShowCJ;
    }
    public void setPaidShowCJ(Integer paidShowCJ) {
        this.paidShowCJ = paidShowCJ;
    }
    public Integer getPaidVisitCJ() {
        return paidVisitCJ;
    }
    public void setPaidVisitCJ(Integer paidVisitCJ) {
        this.paidVisitCJ = paidVisitCJ;
    }
    public Integer getPaidOrderCJ() {
        return paidOrderCJ;
    }
    public void setPaidOrderCJ(Integer paidOrderCJ) {
        this.paidOrderCJ = paidOrderCJ;
    }
    public Integer getPaidShowSS() {
        return paidShowSS;
    }
    public void setPaidShowSS(Integer paidShowSS) {
        this.paidShowSS = paidShowSS;
    }
    public Integer getPaidVisitSS() {
        return paidVisitSS;
    }
    public void setPaidVisitSS(Integer paidVisitSS) {
        this.paidVisitSS = paidVisitSS;
    }
    public Integer getPaidOrderSS() {
        return paidOrderSS;
    }
    public void setPaidOrderSS(Integer paidOrderSS) {
        this.paidOrderSS = paidOrderSS;
    }
    public Integer getPaidShowQZ() {
        return paidShowQZ;
    }
    public void setPaidShowQZ(Integer paidShowQZ) {
        this.paidShowQZ = paidShowQZ;
    }
    public Integer getPaidVisitQZ() {
        return paidVisitQZ;
    }
    public void setPaidVisitQZ(Integer paidVisitQZ) {
        this.paidVisitQZ = paidVisitQZ;
    }
    public Integer getPaidOrderQZ() {
        return paidOrderQZ;
    }
    public void setPaidOrderQZ(Integer paidOrderQZ) {
        this.paidOrderQZ = paidOrderQZ;
    }
    public Double getOrderAmount() {
        return orderAmount;
    }
    public void setOrderAmount(Double orderAmount) {
        this.orderAmount = orderAmount;
    }
    public Float getCVR() {
        return CVR;
    }
    public void setCVR(Float cVR) {
        CVR = cVR;
    }
    public String getCreateOn() {
        return createOn;
    }
    public void setCreateOn(String createOn) {
        this.createOn = createOn;
    }
    public String getModifyOn() {
        return modifyOn;
    }
    public void setModifyOn(String modifyOn) {
        this.modifyOn = modifyOn;
    }
    public String getGoodsName() {
        return goodsName;
    }
    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }
    public String getGoodsImg() {
        return goodsImg;
    }
    public void setGoodsImg(String goodsImg) {
        this.goodsImg = goodsImg;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Integer getShopId() {
        return shopId;
    }
    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }
    public Integer getShopType() {
        return shopType;
    }
    public void setShopType(Integer shopType) {
        this.shopType = shopType;
    }
    public String getGoodsId() {
        return goodsId;
    }
    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }
    public String getGoodsNumber() {
        return goodsNumber;
    }
    public void setGoodsNumber(String goodsNumber) {
        this.goodsNumber = goodsNumber;
    }
    public Integer getShows() {
        return shows;
    }
    public void setShows(Integer shows) {
        this.shows = shows;
    }
    public Integer getPaidShows() {
        return paidShows;
    }
    public void setPaidShows(Integer paidShows) {
        this.paidShows = paidShows;
    }
    public Integer getVisits() {
        return visits;
    }
    public void setVisits(Integer visits) {
        this.visits = visits;
    }
    public Integer getPaidVisits() {
        return paidVisits;
    }
    public void setPaidVisits(Integer paidVisits) {
        this.paidVisits = paidVisits;
    }
    public Integer getViews() {
        return views;
    }
    public void setViews(Integer views) {
        this.views = views;
    }
    public Integer getOrders() {
        return orders;
    }
    public void setOrders(Integer orders) {
        this.orders = orders;
    }
    public Integer getPaidOrders() {
        return paidOrders;
    }
    public void setPaidOrders(Integer paidOrders) {
        this.paidOrders = paidOrders;
    }
    public Integer getCollects() {
        return collects;
    }
    public void setCollects(Integer collects) {
        this.collects = collects;
    }
    public Integer getPaidCollects() {
        return paidCollects;
    }
    public void setPaidCollects(Integer paidCollects) {
        this.paidCollects = paidCollects;
    }
    public Integer getCarts() {
        return carts;
    }
    public void setCarts(Integer carts) {
        this.carts = carts;
    }
    public Integer getPaidCarts() {
        return paidCarts;
    }
    public void setPaidCarts(Integer paidCarts) {
        this.paidCarts = paidCarts;
    }
    public Integer getChats() {
        return chats;
    }
    public void setChats(Integer chats) {
        this.chats = chats;
    }
    public Integer getPaidChats() {
        return paidChats;
    }
    public void setPaidChats(Integer paidChats) {
        this.paidChats = paidChats;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String getRemark() {
        return remark;
    }
    public void setRemark(String remark) {
        this.remark = remark;
    }
    public Integer getOrderUsers() {
        return orderUsers;
    }
    public void setOrderUsers(Integer orderUsers) {
        this.orderUsers = orderUsers;
    }
    public String getCvrTag() {
        return cvrTag;
    }
    public void setCvrTag(String cvrTag) {
        this.cvrTag = cvrTag;
    }

}
