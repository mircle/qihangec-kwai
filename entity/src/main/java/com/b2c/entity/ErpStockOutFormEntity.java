package com.b2c.entity;

/**
 * 描述：
 * 出库表单Entity
 *
 * @author qlp
 * @date 2019-06-21 11:51
 */
public class ErpStockOutFormEntity {
    private Long id;
    private String stockOutNo;
    private String sourceNo;
    private String sourceChannel;
    private Long sourceId;
    private Integer outType;//出库类型：出库类型1订单拣货出库2采购退货出库
    private Integer status;
    private Integer goodsCount;
    private Integer orderCount;
    private Integer printStatus;
    private Long printTime;
    private String createTime;
    private String createBy;
    private String modifyTime;
    private String modifyBy;
    private Long quantity;
    private Long currQty;
    private Long completeTime;
    private Integer stockOutUserId;
    private String stockOutUserName;
    private String remark;

    public String getSourceChannel() {
        return sourceChannel;
    }

    public void setSourceChannel(String sourceChannel) {
        this.sourceChannel = sourceChannel;
    }

    public Long getCurrQty() {

        return currQty;
    }

    public void setCurrQty(Long currQty) {
        this.currQty = currQty;
    }

    public String getSourceNo() {
        return sourceNo;
    }

    public void setSourceNo(String sourceNo) {
        this.sourceNo = sourceNo;
    }

    public Long getSourceId() {
        return sourceId;
    }

    public void setSourceId(Long sourceId) {
        this.sourceId = sourceId;
    }

    public Integer getOutType() {
        return outType;
    }

    public void setOutType(Integer outType) {
        this.outType = outType;
    }

    public Integer getStockOutUserId() {
        return stockOutUserId;
    }

    public void setStockOutUserId(Integer stockOutUserId) {
        this.stockOutUserId = stockOutUserId;
    }

    public String getStockOutUserName() {
        return stockOutUserName;
    }

    public void setStockOutUserName(String stockOutUserName) {
        this.stockOutUserName = stockOutUserName;
    }

    public Long getCompleteTime() {
        return completeTime;
    }

    public void setCompleteTime(Long completeTime) {
        this.completeTime = completeTime;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getGoodsCount() {
        return goodsCount;
    }

    public void setGoodsCount(Integer goodsCount) {
        this.goodsCount = goodsCount;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Long getPrintTime() {
        return printTime;
    }

    public void setPrintTime(Long printTime) {
        this.printTime = printTime;
    }

    public String getStockOutNo() {
        return stockOutNo;
    }

    public void setStockOutNo(String stockOutNo) {
        this.stockOutNo = stockOutNo;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getPrintStatus() {
        return printStatus;
    }

    public void setPrintStatus(Integer printStatus) {
        this.printStatus = printStatus;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(String modifyBy) {
        this.modifyBy = modifyBy;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
