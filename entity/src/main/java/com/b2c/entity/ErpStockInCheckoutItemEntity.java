package com.b2c.entity;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-09-29 11:02
 */
public class ErpStockInCheckoutItemEntity {
    private Long id;//` bigint(20) NOT NULL AUTO_INCREMENT,
    private Long checkoutId;//` bigint(20) NOT NULL COMMENT '入库检验单id',
    private Integer goodsId;//` int(11) NOT NULL COMMENT '商品id',
    private Integer specId;//` int(11) NOT NULL COMMENT '商品规格id',
    private String specNumber;// COMMENT '商品规格编码',
    private Long quantity;//` bigint(20) NOT NULL COMMENT '总数量',
    private Long inQuantity;//` bigint(20) NOT NULL COMMENT '入库数量',
    private Long invoiceInfoId;//

    public Long getInvoiceInfoId() {
        return invoiceInfoId;
    }

    public void setInvoiceInfoId(Long invoiceInfoId) {
        this.invoiceInfoId = invoiceInfoId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCheckoutId() {
        return checkoutId;
    }

    public void setCheckoutId(Long checkoutId) {
        this.checkoutId = checkoutId;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getSpecId() {
        return specId;
    }

    public void setSpecId(Integer specId) {
        this.specId = specId;
    }

    public String getSpecNumber() {
        return specNumber;
    }

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getInQuantity() {
        return inQuantity;
    }

    public void setInQuantity(Long inQuantity) {
        this.inQuantity = inQuantity;
    }
}
