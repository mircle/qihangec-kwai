package com.b2c.entity;

/**
 * 描述：
 * 基础地区表
 *
 * @author qlp
 * @date 2019-01-30 13:53
 */
public class BaseAreaEntity {
    private int id;
    private String name;
    private String code;
    private String parentCode;
    private int status;
    private int siblingSort;
    private int sortOrder;
    private int hasChild;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getSiblingSort() {
        return siblingSort;
    }

    public void setSiblingSort(int siblingSort) {
        this.siblingSort = siblingSort;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }

    public int getHasChild() {
        return hasChild;
    }

    public void setHasChild(int hasChild) {
        this.hasChild = hasChild;
    }
}
