package com.b2c.entity.apitao;

import java.math.BigDecimal;

public class PddOrderStatis {
    private String goodsSpec;
    private String goodsSpecNum;
    private Long quantity;
    private BigDecimal price;
    private Long orderCount;

    public String getGoodsSpec() {
        return goodsSpec;
    }

    public void setGoodsSpec(String goodsSpec) {
        this.goodsSpec = goodsSpec;
    }

    public String getGoodsSpecNum() {
        return goodsSpecNum;
    }

    public void setGoodsSpecNum(String goodsSpecNum) {
        this.goodsSpecNum = goodsSpecNum;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Long orderCount) {
        this.orderCount = orderCount;
    }
}
