package com.b2c.entity;

public class ErpContactEntity {
    private  Long id;

    //客户名称
    private  String name = "0";

    //客户编码
    private  String number;

    //客户类别
    private  Integer cCategory;

    //分类名称
    private  String cCategoryName = "";

    //税率
    private  Double taxRate;

    //期初应付款
    private  Double amount;

    //期初预付款
    private  Double periodMoney;

    //初期往来余额
    private  Double difMoney;

    //余额日期
    private  String beginDate;

    //备注
    private  String remark = "";

    //职位
    private  String place = "";

    //客户联系方式
    private  String linkMans;

    //-10客户  10供应商
    private  Integer type;

    //联系方式
    private  String contact = "";

    //客户等级ID
    private  Integer cLevel;

    //客户等级
    private  String cLevelName = "";

    //省
    private  String province;

    //市
    private  String city;

    //区县
    private  String county;

    //收货地址详情
    private  String address;

    //""
    private  String pinYin;

    //0启用   1禁用
    private  Integer disable;

    //0正常 1删除
    private  Integer isDelete;
    private Long userId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getcCategory() {
        return cCategory;
    }

    public void setcCategory(Integer cCategory) {
        this.cCategory = cCategory;
    }

    public String getcCategoryName() {
        return cCategoryName;
    }

    public void setcCategoryName(String cCategoryName) {
        this.cCategoryName = cCategoryName;
    }

    public Double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getPeriodMoney() {
        return periodMoney;
    }

    public void setPeriodMoney(Double periodMoney) {
        this.periodMoney = periodMoney;
    }

    public Double getDifMoney() {
        return difMoney;
    }

    public void setDifMoney(Double difMoney) {
        this.difMoney = difMoney;
    }

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getLinkMans() {
        return linkMans;
    }

    public void setLinkMans(String linkMans) {
        this.linkMans = linkMans;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public Integer getcLevel() {
        return cLevel;
    }

    public void setcLevel(Integer cLevel) {
        this.cLevel = cLevel;
    }

    public String getcLevelName() {
        return cLevelName;
    }

    public void setcLevelName(String cLevelName) {
        this.cLevelName = cLevelName;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPinYin() {
        return pinYin;
    }

    public void setPinYin(String pinYin) {
        this.pinYin = pinYin;
    }

    public Integer getDisable() {
        return disable;
    }

    public void setDisable(Integer disable) {
        this.disable = disable;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
