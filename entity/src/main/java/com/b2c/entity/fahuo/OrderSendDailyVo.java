package com.b2c.entity.fahuo;

import java.math.BigDecimal;

public class OrderSendDailyVo {
    private String goodsImg;
    private String goodsName;
    private String goodsNumber;
    private Long goodsId;
    private BigDecimal amount;
    private Integer quantity;
    private BigDecimal price;
    private BigDecimal purPrice;
    private BigDecimal itemAmount;

    
    public String getGoodsImg() {
        return goodsImg;
    }
    public void setGoodsImg(String goodsImg) {
        this.goodsImg = goodsImg;
    }
    public String getGoodsName() {
        return goodsName;
    }
    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }
    public String getGoodsNumber() {
        return goodsNumber;
    }
    public void setGoodsNumber(String goodsNumber) {
        this.goodsNumber = goodsNumber;
    }
    public Long getGoodsId() {
        return goodsId;
    }
    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }
    public BigDecimal getAmount() {
        return amount;
    }
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    public Integer getQuantity() {
        return quantity;
    }
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
    public BigDecimal getPrice() {
        return price;
    }
    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    public BigDecimal getPurPrice() {
        return purPrice;
    }
    public void setPurPrice(BigDecimal purPrice) {
        this.purPrice = purPrice;
    }
    public BigDecimal getItemAmount() {
        return itemAmount;
    }
    public void setItemAmount(BigDecimal itemAmount) {
        this.itemAmount = itemAmount;
    }

    
}
