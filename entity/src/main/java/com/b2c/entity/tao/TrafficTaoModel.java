package com.b2c.entity.tao;

import java.math.BigDecimal;

public class TrafficTaoModel {
    private Integer id;
    private String goodsId;
    private String title;
    private BigDecimal payAmount;
    private Double buyers;
    private Double carts;
    private Double colls;
    private Double views;
    private String date;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public BigDecimal getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(BigDecimal payAmount) {
        this.payAmount = payAmount;
    }

    public Double getBuyers() {
        return buyers;
    }

    public void setBuyers(Double buyers) {
        this.buyers = buyers;
    }

    public Double getCarts() {
        return carts;
    }

    public void setCarts(Double carts) {
        this.carts = carts;
    }

    public Double getColls() {
        return colls;
    }

    public void setColls(Double colls) {
        this.colls = colls;
    }

    public Double getViews() {
        return views;
    }

    public void setViews(Double views) {
        this.views = views;
    }
}
