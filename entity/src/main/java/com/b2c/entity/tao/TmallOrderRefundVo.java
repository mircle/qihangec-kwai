package com.b2c.entity.tao;

import java.math.BigDecimal;


public class TmallOrderRefundVo {
    private Long id;
    private String refundId;
    private String orderId;
    private Integer afterSalesType;
    private String tid;
    private String goodsImg;
    private String goodsName;
    private String spec;
    private Long num;
    private BigDecimal refundFee;
    private String reason;
    private String desc;
    private String logisticsCompany;
    private String logisticsCode;
    private Long created;
    private Long createOn;
    private Integer status;
    private Integer auditStatus;
    private String auditTime;
    private Integer shopId;
    private Integer erpGoodsId;
    private Integer erpGoodsSpecId;
    private String specNumber;

    
    
    public Integer getErpGoodsId() {
        return erpGoodsId;
    }
    public void setErpGoodsId(Integer erpGoodsId) {
        this.erpGoodsId = erpGoodsId;
    }
    public Integer getErpGoodsSpecId() {
        return erpGoodsSpecId;
    }
    public void setErpGoodsSpecId(Integer erpGoodsSpecId) {
        this.erpGoodsSpecId = erpGoodsSpecId;
    }
    public String getSpecNumber() {
        return specNumber;
    }
    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }
    public String getOrderId() {
        return orderId;
    }
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
    
    public Integer getAuditStatus() {
        return auditStatus;
    }
    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }
    public Integer getStatus() {
        return status;
    }
    public void setStatus(Integer status) {
        this.status = status;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getRefundId() {
        return refundId;
    }
    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }
    public String getGoodsImg() {
        return goodsImg;
    }
    public void setGoodsImg(String goodsImg) {
        this.goodsImg = goodsImg;
    }
    public String getGoodsName() {
        return goodsName;
    }
    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }
    public String getSpec() {
        return spec;
    }
    public void setSpec(String spec) {
        this.spec = spec;
    }
    public Long getNum() {
        return num;
    }
    public void setNum(Long num) {
        this.num = num;
    }
    public BigDecimal getRefundFee() {
        return refundFee;
    }
    public void setRefundFee(BigDecimal refundFee) {
        this.refundFee = refundFee;
    }
    public String getReason() {
        return reason;
    }
    public void setReason(String reason) {
        this.reason = reason;
    }
    public String getDesc() {
        return desc;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }
    public String getLogisticsCompany() {
        return logisticsCompany;
    }
    public void setLogisticsCompany(String logisticsCompany) {
        this.logisticsCompany = logisticsCompany;
    }
    public String getLogisticsCode() {
        return logisticsCode;
    }
    public void setLogisticsCode(String logisticsCode) {
        this.logisticsCode = logisticsCode;
    }
    public Long getCreated() {
        return created;
    }
    public void setCreated(Long created) {
        this.created = created;
    }
    public Long getCreateOn() {
        return createOn;
    }
    public void setCreateOn(Long createOn) {
        this.createOn = createOn;
    }
    public String getAuditTime() {
        return auditTime;
    }
    public void setAuditTime(String auditTime) {
        this.auditTime = auditTime;
    }
    public Integer getShopId() {
        return shopId;
    }
    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }
    public Integer getAfterSalesType() {
        return afterSalesType;
    }
    public void setAfterSalesType(Integer afterSalesType) {
        this.afterSalesType = afterSalesType;
    }
    public String getTid() {
        return tid;
    }
    public void setTid(String tid) {
        this.tid = tid;
    }

    
}
