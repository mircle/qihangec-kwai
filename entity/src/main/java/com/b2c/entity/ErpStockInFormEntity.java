package com.b2c.entity;

import java.util.Date;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-10-09 14:53
 */
public class ErpStockInFormEntity {
    private Long id;//` bigint(20) NOT NULL AUTO_INCREMENT,
    private String number;//` varchar(30) NOT NULL DEFAULT '' COMMENT '单据编号',
    private Long checkoutId;//` bigint(20) NOT NULL COMMENT '验货单id',
    private Long invoiceId;// 采购id,
    private String remark;//` varchar(255) DEFAULT NULL COMMENT '备注',
    private Integer isDelete;//` tinyint(1) DEFAULT 0 COMMENT '1删除  0正常',
    private Integer stockInUserId;//` int(11) DEFAULT NULL COMMENT '入库人',
    private String stockInUserName;//` varchar(25) DEFAULT NULL,
    private Date stockInTime;//` datetime DEFAULT NULL COMMENT '入库时间',
    private Long quantity;//入库总数量，计算而来
//    private String contractNo;//合同号，来自采购单
//    private String checkoutNo;//验货单号，来自验货单
//    private String billNo;//采购单号，来自采购单
    private Long stockInTime1;//入库时间
    private String sourceNo;
    private Integer inType;

    public Integer getInType() {
        return inType;
    }

    public void setInType(Integer inType) {
        this.inType = inType;
    }

    public String getSourceNo() {
        return sourceNo;
    }

    public void setSourceNo(String sourceNo) {
        this.sourceNo = sourceNo;
    }

    public Long getStockInTime1() {
        return stockInTime1;
    }

    public void setStockInTime1(Long stockInTime1) {
        this.stockInTime1 = stockInTime1;
    }

//    public String getBillNo() {
//        return billNo;
//    }
//
//    public void setBillNo(String billNo) {
//        this.billNo = billNo;
//    }

    public Long getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Long invoiceId) {
        this.invoiceId = invoiceId;
    }

//    public String getCheckoutNo() {
//        return checkoutNo;
//    }
//
//    public void setCheckoutNo(String checkoutNo) {
//        this.checkoutNo = checkoutNo;
//    }
//
//    public String getContractNo() {
//        return contractNo;
//    }
//
//    public void setContractNo(String contractNo) {
//        this.contractNo = contractNo;
//    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Long getCheckoutId() {
        return checkoutId;
    }

    public void setCheckoutId(Long checkoutId) {
        this.checkoutId = checkoutId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Integer getStockInUserId() {
        return stockInUserId;
    }

    public void setStockInUserId(Integer stockInUserId) {
        this.stockInUserId = stockInUserId;
    }

    public String getStockInUserName() {
        return stockInUserName;
    }

    public void setStockInUserName(String stockInUserName) {
        this.stockInUserName = stockInUserName;
    }

    public Date getStockInTime() {
        return stockInTime;
    }

    public void setStockInTime(Date stockInTime) {
        this.stockInTime = stockInTime;
    }
}
