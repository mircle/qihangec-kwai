package com.b2c.entity;

import java.math.BigDecimal;

/**
 * @Description: 用户
 * pbd add 2019/1/8 10:00
 */
public class UserEntity {
    /**
     * 用户id
     */
    private int id;
    /**
     * 用户名
     */
    private String userName;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 密码
     */
    private String pwd;
    /**
     * 密码随机数
     */
    private String pwdRandStr;
    /**
     * 用户昵称
     */
    private String nickName;
    /**
     * 账户余额
     */
    private BigDecimal balance;
    /**
     * 性别 1男 2女
     */
    private int sex;
    /**
     * 用户头像
     */
    private String headImg;
    /**
     * wechat openid
     */
    private String wxOpenId;
    /**
     * wehat unionid
     */
    private String wxUnionId;
    private String country;
    private String province;
    private String city;
    /**
     * 推荐人id
     */
    private int recommenderId;
    /**
     * 注册ip
     */
    private String regIp;
    /**
     * 注册终端
     */
    private String regDevice;
    /**
     * 注册时间
     */
    private Long regTime;
    /**
     * 登录次数
     */
    private int loginCount;
    /**
     * 最后一次登录时间
     */
    private int lastLoinTime;
    /**
     * 最后一次登录Ip
     */
    private String lastLoginIp;
    /**
     * 最后一次登录设备
     */
    private String lastLoginDevice;
    /**
     * 用户类型 1普通用户 2.业务员
     */
    private int type;
    /**
     * 用户分组id
     */
//    private int userGroupId;
    /**
     * 用户状态 1正常 9禁用
     */
    private String state;
    /**
     * 用户分组id
     */
    private int salesmanGroup;

    /**
     * 成为业务员时间
     */
    private Integer salesmanTime;


    private Double orderMoney; //分销金额

    private Integer isDeveloper;//是否渠道开发商

    private String idCode;
    private String recommendCode;
    /**
     * 业务员Id
     */
    private Integer developerId;
    private String developerName;//业务员

    public String getDeveloperName() {
        return developerName;
    }

    public void setDeveloperName(String developerName) {
        this.developerName = developerName;
    }

    public Integer getDeveloperId() {
        return developerId;
    }

    public void setDeveloperId(Integer developerId) {
        this.developerId = developerId;
    }

    public String getIdCode() {
        return idCode;
    }

    public void setIdCode(String idCode) {
        this.idCode = idCode;
    }

    public String getRecommendCode() {
        return recommendCode;
    }

    public void setRecommendCode(String recommendCode) {
        this.recommendCode = recommendCode;
    }

    public Integer getIsDeveloper() {
        return isDeveloper;
    }

    public void setIsDeveloper(Integer isDeveloper) {
        this.isDeveloper = isDeveloper;
    }

    public int getSalesmanGroup() {
        return salesmanGroup;
    }

    public void setSalesmanGroup(int salesmanGroup) {
        this.salesmanGroup = salesmanGroup;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Double getOrderMoney() {
        return orderMoney;
    }

    public void setOrderMoney(Double orderMoney) {
        this.orderMoney = orderMoney;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getPwdRandStr() {
        return pwdRandStr;
    }

    public void setPwdRandStr(String pwdRandStr) {
        this.pwdRandStr = pwdRandStr;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public String getWxOpenId() {
        return wxOpenId;
    }

    public void setWxOpenId(String wxOpenId) {
        this.wxOpenId = wxOpenId;
    }

    public String getWxUnionId() {
        return wxUnionId;
    }

    public void setWxUnionId(String wxUnionId) {
        this.wxUnionId = wxUnionId;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getRecommenderId() {
        return recommenderId;
    }

    public void setRecommenderId(int recommenderId) {
        this.recommenderId = recommenderId;
    }

    public String getRegIp() {
        return regIp;
    }

    public void setRegIp(String regIp) {
        this.regIp = regIp;
    }

    public String getRegDevice() {
        return regDevice;
    }

    public void setRegDevice(String regDevice) {
        this.regDevice = regDevice;
    }

    public Long getRegTime() {
        return regTime;
    }

    public void setRegTime(Long regTime) {
        this.regTime = regTime;
    }

    public int getLoginCount() {
        return loginCount;
    }

    public void setLoginCount(int loginCount) {
        this.loginCount = loginCount;
    }

    public int getLastLoinTime() {
        return lastLoinTime;
    }

    public void setLastLoinTime(int lastLoinTime) {
        this.lastLoinTime = lastLoinTime;
    }

    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }

    public String getLastLoginDevice() {
        return lastLoginDevice;
    }

    public void setLastLoginDevice(String lastLoginDevice) {
        this.lastLoginDevice = lastLoginDevice;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

//    public int getUserGroupId() {
//        return userGroupId;
//    }
//
//    public void setUserGroupId(int userGroupId) {
//        this.userGroupId = userGroupId;
//    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getSalesmanTime() {
        return salesmanTime;
    }

    public void setSalesmanTime(Integer salesmanTime) {
        this.salesmanTime = salesmanTime;
    }
}
