package com.b2c.entity;

public class WaitSendGoodsSpecModel {

    private String goodsNum;
    private String goodsSpec;
    private String goodsSpecNum;
    private Integer quantity;
    private Integer pickingQty;
    private Integer currentQty;

    public String getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(String goodsNum) {
        this.goodsNum = goodsNum;
    }

    public String getGoodsSpec() {
        return goodsSpec;
    }

    public void setGoodsSpec(String goodsSpec) {
        this.goodsSpec = goodsSpec;
    }

    public String getGoodsSpecNum() {
        return goodsSpecNum;
    }

    public void setGoodsSpecNum(String goodsSpecNum) {
        this.goodsSpecNum = goodsSpecNum;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getCurrentQty() {
        return currentQty;
    }

    public void setCurrentQty(Integer currentQty) {
        this.currentQty = currentQty;
    }

    public Integer getPickingQty() {
        return pickingQty;
    }

    public void setPickingQty(Integer pickingQty) {
        this.pickingQty = pickingQty;
    }
}
