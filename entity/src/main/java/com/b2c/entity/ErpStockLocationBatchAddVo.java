package com.b2c.entity;

/**
 * 批量添加仓位信息vo
 */
public class ErpStockLocationBatchAddVo {
    private Integer id;
    private String number;
    private String name;
    private String houseNo;//仓库编码
    private String reservoirNo;//库区编码
//    private String store_house;
    private Integer houseId;//仓库id
//    private String reservoir;
    private String reservoirId;//库区id
    private Integer isDelete;

    public ErpStockLocationBatchAddVo() {
    }

    public ErpStockLocationBatchAddVo(Integer id, String number, String name, String houseNo, String reservoirNo) {
        this.id = id;
        this.number = number;
        this.name = name;
//        this.store_house = store_house;
//        this.reservoir = reservoir;
        this.houseNo = houseNo;
        this.reservoirNo = reservoirNo;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public String getReservoirNo() {
        return reservoirNo;
    }

    public void setReservoirNo(String reservoirNo) {
        this.reservoirNo = reservoirNo;
    }

    public Integer getHouseId() {
        return houseId;
    }

    public void setHouseId(Integer houseId) {
        this.houseId = houseId;
    }

    public String getReservoirId() {
        return reservoirId;
    }

    public void setReservoirId(String reservoirId) {
        this.reservoirId = reservoirId;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public String getStore_house() {
//        return store_house;
//    }
//
//    public void setStore_house(String store_house) {
//        this.store_house = store_house;
//    }
//
//    public String getReservoir() {
//        return reservoir;
//    }
//
//    public void setReservoir(String reservoir) {
//        this.reservoir = reservoir;
//    }
}
