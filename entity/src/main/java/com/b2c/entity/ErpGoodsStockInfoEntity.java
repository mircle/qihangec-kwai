package com.b2c.entity;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-09-20 09:55
 */
public class ErpGoodsStockInfoEntity {
    private Long id;
    private Integer goodsId;
    private String goodsNumber;  //商品编码
    private Integer specId;
    private String specNumber;  //规格编码
    private Integer locationId;//仓位id
    private Integer parentId1;//仓库id
    private Integer parentId2;//库区id
    private Long currentQty;  //当前数量
    private Long lockedQty;  //锁定数量
    private Integer disable; //0启用   1禁用
    private Integer isDelete; //0启用   1禁用
    private String locationName;//仓位地址

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Long getLockedQty() {
        return lockedQty;
    }

    public void setLockedQty(Long lockedQty) {
        this.lockedQty = lockedQty;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getParentId1() {
        return parentId1;
    }

    public void setParentId1(Integer parentId1) {
        this.parentId1 = parentId1;
    }

    public Integer getParentId2() {
        return parentId2;
    }

    public void setParentId2(Integer parentId2) {
        this.parentId2 = parentId2;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(String goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public Integer getSpecId() {
        return specId;
    }

    public void setSpecId(Integer specId) {
        this.specId = specId;
    }

    public String getSpecNumber() {
        return specNumber;
    }

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }


    public Long getCurrentQty() {
        return currentQty;
    }

    public void setCurrentQty(Long currentQty) {
        this.currentQty = currentQty;
    }

    public Integer getDisable() {
        return disable;
    }

    public void setDisable(Integer disable) {
        this.disable = disable;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }
}
