package com.b2c.entity.datacenter;

/**
 * 描述：
 * 阿里订单收货地址entity
 *
 * @author qlp
 * @date 2019-09-16 16:21
 */
public class DcAliOrderAddressEntity {
    private Long id;//` bigint(20) NOT NULL AUTO_INCREMENT,
    private Long orderId;//` bigint(20) NOT NULL COMMENT '订单id',
    private String mobile;//` varchar(20) DEFAULT NULL,
    private String province;//` varchar(20) DEFAULT NULL,
    private String city;//` varchar(30) DEFAULT NULL,
    private String area;//` varchar(30) DEFAULT NULL,
    private String areaCode;//` varchar(10) DEFAULT NULL,
    private String town;//` varchar(30) DEFAULT NULL,
    private String townCode;//` varchar(20) DEFAULT NULL,
    private String address;//` varchar(50) DEFAULT NULL,
    private String contactPerson;//` varchar(20) DEFAULT NULL,

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getTownCode() {
        return townCode;
    }

    public void setTownCode(String townCode) {
        this.townCode = townCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }
}
