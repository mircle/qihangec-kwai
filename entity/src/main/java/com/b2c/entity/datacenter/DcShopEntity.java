package com.b2c.entity.datacenter;

import com.b2c.entity.erp.ErpPullOrderLogEntity;

public class DcShopEntity {
    /**
     * id
     */
    private Integer id;

    /**
     * name
     */
    private String name;
    private String ename;
    private String nickName;
    private String company;
    private String sessionKey;
    private Long sellerUserId =0l;

    /**
     * 对应第三方平台id
     */
    private Integer type;
    private String typeName;
    private String url;
    private Integer orderNum;//排序字段


    /**
     * 更新时间
     */
    private Long modifyOn;

    /**
     * 描述
     */
    private String remark;
    private Integer isDelete;

    private String appkey;//抖音appkey
    private String appSercet;//抖音appSercet
    private ErpPullOrderLogEntity orderPullLog;//订单拉取记录,最后一条

    //统计类
    private Integer todayOrderCount =0;//今日订单数量；
    private Integer todayValidOrder = 0;//今日有效订单
    private Double todayValidOrderAmout = 0.0;//今日有效成交金额
    private Integer waitSendOrder = 0;//待发货数量
    private Integer waitAuditReturn = 0;//待处理退货

    
    public Integer getTodayValidOrder() {
        return todayValidOrder;
    }

    public void setTodayValidOrder(Integer todayValidOrder) {
        this.todayValidOrder = todayValidOrder;
    }

    public Double getTodayValidOrderAmout() {
        return todayValidOrderAmout;
    }

    public void setTodayValidOrderAmout(Double todayValidOrderAmout) {
        this.todayValidOrderAmout = todayValidOrderAmout;
    }

    public Integer getWaitSendOrder() {
        return waitSendOrder;
    }

    public void setWaitSendOrder(Integer waitSendOrder) {
        this.waitSendOrder = waitSendOrder;
    }

    public Integer getWaitAuditReturn() {
        return waitAuditReturn;
    }

    public void setWaitAuditReturn(Integer waitAuditReturn) {
        this.waitAuditReturn = waitAuditReturn;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public ErpPullOrderLogEntity getOrderPullLog() {
        return orderPullLog;
    }

    public void setOrderPullLog(ErpPullOrderLogEntity orderPullLog) {
        this.orderPullLog = orderPullLog;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public Long getSellerUserId() {
        return sellerUserId;
    }

    public void setSellerUserId(Long sellerUserId) {
        this.sellerUserId = sellerUserId;
    }

    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getModifyOn() {
        return modifyOn;
    }

    public void setModifyOn(Long modifyOn) {
        this.modifyOn = modifyOn;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAppkey() {
        return appkey;
    }

    public void setAppkey(String appkey) {
        this.appkey = appkey;
    }

    public String getAppSercet() {
        return appSercet;
    }

    public void setAppSercet(String appSercet) {
        this.appSercet = appSercet;
    }

    public Integer getTodayOrderCount() {
        return todayOrderCount;
    }

    public void setTodayOrderCount(Integer todayOrderCount) {
        this.todayOrderCount = todayOrderCount;
    }
}
