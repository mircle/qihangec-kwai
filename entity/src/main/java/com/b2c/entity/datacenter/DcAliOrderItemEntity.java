package com.b2c.entity.datacenter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 描述：
 * 阿里订单item
 *
 * @author qlp
 * @date 2019-09-16 16:26
 */
public class DcAliOrderItemEntity {
    private Long id;//` bigint(20) NOT NULL AUTO_INCREMENT,
    private Long orderId;//` bigint(20) NOT NULL COMMENT '订单id',

    private Long subItemID;//子订单id，用于阿里发货
    private BigDecimal itemAmount;//` decimal(10,2) DEFAULT NULL,
    private String name;//` varchar(50) DEFAULT NULL,
    private BigDecimal price;//` decimal(10,2) DEFAULT NULL,
    private Long productId;//` bigint(20) DEFAULT NULL,
    private String productImgUrl;//` varchar(100) DEFAULT NULL,
    private String productSnapshotUrl;//` varchar(100) DEFAULT NULL,
    private BigDecimal quantity;//` decimal(4,0) DEFAULT NULL,
    private BigDecimal refundQuantity;//` decimal(4,0) DEFAULT NULL,
    private BigDecimal refund;//` decimal(4,0) DEFAULT NULL,
    private String productCargoNumber;//` varchar(30) DEFAULT NULL COMMENT '商品货号，对应系统商品编码',
    private String cargoNumber;//` varchar(30) DEFAULT NULL COMMENT '单品货号，对应系统sku编码',
    private Long skuId;//对应erp_goods_spec id用于确认订单的时候
    private String skuInfos;//` varchar(100) DEFAULT NULL COMMENT 'SKU信息json',
    private String skuInfo;//sku字符串信息
    private String status;//` varchar(10) DEFAULT NULL COMMENT '子订单状态',
    private String statusStr;//` varchar(10) DEFAULT NULL COMMENT '子订单状态',
    private String closeReason;//` varchar(10) DEFAULT NULL COMMENT '关闭原因',
    private Integer logisticsStatus;//` int(11) DEFAULT NULL COMMENT '1 未发货 2 已发货 3 已收货 4 已经退货 5 部分发货 8 还未创建物流订单',
    private Date gmtCreate;//` datetime DEFAULT NULL COMMENT '创建时间',
    private Date gmtModified;//` datetime DEFAULT NULL COMMENT '修改时间',
    private Date gmtCompleted;//` datetime DEFAULT NULL COMMENT '明细完成时间',
    private String gmtPayExpireTime;//` varchar(20) DEFAULT NULL COMMENT '库存超时时间，格式为“yyyy-MM-dd HH:mm:ss”',
    private String refundId;//` varchar(20) DEFAULT NULL COMMENT '退款单号',
    private String refundStatus;//` varchar(10) DEFAULT NULL COMMENT '退款状态',

    private Integer newSpecId;//新规格id
    private Integer erpGoodsId;//
    private Integer erpGoodsSpecId;//
    private String newSpecNumber;//新规格编码

    private Integer erpGoodId;//仓库商品Id
    private String erpSpec;//仓库商品规格
    private String newSpec;//新规格

    /**********商品仓库库存信息**********/
    private Long currentQty;
    private Long lockedQty;
    private Long pickQty;

    public Integer getErpGoodsId() {
        return erpGoodsId;
    }

    public void setErpGoodsId(Integer erpGoodsId) {
        this.erpGoodsId = erpGoodsId;
    }

    public Integer getErpGoodsSpecId() {
        return erpGoodsSpecId;
    }

    public void setErpGoodsSpecId(Integer erpGoodsSpecId) {
        this.erpGoodsSpecId = erpGoodsSpecId;
    }

    public BigDecimal getRefundQuantity() {
        return refundQuantity;
    }

    public void setRefundQuantity(BigDecimal refundQuantity) {
        this.refundQuantity = refundQuantity;
    }

    public String getErpSpec() {
        return erpSpec;
    }

    public void setErpSpec(String erpSpec) {
        this.erpSpec = erpSpec;
    }

    public String getNewSpec() {
        return newSpec;
    }

    public void setNewSpec(String newSpec) {
        this.newSpec = newSpec;
    }

    public Long getCurrentQty() {
        return currentQty;
    }

    public void setCurrentQty(Long currentQty) {
        this.currentQty = currentQty;
    }

    public Long getLockedQty() {
        return lockedQty;
    }

    public void setLockedQty(Long lockedQty) {
        this.lockedQty = lockedQty;
    }

    public Long getPickQty() {
        return pickQty;
    }

    public void setPickQty(Long pickQty) {
        this.pickQty = pickQty;
    }

    public Integer getErpGoodId() {
        return erpGoodId;
    }

    public void setErpGoodId(Integer erpGoodId) {
        this.erpGoodId = erpGoodId;
    }

    public Integer getNewSpecId() {
        return newSpecId;
    }

    public void setNewSpecId(Integer newSpecId) {
        this.newSpecId = newSpecId;
    }

    public String getNewSpecNumber() {
        return newSpecNumber;
    }

    public void setNewSpecNumber(String newSpecNumber) {
        this.newSpecNumber = newSpecNumber;
    }

    public Long getSubItemID() {
        return subItemID;
    }

    public void setSubItemID(Long subItemID) {
        this.subItemID = subItemID;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public BigDecimal getItemAmount() {
        return itemAmount;
    }

    public void setItemAmount(BigDecimal itemAmount) {
        this.itemAmount = itemAmount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductImgUrl() {
        return productImgUrl;
    }

    public void setProductImgUrl(String productImgUrl) {
        this.productImgUrl = productImgUrl;
    }

    public String getProductSnapshotUrl() {
        return productSnapshotUrl;
    }

    public void setProductSnapshotUrl(String productSnapshotUrl) {
        this.productSnapshotUrl = productSnapshotUrl;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getRefund() {
        return refund;
    }

    public void setRefund(BigDecimal refund) {
        this.refund = refund;
    }

    public String getProductCargoNumber() {
        return productCargoNumber;
    }

    public void setProductCargoNumber(String productCargoNumber) {
        this.productCargoNumber = productCargoNumber;
    }

    public String getCargoNumber() {
        return cargoNumber;
    }

    public void setCargoNumber(String cargoNumber) {
        this.cargoNumber = cargoNumber;
    }

    public String getSkuInfos() {
        return skuInfos;
    }

    public void setSkuInfos(String skuInfos) {
        this.skuInfos = skuInfos;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusStr() {
        return statusStr;
    }

    public void setStatusStr(String statusStr) {
        this.statusStr = statusStr;
    }

    public String getCloseReason() {
        return closeReason;
    }

    public void setCloseReason(String closeReason) {
        this.closeReason = closeReason;
    }

    public Integer getLogisticsStatus() {
        return logisticsStatus;
    }

    public void setLogisticsStatus(Integer logisticsStatus) {
        this.logisticsStatus = logisticsStatus;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Date getGmtCompleted() {
        return gmtCompleted;
    }

    public void setGmtCompleted(Date gmtCompleted) {
        this.gmtCompleted = gmtCompleted;
    }

    public String getGmtPayExpireTime() {
        return gmtPayExpireTime;
    }

    public void setGmtPayExpireTime(String gmtPayExpireTime) {
        this.gmtPayExpireTime = gmtPayExpireTime;
    }

    public String getRefundId() {
        return refundId;
    }

    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }

    public String getRefundStatus() {
        return refundStatus;
    }

    public void setRefundStatus(String refundStatus) {
        this.refundStatus = refundStatus;
    }

    public String getSkuInfo() {
        return skuInfo;
    }

    public void setSkuInfo(String skuInfo) {
        this.skuInfo = skuInfo;
    }
}
