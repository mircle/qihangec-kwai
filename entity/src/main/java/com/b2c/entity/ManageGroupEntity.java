package com.b2c.entity;

/**
 * @Description:管理员角色 pbd add 2019/3/20 9:11
 */
public class ManageGroupEntity {
    /**
     * id
     */
    private int id;
    /**
     * 分组名
     */
    private String groupName;
    /**
     * 分组描述
     */
    private String description;
    /**
     * 创建时间
     */
    private int createOn;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCreateOn() {
        return createOn;
    }

    public void setCreateOn(int createOn) {
        this.createOn = createOn;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
