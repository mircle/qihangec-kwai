package com.b2c.entity.pdd;

import java.math.BigDecimal;
import java.util.List;

public class ShopGoodsEntity {
    private Long id;
  private String thumbUrl;
  
  private Long goodsId;
  
  private String goodsName;
  private String goodsNickName;
  private String remark;
  private String imageUrl;
  
  private Integer isMoreSku;
  
  private Long goodsQuantity;
  private BigDecimal price;
  private Integer isOnsale;
  private Integer shopId;
  private String shopName;
  private Integer shopType;
  private String goodsNum;
  private Integer erpGoodsId;
  private Double costPrice;
  private String publishTime;
  private Integer totalSales;

  private List<ShopGoodsSkuEntity> skuList;
  
  
  public Integer getErpGoodsId() {
    return erpGoodsId;
}

public void setErpGoodsId(Integer erpGoodsId) {
    this.erpGoodsId = erpGoodsId;
}

public String getGoodsNum() {
    return goodsNum;
}

public void setGoodsNum(String goodsNum) {
    this.goodsNum = goodsNum;
}

public Integer getShopId() {
    return shopId;
}

public void setShopId(Integer shopId) {
    this.shopId = shopId;
}


  

public String getThumbUrl() {
    return thumbUrl;
}

public void setThumbUrl(String thumbUrl) {
    this.thumbUrl = thumbUrl;
}

public Long getGoodsId() {
    return goodsId;
}

public void setGoodsId(Long goodsId) {
    this.goodsId = goodsId;
}

public String getGoodsName() {
    return goodsName;
}

public void setGoodsName(String goodsName) {
    this.goodsName = goodsName;
}

public String getImageUrl() {
    return imageUrl;
}

public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
}

public Integer getIsMoreSku() {
    return isMoreSku;
}

public void setIsMoreSku(Integer isMoreSku) {
    this.isMoreSku = isMoreSku;
}

public Long getGoodsQuantity() {
    return goodsQuantity;
}

public void setGoodsQuantity(Long goodsQuantity) {
    this.goodsQuantity = goodsQuantity;
}

public Integer getIsOnsale() {
    return isOnsale;
}

public void setIsOnsale(Integer isOnsale) {
    this.isOnsale = isOnsale;
}

public List<ShopGoodsSkuEntity> getSkuList() {
    return skuList;
}

public void setSkuList(List<ShopGoodsSkuEntity> skuList) {
    this.skuList = skuList;
}

public Double getCostPrice() {
    return costPrice;
}

public void setCostPrice(Double costPrice) {
    this.costPrice = costPrice;
}

public String getGoodsNickName() {
    return goodsNickName;
}

public void setGoodsNickName(String goodsNickName) {
    this.goodsNickName = goodsNickName;
}

public String getRemark() {
    return remark;
}

public void setRemark(String remark) {
    this.remark = remark;
}

public Integer getShopType() {
    return shopType;
}

public void setShopType(Integer shopType) {
    this.shopType = shopType;
}

public BigDecimal getPrice() {
    return price;
}

public void setPrice(BigDecimal price) {
    this.price = price;
}

public String getPublishTime() {
    return publishTime;
}

public void setPublishTime(String publishTime) {
    this.publishTime = publishTime;
}

public Long getId() {
    return id;
}

public void setId(Long id) {
    this.id = id;
}

public String getShopName() {
    return shopName;
}

public void setShopName(String shopName) {
    this.shopName = shopName;
}

public Integer getTotalSales() {
    return totalSales;
}

public void setTotalSales(Integer totalSales) {
    this.totalSales = totalSales;
}

    

}