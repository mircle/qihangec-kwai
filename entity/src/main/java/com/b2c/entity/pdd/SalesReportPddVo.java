package com.b2c.entity.pdd;

import java.math.BigDecimal;

public class SalesReportPddVo {
    private String goodsImg;
    private String goodsName;
    private String goodsNum;
    private String specNumber;
    private String goodsSpec;
    private Long pddGoodId;
    private Long erpGoodsId;
    private BigDecimal amount;
    private Integer quantity;
    private BigDecimal costPrice;
    private BigDecimal goodsPrice;
    
    private BigDecimal freight;

    public BigDecimal getFreight() {
        return freight;
    }

    public void setFreight(BigDecimal freight) {
        this.freight = freight;
    }

    public String getGoodsImg() {
        return goodsImg;
    }
    public void setGoodsImg(String goodsImg) {
        this.goodsImg = goodsImg;
    }
    public String getGoodsSpec() {
        return goodsSpec;
    }
    public void setGoodsSpec(String goodsSpec) {
        this.goodsSpec = goodsSpec;
    }


    public String getGoodsName() {
        return goodsName;
    }
    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }
    public Long getPddGoodId() {
        return pddGoodId;
    }
    public void setPddGoodId(Long pddGoodId) {
        this.pddGoodId = pddGoodId;
    }
    public Long getErpGoodsId() {
        return erpGoodsId;
    }
    public void setErpGoodsId(Long erpGoodsId) {
        this.erpGoodsId = erpGoodsId;
    }
    public BigDecimal getAmount() {
        return amount;
    }
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    public Integer getQuantity() {
        return quantity;
    }
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
    public BigDecimal getCostPrice() {
        return costPrice;
    }
    public void setCostPrice(BigDecimal costPrice) {
        this.costPrice = costPrice;
    }
    public String getGoodsNum() {
        return goodsNum;
    }
    public void setGoodsNum(String goodsNum) {
        this.goodsNum = goodsNum;
    }
    public BigDecimal getGoodsPrice() {
        return goodsPrice;
    }
    public void setGoodsPrice(BigDecimal goodsPrice) {
        this.goodsPrice = goodsPrice;
    }
    public String getSpecNumber() {
        return specNumber;
    }
    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    
}
