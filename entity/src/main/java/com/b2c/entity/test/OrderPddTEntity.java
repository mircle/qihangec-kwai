package com.b2c.entity.test;

import java.io.Serializable;
import java.math.BigDecimal;


public class OrderPddTEntity implements Serializable {
    private String orderSn;
    private Long id;
    private BigDecimal amout;

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmout() {
        return amout;
    }

    public void setAmout(BigDecimal amout) {
        this.amout = amout;
    }
}
