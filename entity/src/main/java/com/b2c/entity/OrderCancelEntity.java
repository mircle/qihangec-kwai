package com.b2c.entity;

import com.b2c.entity.mall.OrderCancelItemEntity;
import java.math.BigDecimal;
import java.util.List;
/**
 * 描述：
 *
 * @author qlp
 * @date 2019-02-21 12:08
 */
public class OrderCancelEntity {
    private Long id;
    private int userId;
    private Long orderId;
    private String specNumber;//规格编码
    private BigDecimal totalAmount;//订单总金额
//    private int orderItemId;
//    private int goodsId;
//    private String specNumber;//规格编码
//    private double goodsTotalPrice;
    private Integer totalQuantity;//退货总数量
    private int state;
    private String orderCancelNum;
    private String reason;
    private String reasonImage;
    private String comment;
    private Long createOn;//创建时间
    private Long finishOn;//完成时间
//    private Integer goodNum;//退货商品数量
    private Integer type;//退款类型（0:退货退款，1:仅退款）
    private String sendCompany;//发货物流公司
    private String sendCompanyCode;//发货物流公司代码
    private String sendCode;//发货物流单号

    private List<OrderCancelItemEntity> items;//退货商品列表

    public String getSpecNumber() {
        return specNumber;
    }

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Integer totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public String getReasonImage() {
        return reasonImage;
    }

    public void setReasonImage(String reasonImage) {
        this.reasonImage = reasonImage;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getOrderCancelNum() {
        return orderCancelNum;
    }

    public void setOrderCancelNum(String orderCancelNum) {
        this.orderCancelNum = orderCancelNum;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getCreateOn() {
        return createOn;
    }

    public void setCreateOn(Long createOn) {
        this.createOn = createOn;
    }

    public Long getFinishOn() {
        return finishOn;
    }

    public void setFinishOn(Long finishOn) {
        this.finishOn = finishOn;
    }


    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getSendCompany() {
        return sendCompany;
    }

    public void setSendCompany(String sendCompany) {
        this.sendCompany = sendCompany;
    }

    public String getSendCompanyCode() {
        return sendCompanyCode;
    }

    public void setSendCompanyCode(String sendCompanyCode) {
        this.sendCompanyCode = sendCompanyCode;
    }

    public String getSendCode() {
        return sendCode;
    }

    public void setSendCode(String sendCode) {
        this.sendCode = sendCode;
    }

    public List<OrderCancelItemEntity> getItems() {
        return items;
    }

    public void setItems(List<OrderCancelItemEntity> items) {
        this.items = items;
    }

}
