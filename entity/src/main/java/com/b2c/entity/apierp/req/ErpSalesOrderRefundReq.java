package com.b2c.entity.apierp.req;


import com.b2c.entity.apierp.ErpSalesOrderItemEntity;

import java.io.Serializable;
import java.util.List;

/**
 * 销售订单退货
 */
public class ErpSalesOrderRefundReq  implements Serializable {
    //订单Id
    private Long orderId;
    private String logisticsCompany;
    private String logisticsCode;
    //售后明细
    private List<ErpSalesOrderItemEntity> items;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public List<ErpSalesOrderItemEntity> getItems() {
        return items;
    }

    public void setItems(List<ErpSalesOrderItemEntity> items) {
        this.items = items;
    }

    public String getLogisticsCompany() {
        return logisticsCompany;
    }

    public void setLogisticsCompany(String logisticsCompany) {
        this.logisticsCompany = logisticsCompany;
    }

    public String getLogisticsCode() {
        return logisticsCode;
    }

    public void setLogisticsCode(String logisticsCode) {
        this.logisticsCode = logisticsCode;
    }
}
