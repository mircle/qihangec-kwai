package com.b2c.entity.kwai;

import java.math.BigDecimal;
import java.util.List;

public class DcKwaiOrderEntity {
    /**
     * dc订单id
     */
    private Long id;

    /**
     * 快手订单id
     */
    private Long oid;

    /**
     * 创建时间
     */
    private Long createtime;

    /**
     * 折扣价格
     */
    private BigDecimal discountfee;

    /**
     * 运费
     */
    private BigDecimal expressfee;

    /**
     * sku数量
     */
    private Integer num;

    /**
     * 收件人姓名
     */
    private String consignee;

    /**
     * 收件人手机号
     */
    private String mobile;

    /**
     * 支付时间
     */
    private Long paytime;

    /**
     * 买家备注
     */
    private String buyerRemark;
    /**
     * 卖家备注
     */
    private String sellerRemark;

    /**
     * 订单状态
     */
    private Integer status;

    /**
     * 是否退款 0未退款 1该订单申请过退款
     */
    private Integer refund;

    /**
     * 子订单商品总价
     */
    private BigDecimal totalfee;

    /**
     * 省
     */
    private String province;

    /**
     * 市
     */
    private String city;

    /**
     * 区
     */
    private String district;

    /**
     * 详细地址
     */
    private String address;

    /**
     * 快递公司
     */
    private String logisticsCompany;

    /**
     * 快递公司编码
     */
    private String logisticsCompanyCode;

    /**
     * 快递单号
     */
    private String logisticsCode;

    /**
     * 订单审核状态（0待审核1已审核）
     */
    private Integer auditStatus;

    /**
     * 创建时间
     */
    private Long createon;

    /**
     * 发货状态（0待出库1拣货中2已拣货3已出库4已发货）
     */
    private Integer sendStatus;

    /**
     * 发货时间（仓库真实发货时间）
     */
    private Long sendTime;
    /**
     * 商品信息
     */
    private List<DcKwaiOrdersItemEntity> items;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOid() {
        return oid;
    }

    public void setOid(Long oid) {
        this.oid = oid;
    }

    public Long getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Long createtime) {
        this.createtime = createtime;
    }

    public BigDecimal getDiscountfee() {
        return discountfee;
    }

    public void setDiscountfee(BigDecimal discountfee) {
        this.discountfee = discountfee;
    }

    public BigDecimal getExpressfee() {
        return expressfee;
    }

    public void setExpressfee(BigDecimal expressfee) {
        this.expressfee = expressfee;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Long getPaytime() {
        return paytime;
    }

    public void setPaytime(Long paytime) {
        this.paytime = paytime;
    }

    public String getBuyerRemark() {
        return buyerRemark;
    }

    public void setBuyerRemark(String buyerRemark) {
        this.buyerRemark = buyerRemark;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getRefund() {
        return refund;
    }

    public void setRefund(Integer refund) {
        this.refund = refund;
    }

    public BigDecimal getTotalfee() {
        return totalfee;
    }

    public void setTotalfee(BigDecimal totalfee) {
        this.totalfee = totalfee;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLogisticsCompany() {
        return logisticsCompany;
    }

    public void setLogisticsCompany(String logisticsCompany) {
        this.logisticsCompany = logisticsCompany;
    }

    public String getLogisticsCompanyCode() {
        return logisticsCompanyCode;
    }

    public void setLogisticsCompanyCode(String logisticsCompanyCode) {
        this.logisticsCompanyCode = logisticsCompanyCode;
    }

    public String getLogisticsCode() {
        return logisticsCode;
    }

    public void setLogisticsCode(String logisticsCode) {
        this.logisticsCode = logisticsCode;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public Long getCreateon() {
        return createon;
    }

    public void setCreateon(Long createon) {
        this.createon = createon;
    }

    public Integer getSendStatus() {
        return sendStatus;
    }

    public void setSendStatus(Integer sendStatus) {
        this.sendStatus = sendStatus;
    }

    public Long getSendTime() {
        return sendTime;
    }

    public void setSendTime(Long sendTime) {
        this.sendTime = sendTime;
    }

    public String getSellerRemark() {
        return sellerRemark;
    }

    public void setSellerRemark(String sellerRemark) {
        this.sellerRemark = sellerRemark;
    }

    public List<DcKwaiOrdersItemEntity> getItems() {
        return items;
    }

    public void setItems(List<DcKwaiOrdersItemEntity> items) {
        this.items = items;
    }
}
