package com.b2c.entity.xhs;

public class XhsOrderItemEntity {
    private Long id;//` BIGINT(20) NOT NULL AUTO_INCREMENT,
	private Long orderId;//` BIGINT(20) NOT NULL COMMENT '订单id',
    private String itemId;//` VARCHAR(50) NULL DEFAULT NULL COMMENT '商品id' COLLATE 'utf8_general_ci',
    private String itemName;//` VARCHAR(100) NULL DEFAULT NULL COMMENT '商品名称' COLLATE 'utf8_general_ci',
    private String erpCode;//` VARCHAR(50) NULL DEFAULT NULL COMMENT '商家编码(若为组合品，暂不支持组合品的商家编码，但skulist会返回子商品商家编码)' COLLATE 'utf8_general_ci',
    private String itemSpec;//` VARCHAR(50) NULL DEFAULT NULL COMMENT '规格' COLLATE 'utf8_general_ci',
    private String itemImage;//` VARCHAR(250) NULL DEFAULT NULL COMMENT '商品图片url' COLLATE 'utf8_general_ci',
    private Integer quantity;//` INT(11) NOT NULL COMMENT '数量',
    private Long totalPaidAmount;//` INT(11) NOT NULL COMMENT '总支付金额（考虑总件数）商品总实付',
    private Long totalMerchantDiscount;//` INT(11) NOT NULL COMMENT '商家承担总优惠',
    private Long totalRedDiscount;//` INT(11) NOT NULL COMMENT '平台承担总优惠',
    private Integer itemTag;//` INT(11) NOT NULL COMMENT '是否赠品，1 赠品 0 普通商品',
    private Integer erpSkuId;

    public Integer getErpSkuId() {
        return erpSkuId;
    }

    public void setErpSkuId(Integer erpSkuId) {
        this.erpSkuId = erpSkuId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getErpCode() {
        return erpCode;
    }

    public void setErpCode(String erpCode) {
        this.erpCode = erpCode;
    }

    public String getItemSpec() {
        return itemSpec;
    }

    public void setItemSpec(String itemSpec) {
        this.itemSpec = itemSpec;
    }

    public String getItemImage() {
        return itemImage;
    }

    public void setItemImage(String itemImage) {
        this.itemImage = itemImage;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Long getTotalPaidAmount() {
        return totalPaidAmount;
    }

    public void setTotalPaidAmount(Long totalPaidAmount) {
        this.totalPaidAmount = totalPaidAmount;
    }

    public Long getTotalMerchantDiscount() {
        return totalMerchantDiscount;
    }

    public void setTotalMerchantDiscount(Long totalMerchantDiscount) {
        this.totalMerchantDiscount = totalMerchantDiscount;
    }

    public Long getTotalRedDiscount() {
        return totalRedDiscount;
    }

    public void setTotalRedDiscount(Long totalRedDiscount) {
        this.totalRedDiscount = totalRedDiscount;
    }

    public Integer getItemTag() {
        return itemTag;
    }

    public void setItemTag(Integer itemTag) {
        this.itemTag = itemTag;
    }
}
