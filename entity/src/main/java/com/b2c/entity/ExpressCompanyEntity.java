package com.b2c.entity;

/**
 * 描述：快递公司
 *
 * @author qlp
 * @date 2019-03-08 14:41
 */
public class ExpressCompanyEntity {
    private int id;
    private String code;
    private String name;
    private Integer yzId;
    private Integer status;
    private Integer createOn;
    private Integer isDefault;

    public Integer getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Integer isDefault) {
        this.isDefault = isDefault;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getYzId() {
        return yzId;
    }

    public void setYzId(Integer yzId) {
        this.yzId = yzId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCreateOn() {
        return createOn;
    }

    public void setCreateOn(Integer createOn) {
        this.createOn = createOn;
    }
}
