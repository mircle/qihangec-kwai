package com.b2c.entity;

/**
 * 描述：
 * 供应商
 *
 * @author qlp
 * @date 2019-03-21 13:46
 */
public class ContactEntity {
    private Long id;
    private String name;
    private String number;
    private Integer cCategory;
    private String cCategoryName;
    private Double taxRate;
    private Double amount;
    private Double periodMoney;
    private Double difMoney;
    private String beginDate;
    private String remark;
    private String place;
    private String linkMans;
    private int type;//10供应商  20客户  30订单收货地址
    private String contact;
    private Integer cLevel;
    private String cLevelName;
    private String pinYin;
    private int disable;
    private int isDelete;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getcCategory() {
        return cCategory;
    }

    public void setcCategory(Integer cCategory) {
        this.cCategory = cCategory;
    }

    public String getcCategoryName() {
        return cCategoryName;
    }

    public void setcCategoryName(String cCategoryName) {
        this.cCategoryName = cCategoryName;
    }

    public Double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getPeriodMoney() {
        return periodMoney;
    }

    public void setPeriodMoney(Double periodMoney) {
        this.periodMoney = periodMoney;
    }

    public Double getDifMoney() {
        return difMoney;
    }

    public void setDifMoney(Double difMoney) {
        this.difMoney = difMoney;
    }

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getLinkMans() {
        return linkMans;
    }

    public void setLinkMans(String linkMans) {
        this.linkMans = linkMans;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public Integer getcLevel() {
        return cLevel;
    }

    public void setcLevel(Integer cLevel) {
        this.cLevel = cLevel;
    }

    public String getcLevelName() {
        return cLevelName;
    }

    public void setcLevelName(String cLevelName) {
        this.cLevelName = cLevelName;
    }

    public String getPinYin() {
        return pinYin;
    }

    public void setPinYin(String pinYin) {
        this.pinYin = pinYin;
    }

    public int getDisable() {
        return disable;
    }

    public void setDisable(int disable) {
        this.disable = disable;
    }

    public int getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
