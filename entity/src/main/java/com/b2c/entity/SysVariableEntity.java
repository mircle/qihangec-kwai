package com.b2c.entity;

/**
 * @Description:系统相关参数对象 pbd add 2019/4/3 14:26
 */
public class SysVariableEntity {
    private String key;
    /**
     * 值
     */
    private String value;
    /**
     * 修改时间
     */
    private Long modifyOn;
    /**
     * 备注
     */
    private String remark;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getModifyOn() {
        return modifyOn;
    }

    public void setModifyOn(Long modifyOn) {
        this.modifyOn = modifyOn;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
