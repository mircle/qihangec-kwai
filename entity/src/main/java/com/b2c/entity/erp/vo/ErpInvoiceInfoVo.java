package com.b2c.entity.erp.vo;

import com.b2c.entity.ErpGoodsStockInfoEntity;

import java.util.List;

/**
 * @Description: pbd add 2019/10/9 17:14
 */
public class ErpInvoiceInfoVo {
    private Long id;
    private Long iid;
    private Long quantity;
    private Integer goodsId;
    private String goodsNumber;
    private Integer specId;
    private String specNumber;
    private Integer status;
    private Double price;

    private Long inQuantity;

    private List<ErpGoodsStockInfoEntity> goodStockList;

    public Long getInQuantity() {
        return inQuantity;
    }

    public void setInQuantity(Long inQuantity) {
        this.inQuantity = inQuantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIid() {
        return iid;
    }

    public void setIid(Long iid) {
        this.iid = iid;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(String goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public Integer getSpecId() {
        return specId;
    }

    public void setSpecId(Integer specId) {
        this.specId = specId;
    }

    public String getSpecNumber() {
        return specNumber;
    }

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<ErpGoodsStockInfoEntity> getGoodStockList() {
        return goodStockList;
    }

    public void setGoodStockList(List<ErpGoodsStockInfoEntity> goodStockList) {
        this.goodStockList = goodStockList;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
