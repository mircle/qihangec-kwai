package com.b2c.entity.erp;

import java.math.BigDecimal;
import java.util.List;

/**
 * 描述：
 * 商品Entity
 *
 * @author qlp
 * @date 2019-03-21 15:52
 */
public class ErpGoodsEntity {
    private Integer id;
    private String name;
    private String image;
    private String number;
    private Integer unitId;
    private String unitName;
    private Integer categoryId;
    private Float price;
    private BigDecimal costPrice;
    private BigDecimal wholesalePrice;
    private BigDecimal salePrice;//建议零售价
    private BigDecimal purPrice;
   private String categoryName;
    private String remark;
    private Integer status;
    private String length;
    private String height;
    private String width;
    private String width1;
    private String width2;
    private String width3;
    private Double weight;
    private String barCode;
    //    private Integer locationId;
    private Integer locationId;
//    private Integer reservoirId;
//    private Integer shelfId;
    private Integer disable;
    private String property;
    private Double safeDays;
    private Integer isDelete;
    private Long createTime;

    private Integer yzStatus;
    private Integer ygStatus;
    private List<GoodsSpecEntity> goodsSpecEntities;

    private String attr1;
    private String attr2;
    private String attr3;
    private String attr4;
    private String attr5;

    public String getWidth1() {
        return width1;
    }

    public void setWidth1(String width1) {
        this.width1 = width1;
    }

    public String getWidth2() {
        return width2;
    }

    public void setWidth2(String width2) {
        this.width2 = width2;
    }

    public String getWidth3() {
        return width3;
    }

    public void setWidth3(String width3) {
        this.width3 = width3;
    }

    public String getAttr1() {
        return attr1;
    }

    public void setAttr1(String attr1) {
        this.attr1 = attr1;
    }

    public String getAttr2() {
        return attr2;
    }

    public void setAttr2(String attr2) {
        this.attr2 = attr2;
    }

    public String getAttr3() {
        return attr3;
    }

    public void setAttr3(String attr3) {
        this.attr3 = attr3;
    }

    public String getAttr4() {
        return attr4;
    }

    public void setAttr4(String attr4) {
        this.attr4 = attr4;
    }

    public String getAttr5() {
        return attr5;
    }

    public void setAttr5(String attr5) {
        this.attr5 = attr5;
    }

    public BigDecimal getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(BigDecimal costPrice) {
        this.costPrice = costPrice;
    }

    public BigDecimal getWholesalePrice() {
        return wholesalePrice;
    }

    public void setWholesalePrice(BigDecimal wholesalePrice) {
        this.wholesalePrice = wholesalePrice;
    }

    public BigDecimal getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(BigDecimal salePrice) {
        this.salePrice = salePrice;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Integer getYzStatus() {
        return yzStatus;
    }

    public void setYzStatus(Integer yzStatus) {
        this.yzStatus = yzStatus;
    }

    public Integer getYgStatus() {
        return ygStatus;
    }

    public void setYgStatus(Integer ygStatus) {
        this.ygStatus = ygStatus;
    }

    public List<GoodsSpecEntity> getGoodsSpecEntities() {
        return goodsSpecEntities;
    }

    public void setGoodsSpecEntities(List<GoodsSpecEntity> goodsSpecEntities) {
        this.goodsSpecEntities = goodsSpecEntities;
    }


    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

//    public Integer getReservoirId() {
//        return reservoirId;
//    }
//
//    public void setReservoirId(Integer reservoirId) {
//        this.reservoirId = reservoirId;
//    }
//
//    public Integer getShelfId() {
//        return shelfId;
//    }
//
//    public void setShelfId(Integer shelfId) {
//        this.shelfId = shelfId;
//    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getUnitId() {
        return unitId;
    }

    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

   public String getCategoryName() {
       return categoryName;
   }

   public void setCategoryName(String categoryName) {
       this.categoryName = categoryName;
   }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

//    public Integer getLocationId() {
//        return locationId;
//    }
//
//    public void setLocationId(Integer locationId) {
//        this.locationId = locationId;
//    }
//
//    public String getLocationName() {
//        return locationName;
//    }
//
//    public void setLocationName(String locationName) {
//        this.locationName = locationName;
//    }

    public Integer getDisable() {
        return disable;
    }

    public void setDisable(Integer disable) {
        this.disable = disable;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public Double getSafeDays() {
        return safeDays;
    }

    public void setSafeDays(Double safeDays) {
        this.safeDays = safeDays;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public BigDecimal getPurPrice() {
        return purPrice;
    }

    public void setPurPrice(BigDecimal purPrice) {
        this.purPrice = purPrice;
    }
}
