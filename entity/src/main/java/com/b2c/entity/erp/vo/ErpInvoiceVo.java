package com.b2c.entity.erp.vo;

import com.b2c.entity.erp.InvoiceInfoEntity;

import java.util.List;

/**
 * @Description: pbd add 2019/10/9 17:14
 */
public class ErpInvoiceVo {
    private Long id;
    private Long checkoutTime;
    private Long createTime;
    private String billNo;
    private Long totalQuantity;
    private Long inQuantity;
    private String stockInName;

    private List<InvoiceInfoEntity> invoiceInfoList;

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCheckoutTime() {
        return checkoutTime;
    }

    public void setCheckoutTime(Long checkoutTime) {
        this.checkoutTime = checkoutTime;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public Long getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Long totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public Long getInQuantity() {
        return inQuantity;
    }

    public void setInQuantity(Long inQuantity) {
        this.inQuantity = inQuantity;
    }

    public String getStockInName() {
        return stockInName;
    }

    public void setStockInName(String stockInName) {
        this.stockInName = stockInName;
    }

    public List<InvoiceInfoEntity> getInvoiceInfoList() {
        return invoiceInfoList;
    }

    public void setInvoiceInfoList(List<InvoiceInfoEntity> invoiceInfoList) {
        this.invoiceInfoList = invoiceInfoList;
    }
}
