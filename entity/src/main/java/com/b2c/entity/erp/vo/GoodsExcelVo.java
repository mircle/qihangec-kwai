package com.b2c.entity.erp.vo;

/**
 * 描述：
 * 商品excel批量导入vo
 *
 * @author qlp
 * @date 2019-04-15 14:55
 */
public class GoodsExcelVo {
    private String number;//商品编号
    private String title;//商品标题
    private Integer categoryId;//分类id
    private Integer unitId;//计量单位id
    private String unit;//计量单位
    private String specNumber;//规格编码
    //    private String specName;//规格型号
    private String color;//颜色
    private String size;//尺码
    private Integer mixStock;//库存预警，最低数量
//    private Integer maxStock;//库存预警，最高数量
//    private Integer stock;//初始库存
//    private String storeHouseNo;//仓库编码
    private Integer locationId;//仓库id
//    private Integer reservoirId;
//    private String reservoirName;
//    private Integer shelfId;
//    private String shelfName;


    public Integer getUnitId() {
        return unitId;
    }

    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getSpecNumber() {
        return specNumber;
    }

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public Integer getMixStock() {
        return mixStock;
    }

    public void setMixStock(Integer mixStock) {
        this.mixStock = mixStock;
    }

//    public Integer getMaxStock() {
//        return maxStock;
//    }
//
//    public void setMaxStock(Integer maxStock) {
//        this.maxStock = maxStock;
//    }
//
//    public Integer getStock() {
//        return stock;
//    }
//
//    public void setStock(Integer stock) {
//        this.stock = stock;
//    }

}
