package com.b2c.entity.erp.enums;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-03-06 09:20
 */
public enum InvoiceBillStatusEnum {
    //PO采购订单 OI其他入库 PUR采购入库 BAL初期余额
    UNCOMMIT("未提交", -1),//待审核
    WaitAudit("待审核", 0),
    Audited("已审核", 1),//待入库、待出库
    Invalid("已作废", 2),
    StockIn("已入库", 3),
    Picked("已捡货", 21),
    LogisticsPrinted("快递面单已打印", 22),
    Out("已出库", 23),
    CANCEL("已取消", 99);

    private String name;
    private int index;

    // 构造方法
    private InvoiceBillStatusEnum(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (InvoiceBillStatusEnum c : InvoiceBillStatusEnum.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
