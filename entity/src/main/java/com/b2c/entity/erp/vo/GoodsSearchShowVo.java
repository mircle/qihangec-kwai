package com.b2c.entity.erp.vo;

/**
 * 描述：
 * 商品搜索展示VO
 *
 * @author qlp
 * @date 2019-03-21 16:09
 */
public class GoodsSearchShowVo {
    //    private Integer specId;//商品规格id
    private Long itemId;//订单itemId
    private Integer goodsId;//商品id
    private String name;//商品名称
//    private String specName;//规格名称
    private String colorValue;//颜色
    private String colorImage;//颜色图片
    private String styleValue;//款式
    private String sizeValue;//尺码
    private String goodsNumber;//商品编号
    private Integer specId;//商品规格id
    private String specNumber;//规格编号
    private String unit;//单位名称
    private Double price = 0.0;//单价价格
    private Double freight;//运费
    private Double costPrice=0d;//成本价
    private Double salePrice=0.0;//建议零售价
    private Double wholesalePrice;//建议批发价
    private Float disRate = 0f;//折扣率(%)
    private Double disAmount = 0.0;//折扣额
    private Double amount = 0.0;//购入金额
    private Long quantity = 0L;//数量
    private Long currentQty=0L;//库存

    
    
    public String getColorImage() {
        return colorImage;
    }

    public void setColorImage(String colorImage) {
        this.colorImage = colorImage;
    }

    public Double getWholesalePrice() {
        return wholesalePrice;
    }

    public void setWholesalePrice(Double wholesalePrice) {
        this.wholesalePrice = wholesalePrice;
    }

    public String getColorValue() {
        return colorValue;
    }

    public void setColorValue(String colorValue) {
        this.colorValue = colorValue;
    }

    public String getSizeValue() {
        return sizeValue;
    }

    public void setSizeValue(String sizeValue) {
        this.sizeValue = sizeValue;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(String goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public Integer getSpecId() {
        return specId;
    }

    public void setSpecId(Integer specId) {
        this.specId = specId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecNumber() {
        return specNumber;
    }

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Float getDisRate() {
        return disRate;
    }

    public void setDisRate(Float disRate) {
        this.disRate = disRate;
    }

    public Double getDisAmount() {
        return disAmount;
    }

    public void setDisAmount(Double disAmount) {
        this.disAmount = disAmount;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Double getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Double salePrice) {
        this.salePrice = salePrice;
    }

    public Long getCurrentQty() {
        return currentQty;
    }

    public void setCurrentQty(Long currentQty) {
        this.currentQty = currentQty;
    }

    public Double getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(Double costPrice) {
        this.costPrice = costPrice;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public Double getFreight() {
        return freight;
    }

    public void setFreight(Double freight) {
        this.freight = freight;
    }

    public String getStyleValue() {
        return styleValue;
    }

    public void setStyleValue(String styleValue) {
        this.styleValue = styleValue;
    }
}
