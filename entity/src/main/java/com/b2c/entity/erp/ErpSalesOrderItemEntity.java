package com.b2c.entity.erp;

import java.math.BigDecimal;

public class ErpSalesOrderItemEntity {

    private Long id;//` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
    private Long orderId;//` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '订单id',
    private BigDecimal itemAmount;//明细总额
    private Integer goodsId;//` INT(11) NOT NULL COMMENT 'erp系统商品id',
    private String goodsNumber;//` VARCHAR(25) NOT NULL COMMENT 'erp系统商品编码',
    private String goodsTitle;//` VARCHAR(45) NOT NULL DEFAULT '' COMMENT '商品名称',
    private String goodsImage;//` VARCHAR(145) NOT NULL DEFAULT '' COMMENT '商品图片（取规格图）',
    private String color;//` VARCHAR(45) NULL DEFAULT NULL COMMENT '颜色',
    private String size;//` VARCHAR(45) NULL DEFAULT NULL COMMENT '尺码',
    private Integer specId;//` INT(11) NOT NULL COMMENT '规格id',
    private String specNumber;//` VARCHAR(25) NOT NULL COMMENT '商品条形码',
    private Integer quantity;//` INT(11) NOT NULL COMMENT '商品数量',
    private BigDecimal price;//` DECIMAL(10,2) NOT NULL COMMENT '商品价格(采购的价格)',
    private BigDecimal discountPrice;//` DECIMAL(10,2) NOT NULL DEFAULT '0.00' COMMENT '优惠后的价格',
    private Integer refundStatus;//` INT(11) NULL DEFAULT '0' COMMENT '售后状态0未申请售后1售后申请中(退款待审核)2同意退货(退款待收货)3买家已发货，待收货(待收货)4已收货（待退款）5退款退货成功(退款完成)6退款拒绝7已确认收货，正在退款中 8退款取消',
    private Integer refundCount;//` INT(11) NOT NULL DEFAULT '0' COMMENT '已售后的数量',
    private String refundId;//` VARCHAR(25) NOT NULL COMMENT '退款单号',
    private String skuInfo;//商品规格
    private String modifySkuRemark;//修改规格说明
    private Long originOrderItemId;//原订单skuid
    private Long isGift;//是否礼品0否1是
    private Integer itemStatus;//状态（0未发货1已发货2已退货）


    public Integer getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(Integer itemStatus) {
        this.itemStatus = itemStatus;
    }

    public Long getIsGift() {
        return isGift;
    }

    public void setIsGift(Long isGift) {
        this.isGift = isGift;
    }

    public String getModifySkuRemark() {
        return modifySkuRemark;
    }

    public void setModifySkuRemark(String modifySkuRemark) {
        this.modifySkuRemark = modifySkuRemark;
    }

    public BigDecimal getItemAmount() {
        return itemAmount;
    }

    public void setItemAmount(BigDecimal itemAmount) {
        this.itemAmount = itemAmount;
    }

    public String getSkuInfo() {
        return skuInfo;
    }

    public void setSkuInfo(String skuInfo) {
        this.skuInfo = skuInfo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(String goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public String getGoodsTitle() {
        return goodsTitle;
    }

    public void setGoodsTitle(String goodsTitle) {
        this.goodsTitle = goodsTitle;
    }

    public String getGoodsImage() {
        return goodsImage;
    }

    public void setGoodsImage(String goodsImage) {
        this.goodsImage = goodsImage;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public Integer getSpecId() {
        return specId;
    }

    public void setSpecId(Integer specId) {
        this.specId = specId;
    }

    public String getSpecNumber() {
        return specNumber;
    }

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(BigDecimal discountPrice) {
        this.discountPrice = discountPrice;
    }

    public Integer getRefundStatus() {
        return refundStatus;
    }

    public void setRefundStatus(Integer refundStatus) {
        this.refundStatus = refundStatus;
    }

    public Integer getRefundCount() {
        return refundCount;
    }

    public void setRefundCount(Integer refundCount) {
        this.refundCount = refundCount;
    }

    public String getRefundId() {
        return refundId;
    }

    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }

    public Long getOriginOrderItemId() {
        return originOrderItemId;
    }

    public void setOriginOrderItemId(Long originOrderItemId) {
        this.originOrderItemId = originOrderItemId;
    }
}
