package com.b2c.entity.erp.enums;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-03-06 09:20
 */
public enum InvoiceHxStateCodeEnum {
    //核销状态 0未付款  1部分付款  2全部付款
    NoPay("未付款", 0),
    Paying("部分付款", 1),
    Payed("全部付款", 2);

    private String name;
    private int index;

    // 构造方法
    private InvoiceHxStateCodeEnum(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (InvoiceHxStateCodeEnum c : InvoiceHxStateCodeEnum.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
