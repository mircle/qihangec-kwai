package com.b2c.entity.erp;

/**
 * 描述：
 * 库存商品分类实体
 *
 * @author ly
 * @date 2019-3-21 11:57 AM
 */
public class GoodsCategoryEntity {
    private Integer id;
    private String number; //分类编码
    private String name;
    private String remark;
    private Integer parentId; //上架分类id
    private String path; //分类路径
    private Integer sort;
    private Integer createOn;

    
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getCreateOn() {
        return createOn;
    }

    public void setCreateOn(Integer createOn) {
        this.createOn = createOn;
    }

}
