package com.b2c.entity.erp.vo;

/**
 * @author ly
 * @desc 计量表
 * @date 2019/3/22
 */
public class UnitEntiy {
    private String name;
    private Integer id;
    private String comment;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
