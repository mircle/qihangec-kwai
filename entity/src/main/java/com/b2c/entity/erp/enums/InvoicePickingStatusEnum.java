package com.b2c.entity.erp.enums;

/**
 * 描述：订单拣货出库状态
 *
 * @author qlp
 * @date 2019-03-06 09:20
 */
public enum InvoicePickingStatusEnum {
    //拣货状态0待打印1已打印2已拣货
    Wait("待打印", 0),
    Print("已打印", 1),//已经打印拣货单
    Picked("已拣货", 2);

    private String name;
    private int index;

    // 构造方法
    private InvoicePickingStatusEnum(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (InvoicePickingStatusEnum c : InvoicePickingStatusEnum.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
