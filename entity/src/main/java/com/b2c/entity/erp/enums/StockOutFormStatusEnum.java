package com.b2c.entity.erp.enums;

/**
 * 描述：
 * 出库商品状态enum
 *
 * @author qlp
 * @date 2019-06-21 14:15
 */
public enum StockOutFormStatusEnum {
    //状态：0待拣货1拣货中2拣货完成3已出库
    WAIT("待拣货", 0),
    Packing("拣货中", 1),
    Picked("已拣货出库", 2),
    OUTED("已出库", 3),
    CANCEL("取消出库", 99);

    private String name;
    private int index;

    // 构造方法
    private StockOutFormStatusEnum(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (StockOutFormStatusEnum c : StockOutFormStatusEnum.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
