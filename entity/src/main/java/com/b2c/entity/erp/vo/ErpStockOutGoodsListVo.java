package com.b2c.entity.erp.vo;

import com.b2c.entity.ErpOrderItemEntity;

import java.util.List;

/**
 * 描述：
 * 出库商品清单list vo
 *
 * @author qlp
 * @date 2019-06-21 14:47
 */
public class ErpStockOutGoodsListVo extends ErpOrderItemEntity {
    private String orderNum;
    private String goodsName;
//    private String specName;
    private String colorImage;
    private String colorValue;
    private String sizeValue;
    private String styleValue;
//    private String locationName;
//    private Integer locationId;
//    private String reservoirName;
//    private Integer reservoirId;
//    private String shelfName;
//    private Integer shelfId;
    private Integer goodsId;
    private Integer specId;
    private Long orderItemId;
    private String sourceChannel;
    private String sourceOrderNo;
    private List<ErpStockOutGoodsStockInfo> stocks;

    public String getSourceChannel() {
        return sourceChannel;
    }

    public void setSourceChannel(String sourceChannel) {
        this.sourceChannel = sourceChannel;
    }

    public String getSourceOrderNo() {
        return sourceOrderNo;
    }

    public void setSourceOrderNo(String sourceOrderNo) {
        this.sourceOrderNo = sourceOrderNo;
    }

    public Long getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(Long orderItemId) {
        this.orderItemId = orderItemId;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getSpecId() {
        return specId;
    }

    public void setSpecId(Integer specId) {
        this.specId = specId;
    }

    public String getColorImage() {
        return colorImage;
    }

    public void setColorImage(String colorImage) {
        this.colorImage = colorImage;
    }

    public List<ErpStockOutGoodsStockInfo> getStocks() {
        return stocks;
    }

    public void setStocks(List<ErpStockOutGoodsStockInfo> stocks) {
        this.stocks = stocks;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public String getColorValue() {
        return colorValue;
    }

    public void setColorValue(String colorValue) {
        this.colorValue = colorValue;
    }

    public String getSizeValue() {
        return sizeValue;
    }

    public void setSizeValue(String sizeValue) {
        this.sizeValue = sizeValue;
    }

    public String getStyleValue() {
        return styleValue;
    }

    public void setStyleValue(String styleValue) {
        this.styleValue = styleValue;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

}
