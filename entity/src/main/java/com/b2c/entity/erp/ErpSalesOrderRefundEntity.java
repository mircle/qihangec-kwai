package com.b2c.entity.erp;

import java.math.BigDecimal;

/**
 * 采购订单售后管理
 */
public class ErpSalesOrderRefundEntity {
    private Long id;//` BIGINT(20) NOT NULL AUTO_INCREMENT,
    private String refundNum;//` VARCHAR(36) NOT NULL COMMENT '退款订单编号',
    private Long orderId;//` BIGINT(20) NOT NULL COMMENT '订单id',
    private String orderNum;//` VARCHAR(36) NOT NULL COMMENT '订单编号',
    private Integer buyerUserId;//` INT(11) NOT NULL COMMENT '用户id',
    private Long refundApplyTime;//` BIGINT(20) NOT NULL COMMENT '申请退货时间',
    private Long createOn;//` BIGINT(20) NOT NULL COMMENT '创建时间',
    private Integer status;//` INT(11) NOT NULL COMMENT '退款状态 -1取消申请；0拒绝退货；1申请中(待审核)；2等待买家发货；3买家已发货(待收货)；4已收货（完成） ',
    private String refundReason;//` VARCHAR(50) NULL DEFAULT NULL COMMENT '退货原因',
    private String refundReasonImage;//` VARCHAR(200) NULL DEFAULT NULL COMMENT '退货理由图片',
    private Long completeTime;//` BIGINT(20) NULL DEFAULT NULL COMMENT '退款完成时间',
    private String result;//` VARCHAR(1000) NULL DEFAULT NULL COMMENT '退款结果（微信通知）',
    private String buyerFeedback;//` VARCHAR(100) NULL DEFAULT NULL COMMENT '买家留言',
    private String sellerMemo;//` VARCHAR(150) NULL DEFAULT NULL COMMENT '卖家备注',
    private Integer type;//` INT(11) NOT NULL DEFAULT '0' COMMENT '退款类型(0:退货）这里只有退货',
    private Integer orderSaleType;//` INT(11) NOT NULL COMMENT '订单销售类型,0:样品;1:实售',
    private String logisticsCompany;//` VARCHAR(20) NULL DEFAULT NULL COMMENT '买家发货物流公司',
    private String logisticsCompanyCode;//` VARCHAR(15) NULL DEFAULT NULL COMMENT '发货物流公司代码',
    private String logisticsCode;//` VARCHAR(30) NULL DEFAULT NULL COMMENT '发货物流单号',
    private Integer totalRefund;//总退货数量
    private BigDecimal totalRefundAmount;// 退款总金额
    private String buyerName;//用户姓名
    private String buyerMobile;//用户手机号
    private String shopName;//店铺名

    public BigDecimal getTotalRefundAmount() {
        return totalRefundAmount;
    }

    public void setTotalRefundAmount(BigDecimal totalRefundAmount) {
        this.totalRefundAmount = totalRefundAmount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRefundNum() {
        return refundNum;
    }

    public void setRefundNum(String refundNum) {
        this.refundNum = refundNum;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public Integer getBuyerUserId() {
        return buyerUserId;
    }

    public void setBuyerUserId(Integer buyerUserId) {
        this.buyerUserId = buyerUserId;
    }

    public Long getRefundApplyTime() {
        return refundApplyTime;
    }

    public void setRefundApplyTime(Long refundApplyTime) {
        this.refundApplyTime = refundApplyTime;
    }

    public Long getCreateOn() {
        return createOn;
    }

    public void setCreateOn(Long createOn) {
        this.createOn = createOn;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRefundReason() {
        return refundReason;
    }

    public void setRefundReason(String refundReason) {
        this.refundReason = refundReason;
    }

    public String getRefundReasonImage() {
        return refundReasonImage;
    }

    public void setRefundReasonImage(String refundReasonImage) {
        this.refundReasonImage = refundReasonImage;
    }

    public Long getCompleteTime() {
        return completeTime;
    }

    public void setCompleteTime(Long completeTime) {
        this.completeTime = completeTime;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getBuyerFeedback() {
        return buyerFeedback;
    }

    public void setBuyerFeedback(String buyerFeedback) {
        this.buyerFeedback = buyerFeedback;
    }

    public String getSellerMemo() {
        return sellerMemo;
    }

    public void setSellerMemo(String sellerMemo) {
        this.sellerMemo = sellerMemo;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getOrderSaleType() {
        return orderSaleType;
    }

    public void setOrderSaleType(Integer orderSaleType) {
        this.orderSaleType = orderSaleType;
    }

    public String getLogisticsCompany() {
        return logisticsCompany;
    }

    public void setLogisticsCompany(String logisticsCompany) {
        this.logisticsCompany = logisticsCompany;
    }

    public String getLogisticsCompanyCode() {
        return logisticsCompanyCode;
    }

    public void setLogisticsCompanyCode(String logisticsCompanyCode) {
        this.logisticsCompanyCode = logisticsCompanyCode;
    }

    public String getLogisticsCode() {
        return logisticsCode;
    }

    public void setLogisticsCode(String logisticsCode) {
        this.logisticsCode = logisticsCode;
    }

    public Integer getTotalRefund() {
        return totalRefund;
    }

    public void setTotalRefund(Integer totalRefund) {
        this.totalRefund = totalRefund;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getBuyerMobile() {
        return buyerMobile;
    }

    public void setBuyerMobile(String buyerMobile) {
        this.buyerMobile = buyerMobile;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }
}
