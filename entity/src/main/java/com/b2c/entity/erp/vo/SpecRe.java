package com.b2c.entity.erp.vo;

import java.util.List;

/**
 * @author ly
 * @desc 规格返回数据
 * @date 2019/3/22
 */
public class SpecRe {
    private Integer id;
    private String number;
    private String name;
    private String categoryName;
    private Integer categoryId;
    private Integer unitId;
    private String unitName;
    private String specName;
    private String specNumber;
    private String lowQty;
    private String highQty;
    private List<SpecRe> detail;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getUnitId() {
        return unitId;
    }

    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getSpecName() {
        return specName;
    }

    public void setSpecName(String specName) {
        this.specName = specName;
    }

    public String getSpecNumber() {
        return specNumber;
    }

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    public String getLowQty() {
        return lowQty;
    }

    public void setLowQty(String lowQty) {
        this.lowQty = lowQty;
    }

    public String getHighQty() {
        return highQty;
    }

    public void setHighQty(String highQty) {
        this.highQty = highQty;
    }

    public List<SpecRe> getDetail() {
        return detail;
    }

    public void setDetail(List<SpecRe> detail) {
        this.detail = detail;
    }
}
