package com.b2c.entity.erp.vo;

import java.util.Date;

/**
 * 描述：
 * 商品规格Vo
 *
 * @author qlp
 * @date 2019-03-22 19:32
 */
public class ErpGoodsSpecStockListVo {
    private Integer id;
    private Integer goodsId;//商品id
    private String goodsNumber;
    private Integer goodsSpecId;//商品规格id
    private String specNumber;
    private String name;//商品名称
    private String number;//商品编码
    private String category;
    private Integer status;
    private Integer disable;
    private String locationName; //仓库名
    private Long locationId; //仓库id

    private Long currentQty;
    private Long lockedQty;//锁定库存
    private Long lockedQty1;//锁定库存
    private String colorValue;
    private String sizeValue;
    private String styleValue;//款式
    private String createTime;
    private Long createTimeL;


    public String getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(String goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getGoodsSpecId() {
        return goodsSpecId;
    }

    public void setGoodsSpecId(Integer goodsSpecId) {
        this.goodsSpecId = goodsSpecId;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    public Long getLockedQty() {
        return lockedQty;
    }

    public void setLockedQty(Long lockedQty) {
        this.lockedQty = lockedQty;
    }

    public Long getCreateTimeL() {
        return createTimeL;
    }

    public void setCreateTimeL(Long createTimeL) {
        this.createTimeL = createTimeL;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getDisable() {
        return disable;
    }

    public void setDisable(Integer disable) {
        this.disable = disable;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getColorValue() {
        return colorValue;
    }

    public void setColorValue(String colorValue) {
        this.colorValue = colorValue;
    }

    public String getSizeValue() {
        return sizeValue;
    }

    public void setSizeValue(String sizeValue) {
        this.sizeValue = sizeValue;
    }

    public String getStyleValue() {
        return styleValue;
    }

    public void setStyleValue(String styleValue) {
        this.styleValue = styleValue;
    }

    public String getSpecNumber() {
        return specNumber;
    }

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCurrentQty() {
        return currentQty;
    }

    public void setCurrentQty(Long currentQty) {
        this.currentQty = currentQty;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Long getLockedQty1() {
        return lockedQty1;
    }

    public void setLockedQty1(Long lockedQty1) {
        this.lockedQty1 = lockedQty1;
    }
}
