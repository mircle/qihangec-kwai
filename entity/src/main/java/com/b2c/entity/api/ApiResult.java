package com.b2c.entity.api;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-01-07 11:53 AM
 */
public class ApiResult<T> {
    private int code;
    private String msg;
    private T data;

    public ApiResult() {
    }

    public ApiResult(ApiResultEnum result, String msg) {
        this.code = result.getIndex();
        this.msg = msg;
    }

    public ApiResult(ApiResultEnum result) {
        this.code = result.getIndex();
        this.msg = result.getName();
    }

    public ApiResult(ApiResultEnum result, T data) {
        this.code = result.getIndex();
        this.msg = result.getName();
        this.data = data;
    }

    public ApiResult(ApiResultEnum result, String msg, T data) {
        this.code = result.getIndex();
        this.msg = msg;
        this.data = data;
    }

    public ApiResult(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public ApiResult(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
