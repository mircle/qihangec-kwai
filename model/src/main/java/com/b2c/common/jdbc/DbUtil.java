package com.b2c.common.jdbc;

import java.sql.*;

public class DbUtil {
    private static String URL = null;
    private static String USER = null;
    private static String PASSWORD = null;

    public static Connection getConnection() {
        return connection;
    }

    private static Connection connection = null;




    public DbUtil(String url,String user,String password) throws ClassNotFoundException, SQLException {
        URL = url;
        USER = user;
        PASSWORD = password;
        //1.加载驱动程序
        Class.forName("com.mysql.cj.jdbc.Driver");
        //2. 获得数据库连接
        connection = DriverManager.getConnection(URL, USER, PASSWORD);
    }




//    public static void main(String[] args) throws Exception {
//        //1.加载驱动程序
//        Class.forName("com.mysql.jdbc.Driver");
//        //2. 获得数据库连接
//        Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
//        //3.操作数据库，实现增删改查
//        Statement stmt = conn.createStatement();
//        ResultSet rs = stmt.executeQuery("SELECT user_name, age FROM imooc_goddess");
//        //如果有数据，rs.next()返回true
//        while(rs.next()){
//            System.out.println(rs.getString("user_name")+" 年龄："+rs.getInt("age"));
//        }
//    }
}
