package com.b2c.common.utils;

import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.regex.Pattern;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;

/**
 * @Description: pbd add 2019/1/8 14:06
 */
public class StringUtil {
    /**
     * 生成订单好
     *
     * @return
     */
    public static String getOrderIdByTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
        String newDate = sdf.format(new Date());
        String result = "";
        Random random = new Random();
        for (int i = 0; i < 3; i++) {
            result += random.nextInt(10);
        }
        return newDate + result;
    }

    public static void subString(String str){
        System.out.println(str.substring(0,str.indexOf("$",str.indexOf("$")+1)));
    }


    /**
     * 获取一定长度的随机字符串
     *
     * @param length 指定字符串长度
     * @return 一定长度的字符串
     */
    public static String randomString(int length) {
        String base = "abcdefghijklmnopqrstuvwxyz0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }
    /**
     * 获取本机ip
     *
     * @return
     */
    public static String getLoalhostIP() {
        String ip = "";
        try {
            ip = InetAddress.getLocalHost().getHostAddress().toString();
//            System.out.println("StringUtil.getLoalhostIP本机IP"+ip);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ip;
    }

    /**
     * 数据保密 （处理一些敏感数据，只显示数据两头中间部分用***代替）
     *
     * @param data    需要保密处理的数据
     * @param partNum 两头需要显示的长度
     * @return
     */
    public static String dataSecrecy1(String data, int partNum) {
        if (data == null || data.trim().equals("")) {
            return "";
        }
        String tempStr = data.substring(0, partNum) + "***" + data.substring(data.length() - partNum);
        return tempStr;
    }

    /**
     * 创建退款流水订单号
     *
     * @return
     */
    public static String createCancelOrderNum() {
        return "rtv" + System.nanoTime();
    }

    /**
     * 数据保密 （处理一些敏感数据，只显示数据前面部分后面用***代替）
     *
     * @param data    需要保密处理的数据
     * @param partNum 两头需要显示的长度
     * @return
     */
    public static String dataSecrecy2(String data, int partNum) {
        if (data == null || data.trim().equals("")) {
            return "";
        }
        String tempStr = data.substring(0, partNum) + "***";
        return tempStr;
    }

    /**
     * 手机号敏感处理
     *
     * @param mobile
     * @return
     */
    public static String getSecrecyMobile(String mobile) {
        if (mobile == null || mobile.trim().equals("")) {
            return "";
        }
        String tempStr = mobile.substring(0, 3) + "****" + mobile.substring(mobile.length() - 4, mobile.length());
        return tempStr;
    }

    /**
     * 检查用户是否登录,未登录 true
     *
     * @param userId
     * @return
     */
    public static boolean checkSession(Object userId) {
        if (userId != null && userId.toString() != "" && userId.toString() != "0") {
            return false;
        }
        return true;
    }

    /**
     * 截取字符串
     *
     * @param data
     * @param partNum
     * @return
     */
    public static String dataSub(String data, int partNum) {
        if (data == null || data.trim().equals("")) {
            return "";
        }
        if (data.length() > partNum) {
            return data.substring(0, partNum) + "....";
        }
        return data;
    }

    /**
     * 获取空字符对象值
     *
     * @param str
     * @return
     */
    public static String getNullStr(String str) {
        if (str == null || str.trim().equals("") || str.length() == 0) return null;
        return str;
    }

    // public static void main(String[] args) {
    //     //String strs = "a\nb\nc\nd";
    //     //System.out.println(strs.replace("\n", ","));

    // }

    public static String filtration(String str) {
        String regEx = "[`~!@#$%^&*()+=|{}:;\\\\[\\\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？']";
        str = Pattern.compile(regEx).matcher(str).replaceAll("").trim();
        System.out.println("str ======" + str + ".");

        return str;
    }
    public static String getCellValue(Cell cell) {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        String cellValue = "";
        if (cell == null) {
            return cellValue;
        }
        CellType cellType =cell.getCellType();

        // 判断数据的类型
        switch (cellType) {
            case NUMERIC: // 数字、日期
                cellValue = String.valueOf(cell.getNumericCellValue());
                break;
            case STRING: // 字符串
                cellValue = String.valueOf(cell.getStringCellValue());
                break;
            case BOOLEAN: // Boolean
                cellValue = String.valueOf(cell.getBooleanCellValue());
                break;
            case FORMULA: // 公式
                cellValue = String.valueOf(cell.getCellFormula());
                break;
            case BLANK: // 空值
                cellValue = cell.getStringCellValue();
                break;
            case ERROR: // 故障
                cellValue = "非法字符";
                break;
            default:
                cellValue = "";
                break;
        }
        return cellValue;
    }

    public static void main(String[] args) {
        subString("$Aq0QIRfxGIWXvQ58MnpXOuuoVHKTrfdU/MaX9fpJ2fU=$DjJrEivgrBuHL1Ej+gfpqlzVEDmUk+l1bHGralFQX3Is4RU9iMKo/p3gNBJadM1ximor1e8APJTI3R3ebvaqmz8Temue5m5HNBrUYNEVFO1W*CgkIARCtHCABKAESPgo8j+EqYSxeNCIqN1+Rka05ggP6m2J/pV6wYXrkx9qo4Fi7ug1iZbKaDYRk/8YPQoSLcP5XjGUvYturMCyIGgA=$1$$");
    }

}
