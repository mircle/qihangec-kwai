package com.b2c.repository;

import com.b2c.entity.pdd.ShopDataReportModel;
import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.datacenter.DcShopEntity;
import com.b2c.entity.datacenter.DcSysThirdSettingEntity;
import com.b2c.entity.vo.ShopOrderStatisticsModel;
import com.b2c.entity.vo.ShopOrderStatisticsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ShopRepository {
    @Autowired
    JdbcTemplate jdbcTemplate;

    protected int getTotalSize() {
        return jdbcTemplate.queryForObject("SELECT FOUND_ROWS() as row_num;", int.class);
    }
    /**
     * 第三方平台列表
     * @return
     */
    public List<DcSysThirdSettingEntity> getDcList() {
        return jdbcTemplate.query("select id,name,app_key,app_secret,modify_on,remark from " + Tables.DcSysThirdSetting + " ", new BeanPropertyRowMapper<>(DcSysThirdSettingEntity.class));
    }

    /**
     * 新增店铺
     * @param entity
     * @return
     */
    public Integer addDcShop(DcShopEntity entity){
       return jdbcTemplate.update("INSERT INTO  "+Tables.DcShop+" set name=?,type=?,modify_on=?,remark=? ",entity.getName(),entity.getType(),entity.getModifyOn(),entity.getRemark());
    }

    /**
     * 删除
     * @param entity
     */
    public void delDcShop(DcShopEntity entity){
         jdbcTemplate.update("delete from  "+Tables.DcShop+" where id=? ",entity.getId());
    }

    /**
     * 查询店铺列表
     * @return
     */
    public PagingResponse<DcShopEntity> getDcShopList(Integer pageIndex,Integer pageSize ,Integer type){
        StringBuffer sb = new StringBuffer();

        sb.append("SELECT * FROM  "+Tables.DcShop);


        List<Object> params = new ArrayList<>();
        if (type>0) {
            sb.append(" where type = ? ");
            params.add(type);
        }
        sb.append(" ORDER BY id ASC LIMIT ?,?");
        params.add((pageIndex - 1) * pageSize);
        params.add(pageSize);

        List<DcShopEntity> lists = jdbcTemplate.query(sb.toString(), new BeanPropertyRowMapper<>(DcShopEntity.class), params.toArray(new Object[params.size()]));
        return new PagingResponse<>(pageIndex, pageSize, getTotalSize(), lists);

    }


    public List<DcShopEntity> getShopList(Integer type) {
        List<Object> params = new ArrayList<>();
        StringBuffer sb = new StringBuffer();
        sb.append("SELECT s.*,t.name as typeName ");
        sb.append(" FROM  " + Tables.DcShop + " as s ");
        sb.append(" LEFT JOIN " + Tables.DcSysThirdSetting + " AS t on t.id=s.type ");
        sb.append(" WHERE isDelete=0 ");
        if(!StringUtils.isEmpty(type)){
            sb.append(" and type=? ");
            params.add(type);
        }
        var list = jdbcTemplate.query(sb.toString(), new BeanPropertyRowMapper<>(DcShopEntity.class),params.toArray(new Object[params.size()]));
        return list;
    }
    public List<DcShopEntity> shopListShow() {
        StringBuffer sb = new StringBuffer();
        sb.append("SELECT s.*,t.name as typeName ");
        sb.append(" FROM  " + Tables.DcShop + " as s ");
        sb.append(" LEFT JOIN " + Tables.DcSysThirdSetting + " AS t on t.id=s.type ");
        sb.append(" WHERE isDelete=0 and isShow=1 ORDER BY s.orderNum desc");
        var list = jdbcTemplate.query(sb.toString(), new BeanPropertyRowMapper<>(DcShopEntity.class));
        return list;
    }

    /**
     * 获取用户拥有权限的店铺list
     * @param userId
     * @return
     */
    public List<DcShopEntity> getShopListByUserId(Integer userId,Integer type) {
        StringBuffer sb = new StringBuffer();
        sb.append("SELECT s.*,t.name as typeName ");
        sb.append(" FROM  " + Tables.DcShop + " as s ");
        sb.append(" LEFT JOIN " + Tables.DcSysThirdSetting + " AS t on t.id=s.type ");
        sb.append(" WHERE isDelete=0 ");

        List<Object> params = new ArrayList<>();

        if(userId!=null && userId > 0){
            sb.append(" AND s.id in (SELECT shop_id FROM dc_manage_user_shop WHERE user_id = ? ) ");
            params.add(userId);
        }
        
        if(type!=null) {
            sb.append(" AND s.type= ? ");
            params.add(type);
        }
        sb.append(" ORDER BY s.orderNum desc ");

        var list = jdbcTemplate.query(sb.toString(), new BeanPropertyRowMapper<>(DcShopEntity.class),params.toArray(new Object[params.size()]));

        return list;
    }


    public DcShopEntity getShop(Integer id) {
        StringBuffer sb = new StringBuffer();
        sb.append("SELECT s.*,sys.name as type_name FROM  "+Tables.DcShop+" s LEFT JOIN "+Tables.DcSysThirdSetting+" sys on sys.id=s.type WHERE s.id=?");
        try {
            return jdbcTemplate.queryForObject(sb.toString(), new BeanPropertyRowMapper<>(DcShopEntity.class), id);
        }catch (Exception e) {
            return null;
        }
    }

    public DcShopEntity getShopByMallId(Long mallId) {
        StringBuffer sb = new StringBuffer();
        sb.append("SELECT s.*,sys.name as type_name FROM  "+Tables.DcShop+" s LEFT JOIN "+Tables.DcSysThirdSetting+" sys on sys.id=s.type WHERE s.sellerUserId=?");
        try {
            return jdbcTemplate.queryForObject(sb.toString(), new BeanPropertyRowMapper<>(DcShopEntity.class), mallId);
        }catch (Exception e) {
            return null;
        }
    }


    public void updateSessionKey(Integer shopId,Long mallId, String sessionKey){
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE "+Tables.DcShop);
        sb.append(" SET ");
        sb.append(" sessionKey=?,modify_on=?");
        sb.append(" WHERE id= ? and sellerUserId = ?");
        jdbcTemplate.update(sb.toString(),sessionKey,System.currentTimeMillis()/1000,shopId,mallId);
    }

    /**
     * 店铺订单统计
     * @param shopId
     * @return
     */
    public ShopOrderStatisticsVo shopOrderStatistics(Integer shopId){
        var shop = jdbcTemplate.queryForObject("SELECT * FROM dc_shop where id=?", new BeanPropertyRowMapper<>(DcShopEntity.class),shopId);
        if(shop.getType().intValue() == 5 ){
            //拼多多
            Integer todayOrder = jdbcTemplate.queryForObject("select COUNT(0) from dc_pdd_orders WHERE date_format(confirm_time,'%Y-%m-%d') = date_format(now(),'%Y-%m-%d') and shopId=?",Integer.class,shopId);
            Integer todayValidOrder = jdbcTemplate.queryForObject("select COUNT(0) from dc_pdd_orders WHERE date_format(confirm_time,'%Y-%m-%d') = date_format(now(),'%Y-%m-%d') and refund_status =1 and shopId=?",Integer.class,shopId);
            BigDecimal todayValidOrderAmout = jdbcTemplate.queryForObject("select SUM(pay_amount) from dc_pdd_orders WHERE date_format(confirm_time,'%Y-%m-%d') = date_format(now(),'%Y-%m-%d') and refund_status =1 and shopId=?",BigDecimal.class,shopId);
            Integer waitSendOrder = jdbcTemplate.queryForObject("select COUNT(0) from dc_pdd_orders WHERE  order_status = 1 AND refund_status=1 and shopId=?",Integer.class,shopId);
            Integer waitAuditReturn = jdbcTemplate.queryForObject("select COUNT(0) from dc_pdd_refund WHERE auditStatus = 0 AND shopId = ? ",Integer.class,shopId);


            ShopOrderStatisticsVo result = new ShopOrderStatisticsVo(todayOrder,todayValidOrder,todayValidOrderAmout,waitSendOrder,waitAuditReturn);
            return result;
        }        
        else if(shop.getType().intValue() == 4 ){
                //淘系
                // Integer orderTotal = jdbcTemplate.queryForObject("SELECT COUNT(0) FROM dc_tmall_order WHERE shopId=?",Integer.class,shopId);
                Integer todayOrder = jdbcTemplate.queryForObject("select COUNT(0) from dc_tmall_order WHERE date_format(createTime,'%Y-%m-%d') = date_format(now(),'%Y-%m-%d') and shopId=?",Integer.class,shopId);
                Integer todayValidOrder = jdbcTemplate.queryForObject("select COUNT(0) from dc_tmall_order WHERE date_format(createTime,'%Y-%m-%d') = date_format(now(),'%Y-%m-%d') and shopId=?",Integer.class,shopId);
                BigDecimal todayValidOrderAmout = jdbcTemplate.queryForObject("select IFNULL(SUM(payAmount),0) from dc_tmall_order WHERE date_format(createTime,'%Y-%m-%d') = date_format(now(),'%Y-%m-%d') and shopId=?",BigDecimal.class,shopId);
                Integer waitSendOrder = jdbcTemplate.queryForObject("select COUNT(0) from dc_tmall_order WHERE  `status`= 2 AND (refundStatus IS NULL) and shopId=?",Integer.class,shopId);
                // Integer refundTotal = jdbcTemplate.queryForObject("select COUNT(0) from dc_tmall_order_refund WHERE shopId=?",Integer.class,shopId);
                Integer waitAuditReturn = jdbcTemplate.queryForObject("select COUNT(0) from dc_tmall_order_refund WHERE has_good_return = 1 AND auditStatus = 0 AND `status` = 'WAIT_SELLER_CONFIRM_GOODS' AND shopId = ? ",Integer.class,shopId);

                ShopOrderStatisticsVo result = new ShopOrderStatisticsVo(todayOrder,todayValidOrder,todayValidOrderAmout,waitSendOrder,waitAuditReturn);
                return result;
            }
            else if(shop.getType().intValue() == 6){
            //抖音(8东方概念20红蜘蛛)
            
            Integer todayOrder = jdbcTemplate.queryForObject("select COUNT(0) from dc_douyin_orders WHERE DATE_FORMAT(create_time,'%Y-%m-%d') = date_format(now(),'%Y-%m-%d') and shop_id=?",Integer.class,shopId);
            Integer todayValidOrder = jdbcTemplate.queryForObject("select COUNT(0) from dc_douyin_orders WHERE DATE_FORMAT(create_time,'%Y-%m-%d') = date_format(now(),'%Y-%m-%d') and (order_status=2 or order_status = 3 ) and shop_id=?",Integer.class,shopId);
            BigDecimal todayValidOrderAmout = jdbcTemplate.queryForObject("select IFNULL(SUM(order_total_amount),0) from dc_douyin_orders WHERE DATE_FORMAT(create_time,'%Y-%m-%d') = date_format(now(),'%Y-%m-%d') and (order_status=2 or order_status = 3 ) and shop_id=?",BigDecimal.class,shopId);
            
            Integer waitSendOrder = jdbcTemplate.queryForObject("select COUNT(0) from dc_douyin_orders WHERE send_status=0 AND order_status = 2 AND shop_id=?",Integer.class,shopId);

            Integer waitAuditReturn= jdbcTemplate.queryForObject("select COUNT(0) from dc_douyin_orders_refund WHERE shop_id=? AND aftersale_type=0 AND refund_status=7 ",Integer.class,shopId);
            // Integer refundTotal = jdbcTemplate.queryForObject("select COUNT(0) from dc_douyin_orders_refund WHERE  shop_id = ? ",Integer.class,shopId);
            ShopOrderStatisticsVo result = new ShopOrderStatisticsVo(todayOrder,todayValidOrder,todayValidOrderAmout,waitSendOrder,waitAuditReturn);
            return result;
        }

        // if(shopId==null || shopId.intValue() == 0) return null;
        // else if(shopId == 99){
        //     //ERP
        //     Integer orderTotal = jdbcTemplate.queryForObject("SELECT COUNT(0) FROM erp_sales_order where shopId=?",Integer.class,shopId);
        //     Integer todayOrder = jdbcTemplate.queryForObject("select COUNT(0) from erp_sales_order WHERE date_format(createOn,'%Y-%m-%d') = date_format(now(),'%Y-%m-%d') and shopId=?",Integer.class,shopId);
        //     Integer waitAuditOrder = jdbcTemplate.queryForObject("select COUNT(0) from erp_sales_order WHERE auditStatus=0 AND deliveredStatus=0 AND `status`=2 AND shopId=?",Integer.class,shopId);
        //     Integer refundTotal = jdbcTemplate.queryForObject("select COUNT(0) from erp_sales_order_refund",Integer.class);
        //     Integer waitAuditReturn = jdbcTemplate.queryForObject("select COUNT(0) from erp_sales_order_refund WHERE `status` = 1 OR `status` = 2 OR `status` = 3 ",Integer.class);
        //     ShopOrderStatisticsVo result = new ShopOrderStatisticsVo(orderTotal,waitAuditOrder,todayOrder,refundTotal,waitAuditReturn);
        //     return result;
        // }
        // else if(shopId == 1){
        //     //阿里
        //     Integer orderTotal = jdbcTemplate.queryForObject("SELECT COUNT(0) FROM dc_ali_order where shopId=?",Integer.class,shopId);
        //     Integer todayOrder = jdbcTemplate.queryForObject("select COUNT(0) from dc_ali_order WHERE date_format(createTime,'%Y-%m-%d') = date_format(now(),'%Y-%m-%d') and shopId=?",Integer.class,shopId);
        //     Integer waitAuditOrder = jdbcTemplate.queryForObject("select COUNT(0) from dc_ali_order WHERE auditStatus=0 AND send_status=0 AND `status`='waitsellersend' AND (refundStatus IS NULL OR refundStatus = 'refundclose') AND shopId=?",Integer.class,shopId);
        //     Integer refundTotal = jdbcTemplate.queryForObject("select COUNT(0) from dc_ali_order_refund",Integer.class);
        //     Integer waitAuditReturn = jdbcTemplate.queryForObject("select COUNT(0) from dc_ali_order_refund WHERE isOnlyRefund = 0 AND auditStatus = 0 AND `status` = 6",Integer.class);
        //     ShopOrderStatisticsVo result = new ShopOrderStatisticsVo(orderTotal,waitAuditOrder,todayOrder,refundTotal,waitAuditReturn);
        //     return result;
        // }
        // else if(shopId == 2 || shopId == 6 || shopId == 7){
        //     //淘系
        //     Integer orderTotal = jdbcTemplate.queryForObject("SELECT COUNT(0) FROM dc_tmall_order WHERE shopId=?",Integer.class,shopId);
        //     Integer todayOrder = jdbcTemplate.queryForObject("select COUNT(0) from dc_tmall_order WHERE date_format(createTime,'%Y-%m-%d') = date_format(now(),'%Y-%m-%d') and shopId=?",Integer.class,shopId);
        //     Integer waitAuditOrder = jdbcTemplate.queryForObject("select COUNT(0) from dc_tmall_order WHERE auditStatus=0 AND send_status=0 AND `status`= 2 AND (refundStatus IS NULL) and shopId=?",Integer.class,shopId);
        //     Integer refundTotal = jdbcTemplate.queryForObject("select COUNT(0) from dc_tmall_order_refund WHERE shopId=?",Integer.class,shopId);
        //     Integer waitAuditReturn = jdbcTemplate.queryForObject("select COUNT(0) from dc_tmall_order_refund WHERE has_good_return = 1 AND auditStatus = 0 AND `status` = 'WAIT_SELLER_CONFIRM_GOODS' AND shopId = ? ",Integer.class,shopId);
        //     ShopOrderStatisticsVo result = new ShopOrderStatisticsVo(orderTotal,waitAuditOrder,todayOrder,refundTotal,waitAuditReturn);
        //     return result;
        // }
        
        // else if(shopId == 8 || shopId == 20){
        //     //抖音(8东方概念20红蜘蛛)
        //     if (shopId == 8) shopId = 2148336;
        //     else if(shopId == 20 ) shopId = 42781469;
        //     Integer orderTotal = jdbcTemplate.queryForObject("SELECT COUNT(0) FROM dc_douyin_orders WHERE shop_id=?",Integer.class,shopId);
        //     Integer todayOrder = jdbcTemplate.queryForObject("select COUNT(0) from dc_douyin_orders WHERE FROM_UNIXTIME(create_time,'%Y-%m-%d') = date_format(now(),'%Y-%m-%d') and shop_id=?",Integer.class,shopId);
        //     Integer waitAuditOrder = jdbcTemplate.queryForObject("select COUNT(0) from dc_douyin_orders WHERE auditStatus=0 AND send_status=0 AND order_status = 2 AND shop_id=?",Integer.class,shopId);
        //     Integer waitAuditReturn= jdbcTemplate.queryForObject("select COUNT(0) from dc_douyin_orders_refund WHERE shop_id=? AND aftersale_type=0 AND refund_status=7 ",Integer.class,shopId);
        //     Integer refundTotal = jdbcTemplate.queryForObject("select COUNT(0) from dc_douyin_orders_refund WHERE  shop_id = ? ",Integer.class,shopId);
        //     ShopOrderStatisticsVo result = new ShopOrderStatisticsVo(orderTotal,waitAuditOrder,todayOrder,refundTotal,waitAuditReturn);
        //     return result;
        // }
        // else if(shopId == 21){
        //     //小红书店铺
        //     Integer orderTotal = jdbcTemplate.queryForObject("SELECT COUNT(0) FROM dc_xhs_order WHERE shopId=? AND orderStatus = 6 AND afterSalesStatus=1",Integer.class,shopId);
        //     Integer completeOrderCount = jdbcTemplate.queryForObject("SELECT COUNT(0) FROM dc_xhs_order WHERE shopId=? AND orderStatus = 7 AND afterSalesStatus=1 AND settleStatus=1 ",Integer.class,shopId);
        //     Long orderAmount = jdbcTemplate.queryForObject("SELECT SUM(totalPayAmount) FROM dc_xhs_order WHERE shopId=? AND orderStatus = 6 AND afterSalesStatus=1",Long.class,shopId);
            
        //     Long completeOrderAmount = jdbcTemplate.queryForObject("SELECT SUM(totalPayAmount) FROM dc_xhs_order WHERE shopId=? AND orderStatus = 7 AND afterSalesStatus=1 AND settleStatus=1",Long.class,shopId);

        //     Double settleOrderAmount = jdbcTemplate.queryForObject("SELECT SUM(settleAmount) FROM dc_xhs_order WHERE shopId=? AND orderStatus = 7 AND afterSalesStatus=1 AND settleStatus=1",Double.class,shopId);
            
        //     Integer todayOrder = jdbcTemplate.queryForObject("select COUNT(0) from dc_xhs_order WHERE FROM_UNIXTIME(createdTime/1000,'%Y-%m-%d') = date_format(now(),'%Y-%m-%d') and shopId=? and (orderStatus<>8 and orderStatus<>9 and orderStatus<>1 )",Integer.class,shopId);
        //     Integer waitAuditOrder = jdbcTemplate.queryForObject("select COUNT(0) from dc_xhs_order WHERE orderStatus = 4 AND afterSalesStatus=1 AND cancelStatus=0 AND shopId=?",Integer.class,shopId);
        //     Integer waitAuditReturn= 0;//jdbcTemplate.queryForObject("select COUNT(0) from dc_douyin_orders_refund WHERE shop_id=? AND aftersale_type=0 AND refund_status=7 ",Integer.class,shopId);
        //     Integer refundTotal = 0;//jdbcTemplate.queryForObject("select COUNT(0) from dc_douyin_orders_refund WHERE  shop_id = ? ",Integer.class,shopId);
        //     ShopOrderStatisticsVo result = new ShopOrderStatisticsVo(orderTotal,waitAuditOrder,todayOrder,refundTotal,waitAuditReturn);
        //     result.setOrderAmount(new BigDecimal(orderAmount).divide(new BigDecimal(100)));
        //     result.setCompleteOrderAmount(new BigDecimal(completeOrderAmount).divide(new BigDecimal(100)));
        //     result.setSettleOrderAmount(new BigDecimal(settleOrderAmount));
        //     result.setCompleteOrderCount(completeOrderCount);
        //     return result;
        // }
        return null;
    }


    
    /**
     * 店铺订单统计
     * @param shopId
     * @return
     */
    public ShopOrderStatisticsModel shopOrderStatisticsPDD(Integer shopId){
        List<Object> params = new ArrayList<>();

        StringBuilder waitSendSQL = new StringBuilder("SELECT COUNT(0) FROM dc_pdd_orders WHERE order_status=1 AND refund_status=1 ");
        StringBuilder todayOrderSQL = new StringBuilder("select COUNT(0) from dc_pdd_orders WHERE date_format(confirm_time,'%Y-%m-%d') = date_format(now(),'%Y-%m-%d') ");
        StringBuilder todayValidOrderSQL = new StringBuilder("select COUNT(0) from dc_pdd_orders WHERE date_format(confirm_time,'%Y-%m-%d') = date_format(now(),'%Y-%m-%d') AND (order_status=1 OR order_status=2 OR order_status=3) AND refund_status=1");
        StringBuilder todaySendSQL = new StringBuilder("SELECT IFNULL(SUM(quantity),0) FROM dc_pdd_orders_item oi left join dc_pdd_orders o ON o.id=oi.order_id WHERE o.order_status=2 and date_format(o.shipping_time,'%Y-%m-%d') = date_format(now(),'%Y-%m-%d')");
        StringBuilder waitRecevingSQL = new StringBuilder("SELECT COUNT(0) FROM dc_pdd_orders WHERE order_status=2 AND refund_status=1 ");
        StringBuilder waitRefundSQL = new StringBuilder("SELECT COUNT(0) FROM dc_pdd_refund WHERE auditStatus=0 AND shipping_status=1 ");
        StringBuilder todayValidOrderAmountSQL = new StringBuilder("select IFNULL(SUM(pay_amount),0) from dc_pdd_orders WHERE date_format(confirm_time,'%Y-%m-%d') = date_format(now(),'%Y-%m-%d') AND (order_status=1 OR order_status=2 OR order_status=3) AND refund_status=1");

        if(shopId !=null && shopId.intValue() > 0){
            waitSendSQL.append(" and shopId=? ");
            todayOrderSQL.append(" and shopId=? ");
            todayValidOrderSQL.append(" and shopId=? ");
            todaySendSQL.append(" and shopId=? ");
            waitRecevingSQL.append(" and shopId=? ");
            waitRefundSQL.append(" and shopId=? ");
            todayValidOrderAmountSQL.append(" and shopId=? ");
            params.add(shopId);
        }

 
        //拼多多
        Integer waitSend = jdbcTemplate.queryForObject(waitSendSQL.toString(),Integer.class,params.toArray(new Object[params.size()]));

        Integer todayOrder = jdbcTemplate.queryForObject(todayOrderSQL.toString(),Integer.class,params.toArray(new Object[params.size()]));

        Integer todayValidOrder = jdbcTemplate.queryForObject(todayValidOrderSQL.toString(),Integer.class,params.toArray(new Object[params.size()]));
        
        BigDecimal todayValidOrderAmount = jdbcTemplate.queryForObject(todayValidOrderAmountSQL.toString(), BigDecimal.class,params.toArray(new Object[params.size()]));

        Integer todaySend = jdbcTemplate.queryForObject(todaySendSQL.toString(),Integer.class,params.toArray(new Object[params.size()]));
        
        //待收货
        Integer waitReceving = jdbcTemplate.queryForObject(waitRecevingSQL.toString(),Integer.class,params.toArray(new Object[params.size()]));

        //已收货
        Integer hasReceving = 0;//jdbcTemplate.queryForObject("SELECT COUNT(0) FROM dc_pdd_orders WHERE order_status=3",Integer.class);

        //退款中
        Integer waitRefund = jdbcTemplate.queryForObject(waitRefundSQL.toString(),Integer.class,params.toArray(new Object[params.size()]));

        //退款成功
        Integer hasRefund = 0;//jdbcTemplate.queryForObject("SELECT COUNT(0) FROM dc_pdd_orders WHERE refund_status=4",Integer.class);
        
        ShopOrderStatisticsModel result = new ShopOrderStatisticsModel(todayOrder,todayValidOrder,  todaySend,  waitSend,  waitReceving,
         hasReceving,  waitRefund,  hasRefund);
         result.setTodayValidOrderAmount(todayValidOrderAmount);
        return result;
      
    }

    @Transactional
    public ShopDataReportModel getShopDateReport(String date) {
        ShopDataReportModel model = new ShopDataReportModel();
        model.setDate(date);
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT '"+date+"' AS `date`,a.*,b.* FROM ");
        sql.append("(");
        sql.append("SELECT SUM(visits) AS fks,COUNT(goodsId) AS fwsps,SUM(collects) AS scs FROM dc_shop_traffic_goods WHERE `date`=?");
        sql.append(")AS a,\n");
        sql.append("(SELECT COUNT(id) AS dds FROM dc_pdd_orders WHERE date_format(pay_time,'%Y-%m-%d') = ? )AS b");
        var m =jdbcTemplate.queryForObject(sql.toString(),new BeanPropertyRowMapper<>(ShopDataReportModel.class),date,date);
        return m;
    }


}
