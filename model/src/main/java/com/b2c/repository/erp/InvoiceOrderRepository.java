package com.b2c.repository.erp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * 描述：
 * 线上订单表单Repository
 *
 * @author qlp
 * @date 2019-06-03 15:00
 */
@Repository
public class InvoiceOrderRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * 查询需要打印的订单（已拣货的）
     *
     * @param pageIndex
     * @param pageSize
     * @param orderNum
     * @param mobile    收件人手机号
     * @param startTime
     * @param endTime
     * @return
     */
//    @Transactional
//    public PagingResponse<InvoiceOrderVo> getWaitPrintOrderList(Integer pageIndex, Integer pageSize, String orderNum, String mobile, Integer startTime, Integer endTime) {
//        StringBuilder sb = new StringBuilder();
//        sb.append("SELECT SQL_CALC_FOUND_ROWS i.id,i.billDate,i.billStatus,i.logisticsCompany,i.logisticsNumber,i.srcOrderNo as orderNo,i.srcOrderId as orderId,i.contactId,i.createTime,");
//        sb.append("c.`contact` as linkMobile,c.name as linkName,c.`address`");
//        sb.append(" FROM ").append(Tables.ErpInvoice).append(" as i ");
//        sb.append(" LEFT JOIN ").append(Tables.ErpContact).append(" as c on c.id=i.`contactId`");
//        sb.append(" WHERE  i.isDelete=0 ");
//
//        List<Object> params = new ArrayList<>();
//
//        sb.append(" AND i.transType=? ");
//        params.add(InvoiceTransTypeEnum.Order.getIndex());
//
//        sb.append(" AND i.billStatus=? ");
//        params.add(InvoiceBillStatusEnum.Picked.getIndex());
//
//        if (!StringUtils.isEmpty(orderNum)) {
//            sb.append(" AND i.srcOrderNo = ? ");
//            params.add(orderNum);
//        }
//        if (!StringUtils.isEmpty(mobile)) {
//            sb.append(" AND c.`contact` = ? ");
//            params.add(mobile);
//        }
//
//        if (startTime > 0) {
//            sb.append("AND i.createTime>=? ");
//            params.add(startTime);
//        }
//        if (endTime > 0) {
//            sb.append("AND i.createTime<? ");
//            params.add(endTime);
//        }
//
//        sb.append(" ORDER BY i.id DESC LIMIT ?,?");
//        params.add((pageIndex - 1) * pageSize);
//        params.add(pageSize);
//
//        var list = jdbcTemplate.query(sb.toString(), new BeanPropertyRowMapper<>(InvoiceOrderVo.class), params.toArray(new Object[params.size()]));
//        if (list != null && list.size() > 0) {
//            //循环查询invoice_info
//
//            StringBuilder sql = new StringBuilder();
//
//            sql.append("SELECT ii.id,ii.iid,ii.billNo,ii.billDate,ii.srcOrderNo,ii.quantity,ii.inQuantity,ii.status,ii.goodsId,ii.specId,ii.specNumber,g.name as goodsName,g.number as goodsNumber,gs.specName,g.locationId,sh.name as locationName FROM ");
//            sql.append(Tables.ErpInvoiceInfo).append(" as ii ");
//            sql.append(" LEFT JOIN ").append(Tables.ErpGoodsSpec).append(" as gs on gs.id=ii.specId ");
//            sql.append(" LEFT JOIN ").append(Tables.ErpGoods).append(" as g on g.id=ii.goodsId ");
//            sql.append(" LEFT JOIN ").append(Tables.ErpStoreHouse).append(" as sh on sh.id=g.locationId");
//
//            sql.append(" WHERE ii.id =?");
//
//            for (InvoiceOrderVo io : list) {
//                io.setItems(jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper<>(InvoiceOrderGoodsPickListVo.class), io.getId()));
//            }
//
//        }
//        return new PagingResponse<>(pageIndex, pageSize, getTotalSize(), list);
//
//    }

//    /***
//     * 更新已拣货订单状态
//     * @return
//     */
//    public Integer updatePickedStatus() {
//
//        String sql = "SELECT * FROM " + Tables.ErpInvoice + " WHERE billStatus=?";
//        //获取待拣货的订单list
//        var list = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(InvoiceEntity.class), InvoiceBillStatusEnum.Normal.getIndex());
//
//        int i = 0;//更新的数据
//        if (list != null && list.size() > 0) {
//
//
//            String updateSQL = "UPDATE " + Tables.ErpInvoice + " SET billStatus=? WHERE id=? ";
//
//            for (InvoiceEntity io : list) {
//                //查询invoice_info (查询有没有未完成拣货的，状态为Wait或Picking)
//                String infoSQL = "SELECT COUNT(0) FROM " + Tables.ErpInvoiceInfo + " WHERE iid=? AND (status=? OR status=?)";
//                int count = jdbcTemplate.queryForObject(infoSQL, Integer.class, io.getId(), InvoiceInfoStatusEnum.Wait.getIndex(), InvoiceInfoStatusEnum.Picking.getIndex());
//                if (count == 0) {
//                    //都完成了拣货，更新invoice状态为已拣货
//                    i += jdbcTemplate.update(updateSQL, InvoiceBillStatusEnum.Picked.getIndex(), io.getId());
//
//                }
//            }
//        }
//
//        return i;
//    }

    /**
     * 获取要导出打印的订单列表
     *
     * @param orderId orders表主键 list
     * @return
     */
//    public List<OrderSendListExportVo> getOrderSendExportList(List<Long> orderId) {
//
//        String sql = "SELECT o.id,o.order_num,o.consignee,o.consignee_mobile,o.address,o.order_detail FROM "
//                + Tables.Order + " as o ";
//        StringBuilder params = new StringBuilder();
//        for (Long id : orderId) {
//            params.append(id).append(",");
//        }
//        params = params.delete(params.lastIndexOf(","), params.length());
//
//        sql += " WHERE o.id in (" + params.toString() + ")";
//
//        List<OrderSendListExportVo> list = jdbcTemplate.query(sql, new OrderSendListExportRowMapper());
//        return list;
//    }

    /**
     * 查询总页数
     *
     * @return
     */
    protected int getTotalSize() {
        return jdbcTemplate.queryForObject("SELECT FOUND_ROWS() as row_num;", int.class);
    }

    /**
     * 批量更新物流信息
     *
     * @param sendList
     * @return
     */
//    public ResultVo<Integer> batchUpdateLogistics(List<OrderSendListExportVo> sendList) {
//        if (sendList == null || sendList.size() == 0)
//            return new ResultVo<>(EnumResultVo.NotFound, "订单不存在");// -404;//没有数据
//        int updateRows = 0;
//
//        String updateSQL = "UPDATE " + Tables.ErpInvoice + " SET billStatus=?,logisticsCompany=?,logisticsCompanyCode=?,logisticsNumber=? WHERE srcOrderNo=? AND billStatus=?";
//        for (OrderSendListExportVo vo : sendList) {
//            updateRows += jdbcTemplate.update(updateSQL, InvoiceBillStatusEnum.LogisticsPrinted.getIndex(), vo.getCompany(), vo.getCompanyCode(), vo.getSendCode(), vo.getOrderNum(), InvoiceBillStatusEnum.Picked.getIndex());
//        }
//
//        return new ResultVo<>(EnumResultVo.SUCCESS, updateRows);
//    }
}
