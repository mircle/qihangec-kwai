package com.b2c.repository.erp;

import java.util.List;

import com.b2c.entity.erp.ErpGoodsSpecEntity;
import com.b2c.entity.ErpGoodsStockInfoEntity;
import com.b2c.repository.Tables;
import com.b2c.entity.ErpGoodsStockInfoItemEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * 描述：
 * 商品库存信息Repository
 *
 * @author qlp
 * @date 2019-09-26 16:35
 */
@Repository
public class ErpGoodsStockInfoRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * 获取商品仓位库存信息
     *
     * @param stockInfoId 商品仓位库存id
     * @return
     */
    public ErpGoodsStockInfoEntity getById(Long stockInfoId) {
        String sql = "SELECT * FROM " + Tables.ErpGoodsStockInfo + " gsi " +
                " WHERE id=?";
        var list = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(ErpGoodsStockInfoEntity.class), stockInfoId);
        if (list == null || list.size() == 0) {
            return null;
        } else
            return list.get(0);
    }

    public void updateDeleteById(Long stockInfoId){
        String sql = "UPDATE "+Tables.ErpGoodsStockInfo + " SET isDelete=1 WHERE id=? AND isDelete=0 AND currentQty=0";
        jdbcTemplate.update(sql,stockInfoId);
    }

    @Transactional
    public void apportionStockLocationForSpec(Integer specId, Integer stockLocationId) {
        /***查询商品信息***/
        var spec = jdbcTemplate.queryForObject("SELECT gs.*,g.number as goodsNumber FROM "+Tables.ErpGoodsSpec+" gs LEFT JOIN "+Tables.ErpGoods+" g on g.id=gs.goodsId WHERE gs.id=?",new BeanPropertyRowMapper<>(ErpGoodsSpecEntity.class),specId);

        /***判断仓位有没有被使用***/
        String sql = "SELECT COUNT(0) FROM erp_goods_stock_info WHERE locationId=? AND isDelete=0";
        Integer count = jdbcTemplate.queryForObject(sql, Integer.class, stockLocationId);
        if(count.intValue() == 0) {
         //没有使用
            String sql1 = "INSERT INTO "+Tables.ErpGoodsStockInfo+" (goodsId,goodsNumber,specId,specNumber,locationId) VALUE (?,?,?,?,?)";
            jdbcTemplate.update(sql1,spec.getGoodsId(),spec.getGoodsNumber(),spec.getId(),spec.getSpecNumber(),stockLocationId);
        }

    }

    public List<ErpGoodsStockInfoItemEntity> getStockInfoItemListBySpecId(Integer specId) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT sii.*,si.locationId,si.specNumber,sl.`number` as locationNumber FROM ").append(Tables.ErpGoodsStockInfoItem).append(" as sii ");
        sql.append(" LEFT JOIN ").append(Tables.ErpGoodsStockInfo).append(" as si on si.id = sii.stockInfoId ");
        sql.append(" LEFT JOIN ").append(Tables.ErpStockLocation).append(" as sl on sl.id = si.locationId ");
        // sql.append(" LEFT JOIN ").append(Tables.ErpGoodsSpec).append(" as gs on gs.id = si.specId ");
        sql.append(" WHERE si.specId = ? and si.isDelete = 0 ORDER BY sii.id desc");

        var list = jdbcTemplate.query(sql.toString(),new BeanPropertyRowMapper<>(ErpGoodsStockInfoItemEntity.class), specId);

        return list;
    }
}
