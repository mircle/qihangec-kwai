// package com.b2c.repository;

// import com.alibaba.fastjson.JSON;
// import com.alibaba.fastjson.JSONArray;
// import com.alibaba.fastjson.JSONObject;
// import com.b2c.entity.result.PagingResponse;
// import com.b2c.mall.entity.GoodsCategoryEntity;
// import com.b2c.mall.entity.MallGoodsEntity;
// import com.b2c.mall.entity.MallGoodsSpecEntity;
// import com.b2c.mall.entity.MallGoodsVo;
// import com.b2c.entity.vo.GoodsAddVo;
// import com.b2c.entity.vo.GoodsAttrVo;
// import com.b2c.entity.vo.GoodsSpecVo;
// import com.b2c.repository.Tables;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.jdbc.core.BeanPropertyRowMapper;
// import org.springframework.jdbc.core.JdbcTemplate;
// import org.springframework.jdbc.core.PreparedStatementCreator;
// import org.springframework.jdbc.support.GeneratedKeyHolder;
// import org.springframework.jdbc.support.KeyHolder;
// import org.springframework.stereotype.Repository;
// import org.springframework.transaction.annotation.Transactional;
// import org.springframework.util.StringUtils;

// import java.math.BigDecimal;
// import java.sql.Connection;
// import java.sql.PreparedStatement;
// import java.sql.SQLException;
// import java.sql.Statement;
// import java.util.ArrayList;
// import java.util.List;

// /**
//  * 商品Repository
//  */
// @Repository
// public class MallGoodsRepository {
//     @Autowired
//     protected JdbcTemplate jdbcTemplate;

//     protected int getTotalSize() {
//         return jdbcTemplate.queryForObject("SELECT FOUND_ROWS() as row_num;", int.class);
//     }
//     /**
//      * 分页查询商品列表
//      * @param pageIndex
//      * @param pageSize
//      * @param title
//      * @param number
//      * @return
//      */
//     public PagingResponse<MallGoodsEntity> getGoodsList(Integer pageIndex, Integer pageSize, String title, String number){
//         List<Object> params = new ArrayList<>();
//         StringBuilder sb = new StringBuilder("select SQL_CALC_FOUND_ROWS * from mall_goods where  1=1 ");
//         if(!StringUtils.isEmpty(title)){
//             sb.append(" AND title LIKE ? ");
//             params.add("%" + title + "%");
//         }
//         if(!StringUtils.isEmpty(number)){
//             sb.append(" AND number = ? ");
//             params.add("%" + number + "%");
//         }
//         sb.append(" ORDER BY id DESC LIMIT ?,?");
//         params.add((pageIndex - 1) * pageSize);
//         params.add(pageSize);
//         var list= jdbcTemplate.query(sb.toString(), new BeanPropertyRowMapper<>(MallGoodsEntity.class), params.toArray(new Object[params.size()]));
//         Integer total =getTotalSize();
//         return new PagingResponse<>(pageIndex, pageSize,total, list);
//     }
//     /**
//      * 查询商品信息
//      * @param id
//      * @return
//      */
//     public MallGoodsVo getMallGoods(Long id){
//         try {
//             var mallgood = jdbcTemplate.queryForObject("select * from mall_goods where  id=? ",new BeanPropertyRowMapper<>(MallGoodsVo.class),id);
//             mallgood.setGoodItems(jdbcTemplate.query("select m.*,s.color_value,s.size_value,s.color_image from mall_goods_spec m LEFT JOIN erp_goods_spec s ON m.specNumber=s.specNumber where m.goodsId=?",new BeanPropertyRowMapper<>(MallGoodsSpecEntity.class),mallgood.getId()));
//             return mallgood;
//         }catch (Exception e){
//             return null;
//         }
//     }

//     /**
//      * 添加商品
//      * @param array
//      */
//     public void addMallGoods(JSONArray array){
//         String SQL="set title=?,image=?,price=?,quantity=?,number=?";
//         array.forEach(obj->{
//             var good = (JSONObject)obj;
//             Long goodId= jdbcTemplate.queryForObject("SELECT IFNULL((SELECT id from mall_goods where id=?),0) id",Long.class,good.getLong("num_iid"));
//             if(goodId.intValue()==0){
//                 jdbcTemplate.update("INSERT mall_goods "+SQL +" ,id=? ",good.getString("title"),good.getString("pic_url"),good.getBigDecimal("price"),good.getInteger("num"), good.getString("outer_id"),good.getLong("num_iid"));
//                 addMallGoodsSepc(good.getLong("num_iid"),good.getJSONArray("items"));
//             }else{
//                 jdbcTemplate.update("update mall_goods "+SQL+"  where id=?",good.getString("title"),good.getString("pic_url"),good.getBigDecimal("price"),good.getInteger("num"),good.getString("outer_id"),goodId);
//                 addMallGoodsSepc(good.getLong("num_iid"),good.getJSONArray("items"));
//             }
//         });
//     }
//     /**
//      * 添加商品sku
//      * @param goodId
//      * @param array
//      */
//     public void addMallGoodsSepc(Long goodId,JSONArray array){
//         String SQL="INSERT mall_goods_spec set goodsId=?,quantity=?,specNumber=?,price=?,specStr=?";
//         if(!StringUtils.isEmpty(array)){
//             jdbcTemplate.update("delete from mall_goods_spec where goodsId=?",goodId);
//             array.forEach(obj->{
//                 var spec = (JSONObject)obj;
//                 jdbcTemplate.update(SQL,goodId,spec.getLong("quantity"),spec.getString("outer_id"),spec.getBigDecimal("price"),spec.getString("properties_name"));
//             });
//         }
//     }

//     /**
//      * 系统发布云购商品
//      *
//      * @param addVo
//      * @param createBy
//      * @return
//      */
//     @Transactional
//     public Integer addGoods(GoodsAddVo addVo, String createBy) {
//         Integer goodId= jdbcTemplate.queryForObject("SELECT IFNULL((SELECT id from goods where goods_number=?),0) id",Integer.class,addVo.getGoodsNumber());

//         if(goodId.intValue()>0) return goodId;

//         //查询商品分类
//         String categorySQL = "SELECT * FROM " + Tables.GoodsCategory + " WHERE id=? LIMIT 1";
//         List<GoodsCategoryEntity> categorys = jdbcTemplate.query(categorySQL, new BeanPropertyRowMapper<>(GoodsCategoryEntity.class), addVo.getCategoryId());

//         Integer category1 = 0;
//         Integer category2 = 0;
//         Integer category3 = addVo.getCategoryId();
//         if (categorys != null && categorys.size() > 0) {
//             category2 = categorys.get(0).getParentId();
//             String[] categoryStr = categorys.get(0).getPath().split("\\|");
//             if (categoryStr.length == 2) {
//                 try {
//                     category1 = Integer.parseInt(categoryStr[0]);
//                 } catch (Exception e) {
//                 }

//             }
//         }
//         BigDecimal commisionRate = addVo.getCommisionRate()==null?  BigDecimal.valueOf(0):addVo.getCommisionRate();
//         BigDecimal orderSocre = BigDecimal.valueOf(100);
//         Integer isStickyPost = 0;//是否置顶
//         BigDecimal commentSocre = BigDecimal.valueOf(5.0);
//         Integer saleNum = 0;
//         Integer publishState = addVo.getTime();//发布状态（2在库中1立即上架）
//         Integer saleState = addVo.getTime();//销售状态（2已下架1上架中）
//         Integer createOn = (int) (System.currentTimeMillis() / 1000);
//         Integer width = 0;
//         Integer height = 0;
//         Integer weight = 0;
//         try {
//             addVo.setErpGoodsId(jdbcTemplate.queryForObject("select id from "+Tables.ErpGoods+" where number=?",Integer.class,addVo.getGoodsNumber()));
//         }catch (Exception e){
//             addVo.setErpGoodsId(0);
//         }
//         //1     添加goods
//         String goodsSQL = "INSERT INTO " + Tables.Goods + " " +
//                 " (title,goods_category_id,sale_price,commision_rate,image,category1,category2,category3,order_socre,is_stickypost,comment_num,comment_socre," +
//                 "sale_num,publish_state,sale_state,create_on,create_by,freight_template,delivery_province,delivery_city,delivery_address,width,height,weight,keyword," +
//                 "goods_number,sale_num_show,deputy_title,erp_goods_id,goodTotalQty) " +
//                 " VALUE (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

//         KeyHolder keyHolder = new GeneratedKeyHolder();
//         Integer finalCategory2 = category2;
//         Integer finalCategory1 = category1;
//         jdbcTemplate.update(new PreparedStatementCreator() {
//             @Override
//             public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
//                 PreparedStatement ps = connection.prepareStatement(goodsSQL, Statement.RETURN_GENERATED_KEYS);
//                 ps.setString(1, addVo.getTitle());
//                 ps.setInt(2, addVo.getCategoryId());
//                 ps.setBigDecimal(3, addVo.getPrice());
//                 ps.setBigDecimal(4, commisionRate);
//                 ps.setString(5, addVo.getImage());
//                 ps.setInt(6, finalCategory1);
//                 ps.setInt(7, finalCategory2);
//                 ps.setInt(8, category3);
//                 ps.setBigDecimal(9, orderSocre);
//                 ps.setInt(10, isStickyPost);
//                 ps.setInt(11, 0);
//                 ps.setBigDecimal(12, commentSocre);
//                 ps.setInt(13, saleNum);
//                 ps.setInt(14, publishState);
//                 ps.setInt(15, saleState);
//                 ps.setInt(16, createOn);
//                 ps.setString(17, createBy);
//                 ps.setInt(18, addVo.getFreightTemplate());
//                 ps.setString(19, "4400000");
//                 ps.setString(20, "4419000");
//                 ps.setString(21, "广东东莞");
//                 ps.setInt(22, width);
//                 ps.setInt(23, height);
//                 ps.setInt(24, weight);
//                 ps.setString(25, addVo.getKeyword());
//                 ps.setString(26, addVo.getGoodsNumber());
//                 ps.setInt(27, addVo.getSaleNumShow());
//                 ps.setString(28, addVo.getDeputyTitle());
//                 ps.setInt(29,addVo.getErpGoodsId());
//                 ps.setLong(30,addVo.getGoodTotalQty());
//                 return ps;
//             }
//         }, keyHolder);

//         Integer goodsId = keyHolder.getKey().intValue();
//         String specTextJson = JSON.toJSONString(addVo.getGoodsSpec());

//         //商品属性JSON  attribute_text
//         String attributeTextJson = JSON.toJSONString(addVo.getGoodsAttr());

//         String detailSQL = "INSERT INTO " + Tables.GoodsDetail + " (goods_id,modify_on,attribute_text,spec_text,detail_text  ) VALUE (?,?,?,?,?)";

//         jdbcTemplate.update(detailSQL, goodsId, 0, attributeTextJson, specTextJson,"");

//         //删除旧的商品附件信息
//         jdbcTemplate.update("delete from " + Tables.GoodsAttachment + " WHERE goods_id=?",goodsId);

//         //3添加商品视频 goods_attachment
//         if (StringUtils.isEmpty(addVo.getVideo()) == false) {
//             jdbcTemplate.update("INSERT INTO " + Tables.GoodsAttachment + " (goods_id,attachment_type,url,create_on,create_by) VALUE (?,?,?,?,?)", goodsId, 1, addVo.getVideo(), createOn, createBy);
//         }
//         //3添加商品附件 goods_attachment
//         if (addVo.getImages() != null && addVo.getImages().length > 0) {
//             for (String img : addVo.getImages()) {
//                 if (StringUtils.isEmpty(img) == false) {
//                     jdbcTemplate.update("INSERT INTO " + Tables.GoodsAttachment + " (goods_id,attachment_type,url,create_on,create_by) VALUE (?,?,?,?,?)", goodsId, 0, img, createOn, createBy);
//                 }
//             }
//         }
//         //4添加商品属性 goods_attribute
//         if (addVo.getGoodsAttr() != null && addVo.getGoodsAttr().size() > 0) {
//             for (GoodsAttrVo attr : addVo.getGoodsAttr()) {
//                 String attrSQL = "INSERT INTO " + Tables.GoodsAttribute + " (goods_id,attribute_id,attribute,value_id,value) VALUE (?,?,?,?,?)";
//                 jdbcTemplate.update(attrSQL, goodsId, attr.getId(), attr.getName(), attr.getValId() == null ? 0 : attr.getValId(), attr.getVal());
//             }
//         }

//         String sepcSQL = "INSERT INTO " + Tables.GoodsSpec + " (spec_number,price,color_name,color_value,color_id,color_image,size_name,size_value,size_id,goods_id,style_id,style_name,style_value,properties_name_json,currentQty) VALUE(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

//         //添加商品规格属性goods_spec_attr
//         String specAttrSQL = "INSERT INTO goods_spec_attr (goods_id,type,k,kid,vid,v,img) VALUE (?,?,?,?,?,?,?)";

//         //5    添加产品规格 goods_spec
//         if (addVo.getGoodsSpec() != null && addVo.getGoodsSpec().size() > 0) {
//             for (GoodsSpecVo specVo : addVo.getGoodsSpec()) {
//                 if (StringUtils.isEmpty(specVo.getImage())) specVo.setImage(addVo.getImage());
//                 jdbcTemplate.update(sepcSQL,
//                         specVo.getCode(),
//                         specVo.getPrice(),
//                         "颜色",
//                         specVo.getColor(),
//                         specVo.getColorId(),
//                         specVo.getImage(),
//                         "尺码",
//                         specVo.getSize(),
//                         specVo.getSizeId(),
//                         goodsId,
//                         specVo.getStyleId(),
//                         "款式",
//                         specVo.getStyle(),
//                         "",
//                         specVo.getStock());
//             }
//         }
//         var colors = jdbcTemplate.query("select s.color_value,s.color_image from mall_goods_spec m LEFT JOIN erp_goods_spec s ON m.specNumber=s.specNumber where s.goodsId=? GROUP BY s.color_value",new BeanPropertyRowMapper<>(MallGoodsSpecEntity.class),addVo.getErpGoodsId());
//         for(var spec :colors){
//             jdbcTemplate.update(specAttrSQL, goodsId,"color", "颜色", 0,0,spec.getColorValue(),spec.getColorImage());
//         }

//         var sizes = jdbcTemplate.query("select s.size_value from mall_goods_spec m LEFT JOIN erp_goods_spec s ON m.specNumber=s.specNumber where s.goodsId=? GROUP BY s.size_value",new BeanPropertyRowMapper<>(MallGoodsSpecEntity.class),addVo.getErpGoodsId());
//         for(var spec :sizes){
//             jdbcTemplate.update(specAttrSQL, goodsId,"size", "尺码", 0,0, spec.getSizeValue(),"");
//         }
//         jdbcTemplate.update("update mall_goods set ygGoodId=? WHERE number=?",goodsId,addVo.getGoodsNumber());
//         return goodsId;
//     }
// }
