package com.b2c.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.TodoEntity;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.entity.result.ResultVo;

@Repository
public class TodoRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    public ResultVo<Long> addTodo(Integer shopId,Integer sourceType, String sourceId, String content) {
        String sql ="INSERT INTO erp_todo (sourceType,sourceId,content,status,shopId) VALUE(?,?,?,?,?)";
        jdbcTemplate.update(sql,sourceType,sourceId,content,0,shopId);
        return new ResultVo<Long>(EnumResultVo.SUCCESS);
    }

    public ResultVo<Long> handle(Integer id, String result) {
        String sql = "UPDATE erp_todo SET result=?,status=1 WHERE id=?";
        jdbcTemplate.update(sql,result,id);
        return new ResultVo<Long>(EnumResultVo.SUCCESS);
    }

    public ResultVo<Long> completed(Integer id) {
        String sql = "UPDATE erp_todo SET status=99 WHERE id=?";
        jdbcTemplate.update(sql,id);
        return new ResultVo<Long>(EnumResultVo.SUCCESS);
    }

    @Transactional
    public PagingResponse<TodoEntity> getList(Integer pageIndex, Integer pageSize, Integer sourceType, String  sourceId, Integer status,String content) {
        List<Object> params = new ArrayList<>();
        StringBuilder sb = new StringBuilder("SELECT SQL_CALC_FOUND_ROWS t.*,shop.`name` as shopName  ");
        sb.append(" FROM ").append(" erp_todo t ");
        sb.append(" LEFT JOIN dc_shop shop on shop.id = t.shopId");
        

        sb.append(" WHERE 1=1 ");
        // if(shopId !=null && shopId.intValue() > 0){
        //     sb.append(" AND shopId=? ");
        //     params.add(shopId);
        // }

        if (StringUtils.hasText(content) ) {
            sb.append(" AND `content` LIKE ? ");
            params.add("%"+content+"%");
        }

        if (status != null) {
            if(status.intValue() > -1){
            sb.append(" AND status = ? ");
            params.add(status);
            }
        }else{
            sb.append(" AND (status = 0 OR status = 1 )  ");
        }


        sb.append(" ORDER BY `status` asc,updateTime DESC");
        sb.append("  LIMIT ?,?");
        params.add((pageIndex - 1) * pageSize);
        params.add(pageSize);

        var list = jdbcTemplate.query(sb.toString(), new BeanPropertyRowMapper<>(TodoEntity.class), params.toArray(new Object[params.size()]));
        int totalSize = getTotalSize();
        return new PagingResponse<>(pageIndex, pageSize, totalSize, list);
    }
    
    protected int getTotalSize() {
        return jdbcTemplate.queryForObject("SELECT FOUND_ROWS() as row_num;", int.class);
    }
}
