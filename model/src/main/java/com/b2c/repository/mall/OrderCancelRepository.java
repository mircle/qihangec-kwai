package com.b2c.repository.mall;

import com.b2c.entity.result.PagingResponse;
import com.b2c.common.utils.DateUtil;
import com.b2c.common.utils.StringUtil;
import com.b2c.entity.OrderCancelEntity;
import com.b2c.entity.OrderItemEntity;
import com.b2c.entity.OrdersEntity;
import com.b2c.entity.enums.OrderCancelStateEnums;
import com.b2c.entity.enums.OrderStateEnums;
import com.b2c.entity.enums.OrderTypeEnums;
import com.b2c.entity.mall.OrderCancelItemEntity;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.vo.RefundOrderDetailVo;
import com.b2c.entity.vo.RefundOrderListVo;
import com.b2c.repository.Tables;
import com.b2c.entity.vo.finance.FinanceRefundOrderItemListVo;
import com.b2c.entity.vo.OrderRefundApplyVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 描述：
 * 退货订单
 *
 * @author qlp
 * @date 2019-02-21 12:05
 */
@Repository
public class OrderCancelRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * 查询总页数
     *
     * @return
     */
    protected int getTotalSize() {
        return jdbcTemplate.queryForObject("SELECT FOUND_ROWS() as row_num;", int.class);
    }

    /**
     * 根据id获取退款订单信息
     *
     * @param id
     * @return
     */
    public OrderCancelEntity getById(Long id) {
        String sql = "SELECT * FROM " + Tables.OrderCancel + " WHERE id=?";
        List<OrderCancelEntity> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(OrderCancelEntity.class), id);
        if (list == null || list.size() == 0) return null;
        else return list.get(0);
    }

    /**
     * 根据退款订单号获取退款订单信息
     *
     * @param refundNo
     * @return
     */
    public OrderCancelEntity getByRefundNo(String refundNo) {
        String sql = "SELECT * FROM " + Tables.OrderCancel + " WHERE order_cancel_num=?";
        List<OrderCancelEntity> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(OrderCancelEntity.class), refundNo);
        if (list == null || list.size() == 0) return null;
        else return list.get(0);
    }

    /**
     * 完成退款
     *
     * @param refundOrderId
     */
    @Transactional
    public void CompleteRefund(Long refundOrderId, String successTime, String result) {
        Integer finishOn = 0;
        try {
            finishOn = DateUtil.dateToStamp(successTime);
        } catch (Exception e) {
            finishOn = 0;
        }
        try {
            OrderCancelEntity order = jdbcTemplate.queryForObject("SELECT * FROM " + Tables.OrderCancel + " WHERE id=?", new BeanPropertyRowMapper<>(OrderCancelEntity.class), refundOrderId);
            var items = jdbcTemplate.query("SELECT * FROM " + Tables.OrderCancelItem + " WHERE order_cancel_id=?", new BeanPropertyRowMapper<>(OrderCancelItemEntity.class), order.getId());
            if (order != null) {
                //更新退货单状态
                String sql = "UPDATE " + Tables.OrderCancel + " SET state=?,finish_on=?,result=? WHERE id=?";
                jdbcTemplate.update(sql, OrderCancelStateEnums.Success.getIndex(), finishOn, result, refundOrderId);

                /**********循环更新退货商品信息 ***********/
                for (var ritem : items) {
                    //更新退货商品信息
                    jdbcTemplate.update("UPDATE " + Tables.OrderItem + " SET after_sale_state=? WHERE id =?", OrderCancelStateEnums.Success.getIndex(), ritem.getOrderItemId());
                }
            }
        } catch (Exception e) {

        }
    }

    /**
     * 添加采购订单退货商品信息
     *
     * @param orderId
     * @param itemIds
     * @param nums
     */
//    @Transactional(rollbackFor = Exception.class)
//    public ResultVo<Integer> addBuyerOrderCancel(Long orderId, String[] itemIds, String[] nums) {
//        if (orderId == null) return new ResultVo<>(EnumResultVo.ParamsError, "没有找到订单信息");
//        if (itemIds == null || itemIds.length == 0) return new ResultVo<>(EnumResultVo.ParamsError, "没有选择要退货的商品");
//        if (nums == null || nums.length == 0) return new ResultVo<>(EnumResultVo.ParamsError, "没有选择要退货数量的商品");
//
//        OrdersEntity order = new OrdersEntity();
//        List<OrderItemEntity> returnOrderItems = new ArrayList<>();
//        Double returnTotalAmount = 0d;//退款总金额
//        int returnTotal = 0; //本次勾选的退货总数
//        try {
//            order = jdbcTemplate.queryForObject("select * from " + Tables.Order + " where id=?", new BeanPropertyRowMapper<>(OrdersEntity.class), orderId);
//            if (order.getState() < OrderStateEnums.Delivered.getIndex() || order.getState() > OrderStateEnums.Completed.getIndex()) {
//                return new ResultVo<>(EnumResultVo.ParamsError, "订单已发货才能申请退货");
//            }
//
//            /*****************判断退货数量信息******************/
//            for (int i = 0, n = itemIds.length; i < n; i++) {
//
//
//                var item = jdbcTemplate.queryForObject("select * from " + Tables.OrderItem + " where id=?", new BeanPropertyRowMapper<>(OrderItemEntity.class), itemIds[i]);
//
//
//                /*******计算可退货数量********/
//                int canReturnCount = item.getCount() - item.getReturnedCount().intValue();
//                int returnCount = Integer.parseInt(nums[i]);//本次退货数量
//
//                //TODO：退款金额没有减去优惠后的金额
//                returnTotalAmount += item.getPrice().doubleValue() * returnCount;
//
//                if (canReturnCount < returnCount) {
//                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//                    return new ResultVo<>(EnumResultVo.DataError, item.getSpecNumber() + "退货数量超过可退数量");
//                }
//                if (returnCount > 0) {
//                    //数量不为0的才加入退货单
//                    returnOrderItems.add(item);
//                    returnTotal += returnCount;
//                }
//            }
//        } catch (Exception e) {
//            return new ResultVo<>(EnumResultVo.DataError, e.getMessage());
//        }
//        if (returnTotal <= 0) return new ResultVo<>(EnumResultVo.DataError, "退货数量为0，无需提交");
//
//        /******************开始退货数据库操作*********************/
//        OrdersEntity finalOrder = order;
//        Double finalReturnTotalAmount = 0.0d;
//        try {
//            String addOrderCancelSQL = "INSERT INTO " + Tables.OrderCancel + " SET order_cancel_num=?,order_id=?,user_id=?,total_amount=?,create_on=unix_timestamp(now()),state=1 ";
//            KeyHolder keyHolder = new GeneratedKeyHolder();
//            //插入退货单主表
//
//            jdbcTemplate.update(new PreparedStatementCreator() {
//                @Override
//                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
//                    PreparedStatement ps = connection.prepareStatement(addOrderCancelSQL, Statement.RETURN_GENERATED_KEYS);
//                    ps.setString(1, StringUtil.createCancelOrderNum());
//                    ps.setLong(2, orderId);
//                    ps.setInt(3, finalOrder.getUserId());
//                    ps.setBigDecimal(4, BigDecimal.valueOf(finalReturnTotalAmount));
//                    return ps;
//                }
//            }, keyHolder);
//            Long orderCancelId = keyHolder.getKey().longValue();
//
//            //插入退货单子表
//            String addOrderCancelItemSQL = "insert into order_cancel_item set order_cancel_id=?,order_id=?,order_item_id=?,goods_id=?,title=?,image=?,spec_id=?,spec_number=?,color=?,size=?,price=?,quantity=?,status=1";
//
////            Integer count = 0;
//            for (int i = 0, n = returnOrderItems.size(); i < n; i++) {
//                var item = returnOrderItems.get(i);
//                int returnNumber = Integer.parseInt(nums[i]);//本次退货数量
//                if (returnNumber > 0) {
//                    /*************添加退货明细*************/
//                    jdbcTemplate.update(addOrderCancelItemSQL, orderCancelId, item.getOrderId(), item.getId(), item.getGoodsId(), item.getTitle(), item.getImage(), item.getSpecId(), item.getSpecNumber(), item.getColor(), item.getSize(), item.getPrice(), returnNumber);
//
//
//                    /*************更新orderitem数据 （售后数量）*************/
//                    jdbcTemplate.update("update " + Tables.OrderItem + " set after_sale_state=1,is_after=1,returnedCount=returnedCount + ? where id=?", returnNumber, item.getId());
//                }
//            }
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            return new ResultVo<>(EnumResultVo.SUCCESS, "申请成功");
//        } catch (Exception e) {
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//            return new ResultVo<>(EnumResultVo.Fail, e.getMessage());
//        }
//    }
    @Transactional(rollbackFor = Exception.class)
    public ResultVo<Integer> addBuyerOrderCancel(OrderRefundApplyVo applyVo) {
        if (applyVo.getOrderId() == null) return new ResultVo<>(EnumResultVo.ParamsError, "参数错误，没有订单id");
        if (applyVo.getReturnItems() == null || applyVo.getReturnItems().size() == 0)
            return new ResultVo<>(EnumResultVo.ParamsError, "参数错误,没有选择要退货的商品");

        OrdersEntity order = new OrdersEntity();

        List<OrderItemEntity> returnOrderItems = new ArrayList<>();
        Double returnTotalAmount = 0d;//退款总金额
        int returnTotal = 0; //本次勾选的退货总数
        try {
            order = jdbcTemplate.queryForObject("select * from " + Tables.Order + " where id=?", new BeanPropertyRowMapper<>(OrdersEntity.class), applyVo.getOrderId());
            if (order.getState() < OrderStateEnums.Delivered.getIndex() || order.getState() > OrderStateEnums.Completed.getIndex()) {
                return new ResultVo<>(EnumResultVo.ParamsError, "订单已发货才能申请退货");
            }

            /*****************判断退货数量信息******************/
            for (var returnItem : applyVo.getReturnItems()) {

                var item = jdbcTemplate.queryForObject("select * from " + Tables.OrderItem + " where id=?", new BeanPropertyRowMapper<>(OrderItemEntity.class), returnItem.getOrderItemId());


                /*******计算可退货数量********/
                int canReturnCount = item.getCount() - item.getReturnedCount().intValue();
                int returnCount = returnItem.getReturnCount();//本次退货数量

                //TODO：退款金额没有减去优惠后的金额
                returnTotalAmount += item.getDiscountPrice().doubleValue() * returnCount;

                if (canReturnCount < returnCount) {
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                    return new ResultVo<>(EnumResultVo.DataError, item.getSpecNumber() + "退货数量超过可退数量");
                }
                if (returnCount > 0) {
                    //条件通过，将退货数量字段设置为当前退货数量
                    item.setReturnedCount(returnCount);
                    //数量不为0的才加入退货单
                    returnOrderItems.add(item);
                    returnTotal += returnCount;
                }
            }
        } catch (Exception e) {
            return new ResultVo<>(EnumResultVo.DataError, e.getMessage());
        }
        if (returnTotal <= 0) return new ResultVo<>(EnumResultVo.DataError, "退货数量为0，无需提交");

        /******************开始退货数据库操作*********************/
        OrdersEntity finalOrder = order;
        Double finalReturnTotalAmount = returnTotalAmount;
        try {
            String addOrderCancelSQL = "INSERT INTO " + Tables.OrderCancel + " SET order_cancel_num=?,order_id=?,user_id=?,total_amount=?,create_on=unix_timestamp(now()),state=1 ";
            KeyHolder keyHolder = new GeneratedKeyHolder();
            //插入退货单主表

            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(addOrderCancelSQL, Statement.RETURN_GENERATED_KEYS);
                    ps.setString(1, StringUtil.createCancelOrderNum());
                    ps.setLong(2, applyVo.getOrderId());
                    ps.setInt(3, finalOrder.getUserId());
                    ps.setBigDecimal(4, BigDecimal.valueOf(finalReturnTotalAmount));
                    return ps;
                }
            }, keyHolder);
            Long orderCancelId = keyHolder.getKey().longValue();

            //插入退货单子表
            String addOrderCancelItemSQL = "insert into order_cancel_item set order_cancel_id=?,order_id=?,order_item_id=?,goods_id=?,title=?,image=?,spec_id=?,spec_number=?,color=?,size=?,price=?,discount_price=?,quantity=?,status=1";

//            Integer count = 0;
            for (int i = 0, n = returnOrderItems.size(); i < n; i++) {
                var item = returnOrderItems.get(i);
                int returnNumber = item.getReturnedCount();//Integer.parseInt(nums[i]);//本次退货数量
                if (returnNumber > 0) {
                    /*************添加退货明细*************/
                    jdbcTemplate.update(addOrderCancelItemSQL, orderCancelId, item.getOrderId(), item.getId(), item.getGoodsId(), item.getTitle(), item.getImage(), item.getSpecId(), item.getSpecNumber(), item.getColor(), item.getSize(), item.getPrice(),item.getDiscountPrice(), returnNumber);


                    /*************更新orderitem数据 （售后数量）*************/
                    jdbcTemplate.update("update " + Tables.OrderItem + " set after_sale_state=1,is_after=1,returnedCount=returnedCount + ? where id=?", returnNumber, item.getId());
                }
            }
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return new ResultVo<>(EnumResultVo.SUCCESS, "申请成功");
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return new ResultVo<>(EnumResultVo.Fail, e.getMessage());
        }
    }


    /**
     * 查询退货订单列表
     *
     * @param pageIndex
     * @param pageSize
     * @param orderNum
     * @param startTime
     * @param endTime
     * @param returnOrderNum
     * @param state
     * @param orderType
     * @return
     */
    @Transactional
    public PagingResponse<RefundOrderListVo> getAfterOrders(Integer pageIndex, Integer pageSize, String orderNum, Integer startTime, Integer endTime, String returnOrderNum,String logisticsCode, Integer state, OrderTypeEnums orderType) {
        StringBuilder builder = new StringBuilder("SELECT SQL_CALC_FOUND_ROWS C.*,D.order_num,D.payment_result_time,B.mobile mj_mobile,D.payment_method,D.type as orderType");
        builder.append(",(SELECT SUM(quantity) FROM " + Tables.OrderCancelItem + " WHERE order_cancel_id=C.id) as totalQuantity");
        builder.append(" FROM " + Tables.OrderCancel + " C ");
        builder.append(" LEFT JOIN " + Tables.Order + " D ON C.order_id=D.id ");
        builder.append(" LEFT JOIN " + Tables.User + " B ON D.user_id=B.id WHERE 1=1");
        List<Object> params = new ArrayList<>();
//        if (orderType.getIndex() == OrderTypeEnums.PT.getIndex()) {
//            //云购平台订单，包含预售订单
//            builder.append(" and D.type=? ");
//            params.add(OrderTypeEnums.PT.getIndex());
////            params.add(OrderTypeEnums.PT_YUSHOU.getIndex());
//        } else {
//            builder.append(" and D.type=? ");
//            params.add(orderType.getIndex());
//        }
        if (StringUtils.isEmpty(orderNum) == false) {
            builder.append(" and D.order_num = ? ");
            params.add(orderNum);
        }
        if (startTime > 0) {
            builder.append("AND C.create_on>=? ");
            params.add(startTime);
        }
        if (endTime > 0) {
            builder.append("AND C.create_on<? ");
            params.add(endTime);
        }
        if (StringUtils.isEmpty(returnOrderNum) == false) {
            builder.append(" and C.order_cancel_num = ? ");
            params.add(returnOrderNum);
        }

        if (StringUtils.isEmpty(logisticsCode) == false) {
            builder.append(" and C.send_code = ? ");
            params.add(logisticsCode);
        }

        if (state > 0) {
            builder.append(" AND C.state=? ");
            params.add(state);
        }
        builder.append(" ORDER BY C.id DESC LIMIT ?,? ");
        params.add((pageIndex - 1) * pageSize);
        params.add(pageSize);
        var list = jdbcTemplate.query(builder.toString(), new BeanPropertyRowMapper<>(RefundOrderListVo.class), params.toArray());
        Integer totalSize = getTotalSize();

        //查询退货明细
        list.forEach(item -> {
            var items = jdbcTemplate.query("SELECT * FROM " + Tables.OrderCancelItem + " WHERE order_cancel_id=? ", new BeanPropertyRowMapper<>(OrderCancelItemEntity.class), item.getId());
            item.setItems(items);
            item.setTotalCount(jdbcTemplate.queryForObject("SELECT SUM(quantity) FROM order_cancel_item WHERE order_cancel_id=? AND order_id=?", Long.class, item.getId(), item.getOrderId()));

        });
        return new PagingResponse<>(pageIndex, pageSize, totalSize, list);
    }

    private StringBuilder getRefundOrderItemSQL() {
        StringBuilder sb = new StringBuilder("SELECT SQL_CALC_FOUND_ROWS oc.order_cancel_num as refundId,o.order_num as orderId,oc.create_on as applyDate,");
        sb.append("oci.quantity as refundQuantity,oc.type as isOnlyRefund,oc.send_company as logisticsCompany,oc.send_code as logisticsCode,oc.state as status,");
        sb.append("oi.title as goodsName,IFNULL(oi.new_spec_number,oi.spec_number) as specNumber,g.goods_number,");
        sb.append("concat('颜色:',oi.color,'尺码:',oi.size) specName,oi.count as buyQuantity,");
        sb.append("(oi.discount_price * oi.count) as buyAmount,sh.name as shopName,o.shopId");
        sb.append(" FROM " + Tables.OrderCancelItem + " oci ");
        sb.append(" LEFT JOIN " + Tables.OrderCancel + " oc ON oc.id = oci.order_cancel_id ");
        sb.append(" LEFT JOIN " + Tables.OrderItem + " oi ON oi.id = oci.order_item_id ");
        sb.append(" LEFT JOIN " + Tables.Order + " o ON o.id = oi.order_id ");
        sb.append(" LEFT JOIN " + Tables.GoodsSpec + " gs ON gs.spec_number = oci.spec_number ");
        sb.append(" LEFT JOIN " + Tables.Goods + " g ON g.id = gs.goods_id ");
        sb.append(" LEFT JOIN ").append(Tables.DcShop).append(" as sh on sh.id = o.shopId ");
        sb.append(" WHERE 1=1 ");
        return sb;
    }

    /**
     * 获取退货item list（供财务使用）
     *
     * @param pageIndex
     * @param pageSize
     * @param orderNum
     * @param returnOrderNum
     * @param startTime
     * @param endTime
     * @return
     */
    @Transactional
    public PagingResponse<FinanceRefundOrderItemListVo> getRefundOrderItemList(Integer pageIndex, Integer pageSize, String orderNum, String returnOrderNum, Integer startTime, Integer endTime) {
        StringBuilder sb = getRefundOrderItemSQL();
        List<Object> params = new ArrayList<>();

        if (StringUtils.isEmpty(orderNum) == false) {
            sb.append(" and o.order_num = ? ");
            params.add(orderNum);
        }
        if (startTime > 0) {
            sb.append("AND oc.create_on>=? ");
            params.add(startTime);
        }
        if (endTime > 0) {
            sb.append("AND oc.create_on<? ");
            params.add(endTime);
        }
        if (StringUtils.isEmpty(returnOrderNum) == false) {
            sb.append(" and oc.order_cancel_num = ? ");
            params.add(returnOrderNum);
        }

        sb.append(" ORDER BY oc.id DESC LIMIT ?,? ");
        params.add((pageIndex - 1) * pageSize);
        params.add(pageSize);
        var list = jdbcTemplate.query(sb.toString(), new BeanPropertyRowMapper<>(FinanceRefundOrderItemListVo.class), params.toArray());

        return new PagingResponse<>(pageIndex, pageSize, getTotalSize(), list);
    }

    public List<FinanceRefundOrderItemListVo> getRefundOrderItemListForExcel(String orderNum, String returnOrderNum, Integer startTime, Integer endTime) {
        StringBuilder sb = getRefundOrderItemSQL();

        List<Object> params = new ArrayList<>();

        if (StringUtils.isEmpty(orderNum) == false) {
            sb.append(" and o.order_num = ? ");
            params.add(orderNum);
        }
        if (startTime > 0) {
            sb.append("AND oc.create_on>=? ");
            params.add(startTime);
        }
        if (endTime > 0) {
            sb.append("AND oc.create_on<? ");
            params.add(endTime);
        }
        if (StringUtils.isEmpty(returnOrderNum) == false) {
            sb.append(" and oc.order_cancel_num = ? ");
            params.add(returnOrderNum);
        }

        sb.append(" ORDER BY oc.id DESC ");

        var list = jdbcTemplate.query(sb.toString(), new BeanPropertyRowMapper<>(FinanceRefundOrderItemListVo.class), params.toArray());

        return list;
    }

    /**
     * 下单系统退货单详情
     *
     * @param id
     * @return
     */
    public RefundOrderDetailVo getRefundOrderById(Long id) {
        String sql = "SELECT A.*,B.mobile,C.consignee,C.consignee_mobile,C.address " +
                "FROM order_cancel A LEFT JOIN user B ON A.user_id=B.id LEFT JOIN orders C ON A.order_id = C.id WHERE  A.id=?";
        return jdbcTemplate.query(sql, new ResultSetExtractor<RefundOrderDetailVo>() {
            @Override
            public RefundOrderDetailVo extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                RefundOrderDetailVo vo = new RefundOrderDetailVo();
                while (resultSet.next()) {
                    vo.setType(resultSet.getInt("type"));
                    vo.setOrderCancelNum(resultSet.getString("order_cancel_num"));
                    vo.setTotalAmount(resultSet.getBigDecimal("total_amount"));
                    vo.setMjMobile(resultSet.getString("mobile"));
                    vo.setConsignee(resultSet.getString("consignee"));
                    vo.setConsigneeMobile(resultSet.getString("consignee_mobile"));
                    vo.setAddress(resultSet.getString("address"));
                    vo.setState(resultSet.getInt("state"));
                    vo.setSendCompany(resultSet.getString("send_company_code"));
                    vo.setSendCode(resultSet.getString("send_code"));
                    vo.setCreateOn(resultSet.getLong("create_on"));
                    vo.setCancelItems(jdbcTemplate.query("SELECT * FROM order_cancel_item WHERE order_cancel_id=? ", new BeanPropertyRowMapper<>(OrderCancelItemEntity.class), id));
                    vo.setTotalCount(jdbcTemplate.queryForObject("SELECT SUM(quantity) FROM order_cancel_item WHERE order_cancel_id=? ", Long.class, id));
                }
                return vo;
            }
        }, id);
/*        sql = "SELECT * FROM " + Tables.OrderLogs + " WHERE order_id = ? AND type = 0 ORDER BY create_on DESC";
        List<OrderLogsEntity> entities = jdbcTemplate.query(sql, new ResultSetExtractor<List<OrderLogsEntity>>() {
            @Override
            public List<OrderLogsEntity> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                List<OrderLogsEntity> entities1 = new ArrayList<>();
                while (resultSet.next()) {
                    OrderLogsEntity entity1 = new OrderLogsEntity();
                    entity1.setComment(resultSet.getString("comment"));
                    entity1.setCreateOn(DateUtil.stampToDateTime(resultSet.getString("create_on")));
                    entities1.add(entity1);
                }
                return entities1;
            }
        }, id);
        entity.setLogs(entities);
        return entity;*/
    }

    /**
     * 查询订单可申请售后的商品列表
     *
     * @param orderId
     * @return
     */
    public List<OrderItemEntity> getOrderItemByAfter(Integer orderId) {
        String sql = "SELECT * FROM order_item WHERE order_id=?";
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(OrderItemEntity.class), orderId);
    }


}
