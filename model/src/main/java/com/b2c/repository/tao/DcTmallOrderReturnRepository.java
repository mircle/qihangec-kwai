package com.b2c.repository.tao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.tao.TmallOrderRefundVo;
import com.b2c.repository.Tables;

@Repository
public class DcTmallOrderReturnRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Transactional
    public PagingResponse<TmallOrderRefundVo> getList(Integer shopId,Integer pageIndex, Integer pageSize, String orderId,
            String refundId,String logisticsCode) {
                
                List<Object> params = new ArrayList<>();
                StringBuilder sb = new StringBuilder("SELECT SQL_CALC_FOUND_ROWS   ");
                sb.append("refund.id,refund.refund_id,refund.oid as orderId,oi.productImgUrl as goodsImg,oi.goodsTitle as goodsName,oi.skuInfo as spec");
                sb.append(",refund.num,refund.refund_fee,refund.reason,refund.desc,refund.logisticsCompany,refund.logisticsCode,refund.created,refund.createOn");
                sb.append(",refund.status,refund.auditStatus,refund.auditTime,o.shopId");

                sb.append(" FROM ").append(Tables.DcTmallOrderRefund).append(" refund ");
                sb.append(" LEFT JOIN ").append(Tables.DcTmallOrderItem).append(" oi on oi.refundId= refund.refund_id ");
                sb.append(" LEFT JOIN ").append(Tables.DcTmallOrder).append(" o on o.id= refund.tid ");
        
                sb.append(" WHERE o.shopId = ?  ");
                params.add(shopId);
        
                if (StringUtils.isEmpty(orderId) == false) {
                    sb.append(" AND refund.tid = ? ");
                    params.add( orderId);
                }
                if (StringUtils.isEmpty(refundId) == false) {
                    sb.append(" AND refund.refund_id = ? ");
                    params.add( refundId);
                }
                if (StringUtils.isEmpty(logisticsCode) == false) {
                    sb.append(" AND refund.logisticsCode = ? ");
                    params.add( logisticsCode);
                }

                sb.append(" ORDER BY refund.id DESC ");
        
        
                sb.append("  LIMIT ?,?");
                params.add((pageIndex - 1) * pageSize);
                params.add(pageSize);
                
                var list = jdbcTemplate.query(sb.toString(), new BeanPropertyRowMapper<>(TmallOrderRefundVo.class), params.toArray(new Object[params.size()]));
        
                int totalSize = getTotalSize();
        
                return new PagingResponse<>(pageIndex, pageSize, totalSize, list);
            }
        
            protected int getTotalSize() {
                return jdbcTemplate.queryForObject("SELECT FOUND_ROWS() as row_num;", int.class);
            }
        
    
}
