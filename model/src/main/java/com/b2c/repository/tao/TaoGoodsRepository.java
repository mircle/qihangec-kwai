package com.b2c.repository.tao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.erp.ErpGoodsEntity;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.tao.TaoGoodsEntity;

@Repository
public class TaoGoodsRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public ResultVo<Long> addGoods(TaoGoodsEntity goodsEntity) {ErpGoodsEntity erpGoods = null;
        Integer erpGoodsId = 0;
        try {
            erpGoods = jdbcTemplate.queryForObject("SELECT * FROM erp_goods WHERE `number`=?",
                    new BeanPropertyRowMapper<>(ErpGoodsEntity.class), goodsEntity.getGoodsNum());
                    erpGoodsId = erpGoods.getId();
        } catch (Exception e) {
        }
        
        try {
            // var g = jdbcTemplate.queryForObject("SELECT * FROM erp_goods WHERE `number`=?",
            //         new BeanPropertyRowMapper<>(ErpGoodsEntity.class), goodsEntity.getGoodsNum());
            String sql = "INSERT INTO dc_tao_goods (goodsId,goodsNum,title,goodsImg,price,publishTime,erpGoodsId,shopId) value (?,?,?,?,?,?,?,?)";
            jdbcTemplate.update(sql, goodsEntity.getGoodsId(), goodsEntity.getGoodsNum(), goodsEntity.getTitle(),
                    goodsEntity.getGoodsImg(), goodsEntity.getPrice(), goodsEntity.getPublishTime(), erpGoodsId,
                    goodsEntity.getShopId());
            return new ResultVo<>(EnumResultVo.SUCCESS, "SUCCESS");

        } catch (Exception e) {
            return new ResultVo<>(EnumResultVo.SystemException, "系统异常"+e.getMessage());
        }

    }

    @Transactional
    public PagingResponse<TaoGoodsEntity> getGoodsList(Integer shopId, Integer pageIndex, Integer pageSize,
            String num) {

        List<Object> params = new ArrayList<>();
        StringBuilder sb = new StringBuilder("SELECT SQL_CALC_FOUND_ROWS  tg.*,eg.`name` as goodsName ");
        sb.append(" FROM dc_tao_goods ").append(" as tg ");
        sb.append(" LEFT JOIN erp_goods ").append(" as eg on eg.id = tg.erpGoodsId ");

        sb.append(" WHERE tg.shopId = ?  ");
        params.add(shopId);

        if (StringUtils.hasText(num)) {
            sb.append(" AND (tg.goodsId = ? OR tg.goodsNum =?) ");
            params.add(num);
            params.add(num);
        }

        sb.append(" ORDER BY tg.goodsId DESC ");

        sb.append("  LIMIT ?,?");
        params.add((pageIndex - 1) * pageSize);
        params.add(pageSize);

        var list = jdbcTemplate.query(sb.toString(), new BeanPropertyRowMapper<>(TaoGoodsEntity.class),
                params.toArray(new Object[params.size()]));

        int totalSize = getTotalSize();

        return new PagingResponse<>(pageIndex, pageSize, totalSize, list);
    }

    protected int getTotalSize() {
        return jdbcTemplate.queryForObject("SELECT FOUND_ROWS() as row_num;", int.class);
    }

}
