package com.b2c.repository;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.xhs.XhsOrderEntity;
import com.b2c.entity.xhs.XhsOrderItemEntity;
import com.b2c.entity.xhs.XhsOrderSettleEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Repository
public class XhsOrderSettleRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public ResultVo<Long> addOrderSettle(XhsOrderSettleEntity entity){
        String sql = "SELECT COUNT(0) FROM dc_xhs_order_settle WHERE orderNo=? ";
        var c = jdbcTemplate.queryForObject(sql,Integer.class,entity.getOrderNo());
        if(c == 0){
            //不存在，新增
            StringBuilder insertSQL = new StringBuilder();
            insertSQL.append("INSERT INTO dc_xhs_order_settle ");
            insertSQL.append(" (orderNo,");
            insertSQL.append(" afterSaleNo,");
            insertSQL.append(" orderCreateTime,");
            insertSQL.append(" orderSettleTime,");
            insertSQL.append(" transactionType,");
            insertSQL.append(" settleAccount,");
            insertSQL.append(" amount,");
            insertSQL.append(" settleAmount,");
            insertSQL.append(" goodsAmount,");
            insertSQL.append(" freightAmount,");
            insertSQL.append(" platformDiscount,");
            insertSQL.append(" goodsTax,");
            insertSQL.append(" freightTax,");
            insertSQL.append(" commission,");
            insertSQL.append(" paymentChannelFee,");
            insertSQL.append(" distributionCommission,");
            insertSQL.append(" huabeiFee,");
            insertSQL.append(" remark");
            insertSQL.append(" ) VALUE (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");

            jdbcTemplate.update(insertSQL.toString(),entity.getOrderNo(),entity.getAfterSaleNo(),entity.getOrderCreateTime(),entity.getOrderSettleTime(),entity.getTransactionType(),
                    entity.getSettleAccount(),entity.getAmount(),entity.getSettleAmount(),entity.getGoodsAmount(),entity.getFreightAmount(),entity.getPlatformDiscount(),
                    entity.getGoodsTax(),entity.getFreightTax(),entity.getCommission(),entity.getPaymentChannelFee(),entity.getDistributionCommission(),entity.getHuabeiFee(),
                    entity.getRemark());

            //更新订单数据
            jdbcTemplate.update("UPDATE dc_xhs_order SET settleAmount=?,settleStatus=1,orderStatus=7 WHERE orderId=?",entity.getAmount(),entity.getOrderNo());

            return new ResultVo<>(EnumResultVo.SUCCESS, "成功");
        }else
            return new ResultVo<>(EnumResultVo.DataExist, "已存在");
    }

    protected int getTotalSize() {
        return jdbcTemplate.queryForObject("SELECT FOUND_ROWS() as row_num;", int.class);
    }

    @Transactional
    public PagingResponse<XhsOrderSettleEntity> getList( Integer pageIndex, Integer pageSize, String orderNum, String startDate, String endDate, Integer type) {
        StringBuffer sb = new StringBuffer();
        sb.append("SELECT SQL_CALC_FOUND_ROWS  * ");
        sb.append(" FROM dc_xhs_order_settle  ");

        sb.append(" WHERE 1=1 ");
        List<Object> params = new ArrayList<>();

        if (!StringUtils.isEmpty(orderNum)) {
            sb.append("AND orderNo = ? ");
            params.add(orderNum);
        }

        sb.append(" ORDER BY id DESC LIMIT ?,?");
        params.add((pageIndex - 1) * pageSize);
        params.add(pageSize);

        List<XhsOrderSettleEntity> lists = jdbcTemplate.query(sb.toString(), new BeanPropertyRowMapper<>(XhsOrderSettleEntity.class), params.toArray(new Object[params.size()]));
        int totalSize = getTotalSize();
        return new PagingResponse<>(pageIndex, pageSize, totalSize, lists);
    }
}
