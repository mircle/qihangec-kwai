package com.b2c.service;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.DailyReportEntity;
import com.b2c.repository.DataReportRepository;
import com.b2c.interfaces.DataReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DataReportServiceImpl implements DataReportService {
    @Autowired
    private DataReportRepository repository;

    @Override
    public PagingResponse<DailyReportEntity> dailyReportPageList(int pageIndex, int pageSize, String startDate, String endDate) {
        return repository.dailyReportPageList(pageIndex,pageSize,startDate,endDate);
    }
}
