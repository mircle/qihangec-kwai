package com.b2c.service.erp;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.erp.*;
import com.b2c.entity.erp.vo.ExpressInfoVo;
import com.b2c.entity.result.ResultVo;
import com.b2c.interfaces.erp.ErpSalesOrderService;
import com.b2c.repository.erp.ErpSalesOrderRepository;
import com.b2c.repository.oms.OrderConfirmRepository;
import com.b2c.entity.vo.erp.ErpSalesOrderDetailVo;
import com.b2c.entity.vo.erp.ErpSalesOrderRefundDetailVo;
import com.b2c.entity.vo.OrderRefundApplyVo;
import com.b2c.entity.vo.OrderImportDaiFaEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ErpSalesOrderServiceImpl implements ErpSalesOrderService {
    @Autowired
    private ErpSalesOrderRepository repository;
    @Autowired
    private OrderConfirmRepository orderConfirmRepository;

    @Override
    public PagingResponse<ErpSalesOrderEntity> getList(int pageIndex, int pageSize, String orderNum, String contactMobile, Integer saleType, Integer clientUserId, Integer status, Integer shopId, Integer auditStatus, Integer startTime, Integer endTime) {
        return repository.getList(pageIndex, pageSize, orderNum, contactMobile, saleType, clientUserId, status,shopId,auditStatus, startTime, endTime);
    }

    @Override
    public PagingResponse<ErpSalesOrderItemView> getOrderItemList(int pageIndex, int pageSize, String orderNum, String sku, Integer clientUserId, Integer shopId, Integer startTime, Integer endTime) {
        return repository.getOrderItemList(pageIndex,pageSize,orderNum,sku,clientUserId,shopId,startTime,endTime);
    }

    @Override
    public PagingResponse<ErpSalesOrderRefundEntity> getRefundList(int pageIndex, int pageSize, String refundNum, String orderNum, String logisticsCode, Integer saleType, Integer clientUserId, Integer status, Integer startTime, Integer endTime) {
        return repository.getRefundList(pageIndex, pageSize, refundNum, orderNum, logisticsCode, saleType, clientUserId, status, startTime, endTime);
    }

    @Override
    public ErpSalesOrderDetailVo getDetailById(Long orderId) {
        return repository.getDetailById(orderId);
    }

    @Override
    public ErpSalesOrderItemEntity getOrderItemByItemId(Long orderItemId) {
        return repository.getOrderItemByItemId(orderItemId);
    }

    @Override
    public ErpSalesOrderRefundDetailVo getRefundDetailById(Long refundId) {
        return repository.getRefundDetailById(refundId);
    }

//    @Override
//    public ResultVo<String> importExcelOrderForPiPi(List<OrderImportPiPiEntity> orderList, Integer buyerUserId) {
//        return repository.importExcelOrderForPiPi(orderList, buyerUserId);
//    }

    @Override
    public ResultVo<String> importExcelOrderForDaiFa(List<OrderImportDaiFaEntity> orderList, Integer buyerUserId) {
        return repository.importExcelOrderForDaiFa(orderList, buyerUserId);
    }

    @Override
    public void updateOrderItemSkuByItemId(ErpSalesOrderItemEntity orderItem) {
        repository.updateOrderItemSkuByItemId(orderItem);
    }

    @Override
    public ResultVo<Integer> orderConfirmAndJoinDeliveryQueueForSales(Long orderId, String receiver, String mobile, String address, String sellerMemo) {
        return orderConfirmRepository.orderConfirmAndJoinDeliveryQueueForSales(orderId, receiver, mobile, address, sellerMemo);
    }

    @Override
    public void cancelOrder(Long orderId) {
         orderConfirmRepository.cancelOrder(orderId);
    }

    @Override
    public ResultVo<Integer> refundApply(OrderRefundApplyVo applyVo) {
        return repository.refundApply(applyVo);
    }

    @Override
    public ResultVo<Integer> refundAgree(Long refundId, ExpressInfoVo exress) {
        return repository.refundAgree(refundId, exress);
    }

    @Override
    public ResultVo<Integer> refundRefuse(Long refundId) {
        return repository.refundRefuse(refundId);
    }

    @Override
    public ResultVo<Integer> editSalesOrder(ErpSalesOrderDetailVo salesOrder) {
        return repository.editSalesOrder(salesOrder);
    }

    @Override
    public void addErpSalesPullOrderLog(Long startTime,Long endTime,Integer shopId,Integer addCount,Integer failCount,Integer updCount,Integer type) {
        repository.addErpSalesPullOrderLog(startTime,endTime,shopId,addCount,failCount,updCount,type);
    }
    @Override
    public ErpPullOrderLogEntity getErpOrderPullLogByShopId(Integer shopId, Integer type) {
        return repository.getErpOrderPullLogByShopId(shopId,type);
    }
    @Override
    public ResultVo<Long> updErpSalesOrderSpec(Long erpSalesOrderItemId, Integer erpGoodSpecId,Integer quantity) {
        return repository.updErpSalesOrderSpec(erpSalesOrderItemId,erpGoodSpecId,quantity);
    }
    @Override
    public void addNotifyMsg(String jsonStr, Integer shopId) {
        repository.addNotifyMsg(jsonStr,shopId);
    }

    @Override
    public void addGift(Long orderId,Integer erpGoodsId, Integer erpGoodSpecId, Integer quantity) {
        repository.addGift(orderId,erpGoodsId,erpGoodSpecId,quantity);
    }

    @Override
    public void deleteGift(Long orderId, Long orderItemId, Integer erpGoodSpecId) {
        repository.deleteGift(orderId, orderItemId, erpGoodSpecId);
    }

    @Override
    public Long getErpSalesOrderId(String orderNum, Integer shopId) {
        return repository.getErpSalesOrderId(orderNum,shopId);
    }

    @Override
    public ResultVo<Long> importErpSalesorder(List<ErpSalesOrderDetailVo> orders) {
        return repository.importErpSalesorder(orders);
    }

    @Override
    public PagingResponse<ErpSalesOrderDetailVo> erpSalesOrderDetails(int pageIndex, int pageSize, Integer shopId, String orderNum, Integer startTime, Integer endTime){
        return repository.erpSalesOrderDetails(pageIndex,pageSize,shopId,orderNum,startTime,endTime);
    }

    @Override
    public void delOrderItemSkuByItemId(Long orderId, Long itemId) {
        repository.delOrderItemSkuByItemId(orderId,itemId);
    }
}
