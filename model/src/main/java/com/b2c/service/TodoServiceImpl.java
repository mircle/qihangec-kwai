package com.b2c.service;

import com.b2c.interfaces.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.TodoEntity;
import com.b2c.entity.result.ResultVo;
import com.b2c.repository.TodoRepository;

@Service
public class TodoServiceImpl implements TodoService {

    @Autowired
    private TodoRepository repository;
    
    @Override
    public ResultVo<Long> addTodo(Integer shopId,Integer sourceType, String sourceId, String content) {
      
        return repository.addTodo(shopId,sourceType, sourceId, content);
    }

    @Override
    public PagingResponse<TodoEntity> getList(Integer pageIndex, Integer pageSize, Integer sourceType, String sourceId,
            Integer status,String content) {
      
        return repository.getList(pageIndex, pageSize, sourceType, sourceId, status,content);
    }

    @Override
    public ResultVo<Long> handle(Integer id, String result) {
        
        return repository.handle(id,result);
    }

    @Override
    public ResultVo<Long> completed(Integer id) {
      
        return repository.completed(id);
    }
    
}
