package com.b2c.service.ecom;

import com.b2c.entity.ExpressCompanyEntity;
import com.b2c.repository.ExpressCompanyRepository;
import com.b2c.interfaces.ExpressCompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-03-08 14:48
 */
@Service
public class ExpressCompanyServiceImpl implements ExpressCompanyService {
    @Autowired
    private ExpressCompanyRepository repository;

    @Override
    public List<ExpressCompanyEntity> getExpressCompany() {
        return repository.getExpressCompany();
    }

    @Override
    public ExpressCompanyEntity getEntityByCode(String code) {
        if(StringUtils.isEmpty(code)) return null;

        return repository.getByCode(code);
    }

    @Override
    public String getCodeByName(String name) {
        ExpressCompanyEntity entity = repository.getByName(name);
        if (entity == null) return "";
        else return entity.getCode();
    }

    @Override
    public String getNameByCode(String code) {
        ExpressCompanyEntity entity = repository.getByCode(code);
        if (entity == null) return code;
        else return entity.getName();
    }

    @Override
    public void update(String code, String name) {
        repository.update(code, name);
    }


}
