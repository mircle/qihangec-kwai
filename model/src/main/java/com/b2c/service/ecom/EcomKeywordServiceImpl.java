package com.b2c.service.ecom;

import javax.annotation.Resource;

import com.b2c.interfaces.ecom.EcomKeywordService;
import org.springframework.stereotype.Service;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ecom.KeywordDataEntity;
import com.b2c.entity.ecom.KeywordsEntity;
import com.b2c.repository.ecom.EcomKeywordRepository;

@Service
public class EcomKeywordServiceImpl implements EcomKeywordService {
    @Resource
    private EcomKeywordRepository repository;

    @Override
    public PagingResponse<KeywordsEntity> getList(Integer pageIndex, Integer pageSize, String category,String category2, String platform,
            String source,String keyword) {
        
        return repository.getList(pageIndex, pageSize, category,category2, platform, source, keyword);
    }

    @Override
    public void addKeyWord(KeywordsEntity entity,KeywordDataEntity dataEntity) {
       repository.addKeyWord(entity,dataEntity);
    }

    @Override
    public PagingResponse<KeywordDataEntity> getDataList(Integer pageIndex, Integer pageSize, Long keywordId) {
        return repository.getDataList(pageIndex, pageSize, keywordId);
    }

    @Override
    public KeywordsEntity getKeywordById(Long keywordId) {

        return repository.getKeywordById(keywordId);
    }

    @Override
    public void addKeywordData(KeywordDataEntity entity) {
        repository.addKeywordData(entity);
    }
    
}
