package com.b2c.service;

import com.b2c.interfaces.ecom.KeywordHotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.bd.KeywordHotEntity;
import com.b2c.repository.bd.KeywordHotRepository;

@Service
public class KeywordHotServiceImpl  implements KeywordHotService {
    @Autowired
    private KeywordHotRepository repository;

    @Override
    public PagingResponse<KeywordHotEntity> getList(Integer pageIndex, Integer pageSize,String keyword, String platform, String category) {
        return repository.getList(pageIndex, pageSize,keyword, platform, category);
    }

    @Override
    public void add(KeywordHotEntity entity) {
        repository.add(entity);
    }
    
}
