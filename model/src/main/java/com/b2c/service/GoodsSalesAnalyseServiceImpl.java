package com.b2c.service;

import java.util.List;

import javax.annotation.Resource;

import com.b2c.repository.GoodsSalesAnalyseRepository;
import com.b2c.entity.vo.GoodsSalesAnalyseVo;
import com.b2c.entity.vo.GoodsSpecSalesAnalyseVo;
import com.b2c.interfaces.GoodsSalesAnalyseService;
import org.springframework.stereotype.Service;

import com.b2c.entity.result.PagingResponse;

@Service
public class GoodsSalesAnalyseServiceImpl implements GoodsSalesAnalyseService {
    @Resource
    private GoodsSalesAnalyseRepository repository;

	@Override
	public PagingResponse<GoodsSalesAnalyseVo> getGoodsSalesReport(Integer pageIndex, Integer pageSize, Integer shopId,
																   String goodsNum) {


		return repository.getGoodsSalesReport(pageIndex, pageSize, shopId, goodsNum);
	}

	@Override
	public List<GoodsSpecSalesAnalyseVo> getGoodsSpecSalesReport(Integer goodsId) {
		return repository.getGoodsSpecSalesReport(goodsId);
	}

	@Override
	public List<GoodsSalesAnalyseVo> getGoodsSalesReportPdd(Integer shopId) {
	
		return repository.getGoodsSalesReportPdd(shopId);
	}
    
}
