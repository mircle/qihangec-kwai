package com.b2c.service.tui;

import java.util.List;

import com.b2c.interfaces.wms.OrderSendReturnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.tui.OrderSendReturnEntity;
import com.b2c.repository.tui.OrderSendReturnRepository;


@Service
public class OrderSendReturnServiceImpl implements OrderSendReturnService {
    @Autowired
    private OrderSendReturnRepository repository;

    @Override
    public PagingResponse<OrderSendReturnEntity> getList(int pageIndex, int pageSize, String orderNum,
            String logisticsCode, Integer status) {

        return repository.getList(pageIndex, pageSize, orderNum, logisticsCode, status);
    }

    @Override
    public List<OrderSendReturnEntity> getListByIds(String ids) {
        
        return repository.getListByIds(ids);
    }

    @Override
    public ResultVo<Integer> remark(Long id, String remark) {
        
        return repository.remark(id, remark);
    }

    @Override
    public ResultVo<Integer> returnOrderStockBadIn(Long returnId, Integer quantity, String billNo) {
        return repository.returnOrderStockBadIn(returnId, quantity, billNo);
    }

    @Override
    public ResultVo<Integer> returnOrderHuanhuoSend(Long returnId, String logisticsCode) {
   
        return repository.returnOrderHuanhuoSend(returnId, logisticsCode);
    }
}
