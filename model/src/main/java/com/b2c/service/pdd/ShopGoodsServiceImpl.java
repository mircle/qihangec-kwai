package com.b2c.service.pdd;

import java.util.List;

import com.b2c.interfaces.pdd.ShopGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.erp.vo.ShopGoodsSkuLinkErpSkuVo;
import com.b2c.entity.pdd.ShopGoodsEntity;
import com.b2c.entity.pdd.ShopGoodsSkuEntity;
import com.b2c.entity.result.ResultVo;
import com.b2c.repository.pdd.PddGoodsRepository;

@Service
public class ShopGoodsServiceImpl  implements ShopGoodsService {
    @Autowired
    private PddGoodsRepository repository;
    
    @Override
    public ResultVo<Long> addGoods(ShopGoodsEntity goodsEntity) {
        return repository.addGoods(goodsEntity);
    }

    @Override
    public PagingResponse<ShopGoodsEntity> getGoodsList(Integer shopType,Integer shopId,Integer pageIndex, Integer pageSize, String num, Long goodsId,
            Integer isOnsale) {

        return repository.getGoodsList(shopType,shopId,pageIndex, pageSize, num, goodsId, isOnsale);
    }

    @Override
    public ResultVo<Integer> linkErpGoodsSpec(Long pddSkuId, String erpCode) {
        return repository.linkErpGoodsSpec(pddSkuId, erpCode);
    }

    @Override
    public ResultVo<Long> updatePublishTime(Long id, String publishTime) {
    
        return repository.updatePublishTime(id,publishTime);
    }

    @Override
    public ResultVo<Long> updateRemark(Long id, String remark) {
      
        return repository.updateRemark(id, remark);
    }

    @Override
    public ResultVo<Long> updateIsOnsale(Long id) {
        return repository.updateIsOnsale(id);
    }

    @Override
    public ResultVo<Long> updateTotalSales(Long id, Integer totalSales) {
        return repository.updateTotalSales(id, totalSales);
    }

    @Override
    public ShopGoodsEntity getGoodsById(Long goodsId) {

        return repository.getGoodsById(goodsId);
    }

    @Override
    public List<ShopGoodsSkuEntity> getGoodsSkuListByGoodsId(Long goodsId) {

        return repository.getGoodsSkuListByGoodsId(goodsId);
    }

    @Override
    public ResultVo<Integer> upGoodsNumById(Long goodsId, String goodsNum) {

        return repository.upGoodsNumById(goodsId, goodsNum);
    }

    @Override
    public ResultVo<Integer> linkErpSkuAll(Long goodsId, List<ShopGoodsSkuLinkErpSkuVo> skuLinkList) {
        return repository.linkErpSkuAll(goodsId, skuLinkList);
    }
    
}
