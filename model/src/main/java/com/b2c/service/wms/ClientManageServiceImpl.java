package com.b2c.service.wms;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ErpContactAddressEntity;
import com.b2c.entity.ErpContactEntity;
import com.b2c.entity.erp.vo.ErpContactAddressVo;
import com.b2c.repository.erp.ClientManageRepository;
import com.b2c.interfaces.wms.ClientManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientManageServiceImpl implements ClientManageService {
    @Autowired
    private ClientManageRepository repository;

    @Override
    public PagingResponse<ErpContactEntity> getClientList(Integer pageIndex, Integer pageSize, String name, String mobile,Integer type,Integer category) {
        return repository.getClientList(pageIndex,pageSize,name,mobile,type,category);
    }

    @Override
    public ErpContactEntity getClientById(Integer id) {
        return repository.getClientById(id);
    }

    @Override
    public Integer addManage(ErpContactEntity entity) {
        return repository.addManage(entity);
    }

    @Override
    public Integer updManage(ErpContactEntity entity) {
        return repository.updManage(entity);
    }

    @Override
    public Integer delManage(Integer id) {
        return repository.delManage(id);
    }


    @Override
    public ErpContactAddressVo getErpContactAddress(Integer contactId) {
        return repository.getErpContactAddress(contactId);
    }

    @Override
    public Integer updAddress(int userId, String consignee, String mobile, String[] areaNameArray, String address) {
        return repository.updAddress(userId,consignee,mobile,areaNameArray,address);
    }

    @Override
    public ErpContactAddressEntity getAddressIn(Integer contactId) {
        return repository.getAddressIn(contactId);
    }

    @Override
    public Integer addAddressIn(Integer userId, String consignee, String mobile, String[] areaNameArray, String[] areaCodeArray, String address) {
        return repository.addAddressIn(userId,consignee,mobile,areaNameArray,areaCodeArray,address);
    }

    @Override
    public Integer updAddressIn(Integer addressId, String consignee, String mobile, String[] areaNameArray, String[] areaCodeArray, String address) {
        return repository.updAddressIn(addressId,consignee,mobile,areaNameArray,areaCodeArray,address);
    }

    @Override
    public Integer delAddressIn(Integer addressId) {
        return repository.delAddressIn(addressId);
    }

    @Override
    public void addContact(Integer categoryId, String name, String place, String contact, String address, String remark) {

        repository.addContact(categoryId, name, place, contact, address, remark);
    }
}
