package com.b2c.service.wms;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.datacenter.ErpGoodsExprotVo;
import com.b2c.entity.erp.vo.ErpGoodsSpecListVo;
import com.b2c.entity.erp.vo.ErpGoodsSpecStockListVo;
import com.b2c.repository.oms.DataCenterInvoiceRepository;
import com.b2c.interfaces.wms.DataInvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DataInvoiceServiceImpl implements DataInvoiceService {
    @Autowired
    DataCenterInvoiceRepository repository;

    @Override
    public PagingResponse<ErpGoodsSpecListVo> getSpecInventoryList(int pageIndex, int pageSize, String goodsNum, String goodsSpecNum) {
        return repository.getSpecInventoryList(pageIndex,pageSize,goodsNum,goodsSpecNum);
    }

    @Override
    public List<ErpGoodsExprotVo> getInvoiceList(String str) {
        return repository.getSpecInventoryList(str);
    }

    @Override
    public PagingResponse<ErpGoodsSpecStockListVo> getGoodsList(int pageIndex, int pageSize) {
        return repository.getGoodsList(pageIndex,pageSize);
    }

    @Override
    public PagingResponse<ErpGoodsSpecStockListVo> getGoodsSpecList(int pageIndex, int pageSize) {
        return repository.getGoodsSpecList(pageIndex,pageSize);
    }

    @Override
    public PagingResponse<ErpGoodsSpecStockListVo> getGoodsInvoiceList(int pageIndex, int pageSize) {
        return repository.getGoodsInvoiceList(pageIndex,pageSize);
    }
}
