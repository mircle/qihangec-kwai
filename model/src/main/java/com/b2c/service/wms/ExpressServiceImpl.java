package com.b2c.service.wms;

import com.alibaba.fastjson.JSONObject;
import com.b2c.entity.ExpressEntity;
import com.b2c.entity.result.ResultVo;
import com.b2c.repository.erp.ExpressRepository;
import com.b2c.interfaces.erp.ExpressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description: pbd add 2019/6/17 10:56
 */
@Service
public class ExpressServiceImpl implements ExpressService {
    @Autowired
    private ExpressRepository repository;

//    @Override
//    public String printImg(PrintImgVo print) {
//        return repository.printImg(print);
//    }

    /**
     * 保存打印快递面单信息
     * @param orderId
     * @param kuaidicom
     * @param kuaidinum
     * @param imgBase64
     * @return
     */
    @Override
    public ResultVo<Integer> printExpress(Long orderId,String kuaidicomName, String kuaidicom, String kuaidinum, String imgBase64) {
        return repository.printExpress(orderId,kuaidicomName,kuaidicom,kuaidinum,imgBase64);
    }

    @Override
    public ExpressEntity getExpress(String orderNum) {
        return repository.getExpress(orderNum);
    }

    @Override
    public void updExpressInfo(JSONObject obj) {
        repository.updExpressInfo(obj);
    }
}
