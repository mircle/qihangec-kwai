package com.b2c.service.wms;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ErpStocktakingInvoice;
import com.b2c.entity.ErpStocktakingInvoiceItem;
import com.b2c.repository.erp.ErpStocktakingInvoiceRepository;
import com.b2c.interfaces.wms.ErpStocktakingInvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ErpStocktakingInvoiceServiceImpl implements ErpStocktakingInvoiceService {
    @Autowired
    private ErpStocktakingInvoiceRepository repository;

    @Override
    public PagingResponse<ErpStocktakingInvoice> getList(Integer pageIndex, Integer pageSize, String No) {
        return repository.getList(pageIndex,pageSize,No);
    }

    @Override
    public void addInvoice(String billNo, String billDate, Long stockLocationId, Integer userId, String userName, List<ErpStocktakingInvoiceItem> invoiceItems) {
        repository.addInvoice(billNo,billDate,stockLocationId,userId,userName,invoiceItems);
    }

    @Override
    public void countedInvoiceItem(Long invoiceId, Integer userId, String userName, List<ErpStocktakingInvoiceItem> invoiceItems) {
        repository.countedInvoiceItem(invoiceId,userId,userName,invoiceItems);
    }

    @Override
    public ErpStocktakingInvoice getInvoiceById(Long id) {
        return repository.getInvoiceById(id);
    }

    @Override
    public List<ErpStocktakingInvoiceItem> getInvoiceItemByIid(Long iid) {
        return repository.getInvoiceItemByIid(iid);
    }

    @Override
    public void countedConfirm(Long invoiceId, Integer userId, String userName) {
        repository.countedConfirm(invoiceId,userId,userName);
    }
}
