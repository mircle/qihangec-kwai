package com.b2c.service.wms;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ErpStockInCheckoutEntity;
import com.b2c.repository.erp.ErpStockInCheckoutRepository;
import com.b2c.interfaces.wms.ErpStockInCheckoutService;
import com.b2c.entity.vo.ErpStockInCheckoutFormAddVo;
import com.b2c.entity.vo.ErpStockInCheckoutFormDetailVo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-09-29 11:45
 */
@Service
public class ErpStockInCheckoutServiceImpl implements ErpStockInCheckoutService {
    @Autowired
    private ErpStockInCheckoutRepository repository;

    @Override
    public Long addCheckoutForm(ErpStockInCheckoutFormAddVo addVo) {
        return repository.addCheckoutForm(addVo);
    }

    @Override
    public List<ErpStockInCheckoutEntity> getCheckoutForm(Long invoiceId) {
        return repository.getCheckoutForm(invoiceId);
    }

    @Override
    public PagingResponse<ErpStockInCheckoutEntity> getCheckoutList(Integer pageIndex, Integer pageSize, String number, Integer status) {
        return repository.getCheckoutList(pageIndex, pageSize, number, status);
    }

    @Override
    public ErpStockInCheckoutFormDetailVo getDetailById(Long id) {
        return repository.getDetailById(id);
    }
}
