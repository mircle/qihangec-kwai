package com.b2c.service.fahuo;

import java.util.List;

import com.b2c.interfaces.fahuo.OrderSendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.erp.vo.GoodsSearchShowVo;
import com.b2c.entity.fahuo.OrderSendDailyVo;
import com.b2c.entity.fahuo.OrderSendEntity;
import com.b2c.entity.result.ResultVo;
import com.b2c.repository.fahuo.OrderSendRepository;

@Service
public class OrderSendServiceImpl implements OrderSendService {
    @Autowired
    private OrderSendRepository repository;

    @Override
    public PagingResponse<OrderSendEntity> getList(Integer pageIndex, Integer pageSize,
            Integer sendStatus, String orderSn, String mobile, String logisticsCode,
            String orderTimeStartDate, String orderTimeEndDate, Integer shopId,Integer isSettle,Integer supplierId) {
  
        return repository.getList(pageIndex, pageSize, sendStatus, orderSn, mobile, logisticsCode, orderTimeStartDate, orderTimeEndDate, shopId,isSettle,supplierId);
    }

    @Override
    public ResultVo<Integer> orderSendTypeEdit(Long orderId, Integer supplierId) {
        return repository.orderSendTypeEdit(orderId, supplierId);
    }

    @Override
    public ResultVo<Integer> orderHandExpress(Long orderSendId, String company, String companyCode, String code) {
        return repository.orderHandExpress(orderSendId, company, companyCode, code);
    }

    @Override
    public Integer pullOrderLogisticsPdd() {
        
        return repository.pullOrderLogisticsPdd();
    }

    @Override
    public List<GoodsSearchShowVo> getItemListByOrderIds(String orderIds) {

        return repository.getItemListByOrderIds(orderIds);
    }

    @Override
    public OrderSendEntity getOrderAndItemsById(Long id) {
        return repository.getOrderAndItemsById(id);
    }

    @Override
    public ResultVo<Integer> orederCancel(Long id, String remark) {
        return repository.orederCancel(id, remark);
    }

    @Override
    public List<OrderSendDailyVo> getSendReport(Integer shopId,String startDate,String endDate) {
       
        return repository.getSendReport(shopId,startDate,endDate);
    }

    @Override
    public List<OrderSendEntity> getList(Integer sendStatus, Integer supplierId,Integer isSettle) {

        return repository.getList(sendStatus, supplierId, isSettle);
    }

    @Override
    public ResultVo<Integer> updOrderRemark(Long orderId, String remark) {
        return repository.updOrderRemark(orderId, remark);
    }

   
    
}
