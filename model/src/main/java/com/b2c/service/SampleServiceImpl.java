package com.b2c.service;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.SampleListEntity;
import com.b2c.interfaces.SampleService;
import com.b2c.repository.SampleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SampleServiceImpl implements SampleService {
    @Autowired
    private SampleRepository sampleRepository;

    @Override
    public PagingResponse<SampleListEntity> getSampleList(Integer pageIndex, Integer pageSize, String goodsNum, String orderDate, Integer zbjId) {
        return sampleRepository.getSampleList(pageIndex,pageSize,goodsNum,orderDate,zbjId);
    }
}
