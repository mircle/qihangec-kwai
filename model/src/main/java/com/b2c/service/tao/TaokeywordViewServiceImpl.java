package com.b2c.service.tao;

import com.b2c.interfaces.tao.TaokeywordViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.tao.TaokeywordViewEntity;
import com.b2c.repository.tao.TaokeywordViewRepository;

@Service
public class TaokeywordViewServiceImpl implements TaokeywordViewService {
    @Autowired
    private TaokeywordViewRepository repository;

    @Override
    public Integer addKeyword(Integer shopId, String keyword, String source, Long goodsId, Integer views, String date) {
   
        return repository.addKeyword(shopId, keyword, source, goodsId, views, date);
    }

    @Override
    public PagingResponse<TaokeywordViewEntity> getList(Integer shopId, Integer pageIndex, Integer pageSize,
            Long goodsId, String keyword,String date) {
      
        return repository.getList(shopId, pageIndex, pageSize, goodsId, keyword,date);
    }
    
}
