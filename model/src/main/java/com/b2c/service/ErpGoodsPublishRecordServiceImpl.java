package com.b2c.service;

import com.b2c.interfaces.ErpGoodsPublishRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ErpGoodsPublishRecordEntity;
import com.b2c.repository.ErpGoodsPublishRecordRepository;
@Service
public class ErpGoodsPublishRecordServiceImpl implements ErpGoodsPublishRecordService {
    @Autowired
    private ErpGoodsPublishRecordRepository recordRepository;

    @Override
    public PagingResponse<ErpGoodsPublishRecordEntity> getList(int pageIndex, int pageSize,Integer shopId,String goodsNum) {
        
        return recordRepository.getList(pageIndex, pageSize,shopId,goodsNum);
    }

    @Override
    public ErpGoodsPublishRecordEntity getEntityById(Integer id) {
        
        return recordRepository.getEntityById(id);
    }

    @Override
    public void add(ErpGoodsPublishRecordEntity entity) {
        
        recordRepository.add(entity);
    }

    @Override
    public void edit(ErpGoodsPublishRecordEntity entity) {
        recordRepository.edit(entity);
    }

    @Override
    public void publishToShop(Integer goodsSupplierId, String shopIds, String publishDate) {
        recordRepository.publishToShop(goodsSupplierId, shopIds, publishDate);
    }


}
