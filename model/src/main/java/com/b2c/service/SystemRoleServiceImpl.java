package com.b2c.service;

import com.b2c.repository.mall.SystemRoleRepository;
import com.b2c.interfaces.SystemRoleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description: pbd add 2019/3/19 11:02
 */
@Service
public class SystemRoleServiceImpl implements SystemRoleService {
    @Autowired
    private SystemRoleRepository systemRoleRepository;


    @Override
    public void addManageUserLogin(Integer userId, String loginIp) {
        systemRoleRepository.addManageUserLogin(userId, loginIp);
    }


}
