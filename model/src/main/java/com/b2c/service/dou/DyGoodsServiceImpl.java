package com.b2c.service.dou;

import com.b2c.interfaces.DyGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.douyin.DyGoodsEntity;
import com.b2c.entity.result.ResultVo;
import com.b2c.repository.dy.DyGoodsRepository;


@Service
public class DyGoodsServiceImpl implements DyGoodsService {

    @Autowired
    private DyGoodsRepository repository;

    @Override
    public ResultVo<Long> addGoods(DyGoodsEntity goodsEntity) {
     
        return repository.addGoods(goodsEntity);
    }

    @Override
    public PagingResponse<DyGoodsEntity> getGoodsList(Integer shopId, Integer pageIndex, Integer pageSize,
            String num) {
        return repository.getGoodsList(shopId, pageIndex, pageSize, num);
    }
    
}
