package com.b2c.erp;

import com.b2c.entity.vo.WmsManageUserMenuVo;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-11-08 13:58
 */
public class DataConfigObject {
    //创建 SingleObject 的一个对象
    private static DataConfigObject instance = new DataConfigObject();
    private String jwtSecret = "";
    private Integer pageSize = 10;
    private String sysName = "电商ERP系统";
    private Integer loginUserId = 0;
    private String loginUserName = "";
    private Integer loginUserGroupId = 0;
    private String pddClientId = "";
    private String pddClientSecret = "";
    private String jdbcURL = null;
    private String jdbcUSER = null;
    private String jdbcPASSWORD = null;

    public String getPddCallbackUrl() {
        return pddCallbackUrl;
    }

    private String pddCallbackUrl = null;

    List<WmsManageUserMenuVo> menuList = new ArrayList<>();


    //让构造函数为 private，这样该类就不会被实例化
    private DataConfigObject() {
        try {
            Properties properties = PropertiesLoaderUtils.loadAllProperties("config.properties");
            pageSize = Integer.parseInt(properties.getProperty("pageSize"));
            jwtSecret = properties.getProperty("jwt_secret");
            pddClientId = properties.getProperty("pdd_client_id");
            pddClientSecret = properties.getProperty("pdd_client_secret");
            pddCallbackUrl = properties.getProperty("pdd_callback_url");
            Properties properties1 = PropertiesLoaderUtils.loadAllProperties("application.properties");
            jdbcURL =  properties1.getProperty("spring.datasource.url");
            jdbcUSER =  properties1.getProperty("spring.datasource.username");
            jdbcPASSWORD =  properties1.getProperty("spring.datasource.password");

        } catch (Exception e) {
            pageSize = 10;
        }
    }
        
    public String getJdbcURL() {
        return jdbcURL;
    }

    public String getJdbcUSER() {
        return jdbcUSER;
    }

    public String getJdbcPASSWORD() {
        return jdbcPASSWORD;
    }

    public String getPddClientId(){
        return pddClientId;
    }

    public String getPddClientSecret(){
        return pddClientSecret;
    }

    public Integer getLoginUserGroupId() {
        return loginUserGroupId;
    }

    public void setLoginUserGroupId(Integer loginUserGroupId) {
        this.loginUserGroupId = loginUserGroupId;
    }

    public Integer getLoginUserId() {
        return loginUserId;
    }

    public void setLoginUserId(Integer loginUserId) {
        this.loginUserId = loginUserId;
    }

    public String getLoginUserName() {
        return loginUserName;
    }

    public void setLoginUserName(String loginUserName) {
        this.loginUserName = loginUserName;
    }

    public List<WmsManageUserMenuVo> getMenuList() {
        return menuList;
    }

    public void setMenuList(List<WmsManageUserMenuVo> menuList) {
        this.menuList = menuList;
    }

    //获取唯一可用的对象
    public static DataConfigObject getInstance() {
        return instance;
    }
    public String getJwtSecret() {
        return jwtSecret;
    }
    public Integer getPageSize() {
        return pageSize;
    }
    public String getSysName() {return sysName;}
}
