package com.b2c.erp.response;

/**
 * 描述：
 * 商品分类属性response
 *
 * @author qlp
 * @date 2018-12-21 3:08 PM
 */
public class CategoryAttributeValueResp {
    private Integer id;
    private String value;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
