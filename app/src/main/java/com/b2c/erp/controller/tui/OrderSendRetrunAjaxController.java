package com.b2c.erp.controller.tui;

import jakarta.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.b2c.entity.api.ApiResult;
import com.b2c.common.utils.OrderNumberUtils;
import com.b2c.entity.DataRow;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.interfaces.wms.OrderSendReturnService;

@RequestMapping(value = "/ajax_tui")
@RestController
public class OrderSendRetrunAjaxController {
    Logger log = LoggerFactory.getLogger(OrderSendRetrunAjaxController.class);
    @Autowired
    private OrderSendReturnService returnService;

    /***
     * 
     * @return
     */
    @RequestMapping(value = "/return_stock_in", method = RequestMethod.POST)
    public ApiResult<Integer> getOrderSend(@RequestBody DataRow data , HttpServletRequest request) {
        if(StringUtils.isEmpty(data.getString("returnId")))return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "参数错误，缺少returnId");
        if(StringUtils.isEmpty(data.getString("stcokNum")))return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "参数错误，缺少仓位编码");

        // var ressult = orderService.orderHandExpress(data.getInt("id"),data.getString("company"),data.getString("companyCode"),data.getString("code"));

        
        
        return new ApiResult<>(EnumResultVo.SystemException.getIndex(),"未实现");
    }
    @RequestMapping(value = "/remark_ajax", method = RequestMethod.POST)
    public ApiResult<Integer> remark_ajax(@RequestBody DataRow data , HttpServletRequest request) {
        if(StringUtils.isEmpty(data.getString("id")))return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "参数错误，缺少id");
        if(StringUtils.isEmpty(data.getString("remark")))return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "参数错误，缺少remark");

        var result = returnService.remark(data.getLong("id"),data.getString("remark"));

        
        
        return new ApiResult<>(result.getCode(),result.getMsg());
    }
    
    
        /**
     * 退货订单商品加入不良品仓库
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/add_bad_stock", method = RequestMethod.POST)
    public ApiResult<Integer> addBadStock(@RequestBody DataRow data,HttpServletRequest request) {
   
        String billNo = "BAD" + OrderNumberUtils.getOrderIdByTime();
        Long returnId = data.getLong("returnId");
        Integer quantity= data.getInt("quantity");
        var result = returnService.returnOrderStockBadIn(returnId,quantity, billNo);

        return new ApiResult<>(result.getCode(),result.getMsg());
    }

    @RequestMapping(value = "/huanhuo_send", method = RequestMethod.POST)
    public ApiResult<Integer> huanhuoSend(@RequestBody DataRow data,HttpServletRequest request) {
   
        
        Long returnId = data.getLong("returnId");
        String code = data.getString("code");
        var result = returnService.returnOrderHuanhuoSend(returnId,code);

        return new ApiResult<>(result.getCode(),result.getMsg());
    }
}
