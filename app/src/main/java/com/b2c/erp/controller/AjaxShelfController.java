//package com.b2c.wms.controller;
//
//import com.b2c.entity.DataRow;
//import com.b2c.entity.erp.ReservoirEntity;
//import com.b2c.wms.entity.ErpStockLocationBatchAddVo;
//import com.b2c.entity.result.EnumResultVo;
//import com.b2c.service.erp.ReservoirService;
//import com.b2c.service.erp.ShelfService;
//import com.b2c.wms.response.ApiResult;
//import com.fasterxml.jackson.databind.exc.InvalidFormatException;
//import org.apache.poi.hssf.usermodel.HSSFWorkbook;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
//import org.apache.poi.ss.usermodel.Workbook;
//import org.apache.poi.xssf.usermodel.XSSFWorkbook;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.util.StringUtils;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.multipart.MultipartFile;
//
//import jakarta.servlet.http.HttpServletRequest;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.util.Iterator;
//import java.util.List;
//
//@RequestMapping("/shelf")
//@RestController
//public class AjaxShelfController {
//
//    @Autowired
//    private ShelfService shelfService;
//    @Autowired
//    private ReservoirService reservoirService;
//
////    /**
////     * 添加库区
////     *
////     * @param model
////     * @param request
////     * @return
////     */
////    @RequestMapping(value = "add_shelf", method = RequestMethod.POST)
////    public ApiResult<Integer> addStockHouse(@RequestBody DataRow model, HttpServletRequest request) {
////        String number = model.getString("number");
////        String name = model.getString("name");
////        Integer houseId = model.getInt("houseId");
////        Integer reservoir = model.getInt("reservoirId");
////
////        if (StringUtils.isEmpty(number) || StringUtils.isEmpty(name))
////            return new ApiResult<>(401, "仓位编号和仓位名不能为空");
////        if (houseId == 0 || reservoir == 0)
////            return new ApiResult<>(402, "请选择仓库和库区");
////
////        if (shelfService.getIdByNumber(number) > 0) return new ApiResult<>(400, "仓位编号已存在");
////        if (shelfService.getIdByName(name) > 0) return new ApiResult<>(400, "仓位名称已存在");
////        //新增仓库
////         int result =shelfService.addShelf(number, name, houseId, reservoir);
////         if(result==-404)return new ApiResult<>(400, "仓库已存在");
////         else if(result==-405)return new ApiResult<>(400, "库区已存在");
////         else if(result ==0 ) return new ApiResult<>(500, "系统异常");
////        return new ApiResult<>(0, "新增成功");
////    }
//
////    /**
////     * 修改库区
////     *
////     * @param model
////     * @param request
////     * @return
////     */
////    @RequestMapping(value = "/update_shelf", method = RequestMethod.POST)
////    public ApiResult<Integer> updateStockHouse(@RequestBody DataRow model, HttpServletRequest request) {
////        Integer id = Integer.parseInt(model.getString("id"));
////        String number = model.getString("number");
////        String name = model.getString("name");
////        Integer store_house = model.getInt("store_house");
////        Integer reservoir = model.getInt("reservoir");
////        //if ( shelfService.getIdByNumber(number) == id) return new ApiResult<>(400,"库区编号已存在");
////        //if ( shelfService.getIdByName(name) == id) return new ApiResult<>(400,"库区名称已存在");
////
////        shelfService.updateShelf(id, number, name, store_house, reservoir);
////        return new ApiResult<>(0, "修改成功");
////    }
//
////    /**
////     * 删除库区
////     *
////     * @param id
////     * @return
////     */
////    @RequestMapping(value = "/del_shelf", method = RequestMethod.POST)
////    public ApiResult<Integer> delStockCategory(@RequestBody Object id) {
////        shelfService.delShelf((int) id);
////        return new ApiResult<>(0, "删除成功");
////    }
//
////    /**
////     * 根据仓库ID获取库区信息
////     *
////     * @param id
////     * @return
////     */
////    @RequestMapping(value = "/reservoir", method = RequestMethod.POST)
////    public ApiResult<List> getReservoir(@RequestBody Object id) {
////        List<ReservoirEntity> list = reservoirService.getIdByHouseId((int) id);
////        return new ApiResult<>(0, "修改成功", list);
////    }
//
//    /**
//     * 批量导入仓位信息(excel)
//     *
//     * @param file
//     * @param request
//     * @return
//     * @throws IOException
//     * @throws InvalidFormatException
//     */
//    @RequestMapping(value = "/shelf_excel_import", method = RequestMethod.POST)
//    public ApiResult<Integer> shelfImportExcel(@RequestParam("excel") MultipartFile file, HttpServletRequest request) throws IOException, InvalidFormatException {
//        int totalSuccess = 0;
//        int totalError = 0;
//        String fileName = file.getOriginalFilename();
//        String dir = System.getProperty("user.dir");
//        String destFileName = dir + File.separator + "uploadedfiles_" + fileName;
//        System.out.println(destFileName);
//        File destFile = new File(destFileName);
//        file.transferTo(destFile);
//
//        System.out.println("上传成功");
//        System.out.println("开始读取EXCEL内容");
//
//        Workbook workbook = null;
//        InputStream fis = null;
//
////        var shelfList = new ArrayList<ErpStockLocationBatchAddVo>();
//
//
//        try {
//            fis = new FileInputStream(destFileName);
//
//            if (fileName.toLowerCase().endsWith("xlsx")) {
//                workbook = new XSSFWorkbook(fis);
//            } else if (fileName.toLowerCase().endsWith("xls")) {
//                workbook = new HSSFWorkbook(fis);
//            }
//
//            Sheet sheet = workbook.getSheetAt(0);
//
//            //得到行的迭代器
//            Iterator<Row> rowIterator = sheet.iterator();
//            int rowCount = 0;
//            //循环每一行
//            while (rowIterator.hasNext()) {
//                Row row = rowIterator.next();
//                //从第二行开始
//                if (rowCount > 0) {
//                    System.out.print("第" + (rowCount++) + "行  ");
//
//                    var excelEntity = new ErpStockLocationBatchAddVo();
//                    try {
//                        //仓库编码
//                        excelEntity.setHouseNo(row.getCell(0).getStringCellValue());
//                        //库区编码
//                        excelEntity.setReservoirNo(row.getCell(1).getStringCellValue());
//                        //仓位编码
//                        excelEntity.setNumber(row.getCell(2).getStringCellValue());
//                        //仓位名
//                        excelEntity.setName(row.getCell(2).getStringCellValue());
//
//                    } catch (Exception e) {
////                        throw e;
//                        return new ApiResult<>(505, "数据格式错误");
//                    }
//
////                    shelfList.add(excelEntity);
//                    //添加到数据库
//                    var result = shelfService.batchAddShelf(excelEntity);
//                    if (result.getCode() == EnumResultVo.SUCCESS.getIndex())
//                        totalSuccess++;
//                    else totalError++;
//                }
//                rowCount++;
//            }
//
//
//        } catch (Exception ex) {
////            workbook = new HSSFWorkbook(fis);
//            return new ApiResult<>(500, ex.getMessage());
//        }
//
////        if (shelfList == null || shelfList.size() == 0) return new ApiResult<>(404, "没有数据");
//
//
//            return new ApiResult<>(0, "添加成功："+totalSuccess +" ，添加失败："+totalError);
//
//    }
//
//
//}
