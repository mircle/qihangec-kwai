package com.b2c.erp.controller.ecom;

import java.math.BigDecimal;

import jakarta.servlet.http.HttpServletRequest;

import com.b2c.erp.controller.CommonControllerUtils;
import com.b2c.interfaces.WmsUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.b2c.entity.api.ApiResult;
import com.b2c.entity.DataRow;
import com.b2c.entity.ecom.GoodsStyleDataEntity;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.interfaces.ecom.GoodsStyleDataService;

@RequestMapping("/ecom")
@Controller
public class GoodsStyleDataController {
    private static Logger log = LoggerFactory.getLogger(GoodsStyleDataController.class);
    @Autowired
    private GoodsStyleDataService service;
    @Autowired
    private WmsUserService manageUserService;
    @RequestMapping(value = "/goods_style_data_list", method = RequestMethod.GET)
    public String list(Model model, HttpServletRequest request) {
  
        
        String page = request.getParameter("page");
        Integer pageIndex = 1;
        Integer pageSize = 50;
        String num = "";

        String platform = null;

        if (!StringUtils.isEmpty(page)) {
            pageIndex = Integer.parseInt(page);
        }

        if (!StringUtils.isEmpty(request.getParameter("num"))) {
            num = request.getParameter("num");
            model.addAttribute("num",num);
        }
        if (!StringUtils.isEmpty(request.getParameter("platform"))) {
            platform = request.getParameter("platform");
            model.addAttribute("platform",platform);
        }

       

        var result = service.getList(pageIndex,pageSize,num,platform);
        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("totalSize", result.getTotalSize());
        model.addAttribute("lists", result.getList());

//        model.addAttribute("view", "ecomdata");
//        model.addAttribute("pView", "sale");
        CommonControllerUtils.setViewKey(model,manageUserService,"ecomdata");
        return "ecom/goods_style_data_list";
    }

    @RequestMapping(value = "/goods_style_data_add", method = RequestMethod.GET)
    public String add(Model model, HttpServletRequest request) {
        return "ecom/goods_style_data_add";
    }

    @ResponseBody
    @RequestMapping(value = "/goods_style_data_add_ajax", method = RequestMethod.POST)
    public  ApiResult<String> addPostAjax(Model model, @RequestBody DataRow data, HttpServletRequest request) {
        GoodsStyleDataEntity kw = new GoodsStyleDataEntity();
        kw.setGoodsId(data.getString("goodsId"));
        kw.setGoodsNum(data.getString("goodsNum"));
        kw.setImg( data.getString("goodsImg"));
        kw.setPlatform(data.getString("platform"));
        kw.setStartDate(data.getString("startDate"));
        kw.setEndDate(data.getString("endDate"));
        kw.setRemark(data.getString("remark"));
        kw.setCtr(new BigDecimal(data.getDouble("ctr")));
        kw.setCvr(new BigDecimal(data.getDouble("cvr")));
        kw.setCollects(data.getInt("collects"));
        kw.setOrders(data.getInt("orders"));
        service.add(kw);
        //keyWordService.addKeyWord(kw);
        return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(), "SUCCESS");
    }
}
