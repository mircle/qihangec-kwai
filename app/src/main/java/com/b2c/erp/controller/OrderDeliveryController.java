package com.b2c.erp.controller;

import com.b2c.entity.result.PagingResponse;
import com.b2c.common.utils.DateUtil;
import com.b2c.interfaces.erp.ErpOrderService;
import com.b2c.erp.DataConfigObject;
import com.b2c.entity.enums.EnumErpOrderSendStatus;
import com.b2c.entity.vo.order.OrderWaitSendListVo;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import jakarta.servlet.http.HttpServletRequest;
 import jakarta.servlet.http.HttpServletResponse;
import javax.swing.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

@RequestMapping("/order_delivery")
@Controller
public class OrderDeliveryController {
    @Autowired
    private ErpOrderService orderService;
    /**
     * 待拣货的订单列表
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/wait_pick_order_list", method = RequestMethod.GET)
    public String getOrderList(Model model, HttpServletRequest request) {
        Integer flag = 0; //0订单总览页面,1发货单页面
        String page = request.getParameter("page");
        Integer pageIndex = 1, pageSize = DataConfigObject.getInstance().getPageSize();
        String orderNum = "";
        String mjMobile = "";
        Integer shopId = null;
        Integer startTime = 0;
        Integer endTime = 0;

        if (!StringUtils.isEmpty(page)) {
            pageIndex = Integer.parseInt(page);
        }
        if (!StringUtils.isEmpty(request.getParameter("orderNum"))) {
            orderNum = request.getParameter("orderNum");
        }
        if (!StringUtils.isEmpty(request.getParameter("shopId"))) {
            shopId = Integer.parseInt(request.getParameter("shopId"));
        }
        if (!StringUtils.isEmpty(request.getParameter("mjMobile"))) {
            mjMobile = request.getParameter("mjMobile");
        }
        if (!StringUtils.isEmpty(request.getParameter("startTime"))) {
            startTime = DateUtil.dateToStamp(request.getParameter("startTime"));
        }
        if (!StringUtils.isEmpty(request.getParameter("endTime"))) {
            endTime = DateUtil.dateToStamp(request.getParameter("endTime" + " 23:59:59"));
        }

        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);

        PagingResponse<OrderWaitSendListVo> result = orderService.getWaitSendOrderList(pageIndex, pageSize, EnumErpOrderSendStatus.WaitOut, orderNum, mjMobile, "", startTime, endTime,null,shopId);
        model.addAttribute("totalSize", result.getTotalSize());
        model.addAttribute("lists", result.getList());


        model.addAttribute("view", "djhorder");
        model.addAttribute("pView", "ck");
        model.addAttribute("ejcd", "销售");
        model.addAttribute("sjcd", "待拣货订单");
        model.addAttribute("pageTitle", "待拣货订单");

        return "order_wait_pick_list";
    }

    @RequestMapping(value = "/order_return_djh_export", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public void outExport(HttpServletRequest request, HttpServletResponse response) {
        Integer startTime = null;
        Integer endTime = null;
        if (!StringUtils.isEmpty(request.getParameter("startTime"))) {
            startTime = DateUtil.dateToStamp(request.getParameter("startTime"));
        }
        if (!StringUtils.isEmpty(request.getParameter("endTime"))) {
            String endDateStr = request.getParameter("endTime") + " 23:59:59";
            endTime = DateUtil.dateTimeToStamp(endDateStr);
        }
        var list = orderService.orderWaitSendListExport(startTime,endTime);

        String excelFileName = "";//excel文件名前缀

        //创建Excel工作薄对象
        HSSFWorkbook workbook = new HSSFWorkbook();

        //创建Excel工作表对象
        HSSFSheet sheet = workbook.createSheet();
        HSSFRow row = null;
        HSSFCell cell = null;
        //第一行为空
        row = sheet.createRow(0);

        cell = row.createCell(0);
        cell.setCellValue("订单编号");


        cell = row.createCell(1);
        cell.setCellValue("SKU");

        cell = row.createCell(2);
        cell.setCellValue("规格");

        cell = row.createCell(3);
        cell.setCellValue("数量");

        int currRowNum = 0;

        for (int i = 0; i < list.size(); i++) {
            currRowNum++;
            var item =list.get(i);
            row = sheet.createRow(currRowNum);

            //订单编号
            cell = row.createCell(0);
            cell.setCellValue(item.getOrderNum());

            cell = row.createCell(1);
            cell.setCellValue(item.getSkuNumber());

            cell = row.createCell(2);
            cell.setCellValue(item.getColorValue()+item.getSizeValue());

            cell = row.createCell(3);
            cell.setCellValue(item.getQuantity());

        }
        response.reset();
        response.setContentType("application/msexcel;charset=UTF-8");
        try {
            SimpleDateFormat newsdf = new SimpleDateFormat("yyyyMMdd");
            String date = newsdf.format(new Date());
            response.addHeader("Content-Disposition", "attachment;filename=\""
                    + new String((excelFileName + date + ".xls").getBytes("GBK"),
                    "ISO8859_1") + "\"");
            OutputStream out = response.getOutputStream();
            workbook.write(out);
            out.flush();
            out.close();
        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(null, "导出失败!");
            e.printStackTrace();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "导出失败!");
            e.printStackTrace();
        }

        return;

    }
}
