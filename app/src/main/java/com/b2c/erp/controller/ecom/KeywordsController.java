// package com.b2c.erp.controller.ecom;

// import com.alibaba.fastjson.JSONObject;
// import com.b2c.entity.result.PagingResponse;
// import com.b2c.common.api.ApiResult;
// import com.b2c.common.utils.DateUtil;
// import com.b2c.entity.DataRow;
// import com.b2c.entity.KeyWordEntity;
// import com.b2c.entity.ecom.KeywordsEntity;
// import com.b2c.entity.erp.ErpSalesOrderEntity;
// import com.b2c.entity.result.EnumResultVo;
// import com.b2c.entity.zbj.LiveDataEntity;
// import com.b2c.erp.DataConfigObject;
// import com.b2c.entity.ErpContactEntity;
// import com.b2c.interfaces.ecom.KeyWordService;
// import com.b2c.interfaces.ecom.EcomKeywordService;

// import org.apache.poi.xssf.usermodel.XSSFRow;
// import org.apache.poi.xssf.usermodel.XSSFSheet;
// import org.apache.poi.xssf.usermodel.XSSFWorkbook;
// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.stereotype.Controller;
// import org.springframework.ui.Model;
// import org.springframework.util.StringUtils;
// import org.springframework.web.bind.annotation.*;
// import org.springframework.web.multipart.MultipartFile;
// import org.yaml.snakeyaml.util.UriEncoder;

// import jakarta.servlet.http.HttpServletRequest;
// import java.io.File;
// import java.io.FileInputStream;
// import java.io.IOException;
// import java.io.InputStream;
// import java.math.BigDecimal;

// @RequestMapping("/ecom")
// @Controller
// public class KeywordsController {
//     @Autowired
//     private EcomKeywordService keywordService;
//     private static Logger log = LoggerFactory.getLogger(KeywordsController.class);
//     /**
//      * 订单列表
//      *
//      * @param model
//      * @param request
//      * @return
//      */
//     @RequestMapping(value = "/keyword_list", method = RequestMethod.GET)
//     public String list(Model model, HttpServletRequest request) {
//         String page = request.getParameter("page");
//         Integer pageIndex = 1;
//         Integer pageSize = 100;
//         String category = "";
//         String category2 = "";
//         String source = "";
//         String keyword = "";
//         String platform = null;

//         if (!StringUtils.isEmpty(page)) {
//             pageIndex = Integer.parseInt(page);
//         }

//         if (!StringUtils.isEmpty(request.getParameter("keyword"))) {
//             keyword = request.getParameter("keyword");
//             model.addAttribute("keyword",keyword);
//         }
//         if (!StringUtils.isEmpty(request.getParameter("platform"))) {
//             platform = request.getParameter("platform");
//             model.addAttribute("platform",platform);
//         }

//         if (!StringUtils.isEmpty(request.getParameter("source"))) {
//             source = request.getParameter("source");
//             model.addAttribute("source", source);
//         }
//         if (!StringUtils.isEmpty(request.getParameter("category"))) {
//             category = request.getParameter("category");
//             model.addAttribute("category", category);
//         }
//         if (!StringUtils.isEmpty(request.getParameter("category2"))) {
//             category2 = request.getParameter("category2");
//             model.addAttribute("category2", category2);
//         }
        
        

//         var result = keywordService.getList(pageIndex,pageSize,category,category2,platform,source,keyword);
//         model.addAttribute("pageIndex", pageIndex);
//         model.addAttribute("pageSize", pageSize);
//         model.addAttribute("totalSize", result.getTotalSize());
//         model.addAttribute("lists", result.getList());

//         model.addAttribute("view", "keyword");
//         model.addAttribute("pView", "sale");
//         return "ecom/keyword_list";
//     }


//     @RequestMapping(value = "/keyword_add", method = RequestMethod.GET)
//     public String add(Model model, HttpServletRequest request) {
//         return "ecom/keyword_add";
//     }

//     @ResponseBody
//     @RequestMapping(value = "/keyword_add_ajax", method = RequestMethod.POST)
//     public  ApiResult<String> addPostAjax(Model model, @RequestBody DataRow data, HttpServletRequest request) {
//         KeywordsEntity kw = new KeywordsEntity();
//         kw.setCategory2(data.getString("category2"));
//         kw.setCategory(data.getString("category"));
//         kw.setKeyword( data.getString("keyword"));
//         kw.setPlatform(data.getString("platform"));
//         kw.setSource(data.getString("source"));
//         kw.setRemark(data.getString("remark"));
//         keywordService.addKeyWord(kw);
//         //keyWordService.addKeyWord(kw);
//         return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(), "SUCCESS");
//     }





// }
