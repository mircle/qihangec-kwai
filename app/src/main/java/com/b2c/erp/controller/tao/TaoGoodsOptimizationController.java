package com.b2c.erp.controller.tao;

import java.util.List;

import jakarta.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.b2c.entity.api.ApiResult;
import com.b2c.entity.DataRow;
import com.b2c.entity.datacenter.DcShopEntity;
import com.b2c.erp.DataConfigObject;
import com.b2c.interfaces.ShopService;
import com.b2c.interfaces.tao.TaoGoodsUpgradeService;

@RequestMapping("/tao")
@Controller
public class TaoGoodsOptimizationController {
    @Autowired
    private ShopService shopService;
    @Autowired
    private TaoGoodsUpgradeService goodsUpgradeService;
    
    @RequestMapping("/goods_optimization_record")
    public String goodsList(Model model, HttpServletRequest request){
        Integer userId = DataConfigObject.getInstance().getLoginUserId();
        //有权限的店铺列表
        List<DcShopEntity> shops = shopService.getShopListByUserId(userId,4);//type=5是拼多多
        model.addAttribute("shops", shops);
        Integer shopId=null;
        if (!StringUtils.isEmpty(request.getParameter("shopId"))){
            shopId = Integer.parseInt(request.getParameter("shopId")); 
            model.addAttribute("shopId",shopId);
        }


        String goodsId="";
        if (!StringUtils.isEmpty(request.getParameter("goodsId"))) {
            goodsId = request.getParameter("goodsId");
            model.addAttribute("goodsId", goodsId);
        }


        Integer pageIndex = 1, pageSize = DataConfigObject.getInstance().getPageSize();
        if (!StringUtils.isEmpty(request.getParameter("page"))) {
            pageIndex = Integer.parseInt(request.getParameter("page"));
        }
       
        var result = goodsUpgradeService.getList(shopId,pageIndex,pageSize,goodsId);

        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("totalSize", result.getTotalSize());
        model.addAttribute("lists", result.getList());

        model.addAttribute("view", "taoshop");
        model.addAttribute("pView", "sale");

        return "tao/goods_optimization_record";
    }

    

    @RequestMapping(value = "/goods_optimization_record_add", method = RequestMethod.GET)
    public String add(Model model, HttpServletRequest request) {
        Integer shopId =6;

        if(StringUtils.hasText(request.getParameter("shopId")))
        {
            try {
                shopId = Integer.parseInt(request.getParameter("shopId"));
            } catch (Exception e) {
            }
        }
        model.addAttribute("shopId", shopId);


        return "tao/goods_optimization_record_add";
    }

    @ResponseBody
    @RequestMapping(value = "/goods_optimization_record_add_ajax", method = RequestMethod.POST)
    public  ApiResult<String> addPostAjax(Model model, @RequestBody DataRow data, HttpServletRequest request) {
        
        Integer shopId = data.getInt("shopId");
        Long goodsId = data.getLong("goodsId");
        String oldTitle = data.getString("oldTitle");
        Integer oldSales = data.getInt("oldSales");
        String newTitle = data.getString("title");
        String remark = data.getString("remark");
        String date = data.getString("date");
        
        var result = goodsUpgradeService.addRecord(shopId,goodsId, oldTitle, oldSales, newTitle, remark, date);
        
        return new ApiResult<>(result.getCode(), result.getMsg());
    }

}
