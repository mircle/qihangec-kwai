package com.b2c.erp.controller;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.api.ApiResult;
import com.b2c.entity.ErpContactEntity;
import com.b2c.entity.DataRow;
import com.b2c.entity.erp.vo.ErpContactAddressVo;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.interfaces.wms.ClientManageService;
import com.b2c.erp.DataConfigObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import jakarta.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/contact")
public class ClientManageController {
    @Autowired
    private ClientManageService manageService;

    
    @RequestMapping(value = "/contact_list",method = RequestMethod.GET)
    public String Partners(Model model, HttpServletRequest request){
        Integer pageIndex = 1, pageSize = DataConfigObject.getInstance().getPageSize();
        if (!StringUtils.isEmpty(request.getParameter("page"))) {
            pageIndex = Integer.parseInt(request.getParameter("page"));
        }
        String name="";
        String mobile="";
        if (!StringUtils.isEmpty(request.getParameter("name"))) {
            name = request.getParameter("name");
        }
        if (!StringUtils.isEmpty(request.getParameter("mobile"))) {
            mobile = request.getParameter("mobile");
        }

        PagingResponse<ErpContactEntity> list = manageService.getClientList(pageIndex, pageSize, name, mobile,null,null);
        model.addAttribute("totalSize", list.getTotalSize());
        model.addAttribute("lists", list.getList());

        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);

        model.addAttribute("view", "contactList");
        model.addAttribute("pView", "kc");

        return "contact/contact_list";
    }

    @RequestMapping(value = "/contact_add",method = RequestMethod.GET)
    public String add(Model model, HttpServletRequest request){


        return "contact/contact_add";
    }

    @ResponseBody
    @RequestMapping(value = "/contact_add_ajax", method = RequestMethod.POST)
    public  ApiResult<String> addPostAjax(Model model, @RequestBody DataRow data, HttpServletRequest request) {

        Integer cCategory = data.getInt("cCategory");
        String name = data.getString("name");
        String place = data.getString("place");
        String contact = data.getString("contact");
        String address = data.getString("address");
        String remark = data.getString("remark");

        manageService.addContact(cCategory,name,place,contact,address, remark);
    
        return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(), "SUCCESS");
    }


    /**
     * 客户管理
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/client_list",method = RequestMethod.GET)
    public String getList(Model model, HttpServletRequest request){
        Integer pageIndex = 1, pageSize = DataConfigObject.getInstance().getPageSize();
        if (!StringUtils.isEmpty(request.getParameter("page"))) {
            pageIndex = Integer.parseInt(request.getParameter("page"));
        }
        String name="";
        String mobile="";
        if (!StringUtils.isEmpty(request.getParameter("name"))) {
            name = request.getParameter("name");
        }
        if (!StringUtils.isEmpty(request.getParameter("mobile"))) {
            mobile = request.getParameter("mobile");
        }
        int type=-10;
        PagingResponse<ErpContactEntity> list = manageService.getClientList(pageIndex, pageSize, name, mobile,type,null);
        model.addAttribute("totalSize", list.getTotalSize());
        model.addAttribute("lists", list.getList());

        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);

        model.addAttribute("view", "khgl");
        model.addAttribute("pView", "kc");
        model.addAttribute("ejcd", "设置");
        model.addAttribute("sjcd", "客户管理");
        return "/client_manage";
    }

    /**
     * 供应商管理
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/supplier_list",method = RequestMethod.GET)
    public String getprList(Model model, HttpServletRequest request){
        Integer pageIndex = 1, pageSize = 20;
        if (!StringUtils.isEmpty(request.getParameter("page"))) {
            pageIndex = Integer.parseInt(request.getParameter("page"));
        }
        String name="";
        String mobile="";
        if (!StringUtils.isEmpty(request.getParameter("name"))) {
            name = request.getParameter("name");
        }
        if (!StringUtils.isEmpty(request.getParameter("mobile"))) {
            mobile = request.getParameter("mobile");
        }
        int type=10;
        PagingResponse<ErpContactEntity> list = manageService.getClientList(pageIndex, pageSize, name, mobile,type,null);
        model.addAttribute("totalSize", list.getTotalSize());
        model.addAttribute("lists", list.getList());

        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);

        model.addAttribute("view", "gysgl");
        model.addAttribute("pView", "kc");
        model.addAttribute("ejcd", "设置");
        model.addAttribute("sjcd", "供应商管理");
        return "/provider_manage";
    }

    /**
     * 地址管理
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value = "adress_list",method = RequestMethod.GET)
    public String addressList(HttpServletRequest request,Model model){
        Integer id = Integer.parseInt(request.getParameter("id"));
        ErpContactAddressVo address = manageService.getErpContactAddress(id);
        model.addAttribute("lists",address);
        model.addAttribute("view", "spgl");
        model.addAttribute("pView", "sj");
        model.addAttribute("ejcd", "数据");
        return "/client_manage_address";
    }

    /**
     * 新增/修改地址
     */
    @RequestMapping(value = "address_edit",method = RequestMethod.POST)
    public String addressEdit(HttpServletRequest request){
        Integer userId = 0;
        if (!StringUtils.isEmpty(request.getParameter("userId")))
            userId = Integer.parseInt(request.getParameter("userId"));
        Integer addressId = 0;
        if (!StringUtils.isEmpty(request.getParameter("addressId")))
            addressId = Integer.parseInt(request.getParameter("addressId"));
        Integer type = 0;
        if (!StringUtils.isEmpty(request.getParameter("type")))
            type = Integer.parseInt(request.getParameter("type"));
        String consignee = request.getParameter("consignee");
        String mobile = request.getParameter("mobile");
        String area = request.getParameter("area");
        String areaCode = request.getParameter("areaCode");
        String address = request.getParameter("address");

        String[] areaNameArray = area.split(" ");
        String[] areaCodeArray = areaCode.split(" ");
        if(addressId > 0){
            //子地址修改
            manageService.updAddressIn(addressId,consignee,mobile,areaNameArray,areaCodeArray,address);
        }else{
            if (type==1){
                //主地址修改
                manageService.updAddress(userId,consignee,mobile,areaNameArray,address);
            }else {
                //新增子地址
                manageService.addAddressIn(userId,consignee,mobile,areaNameArray,areaCodeArray,address);
            }
        }
        return "redirect:/manage/adress_list?id="+userId;
    }
}
