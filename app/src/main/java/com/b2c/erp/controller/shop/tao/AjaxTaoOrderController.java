package com.b2c.erp.controller.shop.tao;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import jakarta.servlet.http.HttpServletRequest;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.b2c.entity.api.ApiResult;
import com.b2c.entity.DataRow;
import com.b2c.entity.datacenter.DcTmallOrderEntity;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.interfaces.tao.DcTmallOrderService;
import com.b2c.interfaces.tao.DcTmallOrderReturnService;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

@RequestMapping(value = "/ajax_tao")
@RestController
public class AjaxTaoOrderController {
    Logger log = LoggerFactory.getLogger(AjaxTaoOrderController.class);

    @Autowired
   private DcTmallOrderService tmallOrderService;
    @Autowired
   private DcTmallOrderReturnService tmallOrderReturnService;
    /***
     * 订单手动选择快递单
     * @return
     */
    @RequestMapping(value = "/order_hand_send", method = RequestMethod.POST)
    public ApiResult<Integer> getOrderSend(@RequestBody DataRow data , HttpServletRequest request) {
        if(StringUtils.isEmpty(data.getString("companyCode")))return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "请选择快递公司");
        if(StringUtils.isEmpty(data.getString("code")))return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "请输入快递单号");

        // var ressult = orderService.orderHandExpress(data.getInt("id"),data.getString("company"),data.getString("companyCode"),data.getString("code"));

        
        // return new ApiResult<>(ressult.getCode(), ressult.getMsg());
        tmallOrderService.orderSend(data.getLong("id"),data.getString("company"),data.getString("code"));
        return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(),"");
    }

    /**
    * 天猫同意退货
    *
    * @param data
    * @param req
    * @return
    */
   @RequestMapping(value = "/reviewRefund", method = RequestMethod.POST)
   public ApiResult<String> reviewRefund(@RequestBody DataRow data, HttpServletRequest req) {

       //var result = tmallOrderReturnService.confirmTmallRefund(data.getLong("id"));

       return new ApiResult<>(EnumResultVo.Fail.getIndex(),"未实现");
   }

    /***
    * 批量补充订单收货地址
    * @param file
    * @param req
    * @return
    * @throws IOException
    * @throws InvalidFormatException
    */
   @RequestMapping(value = "/import_execl_for_tao", method = RequestMethod.POST)
   public ApiResult<Integer> setOrderReceiver(@RequestParam("excel") MultipartFile file, HttpServletRequest req) throws IOException, InvalidFormatException {

       String fileName = file.getOriginalFilename();
       String dir = System.getProperty("user.dir");
       String destFileName = dir + File.separator + "uploadedfiles_" + fileName;
//        System.out.println(destFileName);
       File destFile = new File(destFileName);
       file.transferTo(destFile);
       log.info("/***********批量导入收货地址信息，文件后缀" + destFileName.substring(destFileName.lastIndexOf(".")) + "***********/");

       XSSFSheet sheet;
       InputStream fis = null;

       fis = new FileInputStream(destFileName);

       //订单list
       List<DcTmallOrderEntity> orderList = new ArrayList<>();

       XSSFWorkbook workbook = null;
       try {
           workbook = new XSSFWorkbook(fis);

           sheet = workbook.getSheetAt(0);

           int lastRowNum = sheet.getLastRowNum();//最后一行索引
           XSSFRow row = null;

           //订单实体
           DcTmallOrderEntity order = new DcTmallOrderEntity();
           int currRowNum = 0;
           for (int i = 1; i <= lastRowNum; i++) {
               row = sheet.getRow(i);
               currRowNum = i;
               if (row != null) {

                   //订单号
                   String orderId = row.getCell(0).getStringCellValue();

                   log.info("/***********批量导入收货地址信息***读取到订单ID:" + orderId + "***********/");
                   if (StringUtils.isEmpty(orderId) == false) {
                       order = new DcTmallOrderEntity();
                       order.setId(orderId);


                       //收货信息
                       order.setContactPerson(row.getCell(2).getStringCellValue());
                       order.setMobile(row.getCell(3).getStringCellValue());
                       order.setProvince(row.getCell(5).getStringCellValue());
                       order.setCity(row.getCell(6).getStringCellValue());
                       order.setArea(row.getCell(7).getStringCellValue());
                       order.setAddress(row.getCell(8).getStringCellValue());

                       orderList.add(order);

                   }


               }
           }

           log.info("/***********批量导入收货地址信息***开始更新收货地址***********/");
           //tmallOrderService.batchUpdateTmallOrderReceiver(orderList);
           return new ApiResult<>(0, "SUCCESS");

       } catch (Exception ex) {
//            workbook = new HSSFWorkbook(fis);
           log.info("/***********批量导入收货地址信息***出现异常：" + ex.getMessage() + "***********/");
           return new ApiResult<>(500, ex.getMessage());
       }

   }



}
