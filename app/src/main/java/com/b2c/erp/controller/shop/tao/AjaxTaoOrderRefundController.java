package com.b2c.erp.controller.shop.tao;

import jakarta.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.b2c.entity.api.ApiResult;
import com.b2c.entity.api.ApiResultEnum;
import com.b2c.entity.DataRow;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.interfaces.tao.DcTmallOrderService;

@RequestMapping(value = "/ajax_tao")
@RestController
public class AjaxTaoOrderRefundController {
    @Autowired
   private DcTmallOrderService tmallOrderService;
    /***
     * 订单手动选择快递单
     * @return
     */
    @RequestMapping(value = "/order_refund_apply", method = RequestMethod.POST)
    public ApiResult<Integer> getOrderSend(@RequestBody DataRow data , HttpServletRequest request) {
        if(StringUtils.isEmpty(data.getString("itemId")))return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "参数错误，缺少orderItemId");
        if(StringUtils.isEmpty(data.getString("refundId")))return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "参数错误，缺少refundId");

        // var ressult = orderService.orderHandExpress(data.getInt("id"),data.getString("company"),data.getString("companyCode"),data.getString("code"));

        
        // return new ApiResult<>(ressult.getCode(), ressult.getMsg());
        var result = tmallOrderService.addTaoOrderRefund(data.getLong("itemId"),data.getString("refundId"));
        // return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(),"");
        return new ApiResult<>(result.getCode(), result.getMsg());
    }

    /***
     * 订单手动选择快递单
     * @return
     */
    @RequestMapping(value = "/edit_refund_logistics_code", method = RequestMethod.POST)
    public ApiResult<Integer> editRefundLogisticsCode(@RequestBody DataRow data , HttpServletRequest request) {
        if(StringUtils.isEmpty(data.getString("companyCode")))return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "请选择快递公司");
        if(StringUtils.isEmpty(data.getString("code")))return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "请输入快递单号");
        String address = data.getString("address");

        // var ressult = orderService.orderHandExpress(data.getInt("id"),data.getString("company"),data.getString("companyCode"),data.getString("code"));

        
        // return new ApiResult<>(ressult.getCode(), ressult.getMsg());
        tmallOrderService.editRefundLogisticsCode(data.getLong("id"),data.getString("company"),data.getString("code"),address);
        return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(),"");
    }

    /**
     * 标记已处理
     *
     * @param req
     * @return
     */
    @RequestMapping(value = "/sign_refund", method = RequestMethod.POST)
    public ApiResult<String> signRefund(@RequestBody DataRow data, HttpServletRequest req) {
        Long refId = data.getLong("id");
        if(refId == null || refId<=0){
            return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "参数错误，缺少id");
        }
        Integer status = data.getInt("auditStatus");
        String remark = data.getString("remark");

        tmallOrderService.signRefund(refId,status,remark);
        return new ApiResult<>(ApiResultEnum.SUCCESS, "SUCCESS");
    }

}
