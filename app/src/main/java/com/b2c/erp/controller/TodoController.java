package com.b2c.erp.controller;

import jakarta.servlet.http.HttpServletRequest;

import com.b2c.interfaces.WmsUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.b2c.erp.DataConfigObject;
import com.b2c.interfaces.TodoService;

@RequestMapping("/todo")
@Controller
public class TodoController {
    @Autowired
    private TodoService todoService;
    @Autowired
    private WmsUserService manageUserService;
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String getList(Model model, HttpServletRequest request) {
        Integer pageIndex = 1;
        Integer pageSize = DataConfigObject.getInstance().getPageSize();

        Integer status=null;
        if (StringUtils.hasText(request.getParameter("status"))) {
            status = Integer.parseInt(request.getParameter("status"));
            model.addAttribute("status", status);
        }
        String content=null;
        if (StringUtils.hasText(request.getParameter("content"))) {
            content = request.getParameter("content");
            model.addAttribute("content", content);
        }
        
        var result = todoService.getList(pageIndex, pageSize, null, null, status,content);
        
        model.addAttribute("lists", result.getList());
        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("totalSize", result.getTotalSize());


//        model.addAttribute("view", "todo_list");
//        model.addAttribute("pView", "sale");
        CommonControllerUtils.setViewKey(model,manageUserService,"todo_list");
        return "todo_list";
    }
}
