package com.b2c.erp.controller.tui;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import jakarta.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.b2c.entity.result.PagingResponse;
import com.b2c.common.utils.OrderNumberUtils;
import com.b2c.entity.erp.enums.ContactTypeEnum;
import com.b2c.entity.erp.enums.InvoiceTransTypeEnum;
import com.b2c.entity.tui.OrderSendReturnEntity;
import com.b2c.erp.DataConfigObject;
import com.b2c.entity.ContactEntity;
import com.b2c.interfaces.wms.ContactService;
import com.b2c.interfaces.wms.OrderSendReturnService;

@RequestMapping("/tui")
@Controller
public class OrderSendReturnController {
    private static Logger log = LoggerFactory.getLogger(OrderSendReturnController.class);
    @Autowired
    private OrderSendReturnService returnService;
    @Autowired
    private ContactService contactService;

    /***
     * 退货订单list
     * 
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/order_send_retun_list", method = RequestMethod.GET)
    public String getOrderStockInList(Model model, HttpServletRequest request) {
        Integer pageIndex = 1;
        Integer pageSize = DataConfigObject.getInstance().getPageSize();

        if (StringUtils.isEmpty(request.getParameter("page")) == false) {
            try {
                pageIndex = Integer.parseInt(request.getParameter("page"));
            } catch (Exception e) {
            }
        }

        String orderNum = "";
        if (!StringUtils.isEmpty(request.getParameter("orderNum"))) {
            orderNum = request.getParameter("orderNum");
            model.addAttribute("orderNum", orderNum);
        }
        String logisticsCode = "";
        if (!StringUtils.isEmpty(request.getParameter("logisticsCode"))) {
            logisticsCode = request.getParameter("logisticsCode");
            model.addAttribute("logisticsCode", logisticsCode);
        }

        Integer status = null;
        if (!StringUtils.isEmpty(request.getParameter("status"))) {
            status = Integer.parseInt(request.getParameter("status"));
            model.addAttribute("status", status);
        }

        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);

        PagingResponse<OrderSendReturnEntity> result = returnService.getList(pageIndex, pageSize, orderNum,
                logisticsCode, status);

        model.addAttribute("list", result.getList());
        model.addAttribute("totalSize", result.getTotalSize());

        // List<Long> ids =
        // result.getList().stream().map(o->o.getId()).collect(Collectors.toList());
        // log.info(JSON.toJSONString(ids));

        model.addAttribute("view", "tui_order");
        model.addAttribute("pView", "ck");
        model.addAttribute("ejcd", "收货");
        model.addAttribute("sjcd", "订单退货收货");

        return "tui/order_send_return_list";
    }

    /**
     * 批量生产采购单
     * 
     * @param model
     * @param ids
     * @param request
     * @return
     */
    @RequestMapping(value = "/generate_purchase_for_return_list", method = RequestMethod.POST)
    public String generatePurr(Model model, @RequestParam("ids") String ids, HttpServletRequest request) {
        model.addAttribute("userName", "admin");
        model.addAttribute("view", "tui_order");
        model.addAttribute("pView", "ck");
        List<OrderSendReturnEntity> list = returnService.getListByIds(ids);
        model.addAttribute("list", list);

        model.addAttribute("time", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        // 生成单据编号
        String billNo = "RET" + OrderNumberUtils.getOrderIdByTime();
        model.addAttribute("billNo", billNo);

        // 采购单来源
        model.addAttribute("transType", InvoiceTransTypeEnum.DaiFaRefund.getIndex());
        // 本次所选订单id
        model.addAttribute("srcOrderId", ids);

        // 获取供应商
        List<ContactEntity> contacts = contactService.getList(ContactTypeEnum.Supplier);
        model.addAttribute("contacts", contacts);

        return "tui/generate_purchase_for_return_list";
    }




}
