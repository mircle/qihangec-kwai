package com.b2c.erp.controller.fahuo;

import com.b2c.erp.DataConfigObject;
import com.b2c.interfaces.wms.ErpStockOutFormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jakarta.servlet.http.HttpServletRequest;

/**
 * 描述：
 * 待拣货商品Controller
 *
 * @author qlp
 * @date 2021-06-29 09:44
 */
@RequestMapping("/fahuo")
@Controller
public class WaitPickGoodsController {

    @Autowired
    private ErpStockOutFormService erpStockOutFormService;
    /***
     * 待拣货商品list（存在仓库的拣货单）
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/wait_pick_goods_list", method = RequestMethod.GET)
    public String orderItemList(Model model, HttpServletRequest request) {
        Integer pageIndex = 1;
        Integer pageSize = DataConfigObject.getInstance().getPageSize();

        if (StringUtils.isEmpty(request.getParameter("page")) == false) {
            try {
                pageIndex = Integer.parseInt(request.getParameter("page"));
            } catch (Exception e) {
            }
        }
        String searchUrl = "/fahuo/wait_pick_goods_list";
        model.addAttribute("searchUrl", searchUrl);



        String skuNum = request.getParameter("skuNum");
        model.addAttribute("skuNum", skuNum);


        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);

        var result = erpStockOutFormService.getStockOutFormItemWaitPickGoodsList(pageIndex, pageSize,skuNum);

        model.addAttribute("list", result.getList());
        model.addAttribute("totalSize", result.getTotalSize());

        model.addAttribute("view", "djhordergoods");
        model.addAttribute("pView", "ck");
        model.addAttribute("ejcd", "拣货&出库");
        model.addAttribute("sjcd", "待拣货商品清单");
        return "fahuo/wait_pick_goods_list";
    }

}
