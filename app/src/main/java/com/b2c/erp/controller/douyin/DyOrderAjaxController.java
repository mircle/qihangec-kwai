package com.b2c.erp.controller.douyin;

import jakarta.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.b2c.entity.api.ApiResult;
import com.b2c.entity.DataRow;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.interfaces.dou.DouyinOrderService;

@RequestMapping(value = "/ajax_douyin")
@RestController
public class DyOrderAjaxController {
    @Autowired
    private DouyinOrderService orderService;
    
        /***
     * 订单手动选择快递单
     * @return
     */
    @RequestMapping(value = "/order_hand_send", method = RequestMethod.POST)
    public ApiResult<Integer> getOrderSend(@RequestBody DataRow data , HttpServletRequest request) {
        if(StringUtils.isEmpty(data.getString("companyCode")))return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "请选择快递公司");
        if(StringUtils.isEmpty(data.getString("code")))return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "请输入快递单号");

        // var ressult = orderService.orderHandExpress(data.getInt("id"),data.getString("company"),data.getString("companyCode"),data.getString("code"));

        
        // return new ApiResult<>(ressult.getCode(), ressult.getMsg());
        var result = orderService.orderHandSend(data.getLong("id"),data.getString("company"),data.getString("code"));
        return new ApiResult<>(result.getCode(),result.getMsg());
    }

    @RequestMapping(value = "/order_hand_address", method = RequestMethod.POST)
    public ApiResult<String> orderAddressEdit(HttpServletRequest req,@RequestBody DataRow reqData) throws Exception {
        Long id = reqData.getLong("id");
        String name = reqData.getString("name");
        String mobile = reqData.getString("mobile");
        String address = reqData.getString("address");

        

        orderService.updOrderAddress(id,name,mobile,address);

        return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(), "SUCCESS" );
    }
}
