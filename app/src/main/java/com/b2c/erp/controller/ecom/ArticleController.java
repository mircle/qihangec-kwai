package com.b2c.erp.controller.ecom;

import jakarta.servlet.http.HttpServletRequest;

import com.b2c.erp.controller.CommonControllerUtils;
import com.b2c.interfaces.WmsUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.b2c.entity.api.ApiResult;
import com.b2c.entity.DataRow;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.interfaces.ecom.ArticleService;

@RequestMapping("/ecom")
@Controller
public class ArticleController {
    private static Logger log = LoggerFactory.getLogger(ArticleController.class);
    @Autowired
    private ArticleService service;
    @Autowired
    private WmsUserService manageUserService;
    @RequestMapping(value = "/article_list", method = RequestMethod.GET)
    public String list(Model model, HttpServletRequest request) {
  
        
        String page = request.getParameter("page");
        Integer pageIndex = 1;
        Integer pageSize = 50;
        String num = "";

        String platform = null;

        if (!StringUtils.isEmpty(page)) {
            pageIndex = Integer.parseInt(page);
        }

        if (!StringUtils.isEmpty(request.getParameter("num"))) {
            num = request.getParameter("num");
            model.addAttribute("num",num);
        }
        if (!StringUtils.isEmpty(request.getParameter("platform"))) {
            platform = request.getParameter("platform");
            model.addAttribute("platform",platform);
        }

       

        var result = service.getList(pageIndex,pageSize,num,platform);
        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("totalSize", result.getTotalSize());
        model.addAttribute("lists", result.getList());

//        model.addAttribute("view", "article_list");
//        model.addAttribute("pView", "sale");
        CommonControllerUtils.setViewKey(model,manageUserService,"ecomdata");
        return "ecom/article_list";
    }

    @RequestMapping(value = "/article_add", method = RequestMethod.GET)
    public String add(Model model, HttpServletRequest request) {
        return "ecom/article_add";
    }

    @ResponseBody
    @RequestMapping(value = "/article_add_ajax", method = RequestMethod.POST)
    public  ApiResult<String> addPostAjax(Model model, @RequestBody DataRow data, HttpServletRequest request) {
        String title = data.getString("title");
        String content = data.getString("content");
        String platform = data.getString("platform");

        service.add(title, content, platform, null);
       
        //keyWordService.addKeyWord(kw);
        return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(), "SUCCESS");
    }
}
