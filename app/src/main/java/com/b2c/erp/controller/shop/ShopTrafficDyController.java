package com.b2c.erp.controller.shop;

import java.math.BigDecimal;
import java.util.List;

import jakarta.servlet.http.HttpServletRequest;

import com.b2c.erp.controller.CommonControllerUtils;
import com.b2c.interfaces.WmsUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.b2c.entity.api.ApiResult;
import com.b2c.common.utils.DateUtil;
import com.b2c.entity.DataRow;
import com.b2c.entity.datacenter.DcShopEntity;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.entity.shop.ShopTrafficEntity;
import com.b2c.erp.DataConfigObject;
import com.b2c.interfaces.ShopService;
import com.b2c.interfaces.ShopTrafficGoodsService;

@RequestMapping("/shop")
@Controller
public class ShopTrafficDyController {
    @Autowired
    private ShopService shopService;
    @Autowired
    private ShopTrafficGoodsService shopTrafficGoodsService;
    @Autowired
    private WmsUserService manageUserService;

    @RequestMapping(value = "/shop_traffic_dy", method = RequestMethod.GET)
    public String shopTrafficDy(Model model, HttpServletRequest request) {
        Integer shopType = 6;
      
        Integer pageIndex = 1, pageSize = DataConfigObject.getInstance().getPageSize();
        if (!StringUtils.isEmpty(request.getParameter("page"))) {
            pageIndex = Integer.parseInt(request.getParameter("page"));
        }
        // 店铺列表
        List<DcShopEntity> shops = shopService.getShopListByUserId(null, shopType);
        model.addAttribute("shops", shops);


        Integer shopId=22;
        if (!StringUtils.isEmpty(request.getParameter("shopId"))){
            shopId = Integer.parseInt(request.getParameter("shopId")); 
        }
        model.addAttribute("shopId",shopId);
        String date = null;//date = DateUtil.customizeDate_(1);;
        if(StringUtils.hasText(request.getParameter("date"))){
            date = request.getParameter("date");
        }
        model.addAttribute("date", date);

        var result = shopTrafficGoodsService.getShopTrafficList(pageIndex, 100, shopType, shopId,date);

        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("totalSize", result.getTotalSize());
        model.addAttribute("lists", result.getList());

//        model.addAttribute("view", "doutraffic");
//        model.addAttribute("pView", "dou");
        CommonControllerUtils.setViewKey(model,manageUserService,"dyshop");
        return "eshop/shop_traffic_dy";
    }

    @RequestMapping(value = "/shop_traffic_dy_add", method = RequestMethod.GET)
    public String shopTrafficAddPdd(Model model, HttpServletRequest request) {
        Integer shopType = 6;
        // 店铺列表
        List<DcShopEntity> shops = shopService.getShopListByUserId(null, shopType);
        model.addAttribute("shops", shops);
        model.addAttribute("shopId", 22);
        String date = DateUtil.customizeDate_(1);
        model.addAttribute("date", date);

        return "eshop/shop_traffic_dy_add";
    }

    @ResponseBody
    @RequestMapping(value = "/shop_traffic_dy_add_ajax", method = RequestMethod.POST)
    public  ApiResult<String> addPostAjax(Model model, @RequestBody DataRow data, HttpServletRequest request) {
        ShopTrafficEntity entity = new ShopTrafficEntity();
        Integer shopId = data.getInt("shopId");
        var shop = shopService.getShop(shopId);
        entity.setShopId(shopId);
        entity.setShopType(shop.getType());
        Integer shows = data.getInt("shows");
        entity.setShows(shows);
        
        Integer visits = data.getInt("visits");
        entity.setVisits(visits);
        Integer visitsFs = data.getInt("visitsFs");
        entity.setVisitsFs(visitsFs);
        Integer views = data.getInt("views");
        entity.setViews(views);
        BigDecimal CTR = data.getBigDecimal("CTR");
        entity.setCTR(CTR);
        
        
        Integer orderUsers = data.getInt("orderUsers");
        entity.setOrderUsers(orderUsers);
        Integer orderNewUsers = data.getInt("orderNewUsers");
        entity.setOrderNewUsers(orderNewUsers);
        
        BigDecimal orderAmount = data.getBigDecimal("orderAmount");
        entity.setOrderAmount(orderAmount);
        BigDecimal CVR = data.getBigDecimal("CVR");
        entity.setCVR(CVR);
        BigDecimal ATV = data.getBigDecimal("ATV");
        entity.setATV(ATV);
        
        BigDecimal UVV = data.getBigDecimal("UVV");
        entity.setUVV(UVV);
        
        String date = data.getString("date");
        entity.setDate(date);
        String remark = data.getString("remark");
        entity.setRemark(remark);
        String source = data.getString("source");
        String sourceName = "";
        if(StringUtils.hasText(source)){
            if(source.equals("unknown")){
                sourceName = "未知";
            }else if(source.equals("search")){
                sourceName = "搜索";
            }else if(source.equals("recommend")){
                sourceName = "商城推荐";
            }else if(source.equals("chuchuang")){
                sourceName = "橱窗";
            }else if(source.equals("gouhouyemian")){
                sourceName = "购后页面";
            }else if(source.equals("dianpuye")){
                sourceName = "店铺页";
            }else if(source.equals("other")){
                sourceName = "其他";
            }else if(source.equals("dashboard")){
                sourceName = "总览";
            }
        }
        entity.setSource(source);
        entity.setSourceName(sourceName);

        // service.addKeyword(shopId, keyword, source, goodsId, views, date);
        var result = shopTrafficGoodsService.addShopTrafficForDy(entity);
        return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(), "SUCCESS");
    }
}
