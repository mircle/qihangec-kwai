package com.b2c.erp.controller;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.erp.vo.ErpGoodsListVo;
import com.b2c.entity.erp.vo.ErpGoodsSpecListVo;
import com.b2c.entity.erp.vo.ErpGoodsSpecStockVo;
import com.b2c.entity.erp.vo.GoodsCategoryVo;
import com.b2c.erp.DataConfigObject;
import com.b2c.interfaces.WmsUserService;
import com.b2c.interfaces.erp.ErpGoodsService;
import com.b2c.interfaces.wms.ClientManageService;
import com.b2c.interfaces.wms.StockLocationService;
import com.b2c.interfaces.wms.StockService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import jakarta.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 描述：
 * 商品库存管理Controller
 *
 * @author qlp
 * @date 2019-03-20 10:16
 */
@RequestMapping("/goods")
@Controller
public class GoodsController {
    @Autowired
    private ErpGoodsService goodsService;
    @Autowired
    private StockService stockService;
    @Autowired
    private StockLocationService stockLocationService;
    @Autowired
    private ErpGoodsService erpGoodsService;
    @Autowired
    private WmsUserService manageUserService;
    @Autowired
    private ClientManageService clientManageService;

    private static Logger log = LoggerFactory.getLogger(GoodsController.class);




    /**
     * 库存商品列表
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String getgGoodsList(Model model, HttpServletRequest request) {
        String page = request.getParameter("page");
        Integer pageIndex = 1, pageSize = 20;
        String str = "";
        if (!StringUtils.isEmpty(page)) {
            pageIndex = Integer.parseInt(page);
            model.addAttribute("page",pageIndex);
        }
        if (!StringUtils.isEmpty(request.getParameter("str"))) {
            str = request.getParameter("str");
            model.addAttribute("number",str);
        }

        String attr1 = "";
        if (!StringUtils.isEmpty(request.getParameter("attr1"))) {
            attr1 = request.getParameter("attr1");
        }
        String attr4 = "";
        if (!StringUtils.isEmpty(request.getParameter("attr4"))) {
            attr4 = request.getParameter("attr4");
        }
        Integer categoryId = null;
        if (!StringUtils.isEmpty(request.getParameter("categoryId"))) {
            categoryId = Integer.parseInt(request.getParameter("categoryId"));
            model.addAttribute("categoryId",categoryId);
        }
        Integer contactId = null;
        if (!StringUtils.isEmpty(request.getParameter("contactId"))) {
            contactId = Integer.parseInt(request.getParameter("contactId"));
            model.addAttribute("contactId",contactId);
        }
        Integer status = null;
        if (!StringUtils.isEmpty(request.getParameter("status"))) {
            status = Integer.parseInt(request.getParameter("status"));
            model.addAttribute("status",status);
        }

        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);
        PagingResponse<ErpGoodsListVo> result = stockService.getGoodsList(pageIndex, pageSize,contactId, str,categoryId,status,attr1,attr4);
        model.addAttribute("totalSize", result.getTotalSize());
        model.addAttribute("lists", result.getList());

//        model.addAttribute("unitList", stockService.getUnitList());//计量表信息

//        List<ErpStockLocationEntity> houses = stockLocationService.getListByParentId(0);
//        model.addAttribute("houses", houses);
//        var reservoir = stockLocationService.getListByDepth(2);
////        List<ReservoirEntity> reservoir = reservoirService.getReservoir();
//        model.addAttribute("reservoir", reservoir);
//
//        var shelf = stockLocationService.getListByDepth(3);
//        model.addAttribute("shelf", shelf);
        /*String cateJson = stockService.getCategoryString();
        model.addAttribute("cateJson",cateJson);*/

         //获取供应商，10代表供应商
         var clientResult = clientManageService.getClientList(1,100,null,null,10,null);
         model.addAttribute("client",clientResult.getList());

//        var permissionKey = "spgl";
//        var mp = manageUserService.getEntityByKey(permissionKey);
//
//        model.addAttribute("view", permissionKey);
//        model.addAttribute("pView", mp.getParentKey());
        CommonControllerUtils.setViewKey(model,manageUserService,"spgl");
        model.addAttribute("ejcd", "商品");
        model.addAttribute("sjcd", "商品管理");
        return "goods_list";
    }


    @RequestMapping(value = "/spec_list", method = RequestMethod.GET)
    public String specStockSearch(Model model, HttpServletRequest request) {
        String page = request.getParameter("page");
        Integer pageIndex = 1, pageSize = DataConfigObject.getInstance().getPageSize();

        if (!StringUtils.isEmpty(page)) {
            pageIndex = Integer.parseInt(page);
        }
        String number = "";
        if (!StringUtils.isEmpty(request.getParameter("number"))) {
            number = request.getParameter("number");
            model.addAttribute("number",number);
        }
        Integer categoryId = 0;
        if (!StringUtils.isEmpty(request.getParameter("categoryId"))) {
            categoryId = Integer.parseInt(request.getParameter("categoryId"));
            model.addAttribute("categoryId",categoryId);
        }


        int filter0 = 0;
        if (!StringUtils.isEmpty(request.getParameter("filter0"))) {
            filter0 = Integer.parseInt(request.getParameter("filter0"));
        }
        model.addAttribute("filter0",filter0);

        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);
        PagingResponse<ErpGoodsSpecStockVo> result = stockService.goodsSkuSearch(pageIndex, pageSize, number,filter0,categoryId);
        model.addAttribute("totalSize", result.getTotalSize());
        model.addAttribute("lists", result.getList());
        model.addAttribute("totalQty", result.getData2());


//        model.addAttribute("view", "spgl");
//        model.addAttribute("pView", "sale");
        CommonControllerUtils.setViewKey(model,manageUserService,"spgl");
        return "goods_spec_list";
    }

    /**
     * 库存商品列表
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/goods_list_count", method = RequestMethod.GET)
    public String getgGoodsList_(Model model, HttpServletRequest request) {
        String attr1 = "";
        if (!StringUtils.isEmpty(request.getParameter("attr1"))) {
            attr1 = request.getParameter("attr1");
        }
        String attr2 = "";
        if (!StringUtils.isEmpty(request.getParameter("attr2"))) {
            attr2 = request.getParameter("attr2");
        }
        String attr4 = "";
        if (!StringUtils.isEmpty(request.getParameter("attr4"))) {
            attr4 = request.getParameter("attr4");
        }
        Integer categoryId = null;
        if (!StringUtils.isEmpty(request.getParameter("categoryId"))) {
            categoryId = Integer.parseInt(request.getParameter("categoryId"));
            model.addAttribute("categoryId",categoryId);
        }
        var res =stockService.getGoodsList_1(categoryId,attr1,attr2,attr4);
        model.addAttribute("obj", res);

        model.addAttribute("view", "spgl");
        model.addAttribute("pView", "sale");
        model.addAttribute("ejcd", "商品");
        model.addAttribute("sjcd", "商品管理");
        return "goods_list_count";
    }


    /**
     * 分类管理
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/category_list", method = RequestMethod.GET)
    public String getCategoryList(Model model, HttpServletRequest request) {
        List<GoodsCategoryVo> list = stockService.getCategoryList();
//        model.addAttribute("parent", vo.getEntity());
//        model.addAttribute("list", vo.getList());
        model.addAttribute("categoryList", list);

//        model.addAttribute("view", "spgl");
//        model.addAttribute("pView", "sale");
        CommonControllerUtils.setViewKey(model,manageUserService,"spgl");

        model.addAttribute("ejcd", "数据");
        model.addAttribute("sjcd", "商品分类管理");
        return "category_list";
    }


    @RequestMapping(value = "/goods_edit_spec", method = RequestMethod.GET)
    public String goodsEditSpec(Model model, HttpServletRequest request) {
        Integer goodsId = Integer.parseInt(request.getParameter("id"));
        var good= goodsService.getById(goodsId);
        model.addAttribute("goods", good);
        var goodsSpec = goodsService.getSpecByGoodsId(goodsId);
        model.addAttribute("goodsSpec", goodsSpec);
        model.addAttribute("view", "spgl");
        model.addAttribute("pView", "goods");
        return "goods_edit_spec";
    }

    /**
     * 修改商品规格
     * @param model
     * @param request
     * @return
     */
//    @RequestMapping(value = "/edit_good_spec", method = RequestMethod.GET)
//    public String edit_good_spec(Model model, HttpServletRequest request) {
//        model.addAttribute("view","spgl");
//        return "edit_good_spec";
//    }

    /**
     * 添加商品get
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String addGoods(Model model, HttpServletRequest request) {
        log.info("访问添加商品页面------"+ request.getSession().getAttribute("userName"));
        model.addAttribute("view", "spgl");
        model.addAttribute("pView", "goods");
        model.addAttribute("ejcd", "商品");
        model.addAttribute("sjcd", "商品管理");
        model.addAttribute("unitList", stockService.getUnitList());//计量表信息
//        model.addAttribute("houses", stockLocationService.getListByParentId(0));//默认仓库
        model.addAttribute("brands",goodsService.getBrandList());//获取品牌list
        //获取供应商，10代表供应商
        var clientResult = clientManageService.getClientList(1,100,null,null,10,null);
        model.addAttribute("client",clientResult.getList());

        var permissionKey = "spgl";
        var mp = manageUserService.getEntityByKey(permissionKey);

        model.addAttribute("view", permissionKey);
        model.addAttribute("pView", mp.getParentKey());
        return "goods_add";

//            return "redirect:/goods/list";
    }

    /**
     * 修改商品规格
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String editGoods(@RequestParam Integer id, Model model, HttpServletRequest request) {
       if(id!=null) {
           model.addAttribute("view", "spgl");
           model.addAttribute("pView", "sj");
       }
//       model.addAttribute("unitList", stockService.getUnitList());//计量表信息
//       model.addAttribute("houses", stockLocationService.getListByParentId(0));//默认仓库
        if(!StringUtils.isEmpty(request.getParameter("page"))){
            model.addAttribute("page",request.getParameter("page"));
        }else  model.addAttribute("page","");
        //查询商品信息
       var goods = goodsService.getById(id);

       if (goods == null) return "redirect:/goods/list";
       
        model.addAttribute("goods", goods);


//        List<GoodsSpecAttrEntity> colors = goodsService.getSkuValueList("color", id);
//        model.addAttribute("colors", colors);
//        List<GoodsSpecAttrEntity> sizes = goodsService.getSkuValueList("size", id);
//        model.addAttribute("sizes", sizes);
//        List<GoodsSpecAttrEntity> styles = goodsService.getSkuValueList("style", id);
//        model.addAttribute("styles", styles);

        var permissionKey = "spgl";
        var mp = manageUserService.getEntityByKey(permissionKey);

        model.addAttribute("view", permissionKey);
        model.addAttribute("pView", mp.getParentKey());

        return "goods_edit";
    }
    /**
     * 采购单添加sku
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/purchase_add_sku", method = RequestMethod.GET)
    public String purchase_add_sku(Model model,HttpServletRequest request) {
        /********查询商品********/
        String number = "";
        if (!StringUtils.isEmpty(request.getParameter("number"))) {
            number = request.getParameter("number");
            model.addAttribute("number",number);

            PagingResponse<ErpGoodsSpecListVo> result = erpGoodsService.getSpecByGoodsNumber(1, 10, number);


            model.addAttribute("totalSize", result.getTotalSize());
            model.addAttribute("lists", result.getList());
        }

        model.addAttribute("invoiceId",request.getParameter("invoiceId"));

        return "purchase_add_sku";
    }

    /**
     * 商品规格同步
     * @param model
     * @param request
     * @return
     */
//    @RequestMapping(value = "/syn_erp_good_spec", method = RequestMethod.GET)
//    public String synErpGoodSpec(@RequestParam Integer id,Model model, HttpServletRequest request) {
//        model.addAttribute("view", "spgl");
//        model.addAttribute("pView", "sj");
//        model.addAttribute("ejcd", "数据");
//        model.addAttribute("sjcd", "商品管理");
//        model.addAttribute("list",stockService.getGoodSpecByGoodId(id));
//        return "syn_erp_good_spec";
//    }


}
