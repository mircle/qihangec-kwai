// package com.b2c.erp.controller.shop.pdd;

// import com.alibaba.fastjson.JSONObject;
// import com.b2c.common.api.ApiResult;
// import com.b2c.common.utils.DateUtil;
// import com.b2c.entity.pdd.OrderPddEntity;
// import com.b2c.entity.pdd.OrderPddItemEntity;
// import com.b2c.entity.result.EnumResultVo;
// import com.b2c.entity.result.ResultVo;
// import com.b2c.entity.xhs.XhsOrderEntity;
// import com.b2c.entity.xhs.XhsOrderSettleEntity;
// import com.b2c.erp.DataConfigObject;
// import com.b2c.erp.utils.ExcelToHtml;
// import com.b2c.interfaces.ShopService;
// import com.b2c.interfaces.xhs.XhsOrderSettleService;
// import com.b2c.interfaces.pdd.OrderPddService;
// import com.fasterxml.jackson.databind.exc.InvalidFormatException;

// import org.apache.poi.hpsf.Array;
// import org.apache.poi.hssf.usermodel.HSSFWorkbook;
// import org.apache.poi.ss.usermodel.Row;
// import org.apache.poi.ss.usermodel.Sheet;
// import org.apache.poi.ss.usermodel.Workbook;
// import org.apache.poi.xssf.usermodel.XSSFWorkbook;
// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.stereotype.Controller;
// import org.springframework.ui.Model;
// import org.springframework.util.StringUtils;
// import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestMethod;
// import org.springframework.web.bind.annotation.RequestParam;
// import org.springframework.web.bind.annotation.ResponseBody;
// import org.springframework.web.multipart.MultipartFile;

// import jakarta.servlet.http.HttpServletRequest;
// import java.io.File;
// import java.io.FileInputStream;
// import java.io.IOException;
// import java.io.InputStream;
// import java.math.BigDecimal;
// import java.util.ArrayList;
// import java.util.Iterator;
// import java.util.List;

// @RequestMapping("/pdd")
// @Controller
// public class PddOrderImportController {
//     private static Logger log = LoggerFactory.getLogger(PddOrderImportController.class);
//     @Autowired
//     private ShopService shopService;
//     @Autowired
//     private OrderPddService orderPddService;

//     @RequestMapping(value = "/order_excel_import", method = RequestMethod.GET)
//     public String orderDetailTmall(Model model, @RequestParam Integer shopId, HttpServletRequest request) {

//         // 查询店铺信息
//         var shop = shopService.getShop(shopId);
//         model.addAttribute("shop", shop);
//         model.addAttribute("shopId", shop.getId());

//         model.addAttribute("view", "pddshop");
//         model.addAttribute("pView", "sale");
//         return "order/pdd/order_excel_import";
//     }

//     @ResponseBody
//     @RequestMapping(value = "/order_excel_upload", method = RequestMethod.POST)
//     public ApiResult<String> live_replay_excel_upload(@RequestParam("excel") MultipartFile file, HttpServletRequest req,
//             @RequestParam Integer shopId) throws IOException, InvalidFormatException {

//         String fileName = file.getOriginalFilename();
//         String path = this.getClass().getResource("/").getPath() + File.separator;

//         // String dir = System.getProperty("user.dir");
//         // log.info("根目录"+path);
//         String destFileName = "order_excel_" + DateUtil.getCurrentDate() + "_" + fileName;
//         // System.out.println(destFileName);
//         File destFile = new File(path + destFileName);
//         file.transferTo(destFile);

//         log.info("/保存文件成功，文件路径" + path + destFileName + "***********/");
//         Workbook workbook = null;
//         InputStream fis = null;

//         // var shelfList = new ArrayList<ErpStockLocationBatchAddVo>();

//         int isExist = 0;// 存在数
//         int success = 0;// 成功数
//         int exCount = 0;// 异常数

//         try {
//             fis = new FileInputStream(path + destFileName);

//             if (fileName.toLowerCase().endsWith("xlsx")) {
//                 workbook = new XSSFWorkbook(fis);
//             } else if (fileName.toLowerCase().endsWith("xls")) {
//                 workbook = new HSSFWorkbook(fis);
//             }

//             Sheet sheet = workbook.getSheetAt(0);

//             // 得到行的迭代器
//             Iterator<Row> rowIterator = sheet.iterator();
//             int rowCount = 0;
//             // 循环每一行
//             while (rowIterator.hasNext()) {
//                 try {
//                     Row row = rowIterator.next();
//                     // 从第二行开始
//                     if (rowCount > 0) {
//                         System.out.print("第" + (rowCount) + "行  ");
//                         if(rowCount==63){
//                             String s = "";
//                         }
//                         OrderPddEntity pddEntity = new OrderPddEntity();
//                         pddEntity.setShopId(shopId);

//                         String orderNo = row.getCell(1).getStringCellValue();
//                         pddEntity.setOrderSn(orderNo);
//                         String receiverName = row.getCell(3).getStringCellValue();
//                         pddEntity.setReceiver_name(receiverName);
//                         String receiverPhone = row.getCell(5).getStringCellValue();
//                         pddEntity.setReceiver_phone(receiverPhone);
//                         String province = row.getCell(6).getStringCellValue();
//                         pddEntity.setProvince(province);
//                         String city = row.getCell(7).getStringCellValue();
//                         pddEntity.setCity(city);
//                         String country = row.getCell(8).getStringCellValue();
//                         pddEntity.setCountry(country);
//                         String address = row.getCell(9).getStringCellValue();
//                         pddEntity.setAddress(address);
//                         String orderTime = row.getCell(10).getStringCellValue();
//                         pddEntity.setCreated_time(orderTime);
//                         String payTime = row.getCell(11).getStringCellValue();
//                         pddEntity.setPay_time(payTime);
//                         String trackingNumber = row.getCell(12).getStringCellValue();
//                         pddEntity.setTracking_number(trackingNumber);
//                         String trackingCompany = row.getCell(13).getStringCellValue();
//                         pddEntity.setTracking_company(trackingCompany);
//                         String remark = row.getCell(29).getStringCellValue();
//                         pddEntity.setRemark(remark);
//                         String sendTime = row.getCell(31).getStringCellValue();
//                         pddEntity.setShipping_time(sendTime);

//                         OrderPddItemEntity oi = new OrderPddItemEntity();
//                         String goodsName = row.getCell(14).getStringCellValue();
//                         oi.setGoodsName(goodsName);
//                         String goodsEname = row.getCell(15).getStringCellValue();
//                         oi.setRemark(goodsEname);
//                         String goodsAttr = row.getCell(16).getStringCellValue();
//                         oi.setGoodsSpec(goodsAttr);
//                         Double goodsQuantity = row.getCell(19).getNumericCellValue();
//                         oi.setQuantity(goodsQuantity.intValue());
//                         String goodsAmount = row.getCell(23).getStringCellValue();
//                         if(StringUtils.isEmpty(goodsAmount)) goodsAmount="0.00";
//                         oi.setGoodsPrice(Double.parseDouble(goodsAmount));

//                         pddEntity.setPay_amount(Double.parseDouble(goodsAmount));
//                         pddEntity.setGoods_amount(Double.parseDouble(goodsAmount));
//                         List<OrderPddItemEntity> items = new ArrayList<>();
//                         items.add(oi);
//                         pddEntity.setItems(items);

//                         ResultVo<Integer> res = orderPddService.orderCreatePdd(pddEntity);
//                         // log.info(JSONObject.toJSONString(res));
//                         log.info(orderNo);
                        
//                         // var res = orderSettleService.addOrderSettle(settleEntity);
//                         if (res.getCode() == EnumResultVo.Exist.getIndex()) {
//                             isExist += 1;
//                         } else if (res.getCode() == EnumResultVo.SUCCESS.getIndex()) {
//                             success += 1;
//                         } else
//                             exCount += 1;
//                     }
//                     rowCount++;

//                 } catch (Exception e4) {
//                     exCount++;
//                 }
//             }

//         } catch (Exception ex) {
            
//             // workbook = new HSSFWorkbook(fis);
//             return new ApiResult<>(500, ex.getMessage());
            
//         }
//         //
//         //// if (shelfList == null || shelfList.size() == 0) return new ApiResult<>(404,
//         // "没有数据");

//         log.info("新增成功：" + success + "，已存在：" + isExist + "，异常：" + exCount);
//         return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(),
//                 "新增成功：" + success + "，已存在：" + isExist + "，异常：" + exCount);
//     }

// }
