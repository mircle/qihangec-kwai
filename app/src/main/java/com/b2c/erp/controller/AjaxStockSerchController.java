package com.b2c.erp.controller;

import com.b2c.entity.erp.vo.ErpGoodsSpecStockVo;
import com.b2c.entity.enums.EnumUserActionType;
import com.b2c.interfaces.erp.ErpUserActionLogService;
import com.b2c.interfaces.wms.StockService;
import org.apache.poi.hssf.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import jakarta.servlet.http.HttpServletRequest;
 import jakarta.servlet.http.HttpServletResponse;
import javax.swing.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/stock")
public class AjaxStockSerchController {
    @Autowired
    private StockService stockService;
    @Autowired
    private ErpUserActionLogService logService;

    /**
     * 导出商品
     */
    @RequestMapping(value = "export",method={RequestMethod.POST,RequestMethod.GET})
    @ResponseBody
    public void outExport(HttpServletRequest request, HttpServletResponse response) {
        //获取前台传过来的ids
        Integer houseId=0;
        String str="";
        if (!StringUtils.isEmpty(request.getParameter("house"))) {
            houseId = Integer.parseInt(request.getParameter("house"));
        }
        if (!StringUtils.isEmpty(request.getParameter("str"))) {
            str = request.getParameter("str");
        }

        List<ErpGoodsSpecStockVo> list = stockService.goodsStockInfoSearch(str, houseId);
        //创建Excel工作薄对象
        HSSFWorkbook workbook = new HSSFWorkbook();
        //创建Excel工作表对象
        HSSFSheet sheet = workbook.createSheet();
        sheet.setColumnWidth(0, 2000);
        sheet.setColumnWidth(1, 5000);
        sheet.setColumnWidth(2, 5000);
        sheet.setColumnWidth(3, 6000);
        sheet.setColumnWidth(4, 5000);
        sheet.setColumnWidth(5, 5000);
        sheet.setColumnWidth(6, 4000);

        // 设置表头字体样式
        HSSFFont columnHeadFont = workbook.createFont();
        columnHeadFont.setFontName("宋体");
        columnHeadFont.setFontHeightInPoints((short) 20);

        // 列头的样式
        HSSFCellStyle columnHeadStyle = workbook.createCellStyle();
        columnHeadStyle.setFont(columnHeadFont);
        // 上下居中
        columnHeadStyle.setLocked(true);
        columnHeadStyle.setWrapText(true);
        // 设置普通单元格字体样式
        HSSFFont font = workbook.createFont();
        font.setFontName("宋体");
        font.setFontHeightInPoints((short) 14);

        //创建Excel工作表第一行
        HSSFRow row0 = sheet.createRow(0);
        // 设置行高
        row0.setHeight((short) 500);
        HSSFCell cell = row0.createCell(0);
        //设置单元格内容
        cell.setCellValue(new HSSFRichTextString("id"));
        //设置单元格字体样式
        cell.setCellStyle(columnHeadStyle);
        cell = row0.createCell(1);
        cell.setCellValue(new HSSFRichTextString("商品编号"));
        cell.setCellStyle(columnHeadStyle);
        cell = row0.createCell(2);
        cell.setCellValue(new HSSFRichTextString("商品名称"));
        cell.setCellStyle(columnHeadStyle);
        cell = row0.createCell(3);
        cell.setCellValue(new HSSFRichTextString("sku编码"));
        cell.setCellStyle(columnHeadStyle);
        cell = row0.createCell(4);
        cell.setCellValue(new HSSFRichTextString("商品规格"));
        cell.setCellStyle(columnHeadStyle);
        cell = row0.createCell(5);
        cell.setCellValue(new HSSFRichTextString("所在仓位"));
        cell.setCellStyle(columnHeadStyle);
        cell = row0.createCell(6);
        cell.setCellValue(new HSSFRichTextString("当前库存"));
        cell.setCellStyle(columnHeadStyle);

        // 循环写入数据
        for (int i = 0; i < list.size(); i++) {
            ErpGoodsSpecStockVo item = list.get(i);
            HSSFRow row = sheet.createRow(i + 1);
            cell = row.createCell(0);
            cell.setCellValue(new HSSFRichTextString(String.valueOf(item.getId())));
            cell.setCellStyle(columnHeadStyle);
            cell = row.createCell(1);
            if (StringUtils.isEmpty(item.getNumber())) item.setNumber("");
            cell.setCellValue(new HSSFRichTextString(item.getNumber()));
            cell.setCellStyle(columnHeadStyle);
            cell = row.createCell(2);
            if (StringUtils.isEmpty(item.getGoodsName())) item.setGoodsName("");
            cell.setCellValue(new HSSFRichTextString(item.getGoodsName()));
            cell.setCellStyle(columnHeadStyle);
            cell = row.createCell(3);
            if (StringUtils.isEmpty(item.getSpecNumber())) item.setSpecNumber("");
            cell.setCellValue(new HSSFRichTextString(item.getSpecNumber()));
            cell.setCellStyle(columnHeadStyle);
            cell = row.createCell(4);
            if (StringUtils.isEmpty(item.getColorValue())) item.setColorValue("");
            if (StringUtils.isEmpty(item.getSizeValue())) item.setSizeValue("");
            if (StringUtils.isEmpty(item.getStyleValue())) item.setStyleValue("");
            cell.setCellValue(new HSSFRichTextString(item.getColorValue()+" "+item.getSizeValue()+"  "+item.getStyleValue()));
            cell.setCellStyle(columnHeadStyle);
            cell = row.createCell(5);
            if (StringUtils.isEmpty(item.getLocationName())) item.setLocationName("");
            cell.setCellValue(new HSSFRichTextString(item.getLocationName()));
            cell.setCellStyle(columnHeadStyle);
            cell = row.createCell(6);
            cell.setCellValue(new HSSFRichTextString(item.getQty().toString()));
            cell.setCellStyle(columnHeadStyle);
        }
        response.reset();
        response.setContentType("application/msexcel;charset=UTF-8");
        try {
            SimpleDateFormat newsdf=new SimpleDateFormat("yyyyMMddHHmmss");
            String date = newsdf.format(new Date());
            response.addHeader("Content-Disposition", "attachment;filename=\""
                    + new String(("stock" + date + ".xls").getBytes("GBK"),
                    "ISO8859_1") + "\"");
            OutputStream out = response.getOutputStream();
            workbook.write(out);
            out.flush();
            out.close();
        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(null, "导出失败!");
            e.printStackTrace();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "导出失败!");
            e.printStackTrace();
        }
        //添加系统日志
        logService.addUserAction(Integer.parseInt(request.getSession().getAttribute("userId").toString()), EnumUserActionType.Modify,
                "/stock/export","仓位库存查询导出 , 日期 : "+new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss").format(new Date()),"成功");

        return;
    }

}
