package com.b2c.erp.controller;

import com.b2c.erp.DataConfigObject;
import com.b2c.interfaces.DataReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import jakarta.servlet.http.HttpServletRequest;

/**
 * 数据报表
 */
@Controller
@RequestMapping("/data")
public class DataReportController {
    @Autowired
    private DataReportService dataReportService;
    /**
     * 日报表
     * @param model
     * @return
     */
    @RequestMapping("/daily_report")
    public String dailyReport(Model model, HttpServletRequest request){
        model.addAttribute("searchUrl","/data/daily_report");
        String page = request.getParameter("page");
        Integer pageIndex = 1, pageSize = DataConfigObject.getInstance().getPageSize();

        if (!StringUtils.isEmpty(page)) {
            pageIndex = Integer.parseInt(page);
        }
        String strDate = "";
        if (!StringUtils.isEmpty(request.getParameter("strDate"))) {
            strDate = request.getParameter("strDate");
            model.addAttribute("strDate", strDate);
        }
        String endDate = "";
        if (!StringUtils.isEmpty(request.getParameter("endDate"))) {
            endDate = request.getParameter("endDate");
            model.addAttribute("endDate", endDate);
        }
        var result = dataReportService.dailyReportPageList(pageIndex,pageSize,strDate,endDate);
        model.addAttribute("list", result.getList());
        model.addAttribute("totalSize", result.getTotalSize());
        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);

        model.addAttribute("view", "rbb");
        model.addAttribute("pView", "sj");


        return "report/data_daily_report";
    }
}
