package com.b2c.erp.controller.pdd;

import com.b2c.common.utils.DateUtil;
import com.b2c.entity.api.ApiResult;
import com.b2c.entity.api.ApiResultEnum;
import com.b2c.entity.pdd.ShopDataReportModel;
import com.b2c.interfaces.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import jakarta.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/ajax_pdd")
public class AjaxShopDataReportController {
    @Autowired
    private ShopService shopService;

    @RequestMapping(value = "/report_data", method = RequestMethod.POST)
    public ApiResult<List<ShopDataReportModel>> zhibo_data(HttpServletRequest request) {
//        var list = new ArrayList<ShopDataReportPddVo>();
//        ShopDataReportPddVo v=new ShopDataReportPddVo("2023-2-5",1,3,4,5,6);
//        list.add(v);
//        v=new ShopDataReportPddVo("2023-2-6",2,4,4,5,6);
//        list.add(v);

        String startDate = DateUtil.customizeDate_(7);//前7天
        String endDate = DateUtil.dateToString(new Date(),"yyyy-MM-dd");
        var dataList = shopService.getShopDateReport(startDate,endDate);

        return new ApiResult<>(ApiResultEnum.SUCCESS.getIndex(), "SUCCESS", dataList);

    }
}
