package com.b2c.erp.controller.bigData;

import com.b2c.entity.api.ApiResult;
import com.b2c.entity.DataRow;
import com.b2c.entity.bd.KeywordHotEntity;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.interfaces.ecom.KeywordHotService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import jakarta.servlet.http.HttpServletRequest;

@RequestMapping("/big_data")
@Controller
public class KeywordHotController {
    @Autowired
    private KeywordHotService service;
    private static Logger log = LoggerFactory.getLogger(KeywordHotController.class);
    /**
     * 订单列表
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/keyword_hot_list", method = RequestMethod.GET)
    public String list(Model model, HttpServletRequest request) {
        String page = request.getParameter("page");
        Integer pageIndex = 1;
        Integer pageSize = 100;
   
        String platform = "";
        String category = null;
        String keyword = "";

        if (!StringUtils.isEmpty(page)) {
            pageIndex = Integer.parseInt(page);
        }

        if (!StringUtils.isEmpty(request.getParameter("keyword"))) {
            keyword = request.getParameter("keyword");
            model.addAttribute("keyword",keyword);
        }
        
        if (!StringUtils.isEmpty(request.getParameter("platform"))) {
            platform = request.getParameter("platform");
            model.addAttribute("platform", platform);
        }
        if (!StringUtils.isEmpty(request.getParameter("category"))) {
            category = request.getParameter("category");
            model.addAttribute("category", request.getParameter("category"));
        }
        

        var result = service.getList(pageIndex,pageSize,keyword,platform,category);

        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("totalSize", result.getTotalSize());
        model.addAttribute("lists", result.getList());

        model.addAttribute("view", "keyword_hot_list");
        model.addAttribute("pView", "goods");
        return "bigData/keyword_hot_list";
    }


    @RequestMapping(value = "/keyword_hot_add", method = RequestMethod.GET)
    public String add(Model model, HttpServletRequest request) {
        return "bigData/keyword_hot_add";
    }

    @ResponseBody
    @RequestMapping(value = "/keyword_hot_add_ajax", method = RequestMethod.POST)
    public  ApiResult<String> addPostAjax(Model model, @RequestBody DataRow data, HttpServletRequest request) {
        KeywordHotEntity kw = new KeywordHotEntity();
        kw.setParentId(0l);
        kw.setPlatform(data.getString("platform"));
        kw.setCategory(data.getString("category"));
        kw.setKeyword( data.getString("keyword"));
        kw.setRanking(data.getInt("ranking"));
        kw.setGoodsCount(data.getInt("goodsCount"));
        kw.setSousuorenqi(data.getInt("sousuorenqi"));
        kw.setSousuoredu(data.getInt("sousuoredu"));
        kw.setDianjirenqi(data.getInt("dianjirenqi"));
        kw.setDianjiredu(data.getInt("dianjiredu"));
        
        kw.setDianjilv(data.getBigDecimal("dianjilv"));
        kw.setZhifulv(data.getBigDecimal("zhifulv"));
        kw.setJingzhengzhishu(data.getFloat("jingzhengzhishu"));
        kw.setChengjiaozhishu(data.getFloat("chengjiaozhishu"));
        kw.setIncludeDate(data.getString("include_date"));
    
        service.add(kw);
        
        return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(), "SUCCESS");
    }


}
