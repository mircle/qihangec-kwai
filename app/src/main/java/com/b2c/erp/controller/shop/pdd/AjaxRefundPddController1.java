// package com.b2c.erp.controller.shop.pdd;

// import com.alibaba.fastjson.JSON;
// import com.b2c.common.api.ApiResult;
// import com.b2c.common.api.ApiResultEnum;

// import com.b2c.entity.DataRow;
// import com.b2c.entity.apierp.ErpSalesOrderItemEntity;
// import com.b2c.entity.apierp.req.ErpSalesOrderRefundReq;

// import com.b2c.entity.pdd.RefundPddEntity;
// import com.b2c.entity.result.EnumResultVo;

// import com.b2c.interfaces.erp.ErpSalesOrderService;
// import com.b2c.interfaces.pdd.OrderPddRefundService;
// import com.b2c.interfaces.pdd.OrderPddService;
// import com.b2c.interfaces.ShopService;
// import com.b2c.interfaces.SysThirdSettingService;

// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;
// import org.springframework.beans.factory.annotation.Autowired;

// import org.springframework.util.StringUtils;
// import org.springframework.web.bind.annotation.RequestBody;
// import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestMethod;
// import org.springframework.web.bind.annotation.RestController;

// import jakarta.servlet.http.HttpServletRequest;
// import java.util.ArrayList;
// import java.util.List;

// @RequestMapping("/ajax_pdd")
// @RestController
// public class AjaxRefundPddController {
//     private static Logger log = LoggerFactory.getLogger(AjaxRefundPddController.class);

//     @Autowired
//     private SysThirdSettingService thirdSettingService;
//     @Autowired
//     private OrderPddService orderPddService;
//     @Autowired
//     private ErpSalesOrderService salesOrderService;

//     @Autowired
//     private OrderPddRefundService refundService;

//     @Autowired
//     private ShopService shopService;


//     /**
//      * 订单拦截
//      * @param data
//      * @param req
//      * @return
//      */
//     @RequestMapping(value = "/order_intercept", method = RequestMethod.POST)
//     public ApiResult<String> orderIntercept(@RequestBody DataRow data, HttpServletRequest req) throws Exception {
//         String orderSn = data.getString("orderSn");
//         if(StringUtils.isEmpty(orderSn)){
//             return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "参数错误，缺少orderSn");
//         }
//         Integer shopId = data.getInt("shopId");
//         if(shopId == null || shopId<=0){
//             return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "参数错误，缺少shopId");
//         }
//         String msg = data.getString("msg");
        
//         Integer afterType = data.getInt("afterType");

//         RefundPddEntity refund = refundService.getEntityByOrderSn(orderSn);

//         if(refund==null){
//             //本地库没有退货单，去拼多多接口拉取
            
                
//                     var order = orderPddService.getOrder(orderSn);
//                     var goods = order.getItems().get(0);
//                     Long refundId = refundService.getRefundIdMin() + 1;
//                     RefundPddEntity entity = new RefundPddEntity();
//                     entity.setAfter_sale_reason(msg);
//                     entity.setAfter_sales_status(3);
//                     entity.setAfter_sales_type(afterType);
//                     entity.setConfirm_time(0l);
//                     entity.setCreated_time(System.currentTimeMillis() / 1000);
//                     entity.setDiscount_amount(0.0);
//                     entity.setGoods_id(0);
//                     entity.setGoods_number(goods.getGoodsNum());
//                     entity.setSkuNumber(goods.getGoodsSpecNum());
//                     entity.setGoods_image(goods.getGoodsImg());
//                     entity.setGoods_name(goods.getGoodsName());
//                     entity.setGoods_price(goods.getGoodsPrice());
//                     entity.setSkuInfo(goods.getGoodsSpec());
//                     entity.setId(refundId);
//                     entity.setOrder_amount(order.getPay_amount());
//                     entity.setOrder_sn(orderSn);
//                     entity.setQuantity(goods.getQuantity());
//                     entity.setRefund_amount(order.getPay_amount());
//                     entity.setShopId(shopId);
//                     entity.setTracking_number(order.getTracking_number());
//                     entity.setUpdated_time("");
//                     refund = new RefundPddEntity();
//                     refund.setId(entity.getId());
//                     orderPddService.editRefundPddOrder(entity);
                

            
//         }

//         //更新快递信息到退货数据中
//         var result = orderPddService.interceptOrder(orderSn,refund.getId(),msg,afterType);
//         log.info(msg+"，结果："+ JSON.toJSONString(result));
//         if(result.getCode() == EnumResultVo.SUCCESS.getIndex()){
//             //拦截成功，开始推送到仓库
//             //var result1 = refundService.confirmRefund(result.getData());
//             //log.info("拦截成功，推送到仓库。推送结果："+ JSON.toJSONString(result1));
//             return new ApiResult<>(0, "SUCCESS");
//         }

//         return new ApiResult<>(ApiResultEnum.SystemException, "系统异常");
//     }
//     /**
//      * 标记已处理
//      *
//      * @param req
//      * @return
//      */
//     @RequestMapping(value = "/sign_refund", method = RequestMethod.POST)
//     public ApiResult<String> signRefund(@RequestBody DataRow data, HttpServletRequest req) {
//         Long refId = data.getLong("id");
//         if(refId == null || refId<=0){
//             return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "参数错误，缺少id");
//         }
//         Integer auditStatus = data.getInt("auditStatus");

// //        if (StringUtils.isEmpty(data.getString("companyCode")))
// //            return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "请选择快递公司");
// //        if (StringUtils.isEmpty(data.getString("code")))
// //            return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "请输入快递单号");

// //        var detail = refundService.getEntityById(refId);
// //        if(detail == null) return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "退货不存在");
// //        if(detail.getAfter_sales_type().intValue()!=3 && detail.getAuditStatus().intValue()!=0) return new ApiResult<>(EnumResultVo.SystemException.getIndex(), "已经处理过了");
// //        if(StringUtils.isEmpty(detail.getTracking_number())) return new ApiResult<>(EnumResultVo.SystemException.getIndex(), "还没有物流信息，请更新退货订单");

// //        var result = refundService.confirmRefund(refId);
// //        return new ApiResult<>(result.getCode(), result.getAuditStatustMsg());
//         refundService.signRefund(refId,auditStatus);
//         return new ApiResult<>(ApiResultEnum.SUCCESS, "SUCCESS");
//     }

//     @RequestMapping(value = "/refund_apply_pdd_submit", method = RequestMethod.POST)
//     public ApiResult<Integer> addOrderCancel(HttpServletRequest request) {
//             String logisticsCompany="";
//             if(!StringUtils.isEmpty(request.getParameter("logisticsCompany"))){
//                 logisticsCompany=request.getParameter("logisticsCompany");
//             }
//             String logisticsCode="";
//             if(!StringUtils.isEmpty(request.getParameter("logisticsCode"))){
//                 logisticsCode=request.getParameter("logisticsCode");
//             }
//             Long orderId = Long.parseLong(request.getParameter("order_id"));
//             ErpSalesOrderRefundReq applyVo=new ErpSalesOrderRefundReq();
//             applyVo.setOrderId(orderId);
//             applyVo.setLogisticsCompany(logisticsCompany);
//             applyVo.setLogisticsCode(logisticsCode);
//             List<ErpSalesOrderItemEntity>  itemVoList = new ArrayList<>();

//             String[] idDataArr = request.getParameterValues("item[]");
//             String[] nums = request.getParameterValues("count[]");
//             //循环查找选中的item
//             for (int i = 0; i < idDataArr.length; i++) {
//                 String[] idData = idDataArr[i].split(":");
//                 ErpSalesOrderItemEntity itemVo = new ErpSalesOrderItemEntity();
//                 itemVo.setId(Long.parseLong(idData[1]));
//                 itemVo.setRefundCount(Integer.parseInt(nums[Integer.parseInt(idData[0])]));
//                 itemVoList.add(itemVo);
//             }
//             applyVo.setItems(itemVoList);

//             var result= refundService.addPddOrderRefund(applyVo);
//             return new ApiResult<>(result.getCode(), result.getMsg());

//          //return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(), "功能暂时禁用");

//     }


    
// }
