//package com.b2c.erp.controller.purchase;
//
//import org.apache.poi.hssf.usermodel.HSSFCell;
//import org.apache.poi.hssf.usermodel.HSSFRow;
//import org.apache.poi.hssf.usermodel.HSSFSheet;
//import org.apache.poi.hssf.usermodel.HSSFWorkbook;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import jakarta.servlet.http.HttpServletRequest;
// import jakarta.servlet.http.HttpServletResponse;
//import javax.swing.*;
//import java.io.FileNotFoundException;
//import java.io.IOException;
//import java.io.OutputStream;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//
///**
// * 补货提醒
// */
//@RequestMapping("/purchase")
//@Controller
//public class BuHuoTiXingController {
//    @Autowired
//    private BuHuoTiXingService buHuoTiXingService;
//
//    /**
//     * 抖店补货提醒
//     * @param model
//     * @param request
//     * @return
//     */
//    @RequestMapping(value = "/bu_huo_ti_xing_dy", method = RequestMethod.GET)
//    public String getPurchaseList(Model model, HttpServletRequest request) {
//        var list = buHuoTiXingService.getBuHuoTiXingByDouYin();
//
//        model.addAttribute("list",list);
//        model.addAttribute("view", "bhtips");
//        model.addAttribute("pView", "cg");
//
//
//        return "purchase/bu_huo_ti_xing";
//    }
//
//    @ResponseBody
//    @RequestMapping("/bu_huo_ti_xing_export_dy")
//    public void outExport(HttpServletRequest request, HttpServletResponse response) {
//        var lists = buHuoTiXingService.getBuHuoTiXingByDouYin();
//        /***************根据店铺查询订单导出的信息*****************/
//        String excelFileName = "bu_huo_ti_xing_dy";//excel文件名前缀
//        //创建Excel工作薄对象
//        HSSFWorkbook workbook = new HSSFWorkbook();
//
//        //创建Excel工作表对象
//        HSSFSheet sheet = workbook.createSheet();
//        HSSFRow row = null;
//        HSSFCell cell = null;
//        //第一行为空
//        row = sheet.createRow(0);
//        cell = row.createCell(0);
//        cell.setCellValue("序号");
//        cell = row.createCell(1);
//        cell.setCellValue("款号");
//        cell = row.createCell(2);
//        cell.setCellValue("SKU");
//        cell = row.createCell(3);
//        cell.setCellValue("规格");
//        cell = row.createCell(4);
//        cell.setCellValue("供应商");
//        cell = row.createCell(5);
//        cell.setCellValue("库存总数");
//        cell = row.createCell(6);
//        cell.setCellValue("抖店-待发货总数");
//        cell = row.createCell(7);
//        cell.setCellValue("库存结余");
//
//        int currRowNum = 0;
//        // 循环写入数据
//        for (int i = 0; i < lists.size(); i++) {
//            currRowNum++;
//            //写入订单
//            var itemVo = lists.get(i);
//
//            //创建行
//            row = sheet.createRow(currRowNum);
//            //Id
//            cell = row.createCell(0);
//            cell.setCellValue(i+1);
//            //商品名称
//            cell = row.createCell(1);
//            cell.setCellValue(itemVo.getGoodsNumber());
//            //出库单号
//            cell = row.createCell(2);
//            cell.setCellValue(itemVo.getCode());
//            //退货单号
//            cell = row.createCell(3);
//            cell.setCellValue(itemVo.getSpecDesc());
//            //规格
//            cell = row.createCell(4);
//            cell.setCellValue("");
//            //SKU
//            cell = row.createCell(5);
//            cell.setCellValue(itemVo.getQty());
//            //商品款号
//            cell = row.createCell(6);
//            cell.setCellValue(itemVo.getTotal());
//            cell = row.createCell(7);
//            cell.setCellValue(itemVo.getQty() - itemVo.getTotal());
//        }
//        response.reset();
//        response.setContentType("application/msexcel;charset=UTF-8");
//        try {
//            SimpleDateFormat newsdf = new SimpleDateFormat("yyyyMMdd");
//            String date = newsdf.format(new Date());
//            response.addHeader("Content-Disposition", "attachment;filename=\""
//                    + new String((excelFileName + date + ".xls").getBytes("GBK"),
//                    "ISO8859_1") + "\"");
//            OutputStream out = response.getOutputStream();
//            workbook.write(out);
//            out.flush();
//            out.close();
//        } catch (FileNotFoundException e) {
//            JOptionPane.showMessageDialog(null, "导出失败!");
//            e.printStackTrace();
//        } catch (IOException e) {
//            JOptionPane.showMessageDialog(null, "导出失败!");
//            e.printStackTrace();
//        }
//
//        return;
//    }
//
//}
