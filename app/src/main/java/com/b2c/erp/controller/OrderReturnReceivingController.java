package com.b2c.erp.controller;

import com.b2c.entity.api.ApiResult;
import com.b2c.entity.DataRow;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.interfaces.ShopRefundOrderService;
import com.b2c.interfaces.ShopService;
import com.b2c.interfaces.WmsUserService;
import com.b2c.interfaces.erp.ErpUserActionLogService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import jakarta.servlet.http.HttpServletRequest;

/**
 * 描述：
 * 退货订单收货处理
 *
 * @author qlp
 * @date 2021-06-21 16:52
 */
@RequestMapping("/order_return")
@Controller
public class OrderReturnReceivingController {
    
    @Autowired
    private ShopRefundOrderService shopRefundOrderService;

    @Autowired
    private ShopService shopService;
    @Autowired
    private WmsUserService manageUserService;
    @Autowired
    private ErpUserActionLogService logService;
    private static Logger log = LoggerFactory.getLogger(OrderReturnReceivingController.class);

    /***
     * 退货订单list
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/receiving", method = RequestMethod.GET)
    public String receiving(Model model, HttpServletRequest request) {
        String num = "";
        if (!StringUtils.isEmpty(request.getParameter("num"))) {
            num = request.getParameter("num");
            model.addAttribute("num", num);
        }

       Integer shopType=8;
 /*        if (!StringUtils.isEmpty(request.getParameter("shopType"))) {
            shopType = Integer.parseInt(request.getParameter("shopType"));
            model.addAttribute("shopType", shopType);
        }*/


        var result = shopRefundOrderService.getShopRefundOrder(num,shopType);
        if(result.getCode() == EnumResultVo.ParamsError.getIndex()){
            log.info(result.getMsg());
        }
        if(StringUtils.isEmpty(num) == false) {
            if (result.getData() == null || result.getData().size() == 0) log.info("没有找到数据，物流单号：" + num);
        }
//        log.info(JSON.toJSONString(result));
//        PagingResponse<ErpOrderReturnEntity> result = orderReturnService.getList(pageIndex, pageSize, refNum, orderNum, logisticsCode,refMobile,status);
//
        model.addAttribute("list", result.getData());
        if(result.getData()!=null)
            model.addAttribute("totalSize", result.getData().size());
        else model.addAttribute("totalSize", 0);

//        model.addAttribute("view", "return_order_re");
//        model.addAttribute("pView", "sale");
        CommonControllerUtils.setViewKey(model,manageUserService,"return_order_re");
        model.addAttribute("ejcd", "收货");
        model.addAttribute("sjcd", "订单退货收货");

        return "tui/order_return_receiving";
    }

    /**
     * 标记已签收
     *
     * @param req
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/refund_receive_confirm", method = RequestMethod.POST)
    public ApiResult<String> receiveConfirm(@RequestBody DataRow data, HttpServletRequest req) {
        Long refId = data.getLong("id");
        if(refId == null || refId<=0){
            return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "参数错误，缺少id");
        }
        Integer shopId = data.getInt("shopId");
        if(shopId == null || shopId<=0){
            return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "参数错误，缺少shopId");
        }
        var shop = shopService.getShop(shopId);
        log.info("退货签收，店铺："+shop.getName()+"-"+shop.getTypeName()+",refundId："+refId);

        var result = shopRefundOrderService.refundReceiveConfirm(refId,shop.getType());
        return new ApiResult<>(result.getCode(),result.getMsg());
    }

    /**
     * 标记已处理
     *
     * @param req
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/sign_refund", method = RequestMethod.POST)
    public ApiResult<String> signRefund(@RequestBody DataRow data, HttpServletRequest req) {
        Long refId = data.getLong("id");
        if(refId == null || refId<=0){
            return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "参数错误，缺少id");
        }
        Integer status = data.getInt("auditStatus");
        if(status == null || status <=0){
            return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "参数错误，缺少status");
        }else if(status == 2){
            return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "参数错误，该接口不处理签收");
        }

        String remark = data.getString("remark");

        Integer shopId = data.getInt("shopId");
        if(shopId == null || shopId<=0){
            return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "参数错误，缺少shopId");
        }
        var shop = shopService.getShop(shopId);
        log.info("退货标记，店铺："+shop.getName()+"-"+shop.getTypeName()+",refundId："+refId+",auditStatus:"+status);

        var result =  shopRefundOrderService.signRefund(refId,shop.getType(),status,remark);
        return new ApiResult<>(result.getCode(),result.getMsg());
    }

    /**
     * 更新备注
     * @param req
     * @param reqData
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/refund_remark_updata", method = RequestMethod.POST)
    public ApiResult<String> orderRemark(HttpServletRequest req,@RequestBody DataRow reqData) throws Exception {
        Long refundId = reqData.getLong("refundId");
        String remark = reqData.getString("remark");
        
        if(refundId == null || refundId<=0){
            return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "参数错误，缺少id");
        }

        Integer shopId = reqData.getInt("shopId");
        if(shopId == null || shopId<=0){
            return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "参数错误，缺少shopId");
        }
        var shop = shopService.getShop(shopId);
        log.info("保存备注......................"+remark+"，店铺："+shop.getName()+"-"+shop.getTypeName()+",refundId："+refundId);
      
        var result =  shopRefundOrderService.signRemark(refundId,shop.getType(),remark);
        return new ApiResult<>(result.getCode(),result.getMsg());
        
    }

}
