package com.b2c.erp.controller;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.FundsDetailEntity;
import com.b2c.erp.DataConfigObject;
import com.b2c.interfaces.funds.FundsDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jakarta.servlet.http.HttpServletRequest;

@RequestMapping("/funds")
@Controller
public class FundsDetailController {
    @Autowired
    private FundsDetailService fundsDetailService;

    @RequestMapping(value = "/detail_list", method = RequestMethod.GET)
    public String getOrderList(Model model, HttpServletRequest request) {
        Integer flag = 0; //0订单总览页面,1发货单页面
        String page = request.getParameter("page");
        Integer pageIndex = 1, pageSize = DataConfigObject.getInstance().getPageSize();

        String source = "";
        String sourceNo = "";
        Integer type = null;
        String createDate = "";

        if (!StringUtils.isEmpty(page)) {
            pageIndex = Integer.parseInt(page);
        }

        if (!StringUtils.isEmpty(request.getParameter("sourceNo"))) {
            sourceNo = request.getParameter("sourceNo");
            model.addAttribute("sourceNo",sourceNo);
        }
        if (!StringUtils.isEmpty(request.getParameter("source"))) {
            source = request.getParameter("source");
            model.addAttribute("source",source);
        }

        if (!StringUtils.isEmpty(request.getParameter("type"))) {
            type = Integer.parseInt(request.getParameter("type"));
        }

        if (!StringUtils.isEmpty(request.getParameter("createDate"))) {
            createDate = request.getParameter("createDate");
            model.addAttribute("createDate",createDate);
        }


        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);

        PagingResponse<FundsDetailEntity> result = fundsDetailService.getFundsDetailList(pageIndex, pageSize,type,source,sourceNo,createDate);
        model.addAttribute("totalSize", result.getTotalSize());
        model.addAttribute("lists", result.getList());


        model.addAttribute("view", "funds_det");
        model.addAttribute("pView", "pd");
        model.addAttribute("ejcd", "财务管理");
        model.addAttribute("sjcd", "资金进出明细");
        model.addAttribute("pageTitle", "资金进出明细");

        return "funds/funds_detail_list";
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String fundCreate(Model model, HttpServletRequest request) {
        model.addAttribute("view", "funds_det");
        model.addAttribute("pView", "pd");

        return "funds/funds_create_iframe";
    }

}
