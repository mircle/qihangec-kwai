// package com.b2c.erp.controller.ecom;

// import jakarta.servlet.http.HttpServletRequest;

// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.stereotype.Controller;
// import org.springframework.ui.Model;
// import org.springframework.util.StringUtils;
// import org.springframework.web.bind.annotation.RequestBody;
// import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestMethod;
// import org.springframework.web.bind.annotation.RequestParam;
// import org.springframework.web.bind.annotation.ResponseBody;

// import com.b2c.common.api.ApiResult;
// import com.b2c.entity.DataRow;
// import com.b2c.entity.ecom.KeywordDataEntity;
// import com.b2c.entity.ecom.KeywordsEntity;
// import com.b2c.entity.result.EnumResultVo;
// import com.b2c.interfaces.ecom.EcomKeywordService;

// @RequestMapping("/ecom")
// @Controller
// public class KeywordDataController {
//     @Autowired
//     private EcomKeywordService keywordService;
//     private static Logger log = LoggerFactory.getLogger(KeywordDataController.class);

//     /**
//      * 
//      * @param model
//      * @param id 关键词id
//      * @param request
//      * @return
//      */
//     @RequestMapping(value = "/keyword_data_list", method = RequestMethod.GET)
//     public String list(Model model,@RequestParam Long id, HttpServletRequest request) {
//         String page = request.getParameter("page");
//         Integer pageIndex = 1;
//         Integer pageSize = 100;
//         model.addAttribute("keywordId", id);
//         var keyword = keywordService.getKeywordById(id);
//         model.addAttribute("keyword", keyword);
        
//         var result = keywordService.getDataList(pageIndex,pageSize,id);
//         model.addAttribute("pageIndex", pageIndex);
//         model.addAttribute("pageSize", pageSize);


//         model.addAttribute("totalSize", result.getTotalSize());
//         model.addAttribute("lists", result.getList());

//         model.addAttribute("view", "keyword");
//         model.addAttribute("pView", "sale");
//         return "ecom/keyword_data_list";
//     }


//     @RequestMapping(value = "/keyword_data_add", method = RequestMethod.GET)
//     public String add(Model model,@RequestParam Long id,  HttpServletRequest request) {
//         var keyword = keywordService.getKeywordById(id);
//         model.addAttribute("keyword", keyword);
//         return "ecom/keyword_data_add";
//     }

//     @ResponseBody
//     @RequestMapping(value = "/keyword_data_add_ajax", method = RequestMethod.POST)
//     public  ApiResult<String> addPostAjax(Model model, @RequestBody DataRow data, HttpServletRequest request) {
//         KeywordDataEntity d = new KeywordDataEntity();
//         d.setKeywordId(data.getLong("keywordId"));
//         d.setRank(data.getInt("rank"));
//         d.setGoodsCount(data.getInt("goodsCount"));
//         d.setSousuorenqi(data.getInt("sousuorenqi"));
//         d.setSousuoredu(data.getInt("sousuoredu"));
//         d.setDianjirenqi(data.getInt("dianjirenqi"));
//         d.setDianjiredu(data.getInt("dianjiredu"));
//         d.setDianjilv(data.getFloat("dianjilv"));
//         d.setZhifulv(data.getFloat("zhifulv"));
//         d.setJingzhengzhishu(data.getFloat("jingzhengzhishu"));
//         d.setChengjiaozhishu(data.getFloat("chengjiaozhishu"));
//         d.setShichangchujia(data.getFloat("shichangchujia"));
//         d.setIncludeDate(data.getString("include_date"));




//         keywordService.addKeywordData(d);
//         //keyWordService.addKeyWord(kw);
//         return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(), "SUCCESS");
//     }


// }
