package com.b2c.erp.controller.shop.douyin;

import com.alibaba.fastjson.JSONObject;
import com.b2c.entity.api.ApiResult;
import com.b2c.common.utils.DateUtil;
import com.b2c.common.utils.ExpressClient;
import com.b2c.common.utils.MD5Utils;
import com.b2c.entity.DataRow;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.interfaces.ShopService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import jakarta.servlet.http.HttpServletRequest;
import java.net.http.HttpResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.b2c.common.utils.HttpUtil.map2Url;

@RequestMapping("/ajax_douyin")
@RestController
public class AjaxOauthDYController {
    private static Logger log = LoggerFactory.getLogger(AjaxOauthDYController.class);
    @Autowired
    private ShopService shopService;
    /**
     * 抖音授权
     * @param data
     * @param req
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/oauth", method = RequestMethod.POST)
    public ApiResult<Integer> oauth(@RequestBody DataRow data, HttpServletRequest req) throws Exception {
        var shop = shopService.getShop(data.getInt("shopId"));
        String appKey = shop.getAppkey();
        String appSercet = shop.getAppSercet();
        LinkedHashMap<String, Object> jsonMap1 =new LinkedHashMap<>();
        jsonMap1.put("code","");//截至时间
        jsonMap1.put("grant_type","authorization_self");
        jsonMap1.put("shop_id", shop.getSellerUserId());

        JSONObject jsonObject1 = new JSONObject(true);
        jsonObject1.putAll(jsonMap1);

        String paramJson1 =jsonObject1.toJSONString();

        String timestamp1 = DateUtil.dateToString(new Date(),"yyyy/MM/dd HH:mm:ss");

        String signStr1 = "app_key"+appKey+"method"+"token.create"+"param_json"+paramJson1+"timestamp"+timestamp1+"v"+"2";
        signStr1= appSercet+signStr1+appSercet;

        String sign1 = MD5Utils.MD5Encode(signStr1);
        Map<String, String> params1 = new HashMap<>();
        params1.put("app_key", appKey);
        params1.put("method", "token.create");
        params1.put("param_json",paramJson1);
        params1.put("timestamp", timestamp1);
        params1.put("v","2");
        params1.put("sign",sign1);

        HttpResponse<String> response1 = ExpressClient.doPost("https://openapi-fxg.jinritemai.com/token/create", map2Url(params1));

        var resObj = JSONObject.parseObject(response1.body());
        if(!StringUtils.isEmpty(resObj.getJSONObject("data"))){
            String accessToken = resObj.getJSONObject("data").getString("access_token");
            shopService.updateSessionKey(data.getInt("shopId"),shop.getSellerUserId(),accessToken);
            return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(),"授权成功");
        }

        return new ApiResult<>(EnumResultVo.Fail.getIndex(),"授权失败:"+resObj.getString("message"));

    }
}
