// package com.b2c.erp.controller.douyin;

// import java.math.BigDecimal;
// import java.util.List;

// import jakarta.servlet.http.HttpServletRequest;

// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.stereotype.Controller;
// import org.springframework.ui.Model;
// import org.springframework.util.StringUtils;
// import org.springframework.web.bind.annotation.RequestBody;
// import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestMethod;
// import org.springframework.web.bind.annotation.ResponseBody;

// import com.b2c.common.api.ApiResult;
// import com.b2c.entity.DataRow;
// import com.b2c.entity.datacenter.DcShopEntity;
// import com.b2c.entity.douyin.DyGoodsEntity;
// import com.b2c.erp.DataConfigObject;
// import com.b2c.interfaces.ShopService;
// import com.b2c.interfaces.DyGoodsService;

// @RequestMapping("/douyin")
// @Controller
// public class DyGoodsController {
//     private static Logger log = LoggerFactory.getLogger(DyGoodsController.class);
//     @Autowired
//     private ShopService shopService;
//     @Autowired
//     private DyGoodsService goodsService;

//     /**
//      * 商品列表
//      * @param model
//      * @param request
//      * @param shopId
//      * @return
//      */
//     @RequestMapping("/goods_list")
//     public String goodsList(Model model, HttpServletRequest request){
//         Integer userId = DataConfigObject.getInstance().getLoginUserId();
//         //有权限的店铺列表
//         List<DcShopEntity> shops = shopService.getShopListByUserId(userId,4);//type=5是拼多多
//         model.addAttribute("shops", shops);
        
//         Integer shopId=null;
//         if (!StringUtils.isEmpty(request.getParameter("shopId"))){
//             shopId = Integer.parseInt(request.getParameter("shopId")); 
//             model.addAttribute("shopId",shopId);
//         }


//         String goodsNum="";
//         if (!StringUtils.isEmpty(request.getParameter("goodsNum"))) {
//             goodsNum = request.getParameter("goodsNum");
//             model.addAttribute("goodsNum", goodsNum);
//         }


//         Integer pageIndex = 1, pageSize = DataConfigObject.getInstance().getPageSize();
//         if (!StringUtils.isEmpty(request.getParameter("page"))) {
//             pageIndex = Integer.parseInt(request.getParameter("page"));
//         }
//         var result = goodsService.getGoodsList(shopId,pageIndex,pageSize,goodsNum);

//         model.addAttribute("pageIndex", pageIndex);
//         model.addAttribute("pageSize", pageSize);
//         model.addAttribute("totalSize", result.getTotalSize());
//         model.addAttribute("lists", result.getList());
       

//         model.addAttribute("view", "wdspfbjl");
//         model.addAttribute("pView", "goods");

//         return "douyin/goods_list_dy";
//     }

//     @RequestMapping(value = "/goods_add", method = RequestMethod.GET)
//     public String add(Model model, HttpServletRequest request) {
//         Integer shopId =22;

//         if(StringUtils.hasText(request.getParameter("shopId")))
//         {
//             try {
//                 shopId = Integer.parseInt(request.getParameter("shopId"));
//             } catch (Exception e) {
//             }
//         }
//         model.addAttribute("shopId", shopId);


//         return "douyin/goods_add_dy";
//     }

//     @ResponseBody
//     @RequestMapping(value = "/goods_add_ajax", method = RequestMethod.POST)
//     public  ApiResult<String> addPostAjax(Model model, @RequestBody DataRow data, HttpServletRequest request) {
//         String title = data.getString("title");
//         Integer shopId = data.getInt("shopId");
//         Long goodsId = data.getLong("goodsId");
//         String goodsNum = data.getString("goodsNum");
//         String goodsImg = data.getString("goodsImg");
//         Double price = data.getDouble("price");
//         String publishDate = data.getString("publishDate");
        
//         DyGoodsEntity goods = new DyGoodsEntity();
//         goods.setGoodsId(goodsId);
//         goods.setGoodsImg(goodsImg);
//         goods.setShopId(shopId);
//         goods.setTitle(title);
//         goods.setGoodsNum(goodsNum);
//         goods.setPrice(new BigDecimal(price));
//         goods.setPublishTime(publishDate);

//         var result = goodsService.addGoods(goods);
        
//         return new ApiResult<>(result.getCode(), result.getMsg());
//     }
// }
