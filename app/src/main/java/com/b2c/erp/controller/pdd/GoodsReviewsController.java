package com.b2c.erp.controller.pdd;

import java.util.List;

import jakarta.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import com.b2c.entity.datacenter.DcShopEntity;
import com.b2c.erp.DataConfigObject;
import com.b2c.interfaces.ShopService;

@RequestMapping("/pdd")
@Controller
public class GoodsReviewsController {
    private static Logger log = LoggerFactory.getLogger(GoodsReviewsController.class);
    
    @Autowired
    private ShopService shopService;

    @RequestMapping("/reviews_list")
    public String goodsList(Model model, HttpServletRequest request){
        Integer userId = DataConfigObject.getInstance().getLoginUserId();
        //有权限的店铺列表
        List<DcShopEntity> shops = shopService.getShopListByUserId(userId,5);//type=5是拼多多
        model.addAttribute("shops", shops);
        Integer shopId=null;
        if (!StringUtils.isEmpty(request.getParameter("shopId"))){
            shopId = Integer.parseInt(request.getParameter("shopId")); 
            model.addAttribute("shopId",shopId);
        }



        String goodsNum="";
        if (!StringUtils.isEmpty(request.getParameter("goodsNum"))) {
            goodsNum = request.getParameter("goodsNum");
            model.addAttribute("goodsNum", goodsNum);
        }


        Integer pageIndex = 1, pageSize = DataConfigObject.getInstance().getPageSize();
        if (!StringUtils.isEmpty(request.getParameter("page"))) {
            pageIndex = Integer.parseInt(request.getParameter("page"));
        }
        // var result = goodsService.getGoodsList(shopId,pageIndex,pageSize,goodsNum,null,null);

        // model.addAttribute("pageIndex", pageIndex);
        // model.addAttribute("pageSize", pageSize);
        // model.addAttribute("totalSize", result.getTotalSize());
        // model.addAttribute("lists", result.getList());
       

        model.addAttribute("view", "pddshop");
        model.addAttribute("pView", "sale");

        return "pdd/goods_reviews_list_pdd";
    }
}
