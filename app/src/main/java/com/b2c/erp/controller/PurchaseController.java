package com.b2c.erp.controller;

import com.alibaba.fastjson.JSON;
import com.b2c.entity.result.PagingResponse;
import com.b2c.common.utils.DateUtil;
import com.b2c.entity.erp.InvoiceEntity;
import com.b2c.entity.erp.InvoiceInfoEntity;
import com.b2c.entity.erp.enums.ContactTypeEnum;
import com.b2c.entity.erp.enums.InvoiceBillStatusEnum;
import com.b2c.entity.erp.enums.InvoiceBillTypeEnum;
import com.b2c.entity.erp.enums.InvoiceTransTypeEnum;
import com.b2c.entity.vo.ErpPurchaseStockInItemVo;
import com.b2c.entity.vo.wms.InvoiceDetailVo;
import com.b2c.entity.enums.EnumUserActionType;
import com.b2c.entity.enums.IsDeleteEnum;
import com.b2c.interfaces.erp.ErpGoodsService;
import com.b2c.interfaces.erp.ErpUserActionLogService;
import com.b2c.interfaces.wms.*;
import com.b2c.repository.utils.OrderNumberUtils;
import com.b2c.entity.vo.finance.ErpStockInFormItemVo;
import com.b2c.erp.DataConfigObject;
import com.b2c.entity.ContactEntity;

import org.apache.poi.hssf.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import jakarta.servlet.http.HttpServletRequest;
 import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import javax.swing.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * 描述：
 * 采购Controller
 *
 * @author qlp
 * @date 2019-03-21 13:41
 */
@Controller
@RequestMapping("/purchase")
public class PurchaseController {
    @Autowired
    private ContactService contactService;
    @Autowired
    private InvoiceService invoiceService;
    @Autowired
    private ErpGoodsService goodsService;
    
    @Autowired
    private ErpStockInCheckoutService stockInCheckoutService;
    @Autowired
    private ErpGoodsStockLogsService goodsStockLogsService;
    @Autowired
    private ErpUserActionLogService logService;
    @Autowired
    private StockInService stockInService;
    private static Logger log = LoggerFactory.getLogger(PurchaseController.class);
    /**
     * 出货单列表
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String getPurchaseList(Model model, HttpServletRequest request) {
        Integer pageIndex = 1;
        Integer pageSize = DataConfigObject.getInstance().getPageSize();
        ;
        if (StringUtils.isEmpty(request.getParameter("page")) == false) {
            try {
                pageIndex = Integer.parseInt(request.getParameter("page"));
            } catch (Exception e) {
            }
        }

        String billNo = request.getParameter("billNo");
        model.addAttribute("billNo", billNo);
        String startDate = request.getParameter("startDate");
        model.addAttribute("startDate", startDate);
        String endDate = request.getParameter("endDate");
        model.addAttribute("endDate", endDate);
        String transType = request.getParameter("transType");
        InvoiceTransTypeEnum typeEnum = null;
        if(StringUtils.hasText(transType)){
            typeEnum = InvoiceTransTypeEnum.valueOf(transType);
        }
        Integer contactId = null;
        if(StringUtils.hasText(request.getParameter("contactId"))){
            contactId = Integer.parseInt(request.getParameter("contactId"));
        }

        PagingResponse<InvoiceEntity> result = invoiceService.getList(pageIndex, pageSize, typeEnum, InvoiceBillTypeEnum.Purchase, null, null, null, billNo, startDate, endDate,contactId);
        model.addAttribute("list", result.getList());
        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("totalSize", result.getTotalSize());

        //获取供应商
        List<ContactEntity> contacts = contactService.getList(ContactTypeEnum.Supplier);
        model.addAttribute("contacts", contacts);
        
        model.addAttribute("view", "cglb");
        model.addAttribute("pView", "cg");
        model.addAttribute("ejcd", "采购管理");
        model.addAttribute("sjcd", "采购单列表");

        return "purchase_list";
    }


    /***
     * 单详情(打印，弹窗)
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    public String purchaseDetail(Model model, HttpServletRequest request, @RequestParam Long id) {
        var detail = invoiceService.getInvoiceDetail(id);
        model.addAttribute("detail", detail);
        return "purchase_detail";
    }


//     /***
//      * 验货表单
//      * @param model
//      * @param request
//      * @return
//      */
//     @RequestMapping(value = "/checkout_form", method = RequestMethod.GET)
//     public String checkoutForm(Model model, HttpServletRequest request, @RequestParam Long id) {
//         var invoice = invoiceService.getInvoiceDetail(id);
//         if (invoice == null) {
//             model.addAttribute("msg", "没有找到数据");
//             model.addAttribute("invoice", new InvoiceDetailVo());
//         } else {
//             model.addAttribute("invoice", invoice);
//             //查询验货记录
//             var checkoutList = stockInCheckoutService.getCheckoutForm(id);
//             String number = invoice.getBillNo() + "001";//入库编号
//             if (checkoutList != null) {
//                 number = invoice.getBillNo() + "00" + (checkoutList.size() + 1);
//             }

//             model.addAttribute("number", number);//验货编号
//         }

//         model.addAttribute("order", new ErpOrderEntity());

//         model.addAttribute("view", "cglb");
//         model.addAttribute("pView", "cg");
//         model.addAttribute("ejcd", "采购");
//         model.addAttribute("sjcd", "采购单列表");

//         return "purchase_checkout_form";
//     }

//     /**
//      * 验货表单提交
//      *
//      * @param model
//      * @param request
//      * @return
//      */
//     @RequestMapping(value = "/checkout_form", method = RequestMethod.POST)
//     public String checkoutFormPost(Model model, HttpServletRequest request) {
//         Long invoiceId = Long.parseLong(request.getParameter("invoiceId"));//单据Id
//         String invoiceNo = request.getParameter("invoiceNo");//单据编号
//         String number = request.getParameter("number");//入库检验编号
//         Integer qcReport = Integer.parseInt(request.getParameter("qcReport"));//是否有QC报告
//         String qcInspector = request.getParameter("qcInspector");//QC报告人
//         String[] invoiceInfoIdArray = request.getParameterValues("invoiceInfoId");//invoice info id
//         String[] specNumberArray = request.getParameterValues("specNumber");//检验的商品sku编码
//         String[] inQuantityArray = request.getParameterValues("inQuantity");//检验的商品数量

//         List<ErpStockInCheckoutItemEntity> items = new ArrayList<>();

//         for (int i = 0; i < specNumberArray.length; i++) {
//             long quantity = Long.parseLong(inQuantityArray[i]);
//             Long invoiceInfoId = Long.parseLong(invoiceInfoIdArray[i]);

//             var spec = goodsService.getSpecByNumber(specNumberArray[i]);
//             if (spec != null && quantity > 0) {
//                 ErpStockInCheckoutItemEntity item = new ErpStockInCheckoutItemEntity();
//                 item.setSpecId(spec.getId());
//                 item.setGoodsId(spec.getGoodsId());
//                 item.setSpecNumber(spec.getSpecNumber());
//                 item.setInQuantity(0l);
//                 item.setQuantity(quantity);
//                 item.setInvoiceInfoId(invoiceInfoId);
//                 items.add(item);
//             }

//         }
//         HttpSession session = request.getSession();
//         Integer userId = Integer.parseInt(session.getAttribute("userId").toString());
//         String userName = (String) session.getAttribute("userName");

//         com.b2c.entity.vo.ErpStockInCheckoutFormAddVo addVo = new ErpStockInCheckoutFormAddVo();
//         addVo.setInvoiceId(invoiceId);
//         addVo.setInvoiceNo(invoiceNo);
//         addVo.setNumber(number);
//         addVo.setHasQcReport(qcReport);
//         addVo.setQcInspector(qcInspector);
//         addVo.setCheckoutUserId(userId);
//         addVo.setCheckoutUserName(userName);
//         addVo.setItems(items);


//         stockInCheckoutService.addCheckoutForm(addVo);
//         //添加系统日志
//         logService.addUserAction(userId, EnumUserActionType.Audit, "/purchase/checkout_form?id=" + invoiceId, "入库验货 , id" + invoiceId + " ,编号 : " + invoiceNo + ",日期 : " + new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss").format(new Date()), "成功");
//         return "redirect:/purchase/list";

// //        model.addAttribute("view", "rkjy");
// //        model.addAttribute("pView", "rk");
// //        model.addAttribute("ejcd", "入库");
// //        model.addAttribute("sjcd", "采购入库");
// //        return "checkout";

//     }

    


    /***
     * 采购入库GET
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/stock_in", method = RequestMethod.GET)
    public String purchaseStockIn(Model model, @RequestParam Long id, HttpServletRequest request) {
        HttpSession session = request.getSession();
        Object userid = session.getAttribute("userId");
        String userName = (String) session.getAttribute("userName");
        log.info("访问采购入库页面：{userName:"+userName+",purchaseId,"+id+"}");
        var invoice = invoiceService.getInvoiceDetail(id);
        if (invoice == null) {
            model.addAttribute("msg", "没有找到数据");
            model.addAttribute("invoice", new InvoiceDetailVo());
        } else if(invoice.getBillStatus().intValue() != InvoiceBillStatusEnum.Audited.getIndex()){
            model.addAttribute("msg", "该表单状态:"+InvoiceBillStatusEnum.getName(invoice.getBillStatus())+",不能操作入库");
            model.addAttribute("invoice", invoice);

        }else{
            model.addAttribute("invoice", invoice);
        }
        model.addAttribute("view", "cgrk1");
        model.addAttribute("pView", "rk");
        model.addAttribute("ejcd", "收货&入库");
        model.addAttribute("sjcd", "采购入库(扫码)");
        return "purchase_stock_in";
    }

    /**
     * 采购入库POST
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/stock_in", method = RequestMethod.POST)
    public String stockInPost(Model model, HttpServletRequest request) {
        Long invoiceId = Long.parseLong(request.getParameter("invoiceId"));
        Integer qcReport = Integer.parseInt(request.getParameter("qcReport"));
        String qcInspector = request.getParameter("qcInspector");

        HttpSession session = request.getSession();
        Object userid = session.getAttribute("userId");
        String userName = (String) session.getAttribute("userName");
        log.info("采购入库-提交：{userName:"+userName+"}");
        //查询入库单
        var invoice = invoiceService.getInvoiceDetail(invoiceId);
        if (invoice == null) {
            model.addAttribute("msg", "没有找到数据");
            model.addAttribute("invoice", new InvoiceDetailVo());
            return "redirect:/purchase/stock_in?id=" + invoiceId;
        }
        if(invoice.getBillStatus().intValue() != InvoiceBillStatusEnum.Audited.getIndex()){
            model.addAttribute("msg", "该表单状态不是已审核状态");
            model.addAttribute("invoice", new InvoiceDetailVo());
            return "redirect:/purchase/stock_in?id=" + invoiceId;
        }

        /***组合入库items***/
        String[] specIdArr = request.getParameterValues("specId");
        String[] specNumberArr = request.getParameterValues("specNumber");
        String[] goodsNumberArr = request.getParameterValues("goodsNumber");
        String[] itemIdArr = request.getParameterValues("infoId");
        String[] locationIdArr = request.getParameterValues("locationId");
        String[] locationNumberArr = request.getParameterValues("locationNumber");
        String[] quantityArr = request.getParameterValues("quantity");

        List<ErpPurchaseStockInItemVo> items = new ArrayList<>();
        for (int i = 0; i < specIdArr.length; i++) {
            if (StringUtils.isEmpty(locationNumberArr[i]) == false && StringUtils.isEmpty(quantityArr[i]) == false) {

                ErpPurchaseStockInItemVo item = new ErpPurchaseStockInItemVo();
                item.setSpecId(Integer.parseInt(specIdArr[i]));
                item.setSpecNumber(specNumberArr[i]);
                item.setGoodsNumber(goodsNumberArr[i]);
                item.setLocationId(Integer.parseInt(locationIdArr[i]));
                item.setLocationNumber(locationNumberArr[i]);
                item.setQuantity(Long.parseLong(quantityArr[i]));
                item.setInvoiceInfoId(Long.parseLong(itemIdArr[i]));
                items.add(item);
            }
        }
        log.info("采购入库-提交："+ JSON.toJSONString(items));
        var result = stockInService.purchaseStockIn(invoiceId, qcReport, qcInspector, items, (int) userid, userName);

        //添加系统日志
        logService.addUserAction(Integer.parseInt(request.getSession().getAttribute("userId").toString()), EnumUserActionType.Add,
                "/purchase/stock_in", "采购入库 , id : " + invoiceId + " , 日期 : " + new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss").format(new Date()), "成功");

        return "redirect:/purchase/stock_in?id=" + invoiceId;
    }


    /**
     * 添加采购单GET
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String getPurchaseAdd(Model model, HttpServletRequest request) {
        HttpSession session = request.getSession();
        String userName = (String) session.getAttribute("trueName");
        log.info("访问添加采购单页面：{userName:"+userName+"}");
        model.addAttribute("userName", userName);
        model.addAttribute("time", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        //生成单据编号
        String billNo = "PUR" + OrderNumberUtils.getOrderIdByTime();
        model.addAttribute("billNo", billNo);

        //获取供应商
        List<ContactEntity> contacts = contactService.getList(ContactTypeEnum.Supplier);
        model.addAttribute("contacts", contacts);

        //仓库列表
       /* List<ErpStockLocationEntity> houses = stockLocationService.getList();
        model.addAttribute("houses", houses);
        model.addAttribute("housesJSON", JSON.toJSONString(houses));*/

        model.addAttribute("view", "cglb");
        model.addAttribute("pView", "cg");
        model.addAttribute("ejcd", "采购管理");
        model.addAttribute("sjcd", "新增采购单");

        return "purchase_add";
    }
//
//    /**
//     * 添加购货单POST
//     *
//     * @param model
//     * @param request
//     * @return
//     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String getPurchaseAddPost(Model model, HttpServletRequest request) {

        Integer userId = DataConfigObject.getInstance().getLoginUserId();
        String userName = request.getParameter("userName");

        /***表单信息***/
        String transType = request.getParameter("transType");
        String transTypeName = "";
        if(StringUtils.isEmpty(transType)){
            transType = InvoiceTransTypeEnum.Purchase.getIndex();
            transTypeName = InvoiceTransTypeEnum.Purchase.getName();
        }else{
            transTypeName = InvoiceTransTypeEnum.valueOf(transType).getName();
        }
        String srcOrderId = request.getParameter("srcOrderId");

        Long supplierId = Long.parseLong(request.getParameter("supplierId"));
        String billNo = request.getParameter("billNo");
        String billDate = request.getParameter("billDate");
        Double invoiceDisRate = Double.valueOf(request.getParameter("invoiceDisRate"));//优惠率
        Double invoiceDisAmount = Double.valueOf(request.getParameter("invoiceDisAmount"));//优惠金额
        Double invoiceAmount = Double.valueOf(request.getParameter("invoiceAmount"));//优惠后金额
        Double rpAmount = Double.valueOf(request.getParameter("rpAmount"));//本次付款
        Double arrears = Double.valueOf(request.getParameter("arrears"));//本次欠款
        String contractNo = request.getParameter("contractNo");

        /***要计算的内容***/
        Double totalAmount = 0.0;//购货总金额（商品价格*数量之和）
        Long totalQuantity = 0l;//总数量(商品数量之和）
                Double totalDiscount = 0.0;//总折扣（计算商品折扣和单据折扣之和）


        /***商品信息****/
        String[] specNumber = request.getParameterValues("specNumber");//规格编码组合
        String[] goodsNumber = request.getParameterValues("goodsNumber");//商品编码组合
        String[] goodsId = request.getParameterValues("goodsId");//商品id组合
        String[] specId = request.getParameterValues("specId");//商品规格id组合

        String[] quantity = request.getParameterValues("quantity");//数量组合
        String[] price = request.getParameterValues("price");//购货单价
        String[] disRate = request.getParameterValues("disRate");//折扣率(%)
        String[] disAmount = request.getParameterValues("disAmount");//折扣额
        String[] amount = request.getParameterValues("amount");//购货金额
        //String[] number = request.getParameterValues("number");//序列号
        String[] note = request.getParameterValues("note");//备注
        String[] itemId = request.getParameterValues("itemId");
        

        List<InvoiceInfoEntity> invoiceInfo = new ArrayList<>();
        if (specNumber != null && specNumber.length > 0) {
            //循环组合商品信息
            for (int i = 0; i < specNumber.length; i++) {
                InvoiceInfoEntity info = new InvoiceInfoEntity();
                if (StringUtils.isEmpty(specNumber[i]) == false) {
                    info.setSpecNumber(specNumber[i]);
                    info.setGoodsNumber(goodsNumber[i]);
                    if (goodsId.length > i) info.setGoodsId(Integer.valueOf(goodsId[i]));
                    if (specId.length > i) info.setSpecId(Integer.valueOf(specId[i]));
                    if (goodsNumber.length > i) info.setGoodsNumber(goodsNumber[i]);
                    if (quantity.length > i) info.setQuantity(Long.valueOf(quantity[i]));
                    if (price.length > i) info.setPrice(Double.valueOf(price[i]));
                    if (disRate.length > i) info.setDisRate(Double.valueOf(disRate[i]));
                    if (disAmount.length > i) info.setDisAmount(0d);//Double.valueOf(disAmount[i])
                    if (amount.length > i) info.setAmount(Double.valueOf(amount[i]));
                    //if (number.length > i) info.setSerialno(number[i]);
                    if (note.length > i) info.setDescription(note[i]);
                    //if (locationId.length > i) info.setLocationId(Integer.valueOf(locationId[i]));
                    if (info.getPrice() != null && info.getQuantity() != null) {
                        totalQuantity += info.getQuantity();
                        totalAmount += info.getPrice() * info.getQuantity();
                    }
                    if (info.getDisAmount() != null) totalDiscount += info.getDisAmount();
                    if(itemId!=null && itemId.length > i){
                        info.setSrcOrderNo(itemId[i]);
                    }
                    invoiceInfo.add(info);
                }
            }
        }
        if (invoiceDisAmount != null) totalDiscount += invoiceDisAmount;

        if (invoiceInfo == null || invoiceInfo.size() == 0) return "redirect:/stock/purchase_add";

        InvoiceEntity invoiceEntity = new InvoiceEntity();
        invoiceEntity.setSrcOrderId(srcOrderId);
        invoiceEntity.setContactId(supplierId);
        invoiceEntity.setBillNo(billNo);
        invoiceEntity.setBillDate(billDate);
        invoiceEntity.setContractNo(contractNo);
        invoiceEntity.setUserId(userId);
        invoiceEntity.setUserName(userName);
        invoiceEntity.setTransType(transType);
        invoiceEntity.setTransTypeName(transTypeName);
        invoiceEntity.setTotalAmount(totalAmount);
        invoiceEntity.setDisRate(invoiceDisRate);
        invoiceEntity.setDisAmount(invoiceDisAmount);
        invoiceEntity.setAmount(invoiceAmount);
        invoiceEntity.setTotalDiscount(totalDiscount);
        invoiceEntity.setTotalQuantity(totalQuantity);
        invoiceEntity.setFreight(StringUtils.hasText(request.getParameter("freight")) ? Double.valueOf(request.getParameter("freight")):0.0);
        invoiceEntity.setRpAmount(rpAmount);
        invoiceEntity.setArrears(arrears);
        invoiceEntity.setBillType(InvoiceBillTypeEnum.Purchase.getIndex());
        invoiceEntity.setBillStatus(InvoiceBillStatusEnum.WaitAudit.getIndex());
        invoiceEntity.setChecked(InvoiceBillStatusEnum.WaitAudit.getIndex());
        invoiceEntity.setIsDelete(IsDeleteEnum.Normal.getIndex());
        invoiceEntity.setCreateTime(System.currentTimeMillis() / 1000);

        int result = invoiceService.purchaseFormSubmit(invoiceEntity, invoiceInfo);
        //添加系统日志
        logService.addUserAction(userId, EnumUserActionType.Add, "/purchase/add",
                "采购单 , 编号 : " + invoiceEntity.getBillNo() + ",日期 : " + new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss").format(new Date()), "成功");

        return "redirect:/purchase/list";
    }

    @ResponseBody
    @RequestMapping(value = "/purchase_in_export", method = RequestMethod.GET)
    public void purchasePutDetail(Model model, HttpServletRequest req, HttpServletResponse response) {

        var lists = invoiceService.invoiceItemPOList(req.getParameter("startTime"),req.getParameter("endTime"));

        /***************根据店铺查询订单导出的信息*****************/
        String fileName = "purchase_in_export_";//excel文件名前缀
        //创建Excel工作薄对象
        HSSFWorkbook workbook = new HSSFWorkbook();

        //创建Excel工作表对象
        HSSFSheet sheet = workbook.createSheet();
        HSSFRow row = null;
        HSSFCell cell = null;
        //第一行为空
        row = sheet.createRow(0);
        cell = row.createCell(0);
        cell.setCellValue("");
        cell = row.createCell(0);
        cell.setCellValue("采购单号");
        cell = row.createCell(1);
        cell.setCellValue("合同号");
        cell = row.createCell(2);
        cell.setCellValue("入库时间");
        cell = row.createCell(3);
        cell.setCellValue("数量");
        cell = row.createCell(4);
        cell.setCellValue("采购价");
        cell = row.createCell(5);
        cell.setCellValue("标题");
        cell = row.createCell(6);
        cell.setCellValue("规格编码");
        cell = row.createCell(7);
        cell.setCellValue("规格属性");

        int currRowNum = 0;
        HSSFPatriarch patriarch = sheet.createDrawingPatriarch();
        // 循环写入数据
        for (int i = 0; i < lists.size(); i++) {
            currRowNum++;
            //写入订单
            ErpStockInFormItemVo itemVo = lists.get(i);
            //创建行
            row = sheet.createRow(currRowNum);
            cell = row.createCell(0);
            cell.setCellValue(itemVo.getBillNo());
            //商品名称
            cell = row.createCell(1);
            cell.setCellValue(itemVo.getContractNo());
            cell = row.createCell(2);
            cell.setCellValue(DateUtil.stampToDateTime(itemVo.getStockInTime1()));

            cell = row.createCell(3);
            cell.setCellValue(itemVo.getQuantity());
            //退货单号
            cell = row.createCell(4);
            cell.setCellValue(String.valueOf(itemVo.getPrice()));
            //规格
            cell = row.createCell(5);
            cell.setCellValue(itemVo.getGoodsName());
            cell = row.createCell(6);
            cell.setCellValue(itemVo.getSpecNumber());
            cell = row.createCell(7);
            cell.setCellValue(itemVo.getColorValue()+itemVo.getSizeValue());
        }
        response.reset();
        response.setContentType("application/msexcel;charset=UTF-8");
        try {
            SimpleDateFormat newsdf = new SimpleDateFormat("yyyyMMdd");
            String date = newsdf.format(new Date());
            response.addHeader("Content-Disposition", "attachment;filename=\""
                    + new String((fileName + date + ".xls").getBytes("GBK"),
                    "ISO8859_1") + "\"");
            OutputStream out = response.getOutputStream();
            workbook.write(out);
            out.flush();
            out.close();
        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(null, "导出失败!");
            e.printStackTrace();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "导出失败!");
            e.printStackTrace();
        }
    }


}
