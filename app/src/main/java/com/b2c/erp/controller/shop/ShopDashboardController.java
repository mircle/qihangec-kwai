package com.b2c.erp.controller.shop;

import com.b2c.common.utils.DateUtil;
import com.b2c.entity.datacenter.DcShopEntity;
import com.b2c.erp.DataConfigObject;
import com.b2c.erp.controller.CommonControllerUtils;
import com.b2c.interfaces.WmsUserService;
import com.b2c.interfaces.ShopService;
import com.b2c.interfaces.TodoService;
import com.b2c.entity.vo.ShopOrderStatisticsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

@Controller
public class ShopDashboardController {
    @Autowired
    private ShopService shopService;
    @Autowired
    private WmsUserService manageUserService;
    @Autowired
    private TodoService todoService;

    /**
     * 店铺列表
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/shop/shop_list_dy", method = RequestMethod.GET)
    public String shopListDY(Model model, HttpServletRequest request) {
        HttpSession session = request.getSession();
        Integer userId = DataConfigObject.getInstance().getLoginUserId();

        // 有权限的店铺列表
        List<DcShopEntity> shops = shopService.getShopListByUserId(userId, 6);// type=6是抖音
        model.addAttribute("shops", shops);

        var permissionKey = "dyshop";
        var mp = manageUserService.getEntityByKey(permissionKey);

        model.addAttribute("view", permissionKey);
        model.addAttribute("pView", mp.getParentKey());
        model.addAttribute("ejcd", "抖店对接");
        model.addAttribute("sjcd", "店铺列表");

        return "eshop/shop_list";
    }

    /**
     * 小红书店铺列表
     * 
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/shop/shop_list_xhs", method = RequestMethod.GET)
    public String shopListXHS(Model model, HttpServletRequest request) {
        HttpSession session = request.getSession();
        Integer userId = DataConfigObject.getInstance().getLoginUserId();

        // 有权限的店铺列表
        List<DcShopEntity> shops = shopService.getShopListByUserId(userId, 7);// type=6是抖音
        model.addAttribute("shops", shops);

        var permissionKey = "xhsshop";

        var mp = manageUserService.getEntityByKey(permissionKey);

        model.addAttribute("view", permissionKey);
        model.addAttribute("pView", mp.getParentKey());
        model.addAttribute("ejcd", "抖店对接");
        model.addAttribute("sjcd", "店铺列表");

        return "eshop/shop_list";
    }

    @RequestMapping(value = "/shop/shop_list_pdd", method = RequestMethod.GET)
    public String shopListPDD(Model model, HttpServletRequest request) {
        HttpSession session = request.getSession();
        Integer userId = DataConfigObject.getInstance().getLoginUserId();

        // 有权限的店铺列表
        List<DcShopEntity> shops = shopService.getShopListByUserId(userId, 5);// type=5是拼多多
        model.addAttribute("shops", shops);

        var permissionKey = "pddshop";

        var mp = manageUserService.getEntityByKey(permissionKey);

        model.addAttribute("view", permissionKey);
        model.addAttribute("pView", mp.getParentKey());

        return "eshop/shop_list";
    }

    @RequestMapping(value = "/shop/shop_list_tao", method = RequestMethod.GET)
    public String shopListTAO(Model model, HttpServletRequest request) {
        HttpSession session = request.getSession();
        Integer userId = DataConfigObject.getInstance().getLoginUserId();

        // 有权限的店铺列表
        List<DcShopEntity> shops = shopService.getShopListByUserId(userId, 4);// type=5是淘系
        model.addAttribute("shops", shops);

        var permissionKey = "taoshop";

        var mp = manageUserService.getEntityByKey(permissionKey);

        model.addAttribute("view", permissionKey);
        model.addAttribute("pView", mp.getParentKey());

        return "eshop/shop_list";
    }

    /**
     * 店铺首页
     * 
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/shop/dashboard", method = RequestMethod.GET)
    public String dashboard(Model model, HttpServletRequest request) {
        if(StringUtils.hasText(request.getParameter("shopId"))){
            Integer shopId = Integer.parseInt(request.getParameter("shopId"));
            var shop = shopService.getShop(shopId);
            if (shop.getType().intValue() == 6) {
                return "redirect:/douyin/dashboard?shopId=" + shopId;
            } else if (shop.getType().intValue() == 7) {
                return "redirect:/xhs/order_list?shopId=" + shopId + "&status=";
            } else if (shop.getType().intValue() == 5) {
                return "redirect:/pdd/dashboard?shopId=" + shopId;
            } else if (shop.getType().intValue() == 4) {
                return "redirect:/tao/dashboard?shopId=" + shopId;
            }
        }

        // 店铺列表
        List<DcShopEntity> shops = shopService.shopListShow();
        model.addAttribute("shops", shops);

        int todayOrderTotal = 0;
        int todayValidOrder = 0;// 今日有效订单
        double todayValidOrderAmout = 0.0;// 今日有效订单金额
        int waitSendOrder = 0;
        int waitAuditReturn = 0;// 待处理退货
        for (DcShopEntity sh : shops) {
            var st = shopService.shopOrderStatistics(sh.getId());
            if (st != null) {
                sh.setTodayOrderCount(st.getTodayOrder());
                sh.setTodayValidOrder(st.getTodayValidOrder());
                sh.setTodayValidOrderAmout(st.getTodayValidOrderAmout() == null ? 0.0 : st.getTodayValidOrderAmout().doubleValue());
                sh.setWaitSendOrder(st.getWaitSendOrder());
                sh.setWaitAuditReturn(st.getWaitAuditReturn());

                todayOrderTotal += st.getTodayOrder();
                waitSendOrder += st.getWaitSendOrder();
                todayValidOrder += st.getTodayValidOrder();
                todayValidOrderAmout += st.getTodayValidOrderAmout() == null ? 0.0 : st.getTodayValidOrderAmout().doubleValue();
                waitAuditReturn += st.getWaitAuditReturn();
            }

        }
        model.addAttribute("todayOrderTotal", todayOrderTotal);
        model.addAttribute("todayValidOrder", todayValidOrder);
        model.addAttribute("waitSendOrder", waitSendOrder);
        model.addAttribute("todayValidOrderAmout", todayValidOrderAmout);
        model.addAttribute("waitAuditReturn", waitAuditReturn);

        // 店铺统计信息
        var statistics = shopService.shopOrderStatistics(5);
        if (statistics != null)
            model.addAttribute("report", statistics);
        else
            model.addAttribute("report", new ShopOrderStatisticsVo());

        model.addAttribute("today", DateUtil.dateToString(new Date(), "yyyy-MM-dd"));
        Integer waitSendOrderStatus = 2;// 待发货订单状态

        model.addAttribute("waitSendOrderStatus", waitSendOrderStatus);

        // 代办数据
        var todoList = todoService.getList(1, 50, null, null, null, null);
        model.addAttribute("todoList", todoList.getList());

//        model.addAttribute("view", "shopHome");
//        model.addAttribute("pView", "sale");
        CommonControllerUtils.setViewKey(model,manageUserService,"shopHome");

        return "eshop/shop_dashboard";
    }

    @RequestMapping(value = "/shop/order_list", method = RequestMethod.GET)
    public String orderList(Model model, @RequestParam Integer shopId, HttpServletRequest request) {
        model.addAttribute("menuId", "home");
        model.addAttribute("shopId", shopId);
        // 查询店铺信息
        var shop = shopService.getShop(shopId);
        model.addAttribute("shop", shop);
        String status = "";
        if (StringUtils.isEmpty(request.getParameter("status")) == false) {
            status = request.getParameter("status");
        }
        if (shop.getType().intValue() == 6) {
            return "redirect:/douyin/order_list?shopId=" + shopId + "&status=" + status;
        } else if (shop.getType().intValue() == 7) {
            return "redirect:/xhs/order_list?shopId=" + shopId + "&status=" + status;
        } else if (shop.getType().intValue() == 5) {
            return "redirect:/pdd/order_list?shopId=" + shopId + "&status=" + status;
        } else if (shop.getType().intValue() == 4) {
            return "redirect:/tao/order_list?shopId=" + shopId + "&status=" + status;
        } else
            return "redirect:/shop/dashboard?shopId=" + shopId;
    }

    @RequestMapping(value = "/shop/order_create", method = RequestMethod.GET)
    public String orderCreate(Model model, @RequestParam Integer shopId, HttpServletRequest request) {
        model.addAttribute("menuId", "home");
        model.addAttribute("shopId", shopId);
        // 查询店铺信息
        var shop = shopService.getShop(shopId);
        model.addAttribute("shop", shop);
        String status = "";
        if (StringUtils.isEmpty(request.getParameter("status")) == false) {
            status = request.getParameter("status");
        }
        if (shop.getType().intValue() == 6) {
            return "redirect:/douyin/order_create?shopId=" + shopId + "&status=" + status;
        } else if (shop.getType().intValue() == 7) {
            return "redirect:/xhs/order_create?shopId=" + shopId + "&status=" + status;
        } else if (shop.getType().intValue() == 5) {
            return "redirect:/pdd/order_create?shopId=" + shopId + "&status=" + status;
        } else if (shop.getType().intValue() == 4) {
            return "redirect:/tao/order_create?shopId=" + shopId + "&status=" + status;
        } else
            return "redirect:/shop/dashboard?shopId=" + shopId;
    }

    @RequestMapping(value = "/shop/refund_list", method = RequestMethod.GET)
    public String orderRefundList(Model model, @RequestParam Integer shopId, HttpServletRequest request) {
        // var hasPermission = manageUserService.checkShopPermission(userId,shopId);
        // if(hasPermission==false){
        // model.addAttribute("msg","你没有该店铺的管理权限");
        // }

        model.addAttribute("menuId", "home");
        model.addAttribute("shopId", shopId);
        // 查询店铺信息
        var shop = shopService.getShop(shopId);
        model.addAttribute("shop", shop);
        String status = "";
        if (StringUtils.isEmpty(request.getParameter("status")) == false) {
            status = request.getParameter("status");
        }
        if (shop.getType().intValue() == 6) {
            return "redirect:/douyin/refund_list?shopId=" + shopId + "&status=" + status;
        } else if (shop.getType().intValue() == 7) {
            return "redirect:/xhs/refund_list?shopId=" + shopId + "&status=" + status;
        } else if (shop.getType().intValue() == 5) {
            return "redirect:/pdd/refund_list?shopId=" + shopId + "&status=" + status;
        } else if (shop.getType().intValue() == 4) {
            return "redirect:/tao/refund_list?shopId=" + shopId;
        } else
            return "redirect:/shop/dashboard?shopId=" + shopId;
    }

    // @RequestMapping(value = "/shop/goods_list", method = RequestMethod.GET)
    // public String goodsList(Model model, @RequestParam Integer shopId, HttpServletRequest request) {
    //     // HttpSession session = request.getSession();
    //     // Integer userId = Integer.parseInt(session.getAttribute("userId").toString());
    //     // var hasPermission = manageUserService.checkShopPermission(userId,shopId);
    //     // if(hasPermission==false){
    //     // model.addAttribute("msg","你没有该店铺的管理权限");
    //     // }

    //     model.addAttribute("menuId", "home");
    //     model.addAttribute("shopId", shopId);
    //     // 查询店铺信息
    //     var shop = shopService.getShop(shopId);
    //     model.addAttribute("shop", shop);

    //     if (shop.getType().intValue() == 7) {
    //         return "redirect:/xhs/goods_list?shopId=" + shopId;
    //     } else if (shop.getType().intValue() == 5) {
    //         return "redirect:/pdd/goods_list?shopId=" + shopId;
    //     } else
    //         return "redirect:/shop/dashboard?shopId=" + shopId;
    // }

    @RequestMapping(value = "/shop/order_settle_list", method = RequestMethod.GET)
    public String orderSettleList(Model model, @RequestParam Integer shopId, HttpServletRequest request) {
        model.addAttribute("menuId", "home");
        model.addAttribute("shopId", shopId);
        // 查询店铺信息
        var shop = shopService.getShop(shopId);
        model.addAttribute("shop", shop);
        String status = "";
        if (StringUtils.isEmpty(request.getParameter("status")) == false) {
            status = request.getParameter("status");
        }
        if (shop.getType().intValue() == 6) {
            return "redirect:/douyin/order_list?shopId=" + shopId;
        } else if (shop.getType().intValue() == 7) {
            return "redirect:/xhs/order_settle_list?shopId=" + shopId;
        } else
            return "redirect:/shop/dashboard?shopId=" + shopId;
    }
}
