package com.b2c.erp.controller.api.vo;

import lombok.Data;

@Data
public class GoodsSkuAttrValueVo {
   private String  id;//: '1', // skuValueId：规格值 id
   private String  name;//: '红色', // skuValueName：规格值名称
   private String  imgUrl;//: 'https://img01.yzcdn.cn/1.jpg', // 规格类目图片，只有第一个规格类目可以定义图片
   private String  previewImgUrl;//: 'https://img01.yzcdn.cn/1p.jpg', // 用于预览显示的规格类目图片
}
