package com.b2c.erp.controller.tao;

import com.b2c.entity.UserEntity;

import com.b2c.erp.controller.CommonControllerUtils;
import com.b2c.interfaces.WmsUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import jakarta.servlet.http.HttpServletRequest;

@RequestMapping("/tao")
@Controller
public class TaoOrderImportController {
    @Autowired
    private WmsUserService manageUserService;

    /**
     * excel导入订单预览页面
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/order_excel_import_review", method = RequestMethod.GET)
    public String pipi_excel_import_review(Model model, @RequestParam Integer shopId, HttpServletRequest request) {

        model.addAttribute("shopId", shopId);
        model.addAttribute("orderCount", 0);
        //查询客户列表
//        List<UserEntity> clientList = userService.getClientUserListByType(null, null);
//        model.addAttribute("clientList", clientList);
//        model.addAttribute("view", "taoorder");
//        model.addAttribute("pView", "tao");
        CommonControllerUtils.setViewKey(model,manageUserService,"taoorder");
        return "tao/order_excel_import_review_tao";
    }
}
