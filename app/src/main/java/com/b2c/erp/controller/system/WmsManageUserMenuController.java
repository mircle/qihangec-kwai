package com.b2c.erp.controller.system;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ManageUserEntity;
import com.b2c.interfaces.WmsUserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jakarta.servlet.http.HttpServletRequest;

/**
 * @Description: pbd add 2019/9/28 15:12
 */
@Controller
@RequestMapping("/system")
public class WmsManageUserMenuController {
    @Autowired
    private WmsUserService manageUserService;

    @RequestMapping(value = "/tdgl", method = RequestMethod.GET)
    public String getTdgl(Model model, HttpServletRequest request) {
        String page = request.getParameter("page");
        Integer pageIndex = 1, pageSize = 100;
        String name = "", mobile = "";
        if (!StringUtils.isEmpty(page)) {
            pageIndex = Integer.parseInt(page);
        }
        if (!StringUtils.isEmpty(request.getParameter("name"))) {
            name = request.getParameter("name");
        }
        if (!StringUtils.isEmpty(request.getParameter("mobile"))) {
            mobile = request.getParameter("mobile");
        }
        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);
        PagingResponse<ManageUserEntity> result = manageUserService.getWmsManageUsers(pageIndex, pageSize, name, mobile);
        model.addAttribute("totalSize", result.getTotalSize());
        model.addAttribute("lists", result.getList());
        model.addAttribute("groupLists", manageUserService.getManageGroups());
        return "system/tdgl";
    }
    /**
     * 权限菜单设置
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/manage_user_menu", method = RequestMethod.GET)
    public String manageUserMenu(Model model, HttpServletRequest request) {
        Integer userId=(int)request.getSession().getAttribute("userId");
        var user= manageUserService.getWmsManageUser(userId);
        if(user.getGroupId()>1)return "redirect:/no_access";
        Integer sUserId = Integer.parseInt(request.getParameter("user_id"));

        manageUserService.initUserPermissionMenu(sUserId);
        var sUser= manageUserService.getWmsManageUser(sUserId);
        model.addAttribute("name",sUser.getUserName());
        model.addAttribute("list",manageUserService.getUserPerMissionMenuByWMS(sUserId,0));
        model.addAttribute("userId",sUserId);
        return "system/cdsd";
    }

    @RequestMapping(value = "/set_manage_user_menu", method = RequestMethod.POST)
    public String setManageUserMenu(HttpServletRequest request) {
        int userId = Integer.parseInt(request.getParameter("user_id"));
        manageUserService.setManageUserMenu(userId,request.getParameterValues("purview"));
        return "redirect:/system/tdgl";
    }
    /**
     * 修改管理员密码
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/upd_manage_user_pwd", method = RequestMethod.GET)
    public String updManageUserPwd(Model model, HttpServletRequest request) {
        return "system/modify";
    }


}
