package com.b2c.erp.controller.goods;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import jakarta.servlet.http.HttpServletRequest;

import com.b2c.erp.controller.CommonControllerUtils;
import com.b2c.interfaces.WmsUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.b2c.entity.DataRow;
import com.b2c.entity.ErpGoodsPublishRecordEntity;
import com.b2c.entity.datacenter.DcShopEntity;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.erp.DataConfigObject;
import com.b2c.erp.controller.system.FileVo;
import com.b2c.erp.response.ApiResult;
import com.b2c.interfaces.ErpGoodsPublishRecordService;
import com.b2c.interfaces.ShopService;
import com.b2c.interfaces.erp.ErpGoodsService;

@Controller
public class GoodsPublishController {
    @Autowired
    private ShopService shopService;
    @Autowired
    private ErpGoodsService goodsService;
    @Autowired
    private ErpGoodsPublishRecordService recordService;
    @Autowired
    private WmsUserService manageUserService;
    @RequestMapping(value = "/goods/publish_list", method = RequestMethod.GET)
    public String getList(Model model, HttpServletRequest request) {
        //店铺列表
        List<DcShopEntity> shops = shopService.getShopList(null);
        model.addAttribute("shops", shops);
        

        Integer pageIndex = 1, pageSize = DataConfigObject.getInstance().getPageSize();
        if (!StringUtils.isEmpty(request.getParameter("page"))) {
            pageIndex = Integer.parseInt(request.getParameter("page"));
        }
        String goodsNum = null;
        if (!StringUtils.isEmpty(request.getParameter("goodsNum"))) {
            goodsNum = request.getParameter("goodsNum");
        }
        Integer shopId = null;
        if (!StringUtils.isEmpty(request.getParameter("shopId"))) {
            shopId = Integer.parseInt(request.getParameter("shopId"));
        }

        var result = recordService.getList(pageIndex,pageSize,shopId,goodsNum);

        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("totalSize", result.getTotalSize());
        model.addAttribute("lists", result.getList());


//        model.addAttribute("view", "wdspfbjl");
//        model.addAttribute("pView", "sale");
        CommonControllerUtils.setViewKey(model, manageUserService, "shopGoods");
        return "goods/goods_publish_list";
    }

    @RequestMapping(value = "/goods/image_path_view", method = RequestMethod.GET)
    public String imageView(Model model,@RequestParam Integer id, HttpServletRequest request) {
        //
        var entity = recordService.getEntityById(id);
        String imagePath = entity.getImagePath();
        List<FileVo> backupList = new ArrayList<>();
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
	        FileTime t = null;
            if(StringUtils.hasText(imagePath)){
                // 读取文件夹文件
                final File folder = new File(imagePath);
                for (final File fileEntry : folder.listFiles()) {
                    if (fileEntry.isDirectory() == false) {
            
                        FileVo vo = new FileVo();
                        vo.setName(fileEntry.getName());
                        
                        try {
                            t = Files.readAttributes(Paths.get(imagePath + fileEntry.getName()), BasicFileAttributes.class).lastModifiedTime();
                            vo.setTime(dateFormat.format(t.toMillis()));
                 
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        vo.setPath("file:///"+imagePath.replace("\\","/")+"/"+fileEntry.getName());
                        backupList.add(vo);
                    }
                }
            }
        } catch (Exception e) {
            
        }
        model.addAttribute("fileList", backupList);

        return "goods/goods_publish_image_path_view";
    }


    @RequestMapping(value = "/goods/goods_publish_add", method = RequestMethod.GET)
    public String add(Model model, HttpServletRequest request) {
        //店铺列表
        List<DcShopEntity> shops = shopService.getShopList(null);
        model.addAttribute("shops", shops);
    

        return "goods/goods_publish_add";
    }

    @ResponseBody
    @RequestMapping(value = "/shop/goods_publish_add_ajax", method = RequestMethod.POST)
    public ApiResult<Integer> addPostAjax(Model model, @RequestBody DataRow data, HttpServletRequest request) {

        String goodsNum = data.getString("goodsNum");
        var erpGoods = goodsService.getGoodsEntityByNumber(goodsNum);
        // if(erpGoods == null ) return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "商品编码不正确");
        Integer erpGoodsId = 0;// data.getInt("erpGoodsId");
        if(erpGoods != null ) erpGoodsId = erpGoods.getId();
        else erpGoodsId = 0;
        String goodsName = data.getString("goodsName");
        // String goodsNum = data.getString("goodsNum");
        
        String sourceUrl = data.getString("sourceUrl");
        String imagePath = data.getString("imagePath");
        String goodsImage = data.getString("goodsImage");
        Float goodsCost = data.getFloat("goodsCost");
        String publishDate = data.getString("publishDate");
        String shopIds = data.getString("shopIds");
        
        ErpGoodsPublishRecordEntity entity = new ErpGoodsPublishRecordEntity();
        entity.setErpGoodsId(erpGoodsId);
        entity.setGoodsName(goodsName);
        entity.setGoodsNum(goodsNum);
        entity.setGoodsImage(goodsImage);
        entity.setImagePath(imagePath);
        entity.setPublishDate(publishDate);
        entity.setShopIds(shopIds);
        entity.setSourceUrl(sourceUrl);
        entity.setGoodsCost(goodsCost);
        recordService.add(entity);
        return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(), "SUCCESS");
    }


    @RequestMapping(value = "/goods/goods_publish_edit_shops", method = RequestMethod.GET)
    public String editShops(Model model,@RequestParam Integer id, HttpServletRequest request) {
        //店铺列表
        List<DcShopEntity> shops = shopService.getShopList(null);
        model.addAttribute("shops", shops);
        var entity = recordService.getEntityById(id);
        model.addAttribute("entity", entity);
        model.addAttribute("id", id);
    

        return "goods/goods_publish_edit_shops";
    }

    
    @ResponseBody
    @RequestMapping(value = "/shop/goods_publish_edit_ajax", method = RequestMethod.POST)
    public ApiResult<Integer> editPostAjax(Model model, @RequestBody DataRow data, HttpServletRequest request) {

        // String goodsNum = data.getString("goodsNum");
        // var erpGoods = goodsService.getGoodsEntityByNumber(goodsNum);
        // if(erpGoods == null ) return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "商品编码不正确");
        Integer id = data.getInt("id");
        if(id == null ) return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "缺少id");

        String goodsName = data.getString("goodsName");
        String goodsNum = data.getString("goodsNum");
        Integer erpGoodsId = data.getInt("erpGoodsId");
        String sourceUrl = data.getString("sourceUrl");
        String imagePath = data.getString("imagePath");
        String goodsImage = data.getString("goodsImage");
        Float goodsCost = data.getFloat("goodsCost");
        String publishDate = data.getString("publishDate");
        String shopIds = data.getString("shopIds");
        
        ErpGoodsPublishRecordEntity entity = new ErpGoodsPublishRecordEntity();
        entity.setId(id);
        entity.setErpGoodsId(erpGoodsId);
        entity.setGoodsName(goodsName);
        entity.setGoodsNum(goodsNum);
        entity.setGoodsImage(goodsImage);
        entity.setImagePath(imagePath);
        entity.setPublishDate(publishDate);
        entity.setShopIds(shopIds);
        entity.setSourceUrl(sourceUrl);
        entity.setGoodsCost(goodsCost);
        recordService.edit(entity);

        return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(), "SUCCESS");
    }
}
