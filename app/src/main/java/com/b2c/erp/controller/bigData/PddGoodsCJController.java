package com.b2c.erp.controller.bigData;

import jakarta.servlet.http.HttpServletRequest;

import com.b2c.erp.controller.CommonControllerUtils;
import com.b2c.interfaces.WmsUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping("/big_data")
@Controller
public class PddGoodsCJController {
    private static Logger log = LoggerFactory.getLogger(PddGoodsCJController.class);
    @Autowired
    private WmsUserService manageUserService;

    @RequestMapping(value = "/goods_list_pdd", method = RequestMethod.GET)
    public String list(Model model, HttpServletRequest request) {
        String page = request.getParameter("page");
        Integer pageIndex = 1;
        Integer pageSize = 100;

        String platform = "";
        String category = null;
        String keyword = "";

        if (!StringUtils.isEmpty(page)) {
            pageIndex = Integer.parseInt(page);
        }

        if (!StringUtils.isEmpty(request.getParameter("keyword"))) {
            keyword = request.getParameter("keyword");
            model.addAttribute("keyword", keyword);
        }

        if (!StringUtils.isEmpty(request.getParameter("platform"))) {
            platform = request.getParameter("platform");
            model.addAttribute("platform", platform);
        }
        if (!StringUtils.isEmpty(request.getParameter("category"))) {
            category = request.getParameter("category");
            model.addAttribute("category", request.getParameter("category"));
        }


        // var result = service.getList(pageIndex,pageSize,keyword,platform,category);

        // model.addAttribute("pageIndex", pageIndex);
        // model.addAttribute("pageSize", pageSize);
        // model.addAttribute("totalSize", result.getTotalSize());
        // model.addAttribute("lists", result.getList());

        CommonControllerUtils.setViewKey(model, manageUserService, "pdd_goods_cj");
//        model.addAttribute("view", "pdd_goods_cj");
//        model.addAttribute("pView", "sale");
        return "bigData/goods_list_pdd_cj";
    }
}
