package com.b2c.erp.controller.api.response;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-11-19 15:36
 */
@Data
public class LoginResp implements Serializable {
    private UserInfo userInfo;
    private String roles;
    private String[] permission;
    private String token;
    private int expires;//过期时间，毫秒


}
