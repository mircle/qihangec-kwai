package com.b2c.erp.controller;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ErpGoodsSpecMoveFromEntiy;
import com.b2c.entity.ErpStockLocationEntity;
import com.b2c.entity.enums.EnumUserActionType;
import com.b2c.interfaces.erp.ErpUserActionLogService;
import com.b2c.interfaces.wms.StockLocationService;
import com.b2c.interfaces.wms.StockService;
import com.b2c.repository.utils.OrderNumberUtils;
import com.b2c.erp.DataConfigObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import jakarta.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * 描述：
 * 仓库管理Controller
 *
 * @author ly
 * @date 2019-03-25 17:16
 */
@RequestMapping("/stock_location")
@Controller
public class StockLocationController {
    @Autowired
    private StockLocationService stockLocationService;
    @Autowired
    private StockService stockService;
    @Autowired
    private ErpUserActionLogService logService;




    /**
     * 仓库列表
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String getStockHousesByParentId(Model model,HttpServletRequest request) {
        //仓库列表
        List<ErpStockLocationEntity> houses = stockLocationService.getListByParentId(0);
        model.addAttribute("houses", houses);
        model.addAttribute("view", "ckgl");
        model.addAttribute("pView", "kc");
        model.addAttribute("ejcd", "仓库");
        model.addAttribute("sjcd", "仓库管理");
        return "stock_house";
    }


    /**
     * 获取库区列表
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/reservoir", method = RequestMethod.GET)
    public String getStockHouses(Model model, HttpServletRequest request) {
        String page = request.getParameter("page");
        Integer pageIndex = 1, pageSize = DataConfigObject.getInstance().getPageSize();
        if (!StringUtils.isEmpty(page)) {
            pageIndex = Integer.parseInt(page);
        }

        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);
        PagingResponse<ErpStockLocationEntity> result = stockLocationService.getListByDepth(pageIndex, pageSize,2,"");

        model.addAttribute("reservoir", result.getList());
        model.addAttribute("totalSize", result.getTotalSize());

        List<ErpStockLocationEntity> houses = stockLocationService.getListByParentId(0);
        model.addAttribute("houses", houses);

        model.addAttribute("view", "ckgl");
        model.addAttribute("pView", "kc");
        model.addAttribute("ejcd", "仓库");
        model.addAttribute("sjcd", "库区管理");
        return "reservoir";
    }

    /**
     * 获取仓位列表
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/shelf", method = RequestMethod.GET)
    public String getshelf(Model model, HttpServletRequest request) {
        String page = request.getParameter("page");
        Integer pageIndex = 1, pageSize = DataConfigObject.getInstance().getPageSize();
        if (!StringUtils.isEmpty(page)) {
            pageIndex = Integer.parseInt(page);
        }
        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);

        String number="";
        if(StringUtils.isEmpty(request.getParameter("number"))==false){
            number = request.getParameter("number");
            model.addAttribute("number",number);
        }

        PagingResponse<ErpStockLocationEntity> result = stockLocationService.getListByDepth(pageIndex, pageSize,3,number);

        model.addAttribute("shelf", result.getList());
        model.addAttribute("totalSize", result.getTotalSize());

        List<ErpStockLocationEntity> house = stockLocationService.getListByParentId(0);
        model.addAttribute("house", house);

//        if(house!=null){
//            Integer houseId = Integer.parseInt(request.getParameter("houseId"));
//            List<ReservoirEntity> reservoir = reservoirService.getIdByHouseId(houseId);
//            model.addAttribute("reservoir",reservoir);
//        }

        model.addAttribute("view", "ckgl");
        model.addAttribute("pView", "kc");
        model.addAttribute("ejcd", "仓库");
        model.addAttribute("sjcd", "仓位管理");
        return "shelf";
    }


    @RequestMapping(value = "/move_house", method = RequestMethod.GET)
    public String moveHouse(Model model, HttpServletRequest request) {
        model.addAttribute("view", "ykgl");
        model.addAttribute("pView", "kc");
        model.addAttribute("houses", stockLocationService.getListByParentId(0));
        String billNo = "MOVE" + OrderNumberUtils.getOrderIdByTime();
        model.addAttribute("billNo", billNo);
        return "house/move_house";
    }

    /**
     * 移库列表
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/move_house_list", method = RequestMethod.GET)
    public String moveHouseList(Model model, HttpServletRequest request) {
        //移库单号
        String moveNo=Optional.ofNullable(request.getParameter("moveNo")).orElse("");
        model.addAttribute("view", "ykgl");
        model.addAttribute("pView", "kc");
        Integer pageIndex = 1;
        Integer pageSize = DataConfigObject.getInstance().getPageSize();;
        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);
        PagingResponse<ErpGoodsSpecMoveFromEntiy> vo = stockService.getStockHouseVo(pageIndex, pageSize,moveNo);
        model.addAttribute("totalSize", vo.getTotalSize());
        List<ErpGoodsSpecMoveFromEntiy> list = vo.getList();
        for (ErpGoodsSpecMoveFromEntiy entiy : list) {
            entiy.setOld_locationName(stockLocationService.getName(entiy.getOldLocationId()));
            entiy.setOld_reservoirName(stockLocationService.getName(entiy.getOldReservoirId()));

            entiy.setOld_shelfName(stockLocationService.getName(entiy.getOldShelfId()));

            entiy.setNewlocationName(stockLocationService.getName(entiy.getNewLocationId()));

            entiy.setNewReservoirName(stockLocationService.getName(entiy.getNewReservoirId()));

            entiy.setNewShelfName(stockLocationService.getName(entiy.getNewShelfId()));

            SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Long createTime = entiy.getCreateTime().longValue();
            String time = sd.format(createTime * 1000);
            entiy.setDataTime(time);
        }
        model.addAttribute("list", vo.getList());
        model.addAttribute("res",request.getParameter("res"));
        //添加系统日志
        logService.addUserAction(Integer.parseInt(request.getSession().getAttribute("userId").toString()), EnumUserActionType.StockOperation,
                "/stock_location/move_house_list","移库列表  ,日期 : "+new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss").format(new Date()),"成功");

        return "house/move_house_list";
    }

    /**
     * 移库表单POST
     * @param request
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/add_move_spec_from", method = RequestMethod.POST)
    public String addMoveSpecFrom(HttpServletRequest request,RedirectAttributesModelMap modelMap) {
        //移库在Ids
        String[] ids = request.getParameterValues("id");
        //移出的仓位
        Integer outLocationId = Integer.parseInt(request.getParameter("outLocationId"));
        Integer inLocationId = Integer.parseInt(request.getParameter("inLocationId"));


        String[] remarks = request.getParameterValues("note");
        String[] number = request.getParameterValues("amount");
        String moveNo = request.getParameter("moveNo");
        String createBy = request.getParameter("createBy");

        synchronized(this){
           var result= stockService.addGoodsSpecMoveForm(ids,number,remarks,outLocationId,inLocationId,moveNo,createBy);
           if(result.getCode()==0){
               //添加系统日志
               logService.addUserAction(Integer.parseInt(request.getSession().getAttribute("userId").toString()), EnumUserActionType.StockOperation,
                       "/stock_location/add_move_spec_from","新增移库 , 单据编号 : "+moveNo+" ,日期 : "+new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss").format(new Date()),"成功");
           }
            modelMap.addAttribute("res",result.getMsg());
        }
        return "redirect:/stock_location/move_house_list";
    }

    @RequestMapping(value = "/get_move_house_list", method = RequestMethod.GET)
    public String getIdByEntiy(Model model, HttpServletRequest request, @RequestParam Integer id) {
        ErpGoodsSpecMoveFromEntiy entiy = stockService.getIdByErpGoodsSpecMoveFromEntiy(id);
        entiy.setOld_locationName(stockLocationService.getName(entiy.getOldLocationId()));
        entiy.setOld_reservoirName(stockLocationService.getName(entiy.getOldReservoirId()));
        entiy.setOld_shelfName(stockLocationService.getName(entiy.getOldShelfId()));
        entiy.setNewlocationName(stockLocationService.getName(entiy.getNewLocationId()));
        entiy.setNewReservoirName(stockLocationService.getName(entiy.getNewReservoirId()));
        entiy.setNewShelfName(stockLocationService.getName(entiy.getNewShelfId()));
        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Long createTime = entiy.getCreateTime().longValue();
        String time = sd.format(createTime * 1000);
        entiy.setDataTime(time);
        model.addAttribute("list", entiy);
        return "house/move_house_detail";
    }

}
