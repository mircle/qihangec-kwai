package com.b2c.erp.controller;

import com.b2c.entity.DataRow;

import com.b2c.entity.result.EnumResultVo;
import com.b2c.entity.vo.wms.ErpPurchaseGoodVo;
import com.b2c.erp.DataConfigObject;
import com.b2c.interfaces.erp.ErpGoodsService;
import com.b2c.entity.vo.finance.ErpStockInFormItemVo;
import com.b2c.erp.req.InvoiceDeleteReq;
import com.b2c.erp.response.ApiResult;
import com.b2c.interfaces.wms.InvoiceService;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import jakarta.servlet.http.HttpServletRequest;
 import jakarta.servlet.http.HttpServletResponse;

/**
 * 描述：
 * 采购单AJAX
 *
 * @author qlp
 * @date 2019-03-22 14:25
 */
@RequestMapping(value = "/ajax_purchase")
@RestController
public class AjaxPurchaseController {
    private static Logger log = LoggerFactory.getLogger(AjaxPurchaseController.class);

    @Autowired
    private InvoiceService invoiceService;
    @Autowired
    private ErpGoodsService erpGoodsService;


    /**
     * 关键词搜索商品
     *
     * @return
     */
    @RequestMapping(value = "/invoice_delete", method = RequestMethod.POST)
    public ApiResult<Integer> searchGoodsDetail(@RequestBody InvoiceDeleteReq req) {
//        ResultVo<Integer> resultVo = new ResultVo<>();
//        invoiceService.deleteForm(request.getId());

        return new ApiResult<>(400, "不允许删除");
    }

    /**
     * 采购单据审核
     * @param reqData
     * @return
     */
    @RequestMapping(value = "/purchase_check", method = RequestMethod.POST)
    public ApiResult<Integer> purchase_check(@RequestBody DataRow reqData) {
        Integer erpInvoiceId= reqData.getInt("erpInvoiceId");
        Integer status= reqData.getInt("status");
        invoiceService.checkErpInvoice(erpInvoiceId,status);
        return new ApiResult<>(0, "成功");
    }

    /**
     * 采购退货单据审核
     * @param reqData
     * @param request
     * @return
     */
    @RequestMapping(value = "/purchase_return_check", method = RequestMethod.POST)
    public ApiResult<Integer> purchase_return_check(@RequestBody DataRow reqData,HttpServletRequest request) {
        Integer erpInvoiceId= reqData.getInt("erpInvoiceId");
        Integer status= reqData.getInt("status");

        log.info("采购退货单据审核invoice-提交：{userName:"+ DataConfigObject.getInstance().getLoginUserName() +"}");
        invoiceService.checkInvoice(erpInvoiceId,status,DataConfigObject.getInstance().getLoginUserName());
        return new ApiResult<>(0, "成功");
    }

    @RequestMapping(value = "/erp_invoice_info_edit", method = RequestMethod.POST)
    public ApiResult<Integer> updInvoiceInfo(@RequestBody DataRow reqData) {
        try {
            Double price= StringUtils.isEmpty(reqData.get("price")) ? -1d : reqData.getDouble("price");
            Integer quantity=StringUtils.isEmpty(reqData.get("quantity")) ? -1 : reqData.getInt("quantity");
            invoiceService.updInvoiceInfo(reqData.getInt("erpInvoiceId"),reqData.getInt("erpInvoiceInfoId"),price,quantity);
            return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(),"成功");
        }catch (Exception ex){
            return new ApiResult<>(EnumResultVo.Fail.getIndex(),"异常："+ex.getMessage());
        }
    }

    @RequestMapping(value = "/erp_invoice_item_del", method = RequestMethod.POST)
    public ApiResult<Integer> erp_invoice_item_del(@RequestBody DataRow reqData,HttpServletRequest request) {
        synchronized(this){
            var result= invoiceService.delErpInvoiceInfo(reqData.getLong("erpInvoiceInfoId"),reqData.getLong("erpInvoiceId"));
            return new ApiResult<>(result.getCode(),result.getMsg());
        }
    }

    @RequestMapping(value = "/erp_invoice_item_edit", method = RequestMethod.POST)
    public ApiResult<Integer> erp_invoice_item_edit(@RequestBody DataRow reqData,HttpServletRequest request) {
        synchronized(this){
            ErpPurchaseGoodVo req=new ErpPurchaseGoodVo();
            var good = erpGoodsService.getSpecBySpecId(reqData.getInt("erpGoodSpecId"));

            req.setErpInvoiceInfoId(reqData.getLong("erpInvoiceInfoId"));
            req.setGoodsId(good.getGoodsId());
            req.setSpecId(reqData.getInt("erpGoodSpecId"));
            req.setAmount(reqData.getDouble("amount"));
            req.setGoodsNumber(good.getGoodsNumber());
            req.setSpecNumber(reqData.getString("specNumber"));
            req.setQuantity(reqData.getLong("quantity"));
            req.setPrice(good.getPurPrice());
            var result= invoiceService.addInvoiceItem(reqData.getLong("invoiceId"),req);
            return new ApiResult<>(result.getCode(),result.getMsg());
        }
    }

    @RequestMapping(value = "export",method={RequestMethod.POST,RequestMethod.GET})
    @ResponseBody
    public void exportInvoice(HttpServletRequest request, HttpServletResponse response){
        String startDate=null;
        String endDate=null;
        if(!StringUtils.isEmpty(request.getParameter("startDate"))){
            startDate=request.getParameter("startDate");
        }
        if(!StringUtils.isEmpty(request.getParameter("endDate"))){
            endDate=request.getParameter("endDate");
        }
        var lists = invoiceService.invoiceItemPOList(startDate,endDate);
        /***************根据店铺查询订单导出的信息*****************/
        String excelFileName = "finance_erp_sales_order_item_";//excel文件名前缀
        //创建Excel工作薄对象
        HSSFWorkbook workbook = new HSSFWorkbook();

        //创建Excel工作表对象
        HSSFSheet sheet = workbook.createSheet();
        HSSFRow row = null;
        HSSFCell cell = null;
        //第一行为空
        row = sheet.createRow(0);
        cell = row.createCell(0);
        cell.setCellValue("采购单号");
        cell = row.createCell(1);
        cell.setCellValue("合同号");
        cell = row.createCell(2);
        cell.setCellValue("入库日期");
        cell = row.createCell(3);
        cell.setCellValue("商品标题");
        cell = row.createCell(4);
        cell.setCellValue("商品编码");
        cell = row.createCell(5);
        cell.setCellValue("规格编码");
        cell = row.createCell(6);
        cell.setCellValue("入库数量");
        cell = row.createCell(7);
        cell.setCellValue("采购价格");
        cell = row.createCell(8);
        cell.setCellValue("颜色");
        cell = row.createCell(9);
        cell.setCellValue("尺码");
        cell = row.createCell(10);
        cell.setCellValue("所属店铺");
        cell = row.createCell(11);
        cell.setCellValue("销售类型");
        cell = row.createCell(12);
        cell.setCellValue("客户名称");
        int currRowNum = 0;
        // 循环写入数据
        for (int i = 0; i < lists.size(); i++) {
            currRowNum++;
            //写入订单
            ErpStockInFormItemVo itemVo = lists.get(i);

        /*    //创建行
            row = sheet.createRow(currRowNum);
            cell = row.createCell(0);
            cell.setCellValue(itemVo.getBillNo());
            cell = row.createCell(1);
            cell.setCellValue(itemVo.getContractNo());
            //下单日期
            cell = row.createCell(2);
            cell.setCellValue(DateUtil.stampToDateTime2(itemVo.getStockInTime1()));

            //商品款号
            cell = row.createCell(3);
            cell.setCellValue(itemVo.getGoodsNumber());

            //sku
            cell = row.createCell(4);
            cell.setCellValue(itemVo.getSpecNumber());

            //规格
            cell = row.createCell(5);
            cell.setCellValue(itemVo.getSpecName());

            //数量
            cell = row.createCell(6);
            cell.setCellValue(itemVo.getQuantity());

            //销售单价
            cell = row.createCell(7);
            cell.setCellValue(itemVo.getSalePrice().toString());

            //成本价
            cell = row.createCell(8);
            cell.setCellValue(itemVo.getPurPrice().toString());

            //总成本
            cell = row.createCell(9);
            Double totalPurPrice=itemVo.getPurPrice()*itemVo.getQuantity();
            cell.setCellValue(totalPurPrice.toString());

            //所属店铺
            cell = row.createCell(10);
            cell.setCellValue(itemVo.getShopName());

            //销售类型
            cell = row.createCell(11);
            cell.setCellValue(saleType);

            //客户名称
            cell = row.createCell(12);
            cell.setCellValue(itemVo.getBuyerName());*/
        }
    }




}
