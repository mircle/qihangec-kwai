package com.b2c.erp.controller.ecom;

import javax.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;

import com.b2c.erp.controller.CommonControllerUtils;
import com.b2c.interfaces.WmsUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.b2c.entity.api.ApiResult;
import com.b2c.common.utils.DateUtil;
import com.b2c.entity.DataRow;
import com.b2c.entity.ecom.GoodsStyleReferEntity;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.interfaces.ecom.GoodsStyleReferService;

@RequestMapping("/ecom")
@Controller
public class GoodsStyleReferContrller {
    @Resource
    private GoodsStyleReferService service;
    @Autowired
    private WmsUserService manageUserService;
    
    @RequestMapping(value = "/goods_style_refer_list", method = RequestMethod.GET)
    public String list(Model model, HttpServletRequest request) {
  
        
        String page = request.getParameter("page");
        Integer pageIndex = 1;
        Integer pageSize = 50;
        String num = "";

        String platform = null;

        if (!StringUtils.isEmpty(page)) {
            pageIndex = Integer.parseInt(page);
        }

        // if (!StringUtils.isEmpty(request.getParameter("num"))) {
        //     num = request.getParameter("num");
        //     model.addAttribute("num",num);
        // }
        // if (!StringUtils.isEmpty(request.getParameter("platform"))) {
        //     platform = request.getParameter("platform");
        //     model.addAttribute("platform",platform);
        // }

       

        var result = service.getList(pageIndex,pageSize,platform,"GOODS");
        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("totalSize", result.getTotalSize());
        model.addAttribute("goodsList", result.getList());

        var result1 = service.getList(pageIndex,pageSize,platform,"SITE");
        model.addAttribute("sites", result1.getList());

//        model.addAttribute("view", "ecomdata");
//        model.addAttribute("pView", "sale");
        CommonControllerUtils.setViewKey(model,manageUserService,"ecomdata");
        return "ecom/goods_style_refer_list";
    }

    
    @RequestMapping(value = "/goods_style_refer_add", method = RequestMethod.GET)
    public String add(Model model, HttpServletRequest request) {
        model.addAttribute("date", DateUtil.getCurrentDate());
        return "ecom/goods_style_refer_add";
    }

    @ResponseBody
    @RequestMapping(value = "/goods_style_refer_add_ajax", method = RequestMethod.POST)
    public  ApiResult<String> addPostAjax(Model model, @RequestBody DataRow data, HttpServletRequest request) {
        GoodsStyleReferEntity kw = new GoodsStyleReferEntity();
        kw.setPlatform(data.getString("platform"));
        kw.setType(data.getString("type"));
        kw.setTitle(data.getString("title"));
        kw.setDate(data.getString("date"));
        kw.setImg( data.getString("img"));
        kw.setUrl(data.getString("url"));
        kw.setRemark(data.getString("remark"));
        kw.setSource(data.getString("source"));
        
        service.add(kw);

        return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(), "SUCCESS");
    }
}
