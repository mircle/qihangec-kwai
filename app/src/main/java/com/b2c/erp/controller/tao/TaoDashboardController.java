package com.b2c.erp.controller.tao;

import java.util.Date;
import java.util.List;

import jakarta.servlet.http.HttpServletRequest;

import com.b2c.erp.controller.CommonControllerUtils;
import com.b2c.interfaces.WmsUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.b2c.common.utils.DateUtil;
import com.b2c.entity.datacenter.DcShopEntity;
import com.b2c.erp.DataConfigObject;
import com.b2c.interfaces.ShopService;
import com.b2c.entity.vo.ShopOrderStatisticsVo;

@Controller
public class TaoDashboardController {

    @Autowired
    private ShopService shopService;
    @Autowired
    private WmsUserService manageUserService;
     /**
     * 店铺首页
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/tao/dashboard", method = RequestMethod.GET)
    public String dashboard(Model model,  HttpServletRequest request) {
        Integer shopId =6;
        Integer userId = DataConfigObject.getInstance().getLoginUserId();
        //有权限的店铺列表
        List<DcShopEntity> shops = shopService.getShopListByUserId(userId,4);//type=5是淘系
        model.addAttribute("shops", shops);

        model.addAttribute("menuId", "home");
        model.addAttribute("shopId", shopId);
        //查询店铺信息
        var shop = shopService.getShop(shopId);
        model.addAttribute("shop", shop);
        //店铺列表
//        List<DcShopEntity> shops = shopService.getShopList();
//        model.addAttribute("shops", shops);
        //店铺统计信息
        var statistics = shopService.shopOrderStatistics(shop.getId());
        if(statistics!=null)
            model.addAttribute("report", statistics);
        else  model.addAttribute("report",new ShopOrderStatisticsVo());

        model.addAttribute("today", DateUtil.dateToString(new Date(),"yyyy-MM-dd"));
        Integer waitSendOrderStatus = 2;//待发货订单状态

        model.addAttribute("waitSendOrderStatus",waitSendOrderStatus);
        

//        model.addAttribute("view", "taoshop");
//        model.addAttribute("pView", "tao");
        CommonControllerUtils.setViewKey(model, manageUserService, "taoshop");
        return "tao/dashboard";
    }
    
}
