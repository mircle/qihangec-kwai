package com.b2c.erp.controller.pdd;

import jakarta.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.b2c.entity.api.ApiResult;
import com.b2c.entity.DataRow;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.erp.DataConfigObject;
import com.b2c.interfaces.ShopService;
import com.pdd.pop.sdk.http.PopClient;
import com.pdd.pop.sdk.http.PopHttpClient;

@RequestMapping("/ajax_pdd")
@RestController
public class GoodsReviewsAjaxController {
    private static Logger log = LoggerFactory.getLogger(GoodsReviewsAjaxController.class);

    @Autowired
    private ShopService shopService;

    @RequestMapping(value = "/pull_reviews_list", method = RequestMethod.POST)
    public ApiResult<String> getOrderList(@RequestBody DataRow reqData, HttpServletRequest req)
            throws Exception {
        Integer shopId = reqData.getInt("shopId");// 拼多多shopId

        String clientId = DataConfigObject.getInstance().getPddClientId();
        String clientSecret = DataConfigObject.getInstance().getPddClientSecret();
        var shop = shopService.getShop(shopId);
        // var settingEntity = thirdSettingService.getEntity(shop.getType());
        String accessToken = shop.getSessionKey();

        PopClient client = new PopHttpClient(clientId, clientSecret);

        
        return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(), "SUCCESS");
    }
}
