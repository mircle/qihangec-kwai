package com.b2c.erp.controller.fahuo;

import jakarta.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.b2c.entity.api.ApiResult;
import com.b2c.entity.DataRow;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.interfaces.fahuo.OrderSendService;

@RequestMapping(value = "/ajax_fahuo")
@RestController
public class AjaxOrderSendController {

    @Autowired
    private OrderSendService orderSendService;
    private static Logger log = LoggerFactory.getLogger(AjaxOrderSendController.class);

    /***
     * 订单手动选择快递单
     * 
     * @return
     */
    @RequestMapping(value = "/order_send_type_edit", method = RequestMethod.POST)
    public ApiResult<Integer> sendTypeEdit(@RequestBody DataRow data, HttpServletRequest request) {
        Integer supplierId = data.getInt("supplierId");
        String orderIds = data.getString("id");
        int successCount = 0;
        int failCount = 0;
        for (var oid : orderIds.split(",")) {
            try {
                Long orderId = Long.parseLong(oid);
                var ressult = orderSendService.orderSendTypeEdit(orderId,supplierId);
                log.info("分配供应商"+JSONObject.toJSONString(ressult));
                if(ressult.getCode() == 0){
                    successCount++;
                }else{
                    failCount++;
                }
            } catch (Exception e) {
                log.info("分配供应商错误");
                failCount++;
            }
            
        }
       
        

        // if(StringUtils.isEmpty(data.getString("companyCode")))return new
        // ApiResult<>(EnumResultVo.ParamsError.getIndex(), "请选择快递公司");
        // if(StringUtils.isEmpty(data.getString("code")))return new
        // ApiResult<>(EnumResultVo.ParamsError.getIndex(), "请输入快递单号");

        String remark = "总共"+orderIds.split(",").length+"个订单，成功："+successCount+"失败："+failCount;

        return new ApiResult<>(0, remark);
    }


    /***
     * 订单手动选择快递单
     * @return
     */
    @RequestMapping(value = "/pull_pdd_order_logistics", method = RequestMethod.POST)
    public ApiResult<Integer> getOrderSend( HttpServletRequest request) {
        

        Integer ressult = orderSendService.pullOrderLogisticsPdd();

        return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(), "SUCCESS",ressult);

    }

    @RequestMapping(value = "/order_hand_express", method = RequestMethod.POST)
    public ApiResult<Integer> orderHandSend(@RequestBody DataRow data , HttpServletRequest request) {
        // if(StringUtils.isEmpty(data.getString("companyCode")))return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "请选择快递公司");
        if(StringUtils.isEmpty(data.getString("code")))return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "请输入快递单号");

        var ressult = orderSendService.orderHandExpress(data.getLong("id"),data.getString("company"),data.getString("companyCode"),data.getString("code"));

        return new ApiResult<>(ressult.getCode(), ressult.getMsg());

    }

    @RequestMapping(value = "/order_send_batch", method = RequestMethod.POST)
    public ApiResult<Integer> pullPddOrderSendWuliu(@RequestBody DataRow data , HttpServletRequest request) {
        // if(StringUtils.isEmpty(data.getString("companyCode")))return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "请选择快递公司");
        if(StringUtils.isEmpty(data.getString("id")))return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "参数错误");

        String orderIds = data.getString("id");
        int successCount = 0;
        int failCount = 0;
        for (var oid : orderIds.split(",")) {
            try {
                Long orderId = Long.parseLong(oid);
                var ressult = orderSendService.orderHandExpress(orderId,"","","");
                log.info("批量发货"+orderId+JSONObject.toJSONString(ressult));
                if(ressult.getCode() == 0){
                    successCount++;
                }else{
                    failCount++;
                }
            } catch (Exception e) {
                log.info("分配供应商错误");
                failCount++;
            }
            
        }
       
        

        // if(StringUtils.isEmpty(data.getString("companyCode")))return new
        // ApiResult<>(EnumResultVo.ParamsError.getIndex(), "请选择快递公司");
        // if(StringUtils.isEmpty(data.getString("code")))return new
        // ApiResult<>(EnumResultVo.ParamsError.getIndex(), "请输入快递单号");

        String remark = "总共"+orderIds.split(",").length+"个订单，成功："+successCount+"失败："+failCount; 

        return new ApiResult<>(0, remark);
    }

    @RequestMapping(value = "/order_cancel", method = RequestMethod.POST)
    public ApiResult<Integer> orederCancel(@RequestBody DataRow data , HttpServletRequest request) {
        Long id = data.getLong("id");
        if(id == null ||id<=0)return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "参数不正确，缺少id");
        String remark = data.getString("remark");
        if(StringUtils.isEmpty(remark))return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "请输入取消订单理由");

        var ressult = orderSendService.orederCancel(id,remark);

        return new ApiResult<>(ressult.getCode(), ressult.getMsg());

    }

    @RequestMapping(value = "/order_remark_ajax", method = RequestMethod.POST)
    public ApiResult<String> orderRemark(HttpServletRequest req,@RequestBody DataRow reqData) throws Exception {
        Long id = reqData.getLong("id");
        String remark = reqData.getString("remark");

        log.info("保存订单备注......................"+remark);

        orderSendService.updOrderRemark(id,remark);

        return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(), "SUCCESS" );
    }

    
}
