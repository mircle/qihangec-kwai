package com.b2c.erp.controller.ecom;

import com.b2c.entity.api.ApiResult;
import com.b2c.entity.DataRow;
import com.b2c.entity.ecom.KeywordDataEntity;
import com.b2c.entity.ecom.KeywordsEntity;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.erp.controller.CommonControllerUtils;
import com.b2c.interfaces.WmsUserService;
import com.b2c.interfaces.ecom.EcomKeywordService;
import com.b2c.interfaces.erp.ErpGoodsService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import jakarta.servlet.http.HttpServletRequest;

@RequestMapping("/ecom")
@Controller
public class GoodsKeywordController {
    @Autowired
    private EcomKeywordService keywordService;
    @Autowired
    private ErpGoodsService goodsService;
    @Autowired
    private WmsUserService manageUserService;
    private static Logger log = LoggerFactory.getLogger(GoodsKeywordController.class);
    /**
     * 订单列表
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/goods_keyword_list", method = RequestMethod.GET)
    public String list(Model model, HttpServletRequest request) {
        String page = request.getParameter("page");
        Integer pageIndex = 1;
        Integer pageSize = 100;
        String category = "";
        String category2 = "";
        String source = "";
        String keyword = "";
        String platform = null;

        if (!StringUtils.isEmpty(page)) {
            pageIndex = Integer.parseInt(page);
        }

        if (!StringUtils.isEmpty(request.getParameter("keyword"))) {
            keyword = request.getParameter("keyword");
            model.addAttribute("keyword",keyword);
        }
        if (!StringUtils.isEmpty(request.getParameter("platform"))) {
            platform = request.getParameter("platform");
            model.addAttribute("platform",platform);
        }

        if (!StringUtils.isEmpty(request.getParameter("source"))) {
            source = request.getParameter("source");
            model.addAttribute("source", source);
        }
        if (!StringUtils.isEmpty(request.getParameter("category"))) {
            category = request.getParameter("category");
            model.addAttribute("category", category);
        }
        if (!StringUtils.isEmpty(request.getParameter("category2"))) {
            category2 = request.getParameter("category2");
            model.addAttribute("category2", category2);
        }
        
        

        var result = keywordService.getList(pageIndex,pageSize,category,category2,platform,source,keyword);
        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("totalSize", result.getTotalSize());
        model.addAttribute("lists", result.getList());

        var categoryList = goodsService.getCategoryListByParentId(26);//取牛仔裤子类
        model.addAttribute("categoryList", categoryList);

//        model.addAttribute("view", "keyword");
//        model.addAttribute("pView", "sale");
        CommonControllerUtils.setViewKey(model,manageUserService,"ecomdata");
        return "ecom/goods_keyword_list";
    }


    @RequestMapping(value = "/goods_keyword_add", method = RequestMethod.GET)
    public String add(Model model, HttpServletRequest request) {
        var categoryList = goodsService.getCategoryListByParentId(26);//取牛仔裤子类
        model.addAttribute("categoryList", categoryList);
        return "ecom/goods_keyword_add";
    }

    @ResponseBody
    @RequestMapping(value = "/goods_keyword_add_ajax", method = RequestMethod.POST)
    public  ApiResult<String> addPostAjax(Model model, @RequestBody DataRow data, HttpServletRequest request) {
        KeywordsEntity kw = new KeywordsEntity();
        kw.setCategory2(data.getString("category2"));
        kw.setCategory(data.getString("category"));
        kw.setKeyword( data.getString("keyword"));
        kw.setPlatform(data.getString("platform"));
        kw.setSource(data.getString("source"));
        kw.setRemark(data.getString("remark"));


        KeywordDataEntity d = new KeywordDataEntity();
        // d.setKeywordId(data.getLong("keywordId"));
        d.setRank(data.getInt("rank"));
        d.setGoodsCount(data.getInt("goodsCount"));
        d.setSousuorenqi(data.getInt("sousuorenqi"));
        d.setSousuoredu(data.getInt("sousuoredu"));
        d.setDianjirenqi(data.getInt("dianjirenqi"));
        d.setDianjiredu(data.getInt("dianjiredu"));
        d.setDianjilv(data.getFloat("dianjilv"));
        d.setZhifulv(data.getFloat("zhifulv"));
        d.setJingzhengzhishu(data.getFloat("jingzhengzhishu"));
        d.setChengjiaozhishu(data.getFloat("chengjiaozhishu"));
        d.setShichangchujia(data.getFloat("shichangchujia"));
        d.setIncludeDate(data.getString("include_date"));


        keywordService.addKeyWord(kw,d);

        //keyWordService.addKeyWord(kw);
        return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(), "SUCCESS");
    }


 /**
     * 
     * @param model
     * @param id 关键词id
     * @param request
     * @return
     */
    @RequestMapping(value = "/goods_keyword_data_list", method = RequestMethod.GET)
    public String list(Model model,@RequestParam Long id, HttpServletRequest request) {
        String page = request.getParameter("page");
        Integer pageIndex = 1;
        Integer pageSize = 100;
        model.addAttribute("keywordId", id);
        var keyword = keywordService.getKeywordById(id);
        model.addAttribute("keyword", keyword);
        
        var result = keywordService.getDataList(pageIndex,pageSize,id);
        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);


        model.addAttribute("totalSize", result.getTotalSize());
        model.addAttribute("lists", result.getList());

        model.addAttribute("view", "ecomdata");
        model.addAttribute("pView", "sale");
        return "ecom/goods_keyword_data_list";
    }


    @RequestMapping(value = "/goods_keyword_data_add", method = RequestMethod.GET)
    public String add(Model model,@RequestParam Long id,  HttpServletRequest request) {
        var keyword = keywordService.getKeywordById(id);
        model.addAttribute("keyword", keyword);
        return "ecom/goods_keyword_data_add";
    }

    @ResponseBody
    @RequestMapping(value = "/goods_keyword_data_add_ajax", method = RequestMethod.POST)
    public  ApiResult<String> addDataPostAjax(Model model, @RequestBody DataRow data, HttpServletRequest request) {
        KeywordDataEntity d = new KeywordDataEntity();
        d.setKeywordId(data.getLong("keywordId"));
        d.setRank(data.getInt("rank"));
        d.setGoodsCount(data.getInt("goodsCount"));
        d.setSousuorenqi(data.getInt("sousuorenqi"));
        d.setSousuoredu(data.getInt("sousuoredu"));
        d.setDianjirenqi(data.getInt("dianjirenqi"));
        d.setDianjiredu(data.getInt("dianjiredu"));
        d.setDianjilv(data.getFloat("dianjilv"));
        d.setZhifulv(data.getFloat("zhifulv"));
        d.setJingzhengzhishu(data.getFloat("jingzhengzhishu"));
        d.setChengjiaozhishu(data.getFloat("chengjiaozhishu"));
        d.setShichangchujia(data.getFloat("shichangchujia"));
        d.setIncludeDate(data.getString("include_date"));




        keywordService.addKeywordData(d);
        //keyWordService.addKeyWord(kw);
        return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(), "SUCCESS");
    }


}
