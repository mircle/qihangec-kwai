package com.b2c.erp.controller.funds;

import jakarta.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import com.b2c.common.utils.DateUtil;
import com.b2c.interfaces.funds.FundsDetailService;

@RequestMapping("/funds")
@Controller
public class LedgerController {
    @Autowired
    private FundsDetailService fundsService;

    @RequestMapping("/ledger_day")
    public String ledger(Model model,HttpServletRequest request){
        String startDate = "";
        if(StringUtils.hasText(request.getParameter("startDate")))startDate = request.getParameter("startDate");
        else startDate = DateUtil.dateToString(DateUtil.beforeDayDate(30),"yyyy-MM-dd");
        model.addAttribute("startDate",startDate);
        String endDate = "";
        if(StringUtils.hasText(request.getParameter("endDate")))startDate = request.getParameter("endDate");
        else endDate = DateUtil.getCurrentDate();
        model.addAttribute("endDate", endDate);

        var list = fundsService.getLedgerDayReport(startDate,endDate);
        model.addAttribute("list", list);
        model.addAttribute("pageTitle", "总账（日）");
        model.addAttribute("view", "ledger_day");
        model.addAttribute("pView", "pd");
        return "funds/ledger_day";
    }
}
