package com.b2c.erp.controller;

import com.b2c.entity.DataRow;
import com.b2c.entity.ErpContactEntity;
import com.b2c.entity.ErpContactAddressEntity;
import com.b2c.entity.erp.vo.ErpContactAddressVo;
import com.b2c.entity.result.ResultVo;
import com.b2c.interfaces.wms.ClientManageService;
import com.b2c.entity.BaseAreaEntity;
import com.b2c.erp.response.ApiResult;
import com.b2c.erp.response.AreaApiResult;
import com.b2c.interfaces.BaseAreaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import jakarta.servlet.http.HttpServletRequest;
import java.util.List;

@RequestMapping("ajax_manage")
@RestController
public class AjaxClientManageController {
    @Autowired
    private ClientManageService manageService;

    @Autowired
    private BaseAreaService areaService;

    /**
     * 新增客户
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "add_client", method = RequestMethod.POST)
    public ApiResult<Integer> addClient(@RequestBody ErpContactEntity entity) {


        if(entity.getcCategory()==null) {
            if(entity.getcCategoryName().equals("直播间")){
                entity.setcCategory(99);
            }else if(entity.getcCategoryName().equals("线上店铺")){
                entity.setcCategory(88);
            }
            else{
                entity.setcCategory(0);
            }
        }
//        else {
//            if (entity.getcCategory() == 1) entity.setcCategoryName("消费者");
//            else if (entity.getcCategory() == 99) entity.setcCategoryName("直播间");
//            else entity.setcCategoryName("线上店铺");
//        }
        Integer i = manageService.addManage(entity);
        if (i == -1) return new ApiResult<>(i, "新增失败!!!");
        return new ApiResult<>(0, "新增成功");
    }

    @RequestMapping(value = "upd_client", method = RequestMethod.POST)
    public ApiResult<Integer> updClient(@RequestBody ErpContactEntity entity) {
        
       if(entity.getcCategoryName().equals("直播间")){
           entity.setcCategory(99);
       }else if (entity.getcCategoryName().equals("供应商")){
            entity.setcCategory(77);
        }else if (entity.getcCategoryName().equals("线上店铺")){
            entity.setcCategory(88);
        }else if (entity.getcCategoryName().equals("消费者")){
            entity.setcCategory(1);
        }else{
           entity.setcCategory(0);
       }
        Integer i = manageService.updManage(entity);
        if (i == -1) return new ApiResult<>(i, "更新失败!!!");
        return new ApiResult<>(0, "更新成功");
    }

    /**
     * 删除客户
     * @return
     */
    @RequestMapping(value = "del_client", method = RequestMethod.POST)
    public ApiResult<Integer> delClinet(@RequestBody DataRow model) {
        Integer i = manageService.delManage(model.getInt("userid"));
        if (i == -1) return new ApiResult<>(i, "删除失败!!!");
        return new ApiResult<>(0, "删除成功");
    }

    /**
     * 地址数据初始化
     *
     * @param
     * @return
     */
    @RequestMapping("/address_data")
    public AreaApiResult get(@RequestParam String code) {
        AreaApiResult areaApiResult = new AreaApiResult();
        if (StringUtils.isEmpty(code)) {
            areaApiResult.setStatus(-1);
            return areaApiResult;
        }
        ResultVo<List<BaseAreaEntity>> resultVo = areaService.getListByParent(code);
        if (resultVo.getCode() == 0) {
            //成功
            if (resultVo.getData() != null && resultVo.getData().size() > 0) {
                areaApiResult.setStatus(1);
                for (BaseAreaEntity area : resultVo.getData()) {
                    areaApiResult.addData(area.getCode(), area.getName());
                }
            } else {
                areaApiResult.setStatus(0);
                areaApiResult.setData(null);
                return areaApiResult;
            }
        } else {
            areaApiResult.setStatus(0);
            areaApiResult.setData(null);
            return areaApiResult;
        }
        return areaApiResult;
    }

    @RequestMapping(value = "address_edit_id",method = RequestMethod.POST)
    public ApiResult<ErpContactAddressVo> getErpcpmtactById(@RequestBody Object id ,HttpServletRequest request){
        return new ApiResult<>(0,"", manageService.getErpContactAddress(Integer.parseInt(id.toString())));
    }

    @RequestMapping(value = "address_edit_id2",method = RequestMethod.POST)
    public ApiResult<ErpContactAddressEntity> getErpcpmtactAddressById(@RequestBody Object id , HttpServletRequest request){
        return new ApiResult<>(0,"",manageService.getAddressIn(Integer.parseInt(id.toString())));
    }

    @RequestMapping(value = "address_delete",method = RequestMethod.POST)
    public ApiResult<Integer> delAddress(@RequestBody Object address_id){
        manageService.delAddressIn(Integer.parseInt(address_id.toString()));
        return new ApiResult<>(0,"删除成功");
    }
}
