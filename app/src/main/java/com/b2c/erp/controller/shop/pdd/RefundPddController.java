package com.b2c.erp.controller.shop.pdd;

import com.b2c.entity.datacenter.DcShopEntity;
import com.b2c.erp.DataConfigObject;
import com.b2c.erp.controller.CommonControllerUtils;
import com.b2c.interfaces.ExpressCompanyService;
import com.b2c.interfaces.WmsUserService;
import com.b2c.interfaces.pdd.OrderPddRefundService;
import com.b2c.interfaces.pdd.OrderPddService;
import com.b2c.interfaces.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import jakarta.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-09-24 16:05
 */
@RequestMapping("/pdd")
@Controller
public class RefundPddController {
    @Autowired
    private ShopService shopService;
    @Autowired
    private ExpressCompanyService expressCompanyService;
    @Autowired
    private OrderPddRefundService refundService;
    @Autowired
    private OrderPddService orderPddService;

    @Autowired
    private WmsUserService manageUserService;
    /**
     * 退货订单
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/refund_list", method = RequestMethod.GET)
    public String list(Model model, HttpServletRequest request) {
        //查询店铺信息
        // var shop = shopService.getShop(shopId);
        // model.addAttribute("shop",shop);
        Integer userId = DataConfigObject.getInstance().getLoginUserId();
        //有权限的店铺列表
        List<DcShopEntity> shops = shopService.getShopListByUserId(userId,5);//type=5是拼多多
        model.addAttribute("shops", shops);
        
        Integer shopId = null;
        if (!StringUtils.isEmpty(request.getParameter("shopId"))) {
            try {
                shopId = Integer.parseInt(request.getParameter("shopId"));
            }catch (Exception e){}
        }
        model.addAttribute("shopId",shopId);

        Integer type = null;
        if (!StringUtils.isEmpty(request.getParameter("type"))) {
            try {
                type = Integer.parseInt(request.getParameter("type"));
            }catch (Exception e){}
        }
        model.addAttribute("menuId", "refund_list");
        model.addAttribute("type", type);

        String page = request.getParameter("page");
        Integer pageIndex = 1;
        Integer pageSize = DataConfigObject.getInstance().getPageSize();

        Long goodsId = null;
        // String orderNum = "";
        // String logisticsCode = "";//退货物流单号

        // Integer startTime = null;
        // Integer endTime = null;

        Integer status = null;
        Integer auditStatus = null;

        if (!StringUtils.isEmpty(page)) {
            pageIndex = Integer.parseInt(page);
        }

        // if (!StringUtils.isEmpty(request.getParameter("orderNum"))) {
        //     orderNum = request.getParameter("orderNum");
        //     model.addAttribute("orderNum",orderNum);
        // }

        if (!StringUtils.isEmpty(request.getParameter("goodsId"))) {
            goodsId = Long.parseLong(request.getParameter("goodsId"));
            model.addAttribute("goodsId",goodsId);
        }


        // if (!StringUtils.isEmpty(request.getParameter("logisticsCode"))) {
        //     logisticsCode = request.getParameter("logisticsCode");
        //     model.addAttribute("logisticsCode",logisticsCode);
        // }

        String num = null;
        if (!StringUtils.isEmpty(request.getParameter("num"))) {
            num = request.getParameter("num");
            model.addAttribute("num",num);
        }

        if (!StringUtils.isEmpty(request.getParameter("status"))) {
            status = Integer.parseInt(request.getParameter("status"));
            model.addAttribute("status",status);
        }
        if (!StringUtils.isEmpty(request.getParameter("auditStatus"))) {
            auditStatus = Integer.parseInt(request.getParameter("auditStatus"));
            model.addAttribute("auditStatus",auditStatus);
        }
        Integer shippingStatus = null;
        if (!StringUtils.isEmpty(request.getParameter("shippingStatus"))) {
            shippingStatus = Integer.parseInt(request.getParameter("shippingStatus"));
            model.addAttribute("shippingStatus",shippingStatus);
        }

        // if (!StringUtils.isEmpty(request.getParameter("startTime"))) {
        //     // startTime = DateUtil.dateToStamp(request.getParameter("startTime"));
        //     startTime = DateUtil.dateTimeToStamp(request.getParameter("startTime")) ;
        //     model.addAttribute("startTime",request.getParameter("startTime"));
        // }
        // if (!StringUtils.isEmpty(request.getParameter("endTime"))) {
        //     // endTime = DateUtil.dateTimeToStamp(request.getParameter("endTime") + " 23:59:59");
        //     endTime = DateUtil.dateTimeToStamp(request.getParameter("endTime") );//+ " 23:59:59"
        //     model.addAttribute("endTime",request.getParameter("endTime"));
        // }


        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);
        var result = refundService.getList(pageIndex, pageSize, num, status,auditStatus,shippingStatus,shopId,type,goodsId);

        model.addAttribute("totalSize", result.getTotalSize());
        model.addAttribute("list", result.getList());

        model.addAttribute("company", expressCompanyService.getExpressCompany());
        CommonControllerUtils.setViewKey(model,manageUserService,"pddrefund");
//        model.addAttribute("view", "pddrefund");
//        model.addAttribute("pView", "pdd");
        return "order/pdd/refund_list_pdd";
    }


    /**
     * 退货详情
     *
     * @param model
     * @param id
     * @param request
     * @return
     */
    @RequestMapping(value = "/refund_detail", method = RequestMethod.GET)
    public String getOrderDetailYg(Model model, @RequestParam Long id, HttpServletRequest request) {
        model.addAttribute("menuId", "refund_list");
        var detail = refundService.getEntityById(id);

        if(detail == null) return "redirect:/pdd/refund_list";

        model.addAttribute("orderVo", detail);

        model.addAttribute("menuId", "refund_list");
        model.addAttribute("view", "pddshop");
        model.addAttribute("pView", "sale");
        return "order/pdd/refund_detail_pdd";
    }

    @RequestMapping(value = "/apply_refund_pdd", method = RequestMethod.GET)
    public String getOrderRefundOffline(Model model,@RequestParam Integer shopId, HttpServletRequest request) {
        //查询订单信息
        Long orderId = Long.parseLong(request.getParameter("id"));
        var orderDetail = orderPddService.getOrderDetailAndItemsById(orderId);
        model.addAttribute("menuId", "refund_list");
        model.addAttribute("items", orderDetail.getItems());
        model.addAttribute("orderId", orderId);
        model.addAttribute("shopId", shopId);
        return "order/pdd/order_apply_refund_pdd";
    }

}
