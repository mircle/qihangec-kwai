package com.b2c.erp.controller.report;

import com.b2c.erp.controller.CommonControllerUtils;
import com.b2c.interfaces.WmsUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.b2c.interfaces.GoodsSalesAnalyseService;
import com.b2c.erp.DataConfigObject;

import javax.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;

/**
 * 商品动销分析
 */
@RequestMapping("/report")
@Controller
public class GoodsSalesAnalyseController {
    @Resource
    private GoodsSalesAnalyseService goodsSalesAnalyseService;
    @Autowired
    private WmsUserService manageUserService;
    /**
     * 商品动销分析
     * @param model
     * @param request
     * @return
     */
    @RequestMapping("/goods_sales_analyse")
    public String goodsSales(Model model, HttpServletRequest request) {
        model.addAttribute("searchUrl", "/report/goods_sales_analyse");
        String goodsNum="";
        if (!StringUtils.isEmpty(request.getParameter("goodsNum"))) {
            goodsNum = request.getParameter("goodsNum");
            model.addAttribute("goodsNum", goodsNum);
        }


        Integer pageIndex = 1, pageSize = DataConfigObject.getInstance().getPageSize();
        if (!StringUtils.isEmpty(request.getParameter("page"))) {
            pageIndex = Integer.parseInt(request.getParameter("page"));
        }

        var result = goodsSalesAnalyseService.getGoodsSalesReport(pageIndex, pageSize, null, goodsNum);

        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("totalSize", result.getTotalSize());
        model.addAttribute("lists", result.getList());

//        model.addAttribute("view", "spdx");
//        model.addAttribute("pView", "sale");
        CommonControllerUtils.setViewKey(model,manageUserService,"spdx");

        return "report/goods_sales_analyse";
    }

    
    @RequestMapping("/goods_spec_sales_analyse")
    public String goodsSpecSales(Model model,@RequestParam Integer goodsId, HttpServletRequest request) {
        model.addAttribute("searchUrl", "/report/goods_spec_sales_analyse");
        

        var list = goodsSalesAnalyseService.getGoodsSpecSalesReport(goodsId);

  
        model.addAttribute("totalSize", list.size());
        model.addAttribute("lists", list);

//        model.addAttribute("view", "spdx");
//        model.addAttribute("pView", "sale");
        CommonControllerUtils.setViewKey(model,manageUserService,"spdx");

        return "report/goods_spec_sales_analyse";
    }

}
