package com.b2c.erp.controller.report;

import com.b2c.erp.controller.CommonControllerUtils;
import com.b2c.interfaces.GoodsCategoryAnalyseService;
import com.b2c.interfaces.WmsUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import jakarta.servlet.http.HttpServletRequest;

/**
 * 商品动销分析
 */
@RequestMapping("/report")
@Controller
public class GoodsCategoryAnalyseController {

    @Autowired
    private GoodsCategoryAnalyseService analyseService;
    @Autowired
    private WmsUserService manageUserService;
    /**
     * 商品类目动销分析
     * @param model
     * @param request
     * @return
     */
    @RequestMapping("/goods_category_analyse")
    public String goodsSales(Model model, HttpServletRequest request) {
        model.addAttribute("searchUrl", "/report/goods_category_analyse");
        String page = request.getParameter("page");
        var list = analyseService.getAnalyseReport();
        model.addAttribute("list",list);
        model.addAttribute("totalSize",list.size());

//        model.addAttribute("view", "spdx");
//        model.addAttribute("pView", "sale");
        CommonControllerUtils.setViewKey(model,manageUserService,"spdx");

        return "report/goods_category_analyse";
    }
}
