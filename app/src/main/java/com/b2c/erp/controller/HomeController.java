package com.b2c.erp.controller;

import com.b2c.common.third.express.KuaiDi100ExpressClient;
import com.b2c.interfaces.erp.ErpGoodsService;
import com.b2c.interfaces.wms.StockService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jakarta.servlet.http.HttpServletRequest;
import java.io.*;

/**
 * 描述：
 * 控制面板首页
 *
 * @author qlp
 * @date 2019-05-28 13:34
 */
@Controller
public class HomeController {
    Logger log = LoggerFactory.getLogger(HomeController.class);
    @Autowired
    private StockService stockService;
    @Autowired
    private ErpGoodsService goodsService;
//    @Autowired
//    private KafkaProducer producer;
//    @Reference(version = "1.0.0")
//    private WarehouseStockService warehouseStockService;
//
//    @ResponseBody
//    @RequestMapping("/test")
//    public ApiResult<List<ErpGoodsSpecStockVo>> test(){
//
//        PagingResult<ErpGoodsSpecStockVo> result = warehouseStockService.goodsStockInfoSearch(1,10,"",null);
//        return new ApiResult<>(0,"success",result.getList());
//    }


    @RequestMapping("/")
    public String index(Model model) throws IOException, InterruptedException {
        //测试发送消息
//        producer.send();
        //
        //查询商品大类库存
        var result = stockService.getStockQtyByGoodsCategory();
        model.addAttribute("stockList",result);
//        var yiliao = stockService.getStockQtyByGoodsCategory(1);//医疗器械
//        model.addAttribute("yl",yiliao.getData());
//        var nvzhuang =stockService.getStockQtyByGoodsCategory(5);//女装
//        model.addAttribute("nv",nvzhuang.getData());
//        var nanzhuang =stockService.getStockQtyByGoodsCategory(4);//男装
//        model.addAttribute("nan",nanzhuang.getData());
//        var baihuo =stockService.getStockQtyByGoodsCategory(8);//百货
//        model.addAttribute("bh",baihuo.getData());

        // var goodsList = goodsService.getListFor7Day();
        // model.addAttribute("goodsList",goodsList);
        // model.addAttribute("sjcd", "首页");
        // return "home";
        return "redirect:/shop/dashboard";
    }
    @RequestMapping("/home")
    public String home(Model model) throws IOException, InterruptedException {
        var result = stockService.getStockQtyByGoodsCategory();
        model.addAttribute("stockList",result);
        return "redirect:/shop/dashboard";
    }


    @RequestMapping(value = "/barcode_print", method = RequestMethod.GET)
    public String purchaseDetail(Model model, HttpServletRequest request) {

        return "barcode_print";
    }

    @RequestMapping("/paramError")
    public String paramError(Model model,String msg,String redirect,HttpServletRequest request) throws IOException, InterruptedException {
        model.addAttribute("msg",msg);
//        model.addAttribute("blp",request.getParameter("blp"));
        model.addAttribute("redirect",redirect);
        return "paramError";
    }





//    @RequestMapping("/redirect_ali")
//    public String redirect_ali(Model model, HttpServletRequest request) throws IOException, InterruptedException {
//        /**********第一步：通过1688回调得到code**********/
//        String code = request.getParameter("code");
//
//        String redirectUrl = "http://localhost:8081/redirect_token";
//        String appId = "9380846";
//        String appSecret = "MJC3doohMxCG";
//
//        /************第二部：通过code获取令牌（post）**************/
//        String url = "https://gw.open.1688.com/openapi/http/1/system.oauth2/getToken/" + appId + "?grant_type=authorization_code&need_refresh_token=true&client_id=" + appId + "&client_secret=" + appSecret + "&redirect_uri=" + redirectUrl + "&code=" + code;
//        HttpClient client = HttpClient.newBuilder().build();
//        HttpRequest request = HttpRequest.newBuilder()
//                .uri(URI.create(url))
////                .header("Content-Type","application/x-www-form-urlencoded")
//                .POST(HttpRequest.BodyPublishers.ofString(""))//HttpRequest.BodyPublishers.ofString("name1=value1&name2=value2")
//                .build();
//
//        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
//        System.out.println(response.statusCode());
//
////        TokenVo tokenVo = JSONObject.parseObject(response.body(), TokenVo.class);
//
//
//        //?memberId=tonytp4&_aop_timestamp=1375703483649&access_token=HMKSwKPeSHB7Zk7712OfC2Gn1-kkfVsaM-P&_aop_signature=DE1D9BDE00646F5C1704930003C9FC011AADDE25
//
//
//        return "home";
//    }
//
//    @RequestMapping("/redirect_token")
//    public String redirect_token(Model model, HttpServletRequest request) {
//        String getToken = "";
////        String s=request.
//        return "home";
//    }

    //    @RequestMapping(value = "/get",produces = MediaType.IMAGE_JPEG_VALUE)

   

    @RequestMapping("/kuaidi100")
    public String kuaidi100(Model model, HttpServletRequest req) {
        String url = KuaiDi100ExpressClient.getCode();
        return "redirect:" + url;
    }
    /**
     * 无权限访问页面
     * @param model
     * @param req
     * @return
     */
    @RequestMapping("/no_access")
    public String noAccess(Model model, HttpServletRequest req) {
        return "no_access";
    }


}
