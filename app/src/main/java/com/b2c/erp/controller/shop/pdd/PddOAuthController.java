package com.b2c.erp.controller.shop.pdd;

import com.b2c.erp.DataConfigObject;
import com.b2c.interfaces.ShopService;
import com.b2c.interfaces.SysThirdSettingService;
import com.pdd.pop.ext.glassfish.grizzly.http.util.URLDecoder;
import com.pdd.pop.sdk.http.PopAccessTokenClient;
import com.pdd.pop.sdk.http.token.AccessTokenResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import jakarta.servlet.http.HttpServletRequest;

import java.io.CharConversionException;
import java.io.IOException;

@RequestMapping("/pdd")
@Controller
public class PddOAuthController {
    @Autowired
    private SysThirdSettingService thirdSettingService;
    @Autowired
    private ShopService shopService;

    private static Logger log = LoggerFactory.getLogger(PddOAuthController.class);

    @RequestMapping("/oauth")
    public String oauth(HttpServletRequest req, @RequestParam int shopId) {
        //SysVariableEntity variable = systemService.getVariable(VariableEnums.URL_DATACENTER_INDEX.name());
        String returnUrl = DataConfigObject.getInstance().getPddCallbackUrl() + "/pdd/getToken&state="+shopId;
        String clientId = DataConfigObject.getInstance().getPddClientId();
        String url;
        try {
            url = "https://mms.pinduoduo.com/open.html?response_type=code&client_id=" + clientId + "&redirect_uri=" +  URLDecoder.decode(returnUrl);
            return "redirect:" + url;
        } catch (CharConversionException e) {
            e.printStackTrace();
            return "redirect:/" ;
        }
        
    }

    @RequestMapping("/getToken")
    public String getToken(HttpServletRequest req) throws IOException, InterruptedException {
        log.info("/**********获取拼多多授权token*********/");
        String code = req.getParameter("code");
        String clientId = DataConfigObject.getInstance().getPddClientId();
        String clientSecret = DataConfigObject.getInstance().getPddClientSecret();
        Integer shopId =Integer.parseInt(req.getParameter("state"));

        PopAccessTokenClient accessTokenClient = new PopAccessTokenClient(clientId, clientSecret);

        // 生成AccessToken
        try {
            AccessTokenResponse response = accessTokenClient.generate(code);
            if(response.getErrorResponse()!=null){
                log.info("/***************获取拼多多授权token错误："+response.getErrorResponse().getErrorMsg()+"**************/");
            }else{
                //保存accessToken
                System.out.println(shopId +"--token:" + response.getAccessToken()+",thirdId:"+response.getOwnerId()+",shopId:"+shopId);

                shopService.updateSessionKey(shopId,Long.parseLong(response.getOwnerId()),response.getAccessToken());

//                thirdSettingService.updateEntity(shopId, response.getAccessToken(), response.getRefreshToken(), response.getExpiresIn(),response.getOwnerId());
                return "redirect:/pdd/getTokenSuccess?mallId="+response.getOwnerId();
/*                String state = req.getParameter("state");
                if (state.equalsIgnoreCase("GETORDERLIST")) {
                    //获取订单list
                    return "redirect:/shop/shop_list";
                } else if (state.equalsIgnoreCase("DCGOODSLIST")) {
                    //商品list
                    return "redirect:/goods/pdd_list";
                }*/
            }
        } catch (Exception e) {

            e.printStackTrace();

        }
        return "redirect:/";
    }

    /**
     * 获取授权成功
     * @param req
     * @param model
     * @return
     */
    @RequestMapping("/getTokenSuccess")
    public String getTokeSuccess(HttpServletRequest req, @RequestParam Long mallId, Model model){
        var shop = shopService.getShopByMallId(mallId);
        model.addAttribute("shop",shop);
        model.addAttribute("shopId",shop.getId()); 
        
        model.addAttribute("view", "pddshop");
        model.addAttribute("pView", "pdd");
        return "pdd/get_token_success";
    }



}
