package com.b2c.erp.controller.cw;

import com.b2c.entity.erp.enums.InvoiceTransTypeEnum;
import com.b2c.erp.DataConfigObject;
import com.b2c.interfaces.wms.ErpStockInFormService;
import com.b2c.interfaces.wms.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jakarta.servlet.http.HttpServletRequest;
import java.util.Optional;

@RequestMapping("/funds")
@Controller
public class CWPurchaseDataController {
    @Autowired
    private ErpStockInFormService erpStockInFormService;
    @Autowired
    private InvoiceService invoiceService;

    /**
     * 采购明细财务数据
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/purchase_info_list", method = RequestMethod.GET)
    public String getPurchaseInfoList(Model model, HttpServletRequest request) {

        Integer pageIndex = 1, pageSize = DataConfigObject.getInstance().getPageSize();
        if(ObjectUtils.isEmpty(request.getParameter("page"))==false){
            try {
                pageIndex = Integer.parseInt(request.getParameter("page"));
            } catch (Exception e) {
            }
        }


        String billDate = Optional.ofNullable(request.getParameter("billDate")).orElse("");
        model.addAttribute("billDate", billDate);
        String specNumber = Optional.ofNullable(request.getParameter("specNumber")).orElse("");
        model.addAttribute("specNumber", specNumber);

        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);

        var result = invoiceService.getInvoiceInfoList(pageIndex,pageSize, InvoiceTransTypeEnum.Purchase,specNumber,billDate);

        model.addAttribute("list", result.getList());
        model.addAttribute("totalSize", result.getTotalSize());

        model.addAttribute("view", "cgfk");
        model.addAttribute("pView", "pd");


        return "cw/erp_cw_purchase_data_list";
    }
}
