package com.b2c.erp.controller;

import com.b2c.entity.api.ApiResult;
import com.b2c.entity.DataRow;
import com.b2c.interfaces.wms.InvoiceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/purchase_ajax")
public class PurchaseAjaxController {
    private static Logger log = LoggerFactory.getLogger(PurchaseAjaxController.class);
    @Autowired
    private InvoiceService invoiceService;
    /**
     * 付款
     * @param req
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/payAmount", method = RequestMethod.POST)
    public ApiResult<Integer> returnGoodsToWareHouseSuccess(@RequestBody DataRow req) throws Exception{
        Long id = req.getLong("id");
        Double payAmount = req.getDouble("payAmount");
        log.info("付款AJAX {id:"+id+",amount:"+payAmount+"}");
        var res = invoiceService.purchasePayAmount(id,payAmount);
        return new ApiResult<>(res.getCode(),res.getMsg());
    }

    /**
     * 退款
     * @param req
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/refundAmount", method = RequestMethod.POST)
    public ApiResult<Integer> refundAmount(@RequestBody DataRow req) throws Exception{
        Long id = req.getLong("id");
        Double payAmount = req.getDouble("payAmount");
        log.info("退款REFUND-AJAX {id:"+id+",amount:"+payAmount+"}");
        var res = invoiceService.purchaseRefundAmount(id,payAmount);
        return new ApiResult<>(res.getCode(),res.getMsg());
    }
}
