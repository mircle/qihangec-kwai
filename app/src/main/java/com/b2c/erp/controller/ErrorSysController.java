package com.b2c.erp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import jakarta.servlet.http.HttpServletRequest;

@Controller
public class ErrorSysController {
    @RequestMapping("/sys_err")
    public String error(Model model, HttpServletRequest request){
        model.addAttribute("msg",request.getParameter("msg"));
        return "sys_err";
    }

}
