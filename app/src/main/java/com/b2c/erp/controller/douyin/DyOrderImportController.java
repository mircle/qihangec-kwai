package com.b2c.erp.controller.douyin;

import com.b2c.entity.UserEntity;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import jakarta.servlet.http.HttpServletRequest;

@RequestMapping("/douyin")
@Controller
public class DyOrderImportController {
    

    /**
     * excel导入订单预览页面
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/order_excel_import_review", method = RequestMethod.GET)
    public String pipi_excel_import_review(Model model, @RequestParam Integer shopId, HttpServletRequest request) {

        model.addAttribute("shopId", shopId);
        model.addAttribute("orderCount", 0);

        model.addAttribute("view", "douorder");
        model.addAttribute("pView", "dou");
        return "douyin/order_excel_import_review_dy";
    }
}
