//package com.b2c.wms.controller;
//
//import com.b2c.enums.erp.EnumUserActionType;
//import com.b2c.service.erp.*;
//import com.b2c.vo.erp.ErpCheckoutStockInItemVo;
//import com.b2c.vo.erp.ErpStockInCheckoutFormDetailVo;
//import com.b2c.vo.erp.ErpStockInFormDetailVo;
//import com.b2c.vo.erp.ErpStockOutFormDetailVo;
//import com.b2c.wms.DataConfigObject;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.util.StringUtils;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//
//import jakarta.servlet.http.HttpServletRequest;
//import jakarta.servlet.http.HttpSession;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
///**
// * 入库检验
// */
//
//@RequestMapping("/checkout")
//@Controller
//public class CheckoutController {
//
//    @Autowired
//    private InvoiceService invoiceService;
//    @Autowired
//    private ErpGoodsService goodsService;
//    @Autowired
//    private ErpStockInCheckoutService stockInCheckoutService;
//    @Autowired
//    private ErpGoodsStockLogsService goodsStockLogsService;
//    @Autowired
//    private ErpStockInFormService erpStockInFormService;
//    @Autowired
//    private StockInService stockInService;
//    @Autowired
//    private ErpStockOutFormService stockOutFormService;
//    @Autowired
//    private ErpUserActionLogService logService;
//
//
//    /**
//     * 检验列表
//     *
//     * @param model
//     * @param request
//     * @return
//     */
//    @RequestMapping(value = "checkout_list", method = RequestMethod.GET)
//    public String getCheckout(Model model, HttpServletRequest request) {
//        Integer pageIndex = 1;
//        Integer pageSize = DataConfigObject.getInstance().getPageSize();
//        Integer status = null;
//        if (StringUtils.isEmpty(request.getParameter("page")) == false) {
//            try {
//                pageIndex = Integer.parseInt(request.getParameter("page"));
//            } catch (Exception e) {
//            }
//        }
//        if (StringUtils.isEmpty(request.getParameter("status")) == false) {
//            try {
//                status = Integer.parseInt(request.getParameter("status"));
//            } catch (Exception e) {
//            }
//        }
//
//        String billNo = request.getParameter("billNo");
//
//        model.addAttribute("billNo", billNo);
//
//        var result = stockInCheckoutService.getCheckoutList(pageIndex, pageSize, billNo, status);
//        model.addAttribute("list", result.getList());
//        model.addAttribute("pageIndex", pageIndex);
//        model.addAttribute("pageSize", pageSize);
//        model.addAttribute("totalSize", result.getTotalSize());
//        //添加系统日志
//        logService.addUserAction(Integer.parseInt(request.getSession().getAttribute("userId").toString()), EnumUserActionType.Query,
//                "/checkout/checkout_list","检验列表 , 日期 : "+new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss").format(new Date()),"成功");
//
//        model.addAttribute("view", "rkjy");
//        model.addAttribute("pView", "rk");
//        model.addAttribute("ejcd", "入库");
//        model.addAttribute("sjcd", "验货单");
//        return "checkout_list";
//    }
//
//    /***
//     * 验货单打印详情
//     * @param model
//     * @param request
//     * @return
//     */
//    @RequestMapping(value = "/print_detail", method = RequestMethod.GET)
//    public String purchasePutDetail(Model model, HttpServletRequest request, @RequestParam Long id) {
//        var detail = stockInCheckoutService.getDetailById(id);
//
//        model.addAttribute("detail", detail);
//        //添加系统日志
//        logService.addUserAction(Integer.parseInt(request.getSession().getAttribute("userId").toString()), EnumUserActionType.Query,
//                "/checkout/print_detail?id="+id,"验货单打印 , 日期 : "+new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss").format(new Date()),"成功");
//        return "checkout_print_detail";
//    }
//
//    /***
//     * 采购入库单详情
//     * @param model
//     * @param request
//     * @return
//     */
//    @RequestMapping(value = "/stock_in", method = RequestMethod.GET)
//    public String purchaseInDetail(Model model, @RequestParam Long id, HttpServletRequest request) {
//        model.addAttribute("checkoutId", id);
//        var detail = stockInCheckoutService.getDetailById(id);
//
//        if (detail != null) {
//            model.addAttribute("detail", detail);
//            //查询仓库系统
////            var house = stockLocationService.getListByParentId(0);
////            model.addAttribute("house", house);
//            //添加系统日志
//            logService.addUserAction(Integer.parseInt(request.getSession().getAttribute("userId").toString()), EnumUserActionType.Query,
//                    "/checkout/stock_in?id="+id,"验货入库单 , 日期 : "+new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss").format(new Date()),"成功");
//        } else {
//            model.addAttribute("detail", new ErpStockInCheckoutFormDetailVo());
//            logService.addUserAction(Integer.parseInt(request.getSession().getAttribute("userId").toString()), EnumUserActionType.Query,
//                    "/checkout/stock_in?id="+id,"验货入库单 , 日期 : "+new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss").format(new Date()),"失败,数据为空");
//        }
//
//        return "checkout_stock_in";
//    }
//
//    /**
//     * 验货单入库
//     *
//     * @param model
//     * @param request
//     * @return
//     */
//    @RequestMapping(value = "/stock_in", method = RequestMethod.POST)
//    public String stockInPost(Model model, HttpServletRequest request) {
//        Long checkoutId = Long.parseLong(request.getParameter("checkoutId"));
//        HttpSession session = request.getSession();
//        Object userid = session.getAttribute("userId");
//        String userName = (String) session.getAttribute("userName");
//
//        /***组合入库items***/
//        String[] specIdArr = request.getParameterValues("specId");
//        String[] specNumberArr = request.getParameterValues("specNumber");
//        String[] checkoutItemIdArr = request.getParameterValues("checkoutItemId");
//        String[] locationIdArr = request.getParameterValues("locationId");
//        String[] locationNumberArr = request.getParameterValues("locationNumber");
//        String[] quantityArr = request.getParameterValues("quantity");
//
//        List<ErpCheckoutStockInItemVo> items = new ArrayList<>();
//        for (int i = 0; i < specIdArr.length; i++) {
//            if(StringUtils.isEmpty(locationNumberArr[i])==false && StringUtils.isEmpty(quantityArr[i])==false) {
//                ErpCheckoutStockInItemVo item = new ErpCheckoutStockInItemVo();
//                item.setSpecId(Integer.parseInt(specIdArr[i]));
//                item.setSpecNumber(specNumberArr[i]);
//                item.setLocationId(Integer.parseInt(locationIdArr[i]));
//                item.setLocationNumber(locationNumberArr[i]);
//                item.setQuantity(Long.parseLong(quantityArr[i]));
//                item.setCheckoutItemId(Long.parseLong(checkoutItemIdArr[i]));
//                items.add(item);
//            }
//        }
//
//        var result = stockInService.checkoutStockIn(checkoutId, items, (int) userid, userName);
//        //添加系统日志
//        logService.addUserAction(Integer.parseInt(request.getSession().getAttribute("userId").toString()), EnumUserActionType.Add,
//                "/checkout/stock_in","验货单入库 , id : "+checkoutId+" , 日期 : "+new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss").format(new Date()),"成功");
//
//        return "redirect:/checkout/stock_in?id=" + checkoutId;
//    }
//
//    /***
//     *  入库记录(验货单入库记录)
//     * @param model
//     * @param id
//     * @param request
//     * @return
//     */
//    @RequestMapping(value = "/stock_in_logs", method = RequestMethod.GET)
//    public String stock_in_logs(Model model, @RequestParam Long id, HttpServletRequest request) {
//        model.addAttribute("StockInCheckoutId", id);
//        var detail = stockInCheckoutService.getDetailById(id);
//
//        if (detail != null) {
//            model.addAttribute("detail", detail);
//            var logs = erpStockInFormService.getListByCheckoutId(id);
//            model.addAttribute("logs", logs);
//            //添加系统日志
//            logService.addUserAction(Integer.parseInt(request.getSession().getAttribute("userId").toString()), EnumUserActionType.Query,
//                    "/checkout/stock_in_logs?id="+id,"入库记录 , 日期 : "+new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss").format(new Date()),"成功");
//        } else {
//            model.addAttribute("detail", new ErpStockInCheckoutFormDetailVo());
//            logService.addUserAction(Integer.parseInt(request.getSession().getAttribute("userId").toString()), EnumUserActionType.Query,
//                    "/checkout/stock_in_logs?id="+id,"入库记录 , 日期 : "+new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss").format(new Date()),"失败,数据为空");
//        }
//
//        return "checkout_stock_in_logs";
//    }
//
//    /**
//     * 入库详情（打印，弹窗）
//     *
//     * @param model
//     * @param id
//     * @param request
//     * @return
//     */
//    @RequestMapping(value = "/stock_in_form", method = RequestMethod.GET)
//    public String stock_in_form(Model model, @RequestParam Long id, HttpServletRequest request) {
//        var detail = erpStockInFormService.getById(id);
//
//        if (detail != null) {
//            model.addAttribute("detail", detail);
//            //添加系统日志
//            logService.addUserAction(Integer.parseInt(request.getSession().getAttribute("userId").toString()), EnumUserActionType.Query,
//                        "/checkout/stock_in_form?id="+id,"入库详情（打印，弹窗） , 日期 : "+new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss").format(new Date()),"成功");
//        } else {
//            model.addAttribute("detail", new ErpStockInFormDetailVo());
//            //添加系统日志
//            logService.addUserAction(Integer.parseInt(request.getSession().getAttribute("userId").toString()), EnumUserActionType.Query,
//                    "/checkout/stock_in_form?id="+id,"入库详情（打印，弹窗） , 日期 : "+new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss").format(new Date()),"失败,数据为空");
//        }
//
//        return "checkout_stock_in_form";
//    }
//
//    /**
//     * 出库详情（打印，弹窗）
//     *
//     * @param model
//     * @param id
//     * @param request
//     * @return
//     */
//    @RequestMapping(value = "/stock_out_form", method = RequestMethod.GET)
//    public String stockOutForm(Model model, @RequestParam Long id, HttpServletRequest request) {
//        var detail = stockOutFormService.getErpStockOutFormDetailVo(id);
//
//        if (detail != null) {
//            model.addAttribute("detail", detail);
//            //添加系统日志
//            logService.addUserAction(Integer.parseInt(request.getSession().getAttribute("userId").toString()), EnumUserActionType.Query,
//                    "/checkout/stock_in_form?id="+id,"入库详情（打印，弹窗） , 日期 : "+new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss").format(new Date()),"成功");
//
//        } else {
//            model.addAttribute("detail", new ErpStockOutFormDetailVo());
//            //添加系统日志
//            logService.addUserAction(Integer.parseInt(request.getSession().getAttribute("userId").toString()), EnumUserActionType.Query,
//                    "/checkout/stock_in_form?id="+id,"入库详情（打印，弹窗） , 日期 : "+new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss").format(new Date()),"失败,数据为空");
//        }
//
//        return "checkout_stock_out_form";
//    }
//
//
//
//}
