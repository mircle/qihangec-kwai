package com.b2c.erp.controller.shop.pdd;

import jakarta.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.b2c.entity.api.ApiResult;
import com.b2c.entity.DataRow;
import com.b2c.entity.pdd.ShopGoodsEntity;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.erp.DataConfigObject;
import com.b2c.interfaces.ShopService;
import com.b2c.interfaces.pdd.ShopGoodsService;
import com.pdd.pop.sdk.http.PopClient;
import com.pdd.pop.sdk.http.PopHttpClient;
import com.pdd.pop.sdk.http.api.pop.request.PddGoodsDetailGetRequest;
import com.pdd.pop.sdk.http.api.pop.request.PddGoodsListGetRequest;
import com.pdd.pop.sdk.http.api.pop.response.PddGoodsDetailGetResponse;
import com.pdd.pop.sdk.http.api.pop.response.PddGoodsListGetResponse;

@RequestMapping("/ajax_pdd")
@RestController
public class AjaxGoodsPddController {
    private static Logger log = LoggerFactory.getLogger(AjaxGoodsPddController.class);

    @Autowired
    private ShopService shopService;
    @Autowired
    private ShopGoodsService goodsService;
    

    @RequestMapping(value = "/pull_goods_list", method = RequestMethod.POST)
    public ApiResult<String> getOrderList(@RequestBody DataRow reqData, HttpServletRequest req)
            throws Exception {
        Integer shopId = reqData.getInt("shopId");// 拼多多shopId

        String clientId = DataConfigObject.getInstance().getPddClientId();
        String clientSecret = DataConfigObject.getInstance().getPddClientSecret();
        var shop = shopService.getShop(shopId);
        // var settingEntity = thirdSettingService.getEntity(shop.getType());
        String accessToken = shop.getSessionKey();

        PopClient client = new PopHttpClient(clientId, clientSecret);
        // 调取拼多多接口 pdd.order.list.get 订单列表查询接口（根据成交时间）
        PddGoodsListGetRequest pddReq = new PddGoodsListGetRequest();
        
        PddGoodsListGetResponse pddResp =client.syncInvoke(pddReq,accessToken);
        
        if (pddResp.getErrorResponse() != null) {
            if (pddResp.getErrorResponse().getErrorCode().intValue() == 10019) {
                return new ApiResult<>(EnumResultVo.TokenFail.getIndex(), "Token过期");
            } 
        }else{
            if (pddResp.getGoodsListGetResponse().getTotalCount() > 0) {
                log.info("find goods");
                var gList = pddResp.getGoodsListGetResponse().getGoodsList();
                for (var g1 : gList) {
                    PddGoodsDetailGetRequest detailGetRequest = new PddGoodsDetailGetRequest();
                    detailGetRequest.setGoodsId(g1.getGoodsId());

                    PddGoodsDetailGetResponse detailGetResponse = client.syncInvoke(detailGetRequest,accessToken);
                    

                    ShopGoodsEntity pddGoodsEntity = JSONObject.parseObject(JSONObject.toJSONString(g1), ShopGoodsEntity.class);

                    pddGoodsEntity.setShopId(shopId);
                    pddGoodsEntity.setShopType(shop.getType());
                    try {
                        pddGoodsEntity.setGoodsNum(detailGetResponse.getGoodsDetailGetResponse().getOuterGoodsId());
                    } catch (Exception e) {
        
                    }
                    
                    var res = goodsService.addGoods(pddGoodsEntity);
                    
                }
            }
        }
        
        
        return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(), "SUCCESS");
    }


}
