package com.b2c.erp.controller;


import com.b2c.entity.result.PagingResponse;
import com.b2c.common.utils.OrderNumberUtils;
import com.b2c.entity.erp.vo.ErpGoodsBadStockLogVo;
import com.b2c.entity.erp.vo.ErpGoodsBadStockVo;
import com.b2c.interfaces.erp.ErpUserActionLogService;
import com.b2c.interfaces.wms.StockBadService;
import com.b2c.erp.DataConfigObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jakarta.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 描述：
 * 不良品管理
 *
 * @author qlp
 * @date 2019-05-28 17:01
 */
@Controller
    @RequestMapping("/goods_bad_stock")
public class GoodsBadStockController {
    @Autowired
    private StockBadService badService;
    @Autowired
    private ErpUserActionLogService logService;


    /**
     * 不良品列表
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String getBadList(HttpServletRequest request, Model model) {
        Integer pageIndex = 1;
        Integer pageSize = DataConfigObject.getInstance().getPageSize();;
        if (!StringUtils.isEmpty(request.getParameter("page"))) {
            pageIndex = Integer.parseInt(request.getParameter("page"));
        }

        String number = "";
        if (!StringUtils.isEmpty(request.getParameter("number"))) {
            number = request.getParameter("number");
        }
        Integer type = null;
        if (!StringUtils.isEmpty(request.getParameter("type"))) {
            type = Integer.parseInt(request.getParameter("type"));
        }

        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);

        PagingResponse<ErpGoodsBadStockVo> result = badService.getStockBadList(pageIndex, pageSize, number,type);
        model.addAttribute("lists", result.getList());
        model.addAttribute("totalSize", result.getTotalSize());
        model.addAttribute("view", "stock_bad_list");
        model.addAttribute("pView", "sj");
        model.addAttribute("ejcd", "仓库");
        model.addAttribute("sjcd", "不良品列表");
        return "/erp_goods_bad_stock";
    }

    /**
     * 不良品详情
     */
    @RequestMapping(value = "detail", method = RequestMethod.GET)
    public String getBadDetail(HttpServletRequest request, Model model) {
        Integer stockId = Integer.parseInt(request.getParameter("id"));
        ErpGoodsBadStockVo stockBad = badService.getStockBad(stockId);

        model.addAttribute("stockBad", stockBad);
        List<ErpGoodsBadStockLogVo> badLog = badService.getBadLog(stockId);
        model.addAttribute("badLog", badLog);
        return "/erp_goods_bad_stock_detail";
    }

   @RequestMapping(value = "add",method = RequestMethod.GET)
   public String addBadStock(HttpServletRequest request , Model model){
       //生成单据编号
       String billNo = "BAD" + OrderNumberUtils.getOrderIdByTime();
       model.addAttribute("billNo", billNo);
       model.addAttribute("time", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));

       model.addAttribute("view", "bad_add");
       model.addAttribute("pView", "pd");
       model.addAttribute("ejcd", "仓库");
       model.addAttribute("sjcd", "新增不良品");
       return "erp_goods_bad_stock_add";
   }

}
