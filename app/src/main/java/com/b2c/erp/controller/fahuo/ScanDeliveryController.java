package com.b2c.erp.controller.fahuo;

import com.b2c.interfaces.erp.ErpOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import jakarta.servlet.http.HttpServletRequest;

/**
 * 描述：
 * 扫码出库（扫快递面单码）
 *
 * @author qlp
 * @date 2021-07-06 11:17
 */
@RequestMapping("/fahuo")
@Controller
public class ScanDeliveryController {
    @Autowired
    private ErpOrderService orderService;

    /**
     * 扫码出库（扫码结果）
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/order_scan_delivery", method = RequestMethod.GET)
    public String order_scan_delivery(Model model, HttpServletRequest request) {
        String num = "";
        if (!StringUtils.isEmpty(request.getParameter("num"))) {
            num = request.getParameter("num");
            model.addAttribute("num", num);
        }

//        if (StringUtils.isEmpty(num) == false) {
//            var result = orderService.getOrderAndItemsByLogisticsCodeOrorderNum(num);
////            if(result.size() == 1) return "redirect:/fahuo/order_scan_delivery?orderId="+result.get(0).getId();
//
//            if(result.size() == 1) model.addAttribute("orderId",result.get(0).getId());
//            else model.addAttribute("orderId","0");
//
//            model.addAttribute("list", result);
//            model.addAttribute("totalSize", result.size());
//        } else {
//            model.addAttribute("list", null);
//            model.addAttribute("totalSize", 0);
//            model.addAttribute("orderId","0");
//        }

        model.addAttribute("view", "saomaochuku");
        model.addAttribute("pView", "ck");

        return "/fahuo/order_scan_delivery";
    }




    /**
     * 扫码出库（确认）
     * @param model
     * @param orderId
     * @param request
     * @return
     */
    @RequestMapping(value = "/order_scan_delivery_confirm", method = RequestMethod.GET)
    public String order_scan_delivery_confirm(Model model, @RequestParam Long orderId, HttpServletRequest request) {
        var order = orderService.getOrderAndItemsByOrderId(orderId);
        model.addAttribute("order",order);

        return "/fahuo/order_scan_delivery_confirm";
    }

}
