package com.b2c.erp.controller.report;
import com.b2c.common.utils.DateUtil;
import com.b2c.entity.datacenter.DcShopEntity;
import com.b2c.erp.DataConfigObject;
import com.b2c.erp.controller.CommonControllerUtils;
import com.b2c.interfaces.ShopService;
import com.b2c.interfaces.WmsUserService;
import com.b2c.interfaces.pdd.PddSalesReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jakarta.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class ShopSalesReportController {
    @Autowired
    private ShopService shopService;
    @Autowired
    private PddSalesReportService pddSalesReportService;
    @Autowired
    private WmsUserService manageUserService;

    @RequestMapping(value = "/pdd/sales_report", method = RequestMethod.GET)
    public String sales_report(Model model, HttpServletRequest request) {
        String date = DateUtil.getCurrentDate();
        // String date = DateUtil.customizeDate_(1);
        if (!StringUtils.isEmpty(request.getParameter("date"))){
            date = request.getParameter("date") ;
        }
        model.addAttribute("date",date);



        String endDate = null;
        if (!StringUtils.isEmpty(request.getParameter("endDate"))){
            endDate = request.getParameter("endDate") ;
            model.addAttribute("endDate",endDate);
        }
        Integer shopId = null;
        if(StringUtils.hasText(request.getParameter("shopId"))){
            shopId = Integer.parseInt(request.getParameter("shopId")) ;
        }
        

        var report = pddSalesReportService.getSalesReport(shopId,date,endDate);
        model.addAttribute("report", report);

        Integer userId = DataConfigObject.getInstance().getLoginUserId();
        //有权限的店铺列表
        List<DcShopEntity> shops = shopService.getShopListByUserId(userId,5);//type=5是拼多多
        model.addAttribute("shops", shops);

        model.addAttribute("shopId", shopId);

//        model.addAttribute("view", "pddreport");
//        model.addAttribute("pView", "pdd");
        CommonControllerUtils.setViewKey(model, manageUserService, "pddshop");

        return "order/pdd/sales_report";
    }

    @RequestMapping(value = "/pdd/send_report", method = RequestMethod.GET)
    public String send_report(Model model, HttpServletRequest request) {
        // String date = DateUtil.getCurrentDate();
        String date = DateUtil.customizeDate_(1);
        if (!StringUtils.isEmpty(request.getParameter("date"))){
            date = request.getParameter("date") ;
        }
        model.addAttribute("date",date);
        Integer shopId = null;
        if(StringUtils.hasText(request.getParameter("shopId"))){
            shopId = Integer.parseInt(request.getParameter("shopId")) ;
        }

        var report = pddSalesReportService.getSendReport(date, shopId);
        model.addAttribute("report", report);

        Integer userId = DataConfigObject.getInstance().getLoginUserId();
        //有权限的店铺列表
        List<DcShopEntity> shops = shopService.getShopListByUserId(userId,5);//type=5是拼多多
        model.addAttribute("shops", shops);

        model.addAttribute("shopId", shopId);
  
//        model.addAttribute("view", "pddreport");
//        model.addAttribute("pView", "pdd");
        CommonControllerUtils.setViewKey(model, manageUserService, "pddshop");
        return "order/pdd/send_report";
    }

    @RequestMapping(value = "/pdd/sales_refund_report", method = RequestMethod.GET)
    public String sales_refund_report(Model model, HttpServletRequest request) {
        // String date = DateUtil.getCurrentDate();
        String date = DateUtil.customizeDate_(1);
        if (!StringUtils.isEmpty(request.getParameter("date"))){
            date = request.getParameter("date") ;
        }
        model.addAttribute("date",date);
        Integer shopId = null;
        if(StringUtils.hasText(request.getParameter("shopId"))){
            shopId = Integer.parseInt(request.getParameter("shopId")) ;
        }

        //商品销售退货排行
        var report = pddSalesReportService.getSalesAndRefundReport(shopId);
        model.addAttribute("report", report);

        Integer userId = DataConfigObject.getInstance().getLoginUserId();
        //有权限的店铺列表
        List<DcShopEntity> shops = shopService.getShopListByUserId(userId,5);//type=5是拼多多
        model.addAttribute("shops", shops);

        model.addAttribute("shopId", shopId);
  
//        model.addAttribute("view", "pddreport");
//        model.addAttribute("pView", "pdd");
        CommonControllerUtils.setViewKey(model, manageUserService, "pddshop");
        return "order/pdd/sales_refund_report";
    }


    @RequestMapping(value = "/pdd/finance_report", method = RequestMethod.GET)
    public String finance_report(Model model, HttpServletRequest request) {
        String date = DateUtil.getDateMonth();
        // String date = DateUtil.customizeDate_(1);
        if (!StringUtils.isEmpty(request.getParameter("date"))){
            date = request.getParameter("date") ;
        }
        model.addAttribute("date",date);



        String endDate = null;
        if (!StringUtils.isEmpty(request.getParameter("endDate"))){
            endDate = request.getParameter("endDate") ;
            model.addAttribute("endDate",endDate);
        }
        Integer shopId = null;
        if(StringUtils.hasText(request.getParameter("shopId"))){
            shopId = Integer.parseInt(request.getParameter("shopId")) ;
        }
        

        var report = pddSalesReportService.getFinanceMonthReport(shopId,null);
        model.addAttribute("report", report);

        Integer userId = DataConfigObject.getInstance().getLoginUserId();
        //有权限的店铺列表
        List<DcShopEntity> shops = shopService.getShopListByUserId(userId,5);//type=5是拼多多
        model.addAttribute("shops", shops);

        model.addAttribute("shopId", shopId);

//        model.addAttribute("view", "pddreport");
//        model.addAttribute("pView", "pdd");
        CommonControllerUtils.setViewKey(model, manageUserService, "pddshop");
        return "pdd/finance_report";
    }
     
}
