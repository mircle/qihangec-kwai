package com.b2c.erp.controller;

import com.b2c.interfaces.wms.ErpStockOutFormService;
import com.b2c.interfaces.erp.ErpUserActionLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import jakarta.servlet.http.HttpServletRequest;
import java.util.ArrayList;

@Controller
@RequestMapping("/order_delivery")
public class OrderPickingController {
    @Autowired
    private ErpStockOutFormService erpStockOutFormService;
    @Autowired
    private ErpUserActionLogService logService;



    /**
     * 拣货单详情
     * @param model
     * @param id 拣货单ID
     * @param request
     * @return
     */
    @RequestMapping(value = "/goods_picking_detail", method = RequestMethod.GET)
    public String pickingDetail(Model model,@RequestParam Long id, HttpServletRequest request) {

        var list = erpStockOutFormService.getErpStockOutFormDetailVo(id);

        model.addAttribute("list", list.getItems());
        model.addAttribute("totalSize", list.getItems().size());

        model.addAttribute("view", "jhd");
        model.addAttribute("pView", "ck");
        model.addAttribute("ejcd", "出库");
        model.addAttribute("sjcd", "拣货单详情（ID"+id+"）");
        return "pick/goods_picking_detail";
    }


    /***
     * 拣货确认
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/picking_confirm", method = RequestMethod.GET)
    public String pickingConfirmDetail(Model model, HttpServletRequest request) {
        try {
            Long id = Long.parseLong(request.getParameter("id"));
            var form = erpStockOutFormService.getStockOutFormById(id);

            model.addAttribute("pick", form);
            var goods = erpStockOutFormService.getStockOutFormItemListByFormId(form.getId());

//            var list = erpStockOutFormService.getErpStockOutFormDetailVo(id);
//            model.addAttribute("list", list.getItems());















            if (goods == null) {
                model.addAttribute("goods", new ArrayList<>());
            } else {
                model.addAttribute("goods", goods);
            }

            //查询仓库
//            var houses = stockLocationService.getListByParentId(0);
//            model.addAttribute("houses", houses);

//            List<ReservoirEntity> list = reservoirService.getIdByHouseId((int)id);

        } catch (Exception e) {

        }

        model.addAttribute("view", "jhd");
        model.addAttribute("pView", "ck");
//        return "stock_out_picking_confirm";
        return "fahuo/stock_out_picking_confirm";
    }
}
