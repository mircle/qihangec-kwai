package com.b2c.erp.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.b2c.common.third.express.KuaiDi100ExpressClient;
import com.b2c.common.utils.HttpUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jakarta.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description: pbd add 2019/6/13 17:17
 */
@Controller
public class Kuaidi100Controller {

    @RequestMapping(value = "/callback/kuaidi100", method = RequestMethod.GET)
    public String login(Model model, HttpServletRequest request) {
        String code = request.getParameter("code");
        try {
            String url = "https://b.kuaidi100.com/open/oauth/accessToken";
            Map<String, String> params = new HashMap<>();
            params.put("client_id", "Ywq0uOo6nTEx");
            params.put("client_secret", "6880dfb586d1442a9bbfd523fc30127f");
            params.put("grant_type", "authorization_code");
            params.put("code", code);
            params.put("redirect_uri", "http://localhost:8089/callback/kuaidi100");
            params.put("timestamp", String.valueOf(System.currentTimeMillis()));
            params.put("sign", KuaiDi100ExpressClient.buildSign(params, "6880dfb586d1442a9bbfd523fc30127f"));

            HttpClient client1 = HttpClient.newBuilder().build();
            HttpRequest request1 = HttpRequest.newBuilder()
                    .uri(URI.create(url))
                    .header("Content-Type", "application/x-www-form-urlencoded")
                    .POST(HttpRequest.BodyPublishers.ofString(HttpUtil.map2Url(params)))//HttpRequest.BodyPublishers.ofString("name1=value1&name2=value2")
                    .build();

            HttpResponse<String> response1 = client1.send(request1, HttpResponse.BodyHandlers.ofString());
            if (response1.statusCode() == 200) System.out.println(response1.body());
            JSONObject obj = JSONObject.parseObject(response1.body());
            //查询订单
            String url_1 = "https://b.kuaidi100.com/v6/open/api/send";
            Map<String, String> params_1 = new HashMap<>();
            params_1.put("appid", "Ywq0uOo6nTEx");
            params_1.put("access_token", obj.getString("access_token"));
            JSONObject json = new JSONObject();
            json.put("recMobile", "13077847784");
            json.put("recCompany", "金蝶集团");
            json.put("sendCompany", "快递 100");
            json.put("recName", "刘生");
            json.put("recAddr", "安徽亳州涡阳县牌坊镇 陈兰大药房");
            json.put("reccountry", "中国");
            json.put("sendMobile", "18675586237");
            json.put("sendName", "刘三石");
            json.put("sendAddr", "广东深圳南山区科技南十二路金蝶软件园");
            json.put("orderNum", "20180612001");
            json.put("kuaidiCom", "yunda");
            json.put("weight", 1);
            params_1.put("data", JSON.toJSONString(json));
            params_1.put("timestamp", String.valueOf(System.currentTimeMillis()));
            params_1.put("sign", KuaiDi100ExpressClient.buildSign(params_1, "6880dfb586d1442a9bbfd523fc30127f"));
            HttpClient client2 = HttpClient.newBuilder().build();
            HttpRequest request2 = HttpRequest.newBuilder()
                    .uri(URI.create(url_1))
                    .header("Content-Type", "application/x-www-form-urlencoded")
                    .POST(HttpRequest.BodyPublishers.ofString(HttpUtil.map2Url(params_1)))//HttpRequest.BodyPublishers.ofString("name1=value1&name2=value2")
                    .build();

            HttpResponse<String> response2 = client2.send(request2, HttpResponse.BodyHandlers.ofString());
            if (response2.statusCode() == 200) System.out.println(response2.body());
        } catch (Exception e) {
            System.out.println(e);

        }


        return "login";
    }
}
