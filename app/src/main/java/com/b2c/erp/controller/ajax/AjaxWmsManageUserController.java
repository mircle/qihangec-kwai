package com.b2c.erp.controller.ajax;

import com.b2c.common.utils.CheckUtils;
import com.b2c.entity.DataRow;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.ManageUserEntity;
import com.b2c.erp.DataConfigObject;
import com.b2c.erp.response.ApiResult;
import com.b2c.interfaces.WmsUserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import jakarta.servlet.http.HttpServletRequest;

/**
 * @Description: pbd add 2019/9/29 11:56
 */
@RequestMapping(value = "/ajax_wms_user")
@RestController
public class AjaxWmsManageUserController {
    @Autowired
    private WmsUserService wmsUserService;

    /**
     * 修改添加管理员
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/execute_manage_user", method = RequestMethod.POST)
    public ApiResult<Integer> checkManageUser(@RequestBody DataRow model) {
        ResultVo<Integer> rs = wmsUserService.executeWmsManageUser(model.getInt("userid"), model.getString("username"), model.getString("name"), model.getString("mobile"), model.getString("password"), model.getString("groupid"), model.getString("status"));
        if (rs.getCode() > 0) {
            return new ApiResult<>(rs.getCode(), rs.getMsg());
        }
        return new ApiResult<>(0, "SUCCESS");
    }

    /**
     * 删除管理员
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/del_manage_user", method = RequestMethod.POST)
    public ApiResult<Integer> delManageUser(@RequestBody DataRow model) {
        wmsUserService.delWmsManageUser(model.getInt("userid"));
        return new ApiResult<>(0, "SUCCESS");
    }


    /**
     * 修改管理员密码
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/upd_wms_manage_user_pwd", method = RequestMethod.POST)
    public ApiResult<Integer> updManageUserPwd(@RequestBody DataRow model, HttpServletRequest request) {
        int userId = DataConfigObject.getInstance().getLoginUserId();
        if (StringUtils.isEmpty(model.getString("oldPwd"))) {
            return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "请输入旧密码");
        } else if (StringUtils.isEmpty(model.getString("newPwd"))) {
            return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "请输入新密码");
        } else if (StringUtils.isEmpty(model.getString("repPwd"))) {
            return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "请再次输入新密码");
        } else if (CheckUtils.isSpace(model.getString("repPwd")) == false || CheckUtils.isSpace(model.getString("newPwd")) == false) {
            return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "密码不能包含空格");
        } else if (!model.getString("newPwd").equals(model.getString("repPwd"))) {
            return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "两次密码不一致");
        } else {
            ResultVo<ManageUserEntity> resultVo = wmsUserService.updWmsUserPwd(userId,model.getString("oldPwd"),model.getString("newPwd"));
            if (resultVo.getCode() != 0) {
                return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), resultVo.getMsg());
            } else {
                return new ApiResult<>(0, "SUCCESS");
            }
        }
    }
}
