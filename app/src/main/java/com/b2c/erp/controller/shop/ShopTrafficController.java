package com.b2c.erp.controller.shop;

import java.util.List;

import jakarta.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.b2c.entity.api.ApiResult;
import com.b2c.common.utils.DateUtil;
import com.b2c.entity.DataRow;
import com.b2c.entity.datacenter.DcShopEntity;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.entity.shop.ShopTrafficGoodsEntity;
import com.b2c.erp.DataConfigObject;
import com.b2c.interfaces.ShopService;
import com.b2c.interfaces.ShopTrafficGoodsService;

@RequestMapping("/shop")
@Controller
public class ShopTrafficController {
    @Autowired
    private ShopService shopService;
    @Autowired
    private ShopTrafficGoodsService shopTrafficGoodsService;




    @RequestMapping(value = "/goods_traffic_tao_add", method = RequestMethod.GET)
    public String addTao(Model model, HttpServletRequest request) {
        Integer shopType = 4;
        // 店铺列表
        List<DcShopEntity> shops = shopService.getShopListByUserId(null, shopType);
        model.addAttribute("shops", shops);
        model.addAttribute("shopId", 6);
        String date = DateUtil.customizeDate_(1);
        model.addAttribute("date", date);

        return "eshop/shop_goods_traffic_tao_add";
    }

    @RequestMapping(value = "/goods_traffic_dy", method = RequestMethod.GET)
    public String listDy(Model model, HttpServletRequest request) {
        Integer shopType = 6;
        Integer pageIndex = 1, pageSize = DataConfigObject.getInstance().getPageSize();
        if (!StringUtils.isEmpty(request.getParameter("page"))) {
            pageIndex = Integer.parseInt(request.getParameter("page"));
        }
        // 店铺列表
        List<DcShopEntity> shops = shopService.getShopListByUserId(null, shopType);
        model.addAttribute("shops", shops);

        String goodsNum = null;
        if(StringUtils.hasText(request.getParameter("goodsNum"))){
            goodsNum = request.getParameter("goodsNum");
            model.addAttribute("goodsNum", goodsNum);
        }
        Integer shopId=22;
        if (!StringUtils.isEmpty(request.getParameter("shopId"))){
            shopId = Integer.parseInt(request.getParameter("shopId")); 
        }
        model.addAttribute("shopId",shopId);
        String date = DateUtil.customizeDate_(1);
        if(StringUtils.hasText(request.getParameter("date"))){
            date = request.getParameter("date");
        }
        model.addAttribute("date", date);

        var result = shopTrafficGoodsService.getGoodsTrafficList(pageIndex, 100, shopType, shopId, goodsNum,date);

        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("totalSize", result.getTotalSize());
        model.addAttribute("lists", result.getList());

        model.addAttribute("view", "shop_traffic");
        model.addAttribute("pView", "goods");
        return "eshop/shop_goods_traffic_dy";
    }


    @RequestMapping(value = "/goods_traffic_dy_add", method = RequestMethod.GET)
    public String addDy(Model model, HttpServletRequest request) {
        Integer shopType = 6;
        // 店铺列表
        List<DcShopEntity> shops = shopService.getShopListByUserId(null, shopType);
        model.addAttribute("shops", shops);
        model.addAttribute("shopId", 22);
        String date = DateUtil.customizeDate_(1);
        model.addAttribute("date", date);

        return "eshop/shop_goods_traffic_dy_add";
    }




    @ResponseBody
    @RequestMapping(value = "/goods_traffic_add_ajax", method = RequestMethod.POST)
    public  ApiResult<String> addPostAjax(Model model, @RequestBody DataRow data, HttpServletRequest request) {
       
        Integer shopId = data.getInt("shopId");
        var shop = shopService.getShop(shopId);

        String goodsId = data.getString("goodsId");

        // String goodsNumber = data.getString("goodsNumber");
        Integer shows = data.getInt("shows");
        Integer paidShows = data.getInt("paidShows");
        Integer visits = data.getInt("visits");
        Integer paidVisits = data.getInt("paidVisits");
        Integer views = data.getInt("views");
        Integer orders = data.getInt("orders");
        Integer paidOrders = data.getInt("paidOrders");
        Integer collects = data.getInt("collects");
        Integer paidCollects = data.getInt("paidCollects");
        Integer carts = data.getInt("carts");
        Integer paidCarts = data.getInt("paidCarts");
        Integer chats = data.getInt("chats");
        Integer paidChats = data.getInt("paidChats");
        Integer orderUsers = data.getInt("orderUsers");
        String date = data.getString("date");
        String remark = data.getString("remark");
        ShopTrafficGoodsEntity entity = new ShopTrafficGoodsEntity();
        entity.setShopId(shopId);
        entity.setShopType(shop.getType());
        entity.setGoodsId(goodsId);
        // entity.setGoodsNumber(goodsNumber);
        entity.setShows(shows);
        entity.setPaidShows(paidShows);
        entity.setVisits(visits);
        entity.setPaidVisits(paidVisits);
        entity.setViews(views);
        entity.setOrders(orders);
        entity.setPaidOrders(paidOrders);
        entity.setCollects(collects);
        entity.setPaidCollects(paidCollects);
        entity.setCarts(carts);
        entity.setPaidCarts(paidCarts);
        entity.setChats(chats);
        entity.setPaidChats(paidChats);
        entity.setDate(date);
        entity.setRemark(remark);
        entity.setOrderUsers(orderUsers);
        Double orderAmount = data.getDouble("orderAmount");
        Float CVR = data.getFloat("CVR");
        entity.setOrderAmount(orderAmount);
        entity.setCVR(CVR);
        String cvrTag = data.getString("cvrTag");
        entity.setCvrTag(cvrTag);

        // service.addKeyword(shopId, keyword, source, goodsId, views, date);
        var result = shopTrafficGoodsService.addGoodsTraffic(entity);
        return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(), "SUCCESS");
    }


   

  
}
