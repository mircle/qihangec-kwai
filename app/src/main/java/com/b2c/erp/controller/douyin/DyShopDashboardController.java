package com.b2c.erp.controller.douyin;

import java.util.Date;

import jakarta.servlet.http.HttpServletRequest;

import com.b2c.erp.controller.CommonControllerUtils;
import com.b2c.interfaces.WmsUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.b2c.common.utils.DateUtil;
import com.b2c.interfaces.ShopService;
import com.b2c.entity.vo.ShopOrderStatisticsVo;

@RequestMapping("/douyin")
@Controller
public class DyShopDashboardController {
    @Autowired
    private ShopService shopService;
    @Autowired
    private WmsUserService manageUserService;
    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public String dashboard(Model model, HttpServletRequest request) {

        Integer shopId =22;
        model.addAttribute("menuId", "home");
        model.addAttribute("shopId", shopId);
        //查询店铺信息
        var shop = shopService.getShop(shopId);
        model.addAttribute("shop", shop);
        //店铺列表
//        List<DcShopEntity> shops = shopService.getShopList();
//        model.addAttribute("shops", shops);
        //店铺统计信息
        var statistics = shopService.shopOrderStatistics(shopId);
        if(statistics!=null)
            model.addAttribute("report", statistics);
        else  model.addAttribute("report",new ShopOrderStatisticsVo());
        model.addAttribute("waitSendOrderStatus", 2);

        model.addAttribute("today", DateUtil.dateToString(new Date(),"yyyy-MM-dd"));


//        model.addAttribute("view","dyshop");
//        model.addAttribute("pView", "dou");
        CommonControllerUtils.setViewKey(model, manageUserService, "dyshop");
        return "douyin/shop_dashboard";
    }
}
