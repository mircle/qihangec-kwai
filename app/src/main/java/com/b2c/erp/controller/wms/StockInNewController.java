package com.b2c.erp.controller.wms;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.erp.InvoiceEntity;
import com.b2c.entity.erp.enums.InvoiceBillStatusEnum;
import com.b2c.entity.erp.enums.InvoiceBillTypeEnum;
import com.b2c.entity.erp.enums.InvoiceTransTypeEnum;
import com.b2c.erp.DataConfigObject;
import com.b2c.interfaces.erp.ErpUserActionLogService;
import com.b2c.interfaces.wms.InvoiceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jakarta.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/stock_in")
public class StockInNewController {
    @Autowired
    private InvoiceService invoiceService;
    @Autowired
    private ErpUserActionLogService logService;
    private static Logger log = LoggerFactory.getLogger(StockInNewController.class);
    /***
     * 采购待入库列表
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/purchase_in_wait_list", method = RequestMethod.GET)
    public String getPurchaseList(Model model, HttpServletRequest request) {
        Integer pageIndex = 1;
        Integer pageSize = DataConfigObject.getInstance().getPageSize();
        ;
        if (StringUtils.isEmpty(request.getParameter("page")) == false) {
            try {
                pageIndex = Integer.parseInt(request.getParameter("page"));
            } catch (Exception e) {
            }
        }

        String billNo = request.getParameter("billNo");
        model.addAttribute("billNo", billNo);
        String startDate = request.getParameter("startDate");
        model.addAttribute("startDate", startDate);
        String endDate = request.getParameter("endDate");
        model.addAttribute("endDate", endDate);

        PagingResponse<InvoiceEntity> result = invoiceService.getList(pageIndex, pageSize, InvoiceTransTypeEnum.Purchase, InvoiceBillTypeEnum.Purchase, InvoiceBillStatusEnum.Audited, null, null, billNo, startDate, endDate,null);
        model.addAttribute("list", result.getList());
        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("totalSize", result.getTotalSize());

        model.addAttribute("view", "cgrk_rk");
        model.addAttribute("pView", "rk");
        model.addAttribute("ejcd", "收货&入库");
        model.addAttribute("sjcd", "待入库的采购单");

        return "wms/stock_in_purchase_in_wait_list";
    }

    @RequestMapping(value = "/stock_in_purchase", method = RequestMethod.GET)
    public String purchaseStockIn1(Model model, HttpServletRequest request) {
        if(StringUtils.hasText(request.getParameter("num"))){
            //查询采购单
            var invoice = invoiceService.getInvoiceDetail(request.getParameter("num"));
            if (invoice == null) {
                model.addAttribute("msg", "没有找到数据");
            }else if(invoice.getBillStatus() != InvoiceBillStatusEnum.Audited.getIndex()){
                model.addAttribute("msg", "采购单状态："+InvoiceBillStatusEnum.getName(invoice.getBillStatus())+"。无法操作");
            }
            else {
                return "redirect:/purchase/stock_in?id="+invoice.getId();
            }
        }
        model.addAttribute("view", "cgrk1");
        model.addAttribute("pView", "rk");
        model.addAttribute("ejcd", "收货&入库");
        model.addAttribute("sjcd", "采购入库（扫码）");
        return "wms/purchase_stock_in_purchase";
    }
}
