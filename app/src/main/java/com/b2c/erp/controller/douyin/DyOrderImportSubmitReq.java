package com.b2c.erp.controller.douyin;

import com.b2c.entity.vo.DyOrderImportOrderVo;


import java.util.List;

public class DyOrderImportSubmitReq {

  
    private Integer shopId;

    private List<DyOrderImportOrderVo> orderList;//导入的订单

   
    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public List<DyOrderImportOrderVo> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<DyOrderImportOrderVo> orderList) {
        this.orderList = orderList;
    }
}
