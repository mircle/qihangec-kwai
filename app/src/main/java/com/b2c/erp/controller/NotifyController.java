package com.b2c.erp.controller;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import jakarta.servlet.http.HttpServletRequest;
 import jakarta.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * @Description:仓库相关通知业务 pbd add 2019/6/18 10:56
 */
@RequestMapping("/notify")
@Controller
public class NotifyController {
    //打log用
    private static Logger log = LoggerFactory.getLogger(NotifyController.class);

    @RequestMapping(value = "/express_notify", produces = MediaType.APPLICATION_JSON_VALUE)
    public void expressNotify(HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setHeader("content-type", "text/html;charset=utf-8");
        PrintWriter out = response.getWriter();
        String strs = request.getParameter("param");
        log.info("快递信息推送报文：" + strs);
        if (!StringUtils.isEmpty(strs)) {
            JSONObject obj = JSONObject.parseObject(strs);
            log.info("快递信息推送json报文：" + obj);
        }
        //String res="{\"result\":true,\"returnCode\":\"200\",\"message\":\"接收成功\"}";
        //out.write(res);
    }
}
