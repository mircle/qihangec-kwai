package com.b2c.erp.controller.bigData;

import com.b2c.entity.api.ApiResult;
import com.b2c.entity.DataRow;
import com.b2c.entity.KeyWordEntity;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.interfaces.ecom.KeyWordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import jakarta.servlet.http.HttpServletRequest;
import java.util.List;

@RequestMapping("/big_data")
@RestController
public class AjaxKeyWordController {
    @Autowired
    private KeyWordService keyWordService;

    @RequestMapping(value = "/keyword_parent_list_ajax", method = RequestMethod.POST)
    public ApiResult<List<KeyWordEntity>> addPostAjax(Model model, @RequestBody DataRow data, HttpServletRequest request) {
        Long parentId = data.getLong("parentId");
        var parentList = keyWordService.getParentList(parentId);
        return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(), "SUCCESS",parentList);
    }
}
