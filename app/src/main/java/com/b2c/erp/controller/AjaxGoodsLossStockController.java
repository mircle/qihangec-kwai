package com.b2c.erp.controller;

import com.b2c.entity.erp.vo.ErpGoodsBadStockLogVo;
import com.b2c.entity.enums.EnumUserActionType;
import com.b2c.interfaces.erp.ErpUserActionLogService;
import com.b2c.interfaces.wms.StockBadService;
import com.b2c.erp.req.GoodsSearchReq;
import com.b2c.erp.response.ApiResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import jakarta.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 描述：
 * 出库管理
 *
 * @author qlp
 * @date 2019-05-28 17:01
 */
@RestController
@RequestMapping("/ajax_loss_stock")
public class AjaxGoodsLossStockController {
    @Autowired
    private StockBadService badService;
    @Autowired
    private ErpUserActionLogService logService;

    /**
     * 关键词搜索商品
     *
     * @return
     */
    @RequestMapping(value = "/loss_search_by_number", method = RequestMethod.POST)
    public ApiResult<List<ErpGoodsBadStockLogVo>> searchGoodsDetail(@RequestBody GoodsSearchReq req) {
        List<ErpGoodsBadStockLogVo> likeBad = badService.getLIKEBad(req.getKey());
        return new ApiResult<>(0, "SUCCESS", likeBad);
    }

    /**
     * 报损审核
     * @param id
     * @param request
     * @return
     */
    @RequestMapping(value = "/check_loss",method = RequestMethod.POST)
    public ApiResult<Integer> checkLoss(@RequestBody Object id , HttpServletRequest request){
        Object userName = request.getSession().getAttribute("userName");
        Object userId = request.getSession().getAttribute("userId");

        Integer i = badService.checkLoss(Integer.parseInt(id.toString()), Integer.parseInt(userId.toString()), userName.toString());

        if (i==-1) return new ApiResult<>(404,"报损单审核失败");
        else if (i==-2) return new ApiResult<>(404,"报损单不存在");
        else if (i==-3) return new ApiResult<>(404,"报损单已审核");

        //添加系统日志
        logService.addUserAction(Integer.parseInt(request.getSession().getAttribute("userId").toString()), EnumUserActionType.Audit,
                "/ajax_loss_stock/check_loss","审核报损单 , id : "+id+" ,日期 : "+new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss").format(new Date()),"成功");

        return new ApiResult<>(0,"SUCCESS");
    }
}
