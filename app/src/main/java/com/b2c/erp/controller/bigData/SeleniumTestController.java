package com.b2c.erp.controller.bigData;

import jakarta.servlet.http.HttpServletRequest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("/selenium")
@Controller
public class SeleniumTestController {
    private static Logger log = LoggerFactory.getLogger(KeyWordController.class);
    /**
     * 订单列表
     *
     * @param model
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/pdd_cj", method = RequestMethod.POST)
    public String list(Model model, HttpServletRequest request) {
        

        ChromeOptions options = new ChromeOptions();
        // options.addArguments("--headless");
        options.addArguments("useAutomationExtension=False");

        WebDriver driver = new ChromeDriver(options);

        driver.get("https://mobile.pinduoduo.com");
 
        String title = driver.getTitle();
        System.out.printf(title);
        model.addAttribute("title", title);
 
        //driver.close();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "SUCCESS";
    }
}
