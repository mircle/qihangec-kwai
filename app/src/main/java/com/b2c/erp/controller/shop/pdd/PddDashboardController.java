package com.b2c.erp.controller.shop.pdd;

import com.b2c.interfaces.GoodsSalesAnalyseService;
import com.b2c.common.utils.DateUtil;
import com.b2c.entity.datacenter.DcShopEntity;
import com.b2c.erp.DataConfigObject;
import com.b2c.interfaces.WmsUserService;
import com.b2c.interfaces.ShopService;
import com.b2c.interfaces.TodoService;
import com.b2c.interfaces.pdd.PddSalesReportService;
import com.b2c.entity.vo.ShopOrderStatisticsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

@Controller
public class PddDashboardController {
    @Autowired
    private ShopService shopService;
    @Autowired
    private WmsUserService manageUserService;
    @Autowired
    private PddSalesReportService pddSalesReportService;
    @Autowired
    private TodoService todoService;
    @Autowired
    private GoodsSalesAnalyseService goodsReportService;
   

    @RequestMapping(value = "/pdd/shop_list", method = RequestMethod.GET)
    public String shopDashboardPDD(Model model, HttpServletRequest request) {
        HttpSession session = request.getSession();
        Integer userId = DataConfigObject.getInstance().getLoginUserId();


        //有权限的店铺列表
        List<DcShopEntity> shops = shopService.getShopListByUserId(userId,5);//type=5是拼多多
        model.addAttribute("shops", shops);

        var permissionKey = "pddshop";

        var mp = manageUserService.getEntityByKey(permissionKey);

        model.addAttribute("view", permissionKey);
        model.addAttribute("pView", mp.getParentKey());

        return "eshop/shop_list";
    }




    /**
     * 店铺首页
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/pdd/dashboard", method = RequestMethod.GET)
    public String dashboard(Model model, HttpServletRequest request) {
        Integer userId = DataConfigObject.getInstance().getLoginUserId();
        //有权限的店铺列表
        List<DcShopEntity> shops = shopService.getShopListByUserId(userId,5);//type=5是拼多多
        model.addAttribute("shops", shops);
     
        Integer shopId = 5;
        if(StringUtils.hasText(request.getParameter("shopId"))){
            shopId = Integer.parseInt(request.getParameter("shopId")) ;
        }
        var shop = shopService.getShop(shopId);
        model.addAttribute("shop", shop);
        model.addAttribute("shopId", shopId);


        var permissionKey = "pddshop";
        var mp = manageUserService.getEntityByKey(permissionKey);
        model.addAttribute("view", permissionKey);
        model.addAttribute("pView", mp.getParentKey());

        model.addAttribute("today", DateUtil.dateToString(new Date(),"yyyy-MM-dd"));

        Integer waitSendOrderStatus = 1;//待发货订单状态
        model.addAttribute("waitSendOrderStatus",waitSendOrderStatus);

        //店铺统计信息
        var statistics = shopService.shopOrderStatisticsPDD(shopId);

        if(statistics!=null)
            model.addAttribute("report", statistics);
        else  model.addAttribute("report",new ShopOrderStatisticsVo());

        //代办数据
        // var todoList = todoService.getList(1, 50, null, null, null,null);

        var saleReport = goodsReportService.getGoodsSalesReportPdd(shopId);
        model.addAttribute("saleReport", saleReport);
        // model.addAttribute("todoList", todoList.getList());
        //商品销售退货排行
        var salesAndRefund = pddSalesReportService.getSalesAndRefundReport(shopId);
        model.addAttribute("salesAndRefund", salesAndRefund);


        return "pdd/dashboard";
    }


    

    


    
    
}



