package com.b2c.erp.controller.tao;

import jakarta.servlet.http.HttpServletRequest;

import com.b2c.erp.controller.CommonControllerUtils;
import com.b2c.interfaces.WmsUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.b2c.entity.api.ApiResult;
import com.b2c.common.utils.DateUtil;
import com.b2c.entity.DataRow;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.erp.DataConfigObject;
import com.b2c.interfaces.tao.TaokeywordViewService;

@RequestMapping("/tao")
@Controller
public class TaoGoodsTrafficController {
    @Autowired
    private TaokeywordViewService service;
    @Autowired
    private WmsUserService manageUserService;
    @RequestMapping(value = "/goods_traffic_list", method = RequestMethod.GET)
    public String dashboard(Model model,  HttpServletRequest request) {
        Integer shopId =6;

        if(StringUtils.hasText(request.getParameter("shopId")))
        {
            try {
                shopId = Integer.parseInt(request.getParameter("shopId"));
            } catch (Exception e) {
            }
        }
        model.addAttribute("shopId", shopId);

        String keyword = null;
        if (StringUtils.hasText(request.getParameter("keyword"))) {
            keyword = request.getParameter("keyword");
            model.addAttribute("keyword", keyword);
        }
        Long goodsId = null;
        if (StringUtils.hasText(request.getParameter("goodsId"))) {
            goodsId = Long.parseLong(request.getParameter("goodsId"));
            model.addAttribute("goodsId", goodsId);
        }

        String date = null;
        if (StringUtils.hasText(request.getParameter("date"))) {
            date = request.getParameter("date");
            model.addAttribute("date", date);
        }
        
        Integer pageIndex = 1, pageSize = DataConfigObject.getInstance().getPageSize();
        if (!StringUtils.isEmpty(request.getParameter("page"))) {
            pageIndex = Integer.parseInt(request.getParameter("page"));
        }

        var result = service.getList(shopId,pageIndex,pageSize,goodsId,keyword,date);
        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("totalSize", result.getTotalSize());
        model.addAttribute("lists", result.getList());
        
//        model.addAttribute("view", "taoshop");
//        model.addAttribute("pView", "sale");
        CommonControllerUtils.setViewKey(model,manageUserService,"taoshop");
        return "tao/goods_traffic_list";
    }

    
    @RequestMapping(value = "/goods_traffic_add", method = RequestMethod.GET)
    public String add(Model model, HttpServletRequest request) {
        Integer shopId =6;

        if(StringUtils.hasText(request.getParameter("shopId")))
        {
            try {
                shopId = Integer.parseInt(request.getParameter("shopId"));
            } catch (Exception e) {
            }
        }
        model.addAttribute("shopId", shopId);
        model.addAttribute("date", DateUtil.getCurrentDate());

        return "tao/goods_traffic_add";
    }

    @ResponseBody
    @RequestMapping(value = "/goods_traffic_add_ajax", method = RequestMethod.POST)
    public  ApiResult<String> addPostAjax(Model model, @RequestBody DataRow data, HttpServletRequest request) {
        String source = data.getString("source");
        Integer shopId = data.getInt("shopId");
        String keyword = data.getString("keyword");
        Long goodsId = data.getLong("goodsId");
        Integer views = data.getInt("views");
        String date = data.getString("date");

        service.addKeyword(shopId, keyword, source, goodsId, views, date);
    
        return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(), "SUCCESS");
    }
}
