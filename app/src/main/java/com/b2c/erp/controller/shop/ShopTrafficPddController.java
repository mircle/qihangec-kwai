package com.b2c.erp.controller.shop;

import java.math.BigDecimal;
import java.util.List;

import jakarta.servlet.http.HttpServletRequest;

import com.b2c.erp.controller.CommonControllerUtils;
import com.b2c.interfaces.WmsUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.b2c.entity.api.ApiResult;
import com.b2c.common.utils.DateUtil;
import com.b2c.entity.DataRow;
import com.b2c.entity.datacenter.DcShopEntity;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.entity.shop.ShopTrafficEntity;
import com.b2c.entity.shop.ShopTrafficGoodsEntity;
import com.b2c.erp.DataConfigObject;
import com.b2c.interfaces.ShopService;
import com.b2c.interfaces.ShopTrafficGoodsService;

@RequestMapping("/shop")
@Controller
public class ShopTrafficPddController {
    @Autowired
    private ShopService shopService;
    @Autowired
    private ShopTrafficGoodsService shopTrafficGoodsService;
    @Autowired
    private WmsUserService manageUserService;
    Logger log = LoggerFactory.getLogger(ShopTrafficPddController.class);
    
    /**
     * 
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/goods_traffic_pdd", method = RequestMethod.GET)
    public String listPDD(Model model, HttpServletRequest request) {
        Integer shopType = 5;
        // if(StringUtils.hasText(request.getParameter("shopType"))){
        //     try {
        //         shopType = Integer.parseInt(request.getParameter("shopType"));
        //         model.addAttribute("shopType", shopType);
        //     } catch (Exception e) {
        //     }
        // }
        Integer pageIndex = 1, pageSize = DataConfigObject.getInstance().getPageSize();
        if (!StringUtils.isEmpty(request.getParameter("page"))) {
            pageIndex = Integer.parseInt(request.getParameter("page"));
        }
        // 店铺列表
        List<DcShopEntity> shops = shopService.getShopListByUserId(null, shopType);
        model.addAttribute("shops", shops);

        String goodsNum = null;
        if(StringUtils.hasText(request.getParameter("goodsNum"))){
            goodsNum = request.getParameter("goodsNum");
            model.addAttribute("goodsNum", goodsNum);
        }
        Integer shopId=5;
        if (!StringUtils.isEmpty(request.getParameter("shopId"))){
            shopId = Integer.parseInt(request.getParameter("shopId")); 
        }
        model.addAttribute("shopId",shopId);
        String date = shopTrafficGoodsService.getLastDay(5,shopId);
        if(StringUtils.hasText(date)==false) date=DateUtil.customizeDate_(1);

        if(StringUtils.hasText(request.getParameter("date"))){
            date = request.getParameter("date");
        }else{
//            if(StringUtils.hasText(goodsNum)==false){
//                date = DateUtil.customizeDate_(1);
//            }
        }
        model.addAttribute("date", date);

        var result = shopTrafficGoodsService.getGoodsTrafficList(pageIndex, 100, shopType, shopId, goodsNum,date);

        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("totalSize", result.getTotalSize());
        model.addAttribute("lists", result.getList());

//        model.addAttribute("view", "pddgoodstraffic");pddtraffic
//        model.addAttribute("pView", "pdd");
        CommonControllerUtils.setViewKey(model,manageUserService,"pddshop");
        return "eshop/shop_goods_traffic_pdd";
    }


    @RequestMapping(value = "/goods_traffic_pdd_add", method = RequestMethod.GET)
    public String addPDD(Model model, HttpServletRequest request) {
        Integer shopType = 5;
        // 店铺列表
        List<DcShopEntity> shops = shopService.getShopListByUserId(null, shopType);
        model.addAttribute("shops", shops);
        model.addAttribute("shopId", 5);
        model.addAttribute("goodsNum",request.getParameter("goodsNum"));


        String date = shopTrafficGoodsService.getLastDay(5,5);
        if(StringUtils.hasText(date)==false) date=DateUtil.customizeDate_(1);
        if(StringUtils.hasText(request.getParameter("date"))){
            date = request.getParameter("date");
        }
        model.addAttribute("date", date);

        return "eshop/shop_goods_traffic_pdd_add";
    }

    @RequestMapping(value = "/shop_traffic_pdd", method = RequestMethod.GET)
    public String shopTrafficPdd(Model model, HttpServletRequest request) {
        Integer shopType = 5;
      
        Integer pageIndex = 1, pageSize = 30;//DataConfigObject.getInstance().getPageSize();
        if (!StringUtils.isEmpty(request.getParameter("page"))) {
            pageIndex = Integer.parseInt(request.getParameter("page"));
        }
        // 店铺列表
        List<DcShopEntity> shops = shopService.getShopListByUserId(null, shopType);
        model.addAttribute("shops", shops);


        Integer shopId=5;
        if (!StringUtils.isEmpty(request.getParameter("shopId"))){
            shopId = Integer.parseInt(request.getParameter("shopId")); 
        }
        model.addAttribute("shopId",shopId);
        String date =null;
        if(StringUtils.hasText(request.getParameter("date"))){
            date = request.getParameter("date");
            model.addAttribute("date", date);
        }
        

        var result = shopTrafficGoodsService.getShopTrafficList(pageIndex, pageSize, shopType, shopId,date);

        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("totalSize", result.getTotalSize());
        model.addAttribute("lists", result.getList());

//        model.addAttribute("view", "pddtraffic");
//        model.addAttribute("pView", "pdd");
        CommonControllerUtils.setViewKey(model,manageUserService,"pddshop");
        return "eshop/shop_traffic_pdd";
    }

    @RequestMapping(value = "/shop_traffic_pdd_add", method = RequestMethod.GET)
    public String shopTrafficAddPdd(Model model, HttpServletRequest request) {
        Integer shopType = 5;
        // 店铺列表
        List<DcShopEntity> shops = shopService.getShopListByUserId(null, shopType);
        model.addAttribute("shops", shops);
        model.addAttribute("shopId", 5);
        String date = DateUtil.customizeDate_(1);
        // model.addAttribute("date", date);
        if(StringUtils.hasText( request.getParameter("id"))){
            Long id = Long.parseLong(request.getParameter("id"));
            var e = shopTrafficGoodsService.getById(id);
            model.addAttribute("model",e);
        }

        return "eshop/shop_traffic_pdd_add";
    }

    @ResponseBody
    @RequestMapping(value = "/shop_traffic_pdd_add_ajax", method = RequestMethod.POST)
    public  ApiResult<String> addPostAjax(Model model, @RequestBody DataRow data, HttpServletRequest request) {

        ShopTrafficEntity entity = new ShopTrafficEntity();
        Integer shopId = data.getInt("shopId");
        var shop = shopService.getShop(shopId);
        entity.setShopId(shopId);
        entity.setShopType(shop.getType());
        Integer visits = data.getInt("visits");
        entity.setVisits(visits);

        Integer views = data.getInt("views");
        entity.setViews(views);
        Integer orderUsers = data.getInt("orderUsers");
        entity.setOrderUsers(orderUsers);
        Integer orders = data.getInt("orders");
        entity.setOrders(orders);
        Double CVR = data.getDouble("CVR");
        entity.setCVR(new BigDecimal(CVR));
        Double orderAmount = data.getDouble("orderAmount");
        entity.setOrderAmount(new BigDecimal(orderAmount));
        Integer collects = data.getInt("collects");
        entity.setCollects(collects);
        Integer goods = data.getInt("goods");
        entity.setGoods(goods);

        Double ATV = data.getDouble("ATV");
        if(ATV.doubleValue() ==0.0 && orderUsers.intValue()>0){
            ATV = orderAmount / orderUsers;
        }
        entity.setATV(new BigDecimal(ATV));


        Integer FSNew = data.getInt("FSNew");
        entity.setFSNew(FSNew);
        Double orderLKR = data.getDouble("orderLKR");
        entity.setOrderLKR(new BigDecimal(orderLKR));
        Double UVV = data.getDouble("UVV");
        entity.setUVV(new BigDecimal(UVV));
        Double SH = data.getDouble("SH");
        entity.setSH(new BigDecimal(SH));;
        Double SS = data.getDouble("SS");
        entity.setSS(new BigDecimal(SS));

        Double ZBSC = data.getDouble("ZBSC");
        entity.setZBSC(new BigDecimal(ZBSC));
        Integer ZBViews = data.getInt("ZBViews");
        entity.setZBViews(ZBViews);
        Double ZBRJGK = data.getDouble("ZBRJGK");
        entity.setZBRJGK(new BigDecimal(ZBRJGK));
        Integer ZBOrders = data.getInt("ZBOrders");
        entity.setZBOrders(ZBOrders);
        Double ZBOrderAmount = data.getDouble("ZBOrderAmount");
        entity.setZBOrderAmount(new BigDecimal(ZBOrderAmount));
        Double ZBCVR = data.getDouble("ZBCVR");
        entity.setZBCVR(new BigDecimal(ZBCVR));

        

        Integer FSOrders = data.getInt("FSOrders");
        entity.setFSOrders(FSOrders);
        Double FSROrder = data.getDouble("FSROrder");
        entity.setFSROrder(new BigDecimal(FSROrder));
        Double FSOrderAmount = data.getDouble("FSOrderAmount");
        entity.setFSOrderAmount(new BigDecimal(FSOrderAmount));
        Double FSFGL = data.getDouble("FSFGL");
        entity.setFSFGL(new BigDecimal(FSFGL));

        Integer showsQZ = data.getInt("showsQZ");
        entity.setShowsQZ(showsQZ);
        Integer visitsQZ = data.getInt("visitsQZ");
        entity.setVisitsQZ(visitsQZ);
        Integer ordersQZ = data.getInt("ordersQZ");
        entity.setOrdersQZ(ordersQZ);
        Double roiQZ = data.getDouble("roiQZ");
        entity.setRoiQZ(new BigDecimal(roiQZ));
        Double feeQZ = data.getDouble("feeQZ");
        entity.setFeeQZ(new BigDecimal(feeQZ));

        Integer showsSS = data.getInt("showsSS");
        entity.setShowsSS(showsSS);
        Integer visitsSS = data.getInt("visitsSS");
        entity.setVisitsSS(visitsSS);
        Integer ordersSS = data.getInt("ordersSS");
        entity.setOrdersSS(ordersSS);
        Double roiSS = data.getDouble("roiSS");
        entity.setRoiSS(new BigDecimal(roiSS));
        Double feeSS = data.getDouble("feeSS");
        entity.setFeeSS(new BigDecimal(feeSS));
        Integer collectsSS = data.getInt("collectsSS");
        entity.setCollectsSS(collectsSS);

        Integer showsCJ = data.getInt("showsCJ");
        entity.setShowsCJ(showsCJ);
        Integer visitsCJ = data.getInt("visitsCJ");
        entity.setVisitsCJ(visitsCJ);
        Integer ordersCJ = data.getInt("ordersCJ");
        entity.setOrdersCJ(ordersCJ);
        Double roiCJ = data.getDouble("roiCJ");
        entity.setRoiCJ(new BigDecimal(roiCJ));
        Double feeCJ = data.getDouble("feeCJ");
        entity.setFeeCJ(new BigDecimal(feeCJ));
        Integer collectsCJ = data.getInt("collectsCJ");
        entity.setCollectsCJ(collectsCJ);

        Double dsr = data.getDouble("dsr");
        entity.setDsr(new BigDecimal(dsr));
        
        String date = data.getString("date");
        entity.setDate(date);
        String remark = data.getString("remark");
        entity.setRemark(remark);
        Long id = data.getLong("id");
        entity.setId(id);
        if(id <= 0){
            log.info("新增 网店流量");
            var result = shopTrafficGoodsService.addShopTrafficForPdd(entity);
            return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(), "SUCCESS");
        }else{
            log.info("修改 网店流量"+id);
            var result = shopTrafficGoodsService.editShopTrafficForPdd(entity);
            return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(), "SUCCESS");
        }
    }

    /**
     * pdd商品流量添加AJAX
     * @param model
     * @param data
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/goods_traffic_pdd_add_ajax", method = RequestMethod.POST)
    public  ApiResult<String> goodsTrafficAddPostAjax(Model model, @RequestBody DataRow data, HttpServletRequest request) {
       
        Integer shopId = data.getInt("shopId");
        var shop = shopService.getShop(shopId);

        String goodsId = data.getString("goodsId");

        // String goodsNumber = data.getString("goodsNumber");
        Integer shows = data.getInt("shows");
        Integer paidShows = data.getInt("paidShows");
        Integer visits = data.getInt("visits");
        Integer paidVisits = data.getInt("paidVisits");
        Integer views = data.getInt("views");
        Integer orders = data.getInt("orders");
        Integer paidOrders = data.getInt("paidOrders");
        Integer collects = data.getInt("collects");
        Integer paidCollects = data.getInt("paidCollects");
        Integer carts = data.getInt("carts");
        Integer paidCarts = data.getInt("paidCarts");
        Integer chats = data.getInt("chats");
        Integer paidChats = data.getInt("paidChats");
        Integer orderUsers = data.getInt("orderUsers");
        String date = data.getString("date");
        String remark = data.getString("remark");
        ShopTrafficGoodsEntity entity = new ShopTrafficGoodsEntity();
        entity.setShopId(shopId);
        entity.setShopType(shop.getType());
        entity.setGoodsId(goodsId);
        // entity.setGoodsNumber(goodsNumber);
        entity.setShows(shows);
        entity.setPaidShows(paidShows);
        entity.setVisits(visits);
        entity.setPaidVisits(paidVisits);
        entity.setViews(views);
        entity.setOrders(orders);
        entity.setPaidOrders(paidOrders);
        entity.setCollects(collects);
        entity.setPaidCollects(paidCollects);
        entity.setCarts(carts);
        entity.setPaidCarts(paidCarts);
        entity.setChats(chats);
        entity.setPaidChats(paidChats);
        entity.setDate(date);
        entity.setRemark(remark);
        entity.setOrderUsers(orderUsers);
        Double orderAmount = data.getDouble("orderAmount");
        Float CVR = data.getFloat("CVR");
        entity.setOrderAmount(orderAmount);
        entity.setCVR(CVR);
        String cvrTag = data.getString("cvrTag");
        entity.setCvrTag(cvrTag);
        //场景、搜索、全站

        Integer paidShowCJ  = data.getInt("paidShowCJ");
        entity.setPaidShowCJ(paidShowCJ);
        Integer paidVisitCJ= data.getInt("paidVisitCJ");
        entity.setPaidVisitCJ(paidVisitCJ);
        Integer paidOrderCJ= data.getInt("paidOrderCJ");
        entity.setPaidOrderCJ(paidOrderCJ);
        Integer paidShowSS= data.getInt("paidShowSS");
        entity.setPaidShowSS(paidShowSS);
        Integer paidVisitSS= data.getInt("paidVisitSS");
        entity.setPaidVisitSS(paidVisitSS);
        Integer paidOrderSS= data.getInt("paidOrderSS");
        entity.setPaidOrderSS(paidOrderSS);
        Integer paidShowQZ= data.getInt("paidShowQZ");
        entity.setPaidShowQZ(paidShowQZ);
        Integer paidVisitQZ= data.getInt("paidVisitQZ");
        entity.setPaidVisitQZ(paidVisitQZ);
        Integer paidOrderQZ= data.getInt("paidOrderQZ");
        entity.setPaidOrderQZ(paidOrderQZ);


        // service.addKeyword(shopId, keyword, source, goodsId, views, date);
        var result = shopTrafficGoodsService.addShopGoodsTrafficForPdd(entity);
        return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(), "SUCCESS");
    }

}
