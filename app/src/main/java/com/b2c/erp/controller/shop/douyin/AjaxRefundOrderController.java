package com.b2c.erp.controller.shop.douyin;

import jakarta.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.b2c.entity.api.ApiResult;
import com.b2c.entity.DataRow;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.interfaces.dou.DouyinOrderService;

@RequestMapping(value = "/ajax_douyin")
@RestController
public class AjaxRefundOrderController {
    @Autowired
    private DouyinOrderService orderService;
    
    /***
     * 订单手动选择快递单
     * @return
     */
    @RequestMapping(value = "/order_refund_apply", method = RequestMethod.POST)
    public ApiResult<Integer> getOrderSend(@RequestBody DataRow data , HttpServletRequest request) {
        if(StringUtils.isEmpty(data.getString("itemId")))return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "参数错误，缺少orderItemId");
        if(StringUtils.isEmpty(data.getString("refundId")))return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "参数错误，缺少refundId");
        Long orderItemId = data.getLong("itemId");
        Long refundId = data.getLong("refundId");
        // return new ApiResult<>(ressult.getCode(), ressult.getMsg());
        var result = orderService.handAddRefund(orderItemId,refundId);
        // return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(),"");
        return new ApiResult<>(result.getCode(), result.getMsg());
    }

        /***
     * 订单手动选择快递单
     * @return
     */
    @RequestMapping(value = "/edit_refund_logistics_code", method = RequestMethod.POST)
    public ApiResult<Integer> editRefundLogisticsCode(@RequestBody DataRow data , HttpServletRequest request) {
        if(StringUtils.isEmpty(data.getString("companyCode")))return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "请选择快递公司");
        if(StringUtils.isEmpty(data.getString("code")))return new ApiResult<>(EnumResultVo.ParamsError.getIndex(), "请输入快递单号");
        String logisticsTime = data.getString("logisticsTime");

        // var ressult = orderService.orderHandExpress(data.getInt("id"),data.getString("company"),data.getString("companyCode"),data.getString("code"));

        
        // return new ApiResult<>(ressult.getCode(), ressult.getMsg());
        orderService.editRefundLogisticsCode(data.getLong("id"),data.getString("company"),data.getString("code"),logisticsTime);
        return new ApiResult<>(EnumResultVo.SUCCESS.getIndex(),"");
    }
}
