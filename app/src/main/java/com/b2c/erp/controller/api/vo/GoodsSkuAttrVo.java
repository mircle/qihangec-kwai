package com.b2c.erp.controller.api.vo;

import lombok.Data;

import java.util.List;

@Data
public class GoodsSkuAttrVo {
    private String k;//: '颜色', // skuKeyName：规格类目名称
    private String k_s;//: 's1', // skuKeyStr：sku 组合列表（下方 list）中当前类目对应的 key 值，value 值会是从属于当前类目的一个规格值 id
    private boolean largeImageMode;//: true, //  是否展示大图模式
    private List<GoodsSkuAttrValueVo> v;//
}
