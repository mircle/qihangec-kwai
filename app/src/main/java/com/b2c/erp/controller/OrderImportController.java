package com.b2c.erp.controller;

import jakarta.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
* 描述：
* 订单Controller
*
* @author qlp
* @date 2022-07-30 12:47
*/
@RequestMapping("/order")
@Controller
public class OrderImportController {

    @RequestMapping(value = "/excel_import", method = RequestMethod.GET)
    public String shopListTAO(Model model, HttpServletRequest request) {


        model.addAttribute("view", "excel_import");
        model.addAttribute("pView", "sale");

        return "order/excel_import";
    }

}