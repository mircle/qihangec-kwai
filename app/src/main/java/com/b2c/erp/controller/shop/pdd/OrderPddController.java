package com.b2c.erp.controller.shop.pdd;


import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.api.ApiResult;
import com.b2c.entity.api.ApiResultEnum;
import com.b2c.common.utils.DateUtil;
import com.b2c.entity.datacenter.DcShopEntity;
import com.b2c.entity.datacenter.DcTmallOrderEntity;
import com.b2c.entity.erp.vo.ErpGoodsSpecListVo;
import com.b2c.entity.pdd.OrderPddEntity;
import com.b2c.entity.pdd.OrderPddItemEntity;
import com.b2c.entity.result.ResultVo;
import com.b2c.erp.DataConfigObject;
import com.b2c.erp.controller.CommonControllerUtils;
import com.b2c.erp.controller.order.erp.OrderConfirmReq;
import com.b2c.interfaces.WmsUserService;
import com.b2c.repository.utils.OrderNumberUtils;
import com.b2c.interfaces.erp.ErpGoodsService;
import com.b2c.interfaces.pdd.OrderPddService;
import com.b2c.interfaces.ExpressCompanyService;
import com.b2c.interfaces.ShopService;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import jakarta.servlet.http.HttpServletRequest;
 import jakarta.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-11-13 14:44
 */
@RequestMapping("/pdd")
@Controller
public class OrderPddController {
    @Autowired
    private ShopService shopService;
    @Autowired
    private OrderPddService orderPddService;
    @Autowired
    private ErpGoodsService erpGoodsService;
    @Autowired
    private WmsUserService manageUserService;
    private static Logger log = LoggerFactory.getLogger(OrderPddController.class);
    @Autowired
   private ExpressCompanyService expressCompanyService;
    /**
     * 订单列表
     * @param model
     * @param shopId
     * @param request
     * @return
     */
    @RequestMapping("/order_list")
    public String orderList(Model model, HttpServletRequest request){

        Integer userId = DataConfigObject.getInstance().getLoginUserId();
        //有权限的店铺列表
        List<DcShopEntity> shops = shopService.getShopListByUserId(userId,5);//type=5是拼多多
        model.addAttribute("shops", shops);

        
        // //查询店铺信息
        // var shop = shopService.getShop(shopId);
        // model.addAttribute("shop",shop);

        Integer shopId=5;
        if (!StringUtils.isEmpty(request.getParameter("shopId"))){
            shopId = Integer.parseInt(request.getParameter("shopId")); 
        }
        model.addAttribute("shopId",shopId);

        String orderNum="";
        if (!StringUtils.isEmpty(request.getParameter("orderNum")))
        {orderNum = request.getParameter("orderNum"); model.addAttribute("orderNum",orderNum);}

        String goodsSpecNum="";
        if (!StringUtils.isEmpty(request.getParameter("goodsSpecNum"))) {goodsSpecNum = request.getParameter("goodsSpecNum"); model.addAttribute("goodsSpecNum",goodsSpecNum);}
        String trackingNumber="";
        if (!StringUtils.isEmpty(request.getParameter("trackingNumber"))) {trackingNumber = request.getParameter("trackingNumber"); model.addAttribute("trackingNumber",trackingNumber);}


        Integer status = null;
        if (!StringUtils.isEmpty(request.getParameter("status"))) {
            try {
                status = Integer.parseInt(request.getParameter("status"));
            }catch (Exception e){}
        }
        // if(status == null ) status=1;
        model.addAttribute("status",status);
        Integer refundStatus = null;
        if (!StringUtils.isEmpty(request.getParameter("refundStatus"))) {
            try {
                refundStatus = Integer.parseInt(request.getParameter("refundStatus"));
                model.addAttribute("refundStatus",refundStatus);
            }catch (Exception e){}
        }
        // if(status!=null ){
        //     if(refundStatus == null && status.intValue() == 1){
        //         refundStatus = 1;
        //         model.addAttribute("refundStatus",refundStatus);
        //     }
        //     if(refundStatus == null && status.intValue() == 2){
        //         refundStatus =1 ;
        //         model.addAttribute("refundStatus",refundStatus);
        //     }
        // }

        Integer orderBy = 2;
        if (!StringUtils.isEmpty(request.getParameter("orderBy"))) {
            try {
                orderBy = Integer.parseInt(request.getParameter("orderBy"));

            }catch (Exception e){}
        }
        model.addAttribute("orderBy",orderBy);
        Integer startTime = null;
        if (!StringUtils.isEmpty(request.getParameter("startTime"))){
//            startTime = DateUtil.dateToStamp(request.getParameter("startTime")) ;
            startTime = DateUtil.dateTimeToStamp(request.getParameter("startTime")) ;
            model.addAttribute("startTime",request.getParameter("startTime"));
        }

        Integer endTime=null;
        if (!StringUtils.isEmpty(request.getParameter("endTime"))) {
            endTime = DateUtil.dateTimeToStamp(request.getParameter("endTime") );//+ " 23:59:59"
            model.addAttribute("endTime",request.getParameter("endTime"));
        }

        Integer pageIndex = 1, pageSize = DataConfigObject.getInstance().getPageSize();
        if (!StringUtils.isEmpty(request.getParameter("page"))) {
            pageIndex = Integer.parseInt(request.getParameter("page"));
        }

        //查询订单
        var result = orderPddService.getOrderViewList(pageIndex,pageSize,orderNum,goodsSpecNum,status,refundStatus,shopId,startTime,endTime,null,orderBy,trackingNumber);
//        var result = orderPddService.getOrderListAndItem(pageIndex, pageSize, orderNum, status, shopId, null, startTime, endTime,null);
        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("totalSize", result.getTotalSize());
        model.addAttribute("lists", result.getList());

        model.addAttribute("menuId","pddorder");
        CommonControllerUtils.setViewKey(model,manageUserService,"pddorder");
//        model.addAttribute("view", "pddorder");
//        model.addAttribute("pView", "pdd");
        model.addAttribute("company", expressCompanyService.getExpressCompany());
        return "order/pdd/order_list_pdd";
    }

    @ResponseBody
    @RequestMapping(value = "/order_list_export", method = RequestMethod.GET)
    public void purchasePutDetail(Model model, HttpServletRequest request, HttpServletResponse response) {
        Integer shopId=null;
        if (!StringUtils.isEmpty(request.getParameter("shopId"))){
            shopId = Integer.parseInt(request.getParameter("shopId"));
        }

        String orderNum="";
        if (!StringUtils.isEmpty(request.getParameter("orderNum")))
        {orderNum = request.getParameter("orderNum"); }

        String goodsSpecNum="";
        if (!StringUtils.isEmpty(request.getParameter("goodsSpecNum"))) {goodsSpecNum = request.getParameter("goodsSpecNum");}
        String trackingNumber="";
        if (!StringUtils.isEmpty(request.getParameter("trackingNumber"))) {trackingNumber = request.getParameter("trackingNumber");}


        Integer status = null;
        if (!StringUtils.isEmpty(request.getParameter("status"))) {
            try {
                status = Integer.parseInt(request.getParameter("status"));
            }catch (Exception e){}
        }

        Integer refundStatus = null;
        if (!StringUtils.isEmpty(request.getParameter("refundStatus"))) {
            try {
                refundStatus = Integer.parseInt(request.getParameter("refundStatus"));
                model.addAttribute("refundStatus",refundStatus);
            }catch (Exception e){}
        }
        if(status!=null && status.intValue() == 1){
            if(refundStatus == null){
                refundStatus = 1;
                model.addAttribute("refundStatus",refundStatus);
            }
        }

        Integer startTime = null;
        if (!StringUtils.isEmpty(request.getParameter("startTime"))){
            startTime = DateUtil.dateTimeToStamp(request.getParameter("startTime")) ;
        }

        Integer endTime=null;
        if (!StringUtils.isEmpty(request.getParameter("endTime"))) {
            endTime = DateUtil.dateTimeToStamp(request.getParameter("endTime") );
        }

 
        //查询订单
        var result = orderPddService.getOrderViewList(1,1000,orderNum,goodsSpecNum,status,refundStatus,shopId,startTime,endTime,null,2,trackingNumber);
        var lists = result.getList();

        /***************根据店铺查询订单导出的信息*****************/
        String fileName = "pdd_order_list_";//excel文件名前缀
        //创建Excel工作薄对象
        HSSFWorkbook workbook = new HSSFWorkbook();

        //创建Excel工作表对象
        HSSFSheet sheet = workbook.createSheet();
        HSSFRow row = null;
        HSSFCell cell = null;
        //第一行为空
        row = sheet.createRow(0);
        cell = row.createCell(0);
        cell.setCellValue("");
        cell = row.createCell(0);
        cell.setCellValue("订单编号");
        cell = row.createCell(1);
        cell.setCellValue("商品标题");
        cell = row.createCell(2);
        cell.setCellValue("规格");
        cell = row.createCell(3);
        cell.setCellValue("数量");
        cell = row.createCell(4);
        cell.setCellValue("收货人姓名");
        cell = row.createCell(5);
        cell.setCellValue("收货人电话");
        cell = row.createCell(6);
        cell.setCellValue("收货人地址");
        cell = row.createCell(7);
        cell.setCellValue("物流单号");
        cell = row.createCell(8);
        cell.setCellValue("物流公司");
        cell = row.createCell(9);
        cell.setCellValue("发货时间");


        int currRowNum = 0;
        HSSFPatriarch patriarch = sheet.createDrawingPatriarch();
        // 循环写入数据
        for (int i = 0; i < lists.size(); i++) {
            currRowNum++;
            //写入订单
            var order = lists.get(i);
            //创建行
            row = sheet.createRow(currRowNum);
            cell = row.createCell(0);
            cell.setCellValue(order.getOrderSn().toString());
            //商品名称
            cell = row.createCell(1);
            String goodsName= order.getGoodsNickName();
            String spec=order.getGoodsSpec();
            String num = order.getQuantity().toString();
            
            cell.setCellValue(goodsName);
            cell = row.createCell(2);
            cell.setCellValue(spec);
            cell = row.createCell(3);
            cell.setCellValue(num);
            //收货人
            cell = row.createCell(4);
            cell.setCellValue(order.getReceiverName1());
            cell = row.createCell(5);
            cell.setCellValue(order.getReceiverPhone1());
            cell = row.createCell(6);
            cell.setCellValue(order.getReceiverAddress1());

        }
        response.reset();
        response.setContentType("application/msexcel;charset=UTF-8");
        try {
            SimpleDateFormat newsdf = new SimpleDateFormat("yyyyMMdd");
            String date = newsdf.format(new Date());
            response.addHeader("Content-Disposition", "attachment;filename=\""
                    + new String((fileName + date + ".xls").getBytes("GBK"),
                    "ISO8859_1") + "\"");
            OutputStream out = response.getOutputStream();
            workbook.write(out);
            out.flush();
            out.close();
        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(null, "导出失败!");
            e.printStackTrace();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "导出失败!");
            e.printStackTrace();
        }
    }


    /**
     * 订单详情
     *
     * @param model
     * @param id
     * @param shopId
     * @param request
     * @return
     */
    @RequestMapping(value = "/orderDetail", method = RequestMethod.GET)
    public String orderDetailTmall(Model model, @RequestParam Long id, HttpServletRequest request) {

        OrderPddEntity orderDetail = orderPddService.getOrderDetailAndItemsById(id);

        model.addAttribute("orderVo", orderDetail);

        //查询店铺信息
        var shop = shopService.getShop(orderDetail.getShopId());
        model.addAttribute("shop", shop);
        model.addAttribute("shopId", orderDetail.getShopId());
        model.addAttribute("menuId", "order_list");
        model.addAttribute("view", "pddshop");
        model.addAttribute("pView", "sale");
        return "order/pdd/order_detail_pdd";
    }

        /**
     * 添加礼品
     * @param model
     * @param orderId
     * @param request
     * @return
     */
    @RequestMapping(value = "/add_order_gift", method = RequestMethod.GET)
    public String addGift(Model model,@RequestParam Long orderId,@RequestParam Integer shopId,HttpServletRequest request) {
        model.addAttribute("orderId",orderId);
        model.addAttribute("shopId",shopId);

        String number = "";
        if (!StringUtils.isEmpty(request.getParameter("number"))) {
            number = request.getParameter("number");
            model.addAttribute("number",number);

            PagingResponse<ErpGoodsSpecListVo> result = new PagingResponse<ErpGoodsSpecListVo>(1, 10, 0, null);
            result = erpGoodsService.getSpecByGoodsNumber(1, 10, number);

            model.addAttribute("totalSize", result.getTotalSize());
            model.addAttribute("lists", result.getList());
        }




        return "pdd/order_gift_add";
    }

    /**
     * 确定订单
     * @param model
     * @param orderId
     * @param request
     * @return
     */
    @RequestMapping(value = "/order_confirm", method = RequestMethod.GET)
    public String orderConfirmGet(Model model, @RequestParam Long orderId,HttpServletRequest request) {
        var order = orderPddService.getOrderDetailAndItemsById(orderId);
        if (order == null) {
            model.addAttribute("error", "没有找到订单");
            model.addAttribute("orderVo", new DcTmallOrderEntity());

        } else {
            model.addAttribute("orderVo", order);
//            model.addAttribute("clientId", order.getClientUserId() != null ? order.getClientUserId() : 0);
        }


        return "pdd/order_confirm_pdd";
    }

    /**
     * 订单确认（进入仓库）
     *
     * @param req
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/order_confirm_post", method = RequestMethod.POST)
    public ApiResult<String> confirmOrder(@RequestBody OrderConfirmReq req, HttpServletRequest request) {
        if (req.getOrderId() == null || req.getOrderId() <= 0)
            return new ApiResult<>(ApiResultEnum.ParamsError.getIndex(), "参数错误，缺少orderId");
        if (StringUtils.isEmpty(req.getReceiver()))
            return new ApiResult<>(ApiResultEnum.ParamsError.getIndex(), "参数错误，缺少receiver");
        if (StringUtils.isEmpty(req.getMobile()))
            return new ApiResult<>(ApiResultEnum.ParamsError.getIndex(), "参数错误，缺少mobile");
        if (StringUtils.isEmpty(req.getAddress()))
            return new ApiResult<>(ApiResultEnum.ParamsError.getIndex(), "参数错误，缺少address");

        var order = orderPddService.getOrderDetailAndItemsById(req.getOrderId());

//        Integer shopId = order.getShopId();//拼多多shopId
//        String clientId = DataConfigObject.getInstance().getPddClientId();
//        String clientSecret = DataConfigObject.getInstance().getPddClientSecret();
//        var shop = shopService.getShop(shopId);
//        var settingEntity = thirdSettingService.getEntity(shop.getType());
//        PopClient client = new PopHttpClient(clientId, clientSecret);
//        PddOrderInformationGetRequest pddOrderRequest=new PddOrderInformationGetRequest();
//        pddOrderRequest.setOrderSn(order.getOrderSn());
//        Integer pddOrderStatus=1;
//        try {
//            PddOrderInformationGetResponse pddOrderResponse = client.syncInvoke(pddOrderRequest, settingEntity.getAccess_token());
//            if(!StringUtils.isEmpty(pddOrderResponse.getErrorResponse())){
//                return new ApiResult<>(ApiResultEnum.ParamsError.getIndex(),pddOrderResponse.getErrorResponse().getErrorMsg());
//            }
//            pddOrderStatus =  pddOrderResponse.getOrderInfoGetResponse().getOrderInfo().getOrderStatus();
//        }catch (Exception e){
//            return new ApiResult<>(ApiResultEnum.ParamsError.getIndex(), "拼多多接口查询订单状态异常");
//        }
        log.info("/**********************订单状态判断" + order.getOrder_status() + "**********************/");
        if (order == null)
            return new ApiResult<>(404, "订单不存在");//检查是否已经确认
        else if (order.getAuditStatus().intValue() > 0)
            return new ApiResult<>(501, "订单已经确认过了");
//        else if (pddOrderStatus != EnumPddOrderStatus.WaitSend.getIndex())
//            return new ApiResult<>(402, "订单不是发货中的状态，不能操作");
        // else if(order.getRefund_status() != 1){
        //     return new ApiResult<>(409, "订单售后中，不能操作");
        // }
        log.info("/**********************开始确认订单" + req.getOrderId() + "**********************/");
        synchronized (this) {
            //确认订单，加入到仓库系统待发货订单列表
            ResultVo<Integer> result = orderPddService.orderConfirmAndJoinDeliveryQueueForPdd(req.getOrderId(), req.getReceiver(), req.getMobile(), req.getAddress(), req.getSellerMemo());

            log.info("/**********************确认订单完成" + result + "**********************/");
            return new ApiResult<>(result.getCode(), result.getMsg());
        }
    }

    /**
     * 创建订单
     * @param model
     * @param shopId
     * @param request
     * @return
     */
    @RequestMapping("/order_create")
    public String orderCreate(Model model, HttpServletRequest request){
        model.addAttribute("orderNumber",OrderNumberUtils.getOrderIdByTime());
        model.addAttribute("menuId", "order_create");
        

        Integer userId = DataConfigObject.getInstance().getLoginUserId();
        //有权限的店铺列表
        List<DcShopEntity> shops = shopService.getShopListByUserId(userId,5);//type=5是拼多多
        model.addAttribute("shops", shops);
        
        Integer shopId = null;
        if (!StringUtils.isEmpty(request.getParameter("shopId"))) {
            try {
                shopId = Integer.parseInt(request.getParameter("shopId"));
            }catch (Exception e){}
        }
        model.addAttribute("shopId",shopId);

        model.addAttribute("view", "pddshop");
        model.addAttribute("pView", "sale");   
        return "order/pdd/order_create_pdd";
    }
    /**
     * pdd创建订单
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/order_create", method = RequestMethod.POST)
    public String postSystemOrder(Model model, HttpServletRequest request) {
        model.addAttribute("menuId", "order_create");
        Integer shopId =  Integer.parseInt(request.getParameter("shopId"));
        /***商品信息****/
        String[] specNumber = request.getParameterValues("specNumber");//规格编码组合
        String[] goodsNumber = request.getParameterValues("goodsNumber");//商品编码组合
        String[] goodsId = request.getParameterValues("goodsId");//商品id组合
        String[] specsId = request.getParameterValues("specId");//商品规格id组合
        String[] quantitys = request.getParameterValues("quantity");//数量组合
        String[] prices = request.getParameterValues("note");//商品价格

        String orderNumber = request.getParameter("orderNumber");
        //收件人信息
        String contactMobile = request.getParameter("contactMobile");
        String contactPerson = request.getParameter("contactPerson");
        String area = request.getParameter("area");
        String address = request.getParameter("address");
        Integer saleType = Integer.parseInt(request.getParameter("saleType"));
        String shippingFee= StringUtils.isEmpty(request.getParameter("shippingFee")) ? "0" :request.getParameter("shippingFee");
        String sellerMemo = request.getParameter("sellerMemo");

        String[] areaNameArray = area.split(" ");

        String provinceName = "";
        if (areaNameArray.length > 0) provinceName = areaNameArray[0];
        String cityName = "";
        if (areaNameArray.length > 1) cityName = areaNameArray[1];
        String districtName = "";
        if (areaNameArray.length > 2) districtName = areaNameArray[2];

        OrderPddEntity pddEntity = new OrderPddEntity();
        pddEntity.setShopId(shopId);

        List<OrderPddItemEntity> items = new ArrayList<>();
        double goodsTotalAmount = 0;//商品总价
        for (int i = 0,n=goodsId.length;i<n;i++) {
            if(StringUtils.isEmpty(goodsId[i]))continue;
            OrderPddItemEntity pddItem = new OrderPddItemEntity();
            Integer specId=Integer.parseInt(specsId[i]);
            BigDecimal price = new BigDecimal(prices[i]);
            Integer count =Integer.parseInt(quantitys[i]);
            var spec = erpGoodsService.getSpecBySpecId(specId);

            goodsTotalAmount +=  price.doubleValue() * count;

            pddItem.setErpGoodsId(spec.getGoodsId());
            pddItem.setErpGoodsSpecId(spec.getId());
            pddItem.setGoodsImg(spec.getColorImage());
            pddItem.setGoodsName(spec.getGoodTitle());
            pddItem.setGoodsNum(goodsNumber[i]);
            pddItem.setGoodsPrice(price.doubleValue());
            pddItem.setGoodsSpec(spec.getColorValue()+","+spec.getSizeValue());
            pddItem.setGoodsSpecNum(specNumber[i]);
            pddItem.setQuantity(count);
            pddItem.setIsGift(saleType.intValue()==2 ? 1 : 0);
            items.add(pddItem);
        }
        pddEntity.setItems(items);
        double orderTotalAmount=goodsTotalAmount+Double.valueOf(shippingFee);
        pddEntity.setAddress(new StringBuilder(provinceName).append(cityName).append(districtName).append(address).toString());
        pddEntity.setAfter_sales_status(0);
        pddEntity.setBuyer_memo("");
        pddEntity.setCapital_free_discount(0d);
        pddEntity.setCity(cityName);
        pddEntity.setConfirm_status(1);
        pddEntity.setConfirm_time("");
        pddEntity.setCountry("中国");
        pddEntity.setCreated_time(DateUtil.getCurrentDateTime());
        pddEntity.setDiscount_amount(0d);
        pddEntity.setFree_sf(0);
        pddEntity.setGoods_amount(goodsTotalAmount);
        pddEntity.setGroup_status(1);
        pddEntity.setIs_lucky_flag(saleType);
        pddEntity.setOrderSn(orderNumber);
        pddEntity.setOrder_status(1);
        pddEntity.setPay_amount(orderTotalAmount);
        pddEntity.setPay_no("");
        pddEntity.setPay_time("0");
        pddEntity.setPay_type("");
        pddEntity.setPlatform_discount(0d);
        pddEntity.setPostage(Double.valueOf(shippingFee));
        pddEntity.setProvince(provinceName);
        pddEntity.setReceive_time("");
        pddEntity.setReceiver_name(contactPerson);
        pddEntity.setReceiver_phone(contactMobile);
        pddEntity.setRefund_status(1);
        pddEntity.setRemark(sellerMemo);
        pddEntity.setSeller_discount(0d);
        pddEntity.setShipping_time("");
        pddEntity.setTown("");
        pddEntity.setTracking_number("");
        pddEntity.setTradeType(0);
        pddEntity.setUpdated_at("");
        pddEntity.setOrderConfirmTime(System.currentTimeMillis() / 1000);
        orderPddService.orderCreatePdd(pddEntity);
        return "redirect:/pdd/order_list?shopId="+shopId;
    }

    /**
     * 修改订单item商品
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/order_item_upd_goods", method = RequestMethod.GET)
    public String OrderItemLinkNewGoods(Model model,@RequestParam Long orderItemId,@RequestParam Integer shopId,HttpServletRequest request) {
         //查询订单item
         var orderItem = orderPddService.getOrderItemByItemId(orderItemId);
        
         // var orderItem = salesOrderService.getOrderItemByItemId(orderItemId);
         model.addAttribute("orderItemQty",orderItem.getQuantity());
         // model.addAttribute("orderItemQty",1);
         model.addAttribute("orderItemId",orderItemId);
         model.addAttribute("shopId",shopId);
         model.addAttribute("goodsSpecNum",orderItem.getGoodsSpecNum());
   

        /********查询商品********/
        String number = "";
        if (!StringUtils.isEmpty(request.getParameter("number"))) {
            number = request.getParameter("number");
            model.addAttribute("number",number);

            var result = erpGoodsService.getSpecByGoodsNumber(1, 50, number);

            model.addAttribute("totalSize", result.getTotalSize());
            model.addAttribute("lists", result.getList());
        }else{
            var result = erpGoodsService.getSpecByGoodsNumber(1, 50, orderItem.getGoodsNum());

            model.addAttribute("totalSize", result.getTotalSize());
            model.addAttribute("lists", result.getList()); 
        }

       


       


        return "pdd/erp_goods_sku_list";
    }
    
}
