package com.b2c.erp.controller;

import com.b2c.interfaces.WmsUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;


public class CommonControllerUtils {

    public static void setViewKey(Model model,WmsUserService manageUserService, String permissionKey){

        var mp = manageUserService.getEntityByKey(permissionKey);

        model.addAttribute("view", permissionKey);
        model.addAttribute("pView", mp.getParentKey());
    }
}
