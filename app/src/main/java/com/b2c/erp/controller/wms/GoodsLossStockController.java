package com.b2c.erp.controller.wms;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ErpGoodsStockInfoEntity;
import com.b2c.entity.ErpGoodsStockLossFormEntity;
import com.b2c.entity.erp.vo.ErpGoodsStockLossFormVo;
import com.b2c.entity.enums.EnumUserActionType;
import com.b2c.repository.utils.OrderNumberUtils;
import com.b2c.interfaces.erp.ErpUserActionLogService;
import com.b2c.interfaces.wms.StockBadService;
import com.b2c.erp.DataConfigObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jakarta.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 报损单
 */
@Controller
@RequestMapping("/goods_loss_stock")
public class GoodsLossStockController {
    @Autowired
    private StockBadService badService;
    @Autowired
    private ErpUserActionLogService logService;


    /**新增报损单列表*/
    @RequestMapping(value = "loss_add",method = RequestMethod.GET)
    public String getStockAdd(Model model,HttpServletRequest request){
        //生成单据编号
        String billNo = "LOSS" + OrderNumberUtils.getOrderIdByTime();
        model.addAttribute("billNo", billNo);
        model.addAttribute("time", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        if (!StringUtils.isEmpty(request.getParameter("404"))) model.addAttribute("bug","报损数量超过库存");
        if (!StringUtils.isEmpty(request.getParameter("405"))) model.addAttribute("bug","仓位不正确");

        String[] badList = new String[5];
        model.addAttribute("badList", badList);

        model.addAttribute("view", "loss_list");
        model.addAttribute("pView", "pd");
        model.addAttribute("ejcd", "仓库");
        model.addAttribute("sjcd", "新增报损单");
        return "/erp_goods_loss_add";
    }

    /**
     * 新增报损单
     * @param request
     * @return
     */
    @RequestMapping(value = "loss_add",method = RequestMethod.POST)
    public String lossAdd( HttpServletRequest request){
        Object userId = request.getSession().getAttribute("userId");
        Object userName = request.getSession().getAttribute("userName");

        String[] specNumbers = request.getParameterValues("specNumber");
        String[] quantities = request.getParameterValues("currQty");
        String[] locationNames = request.getParameterValues("locationName");
        String[] remark = request.getParameterValues("remark");

        String lossText = request.getParameter("lossText");
        String billNo = request.getParameter("billNo");
        String billDate = request.getParameter("billDate");
        for (int i=0;i<specNumbers.length;i++){
            if (!StringUtils.isEmpty(specNumbers[i])) {
                ErpGoodsStockInfoEntity lists = badService.getLocationAndQuanty(locationNames[i], specNumbers[i]);
                if (lists== null){
                    return "redirect:/goods_loss_stock/loss_add?405=405";
                }
                if (lists.getCurrentQty() < Integer.parseInt(quantities[i])) {
                    return "redirect:/goods_loss_stock/loss_add?404=404";
                }

            }
        }
        badService.lossAdd(Integer.parseInt(userId.toString()),userName.toString(),lossText,billNo,billDate,specNumbers,quantities,locationNames,remark);
        //添加系统日志
        logService.addUserAction(Integer.parseInt(request.getSession().getAttribute("userId").toString()), EnumUserActionType.Query,
                "/goods_loss_stock/loss_add","新增报损单 , 报损单编号 : "+billNo+" ,日期 : "+new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss").format(new Date()),"成功");

        return "redirect:/goods_loss_stock/list";
    }

    /**
     * 报损单列表
     * @param request
     * @param model
     * @return
     */
    @RequestMapping("/list")
    public String getGoodsBadList(HttpServletRequest request , Model model){
        Integer pageIndex = 1;
        Integer pageSize = DataConfigObject.getInstance().getPageSize();;
        if (!StringUtils.isEmpty(request.getParameter("page"))) {
            pageIndex = Integer.parseInt(request.getParameter("page"));
        }
        String number = "";
        if (!StringUtils.isEmpty(request.getParameter("billNo"))) {
            number = request.getParameter("billNo");
            model.addAttribute("billNo",number);
        }
        String startTime = "";
        if (!StringUtils.isEmpty(request.getParameter("startDate"))) {
            startTime = request.getParameter("startDate");
            model.addAttribute("startDate",startTime);
        }
        String endTime = "";
        if (!StringUtils.isEmpty(request.getParameter("endDate"))) {
            endTime = request.getParameter("endDate");
            model.addAttribute("endDate",endTime);
        }

        model.addAttribute("pageIndex", pageIndex);
        model.addAttribute("pageSize", pageSize);
        PagingResponse<ErpGoodsStockLossFormEntity> result = badService.getLossFromList(pageIndex, pageSize, number, startTime, endTime);
        model.addAttribute("lists", result.getList());
        model.addAttribute("totalSize", result.getTotalSize());
        

        model.addAttribute("view", "loss_list");
        model.addAttribute("pView", "sj");
        model.addAttribute("ejcd", "仓库");
        model.addAttribute("sjcd", "报损单列表");
        return "wms/erp_goods_loss_list";
    }

    @RequestMapping("/loss_check")
    public String getLossCheck(HttpServletRequest request,Model model){
        Integer id = Integer.parseInt(request.getParameter("id"));
        ErpGoodsStockLossFormVo lossItem = badService.getLossItem(id);
        model.addAttribute("detail",lossItem);
        return "erp_goods_loss_check";
    }

    @RequestMapping("/loss_detail")
    public String getLossDetail(HttpServletRequest request,Model model){
        Integer id = Integer.parseInt(request.getParameter("id"));
        ErpGoodsStockLossFormVo lossItem = badService.getLossItem(id);
        //添加系统日志
        logService.addUserAction(Integer.parseInt(request.getSession().getAttribute("userId").toString()), EnumUserActionType.Query,
                "/goods_loss_stock/loss_detail","报损单打印 , id : "+id+" , 日期 : "+new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss").format(new Date()),"成功");

        model.addAttribute("detail",lossItem);
        return "/erp_goods_loss_detail";
    }
}
