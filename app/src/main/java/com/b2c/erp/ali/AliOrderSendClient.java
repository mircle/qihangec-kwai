//package com.b2c.wms.ali;
//
//import com.b2c.common.third.ali.AliClient;
//import com.b2c.common.third.ali.vo.TokenVo;
//import com.b2c.entity.SysVariableEntity;
//import com.b2c.entity.enums.VariableEnums;
//import com.b2c.repository.mall.SystemRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.RequestMapping;
//
//import jakarta.servlet.http.HttpServletRequest;
//import java.io.IOException;
//
//public class AliOrderSendClient {
//    @Autowired
//    private SystemRepository systemRepository;
//
//    /**
//     * 1.账号授权
//     *
//     * @return
//     */
//    public String authorize(Model model, HttpServletRequest request) {
//        SysVariableEntity variable = systemRepository.getVariable(VariableEnums.URL_WMS_INDEX_IN.name());
//        String returnUrl = variable.getValue() + "/order/redirect_ali&state=YOUR_PARM";
//        return AliClient.getAliAuthorizeUrl(returnUrl);
//    }
//
//
//    /**
//     * 获取订单详情
//     */
//    @RequestMapping("/redirect_ali")
//    public String redirect_ali(Model model, HttpServletRequest request) throws IOException, InterruptedException {
//        /**1.获取令牌**/
//        String code = request.getParameter("code");
//        SysVariableEntity variable = systemRepository.getVariable(VariableEnums.URL_MANAGE_INDEX.name());//获取redirectUrl
//        String redirectUrl = variable.getValue() + "/redirect_token";
//        TokenVo tokenVo = AliClient.getTokenVo(code, redirectUrl);
//
//
//        return "/order_wait_send_list";
//    }
//}
