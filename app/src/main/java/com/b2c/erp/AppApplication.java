package com.b2c.erp;

import com.b2c.erp.thymeleaf.PagingDialect;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.CrossOrigin;

//@EnableDubbo//启动基于dubbo注解功能
@CrossOrigin // 允许跨越访问
@ServletComponentScan
@ComponentScan(basePackages = { "com.b2c.erp", "com.b2c.repository", "com.b2c.service","com.b2c.interfaces" }) // ,"com.b2c.middleware.kafka.producer"
@EnableScheduling
@SpringBootApplication
public class AppApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppApplication.class, args);
    }

    @Bean
    public PagingDialect paging() {
    return new PagingDialect();
    }
}
