package com.b2c.erp.req;

/**
 * 描述：
 * 商品入库req
 *
 * @author qlp
 * @date 2019-05-30 16:04
 */
public class GoodsSpecInReq {
    private String specNumber;//商品规格编码
    private Long invoiceId;//采购单据id

    public String getSpecNumber() {
        return specNumber;
    }

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    public Long getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Long invoiceId) {
        this.invoiceId = invoiceId;
    }
}
