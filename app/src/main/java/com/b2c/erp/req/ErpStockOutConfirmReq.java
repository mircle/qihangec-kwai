package com.b2c.erp.req;

public class ErpStockOutConfirmReq {
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
