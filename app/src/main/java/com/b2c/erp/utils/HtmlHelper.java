package com.b2c.erp.utils;


import java.util.Formatter;
import org.apache.poi.ss.usermodel.CellStyle;

public interface HtmlHelper {
    void colorStyles(CellStyle var1, Formatter var2);
}