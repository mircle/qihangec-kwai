//package com.b2c.wms.utils;
//
//import javax.imageio.ImageIO;
//import java.awt.*;
//import java.awt.image.BufferedImage;
//import java.io.*;
//import java.net.HttpURLConnection;
//import java.net.URL;
//
///**
// * 描述：
// * java图片合成
// *
// * @author qlp
// * @date 2019-06-12 09:18
// */
//public class DrawingUtils {
//
//    public static String generateCode(String productImageUrl,String codeUrl, Integer userId, String userName) {
//
//        BufferedImage productBufferedImage; //商品图片
//        BufferedImage codeBufferedImage; //商品图片
////        FileOutputStream fos = null;
//        BufferedInputStream bis = null;
//        HttpURLConnection httpUrl = null;
//        URL url = null;
//        int BUFFER_SIZE = 1024;
//        byte[] buf = new byte[BUFFER_SIZE];
//        int size = 0;
//        try {
//            url = new URL(productImageUrl);
//            httpUrl = (HttpURLConnection) url.openConnection();
//            httpUrl.connect();
//            bis = new BufferedInputStream(httpUrl.getInputStream());
//            productBufferedImage = ImageIO.read(bis);
//
////            fos = new FileOutputStream("c:\\haha.gif");
////            while ((size = bis.read(buf)) != -1) {
////                fos.write(buf, 0, size);
////            }
////            fos.flush();
//        } catch (IOException e) {
//        } catch (ClassCastException e) {
//        } finally {
//            try {
////                fos.close();
//                bis.close();
//                httpUrl.disconnect();
//            } catch (IOException e) {
//            } catch (NullPointerException e) {
//            }
//        }
//
//
//
//        Font font = new Font("微软雅黑", Font.PLAIN, 30);// 添加字体的属性设置
//
//        String projectUrl = PathKit.getWebRootPath() + "/before/codeImg/";
//
//        String imgName = projectUrl + userId + ".png";
//        try {
//            // 加载本地图片
//            String imageLocalUrl = projectUrl + "weixincode2.png";
//            BufferedImage imageLocal = ImageIO.read(new File(imageLocalUrl));
//            // 加载用户的二维码
//            BufferedImage imageCode = ImageIO.read(new URL(codeUrl));
//            // 以本地图片为模板
//            Graphics2D g = imageLocal.createGraphics();
//            // 在模板上添加用户二维码(地址,左边距,上边距,图片宽度,图片高度,未知)
//            g.drawImage(imageCode, 575, imageLocal.getHeight() - 500, 350, 350, null);
//            // 设置文本样式
//            g.setFont(font);
//            g.setColor(Color.BLACK);
//            // 截取用户名称的最后一个字符
//            String lastChar = userName.substring(userName.length() - 1);
//            // 拼接新的用户名称
//            String newUserName = userName.substring(0, 1) + "**" + lastChar + " 的邀请二维码";
//            // 添加用户名称
//            g.drawString(newUserName, 620, imageLocal.getHeight() - 530);
//            // 完成模板修改
//            g.dispose();
//            // 获取新文件的地址
//            File outputfile = new File(imgName);
//            // 生成新的合成过的用户二维码并写入新图片
//            ImageIO.write(imageLocal, "png", outputfile);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        // 返回给页面的图片地址(因为绝对路径无法访问)
////        imgName = Constants.PROJECT_URL + "codeImg/" + userId + ".png";
//
//        return imgName;
//    }
//
//
////    private static float jPEGcompression = 0.75f;// 图片清晰比率
////
////    /**
////     * @Description : 将二维码图片和文字生成到一张图片上
////     * @Param : originalImg 原图
////     * @Param : qrCodeImg 二维码地址
////     * @Param : shareDesc 图片文字
////     * @return : java.lang.String
////     * @Author : houzhenghai
////     * @Date : 2018/8/15
////     */
////    public static String generateImg(String originalImg, String qrCodeImg, String shareDesc) throws Exception {
////        // 加载原图图片
////        BufferedImage imageLocal = ImageIO.read(new URL(originalImg));
////        // 加载用户的二维码
////        BufferedImage imageCode = ImageIO.read(new URL(qrCodeImg));
////        // 以原图片为模板
////        Graphics2D g = imageLocal.createGraphics();
////        AlphaComposite ac = AlphaComposite.getInstance(AlphaComposite.SRC_OVER);
////        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
////        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
////        g.setComposite(ac);
////        g.setBackground(Color.WHITE);
////        // 在模板上添加用户二维码(地址,左边距,上边距,图片宽度,图片高度,未知)
////        g.drawImage(imageCode, 100, imageLocal.getHeight() - 190, 160, 158, null);
////        // 设置文本样式
////        g.setFont(new Font("微软雅黑", Font.PLAIN, 40));
////        g.setColor(Color.red);
////        // 计算文字长度，计算居中的x点坐标
////        g.drawString(shareDesc, imageLocal.getWidth() - 330, imageLocal.getHeight() - 530);
////
////        // 设置文本样式
////        g.setFont(new Font("微软雅黑", Font.PLAIN + Font.BOLD, 16));
////        g.setColor(Color.WHITE);
////        // 计算文字长度，计算居中的x点坐标
////        String caewm = "长按二维码";
////        g.drawString(caewm, 105, imageLocal.getHeight() - 10);
////
////        ByteArrayOutputStream out = new ByteArrayOutputStream();
////        saveAsJPEG(imageLocal, out);
////        out.close();
////        return urlImgDownInputStream(FileUtils.parse(out));
////    }
////
////    /**
////     * 以JPEG编码保存图片
////     *
////     * @param image_to_save
////     *            要处理的图像图片
////     * @param fos
////     *            文件输出流
////     * @throws IOException
////     */
////    private static void saveAsJPEG(BufferedImage imageToSave, ByteArrayOutputStream fos) throws IOException {
////        ImageWriter imageWriter = ImageIO.getImageWritersBySuffix("jpg").next();
////        ImageOutputStream ios = ImageIO.createImageOutputStream(fos);
////        imageWriter.setOutput(ios);
////        if (jPEGcompression >= 0 && jPEGcompression <= 1f) {
////            // new Compression
////            JPEGImageWriteParam jpegParams = (JPEGImageWriteParam) imageWriter.getDefaultWriteParam();
////            jpegParams.setCompressionMode(JPEGImageWriteParam.MODE_EXPLICIT);
////            jpegParams.setCompressionQuality(jPEGcompression);
////
////        }
////        // new Write and clean up
////        ImageIO.setUseCache(false);
////        imageWriter.write(new IIOImage(imageToSave, null, null));
////        ios.close();
////        imageWriter.dispose();
////    }
////
////    /**
////     * 图片流远程上传
////     *
////     * @author houzhenghai
////     * @date 2018年10月19日 下午5:07:24
////     * @param inStream
////     * @param imgType
////     * @return
////     * @throws Exception
////     */
////    private static String urlImgDownInputStream(InputStream inStream) throws Exception {
////        String md5 = MD5Utils.MD5(TimeUtils.getTimestamp().toString());
////        HttpClient httpclient = new DefaultHttpClient();
////        try {
////            HttpPost post = new HttpPost(IMG_UPLOAD_PATH);// 文件服务器上传图片地址
////            MultipartEntity mpEntity = new MultipartEntity();
////            ContentBody contb = new InputStreamBody(inStream, md5 + ".png");
////            mpEntity.addPart("Filedata", contb);
////            post.setEntity(mpEntity);
////            HttpResponse httpResponse = httpclient.execute(post);
////            HttpEntity entity = httpResponse.getEntity();
////            String jsonStr = EntityUtils.toString(entity);
////            JSONObject ob = JSONObject.fromObject(jsonStr);
////            if (!ob.isEmpty() && ob.containsKey("pic_id")) {
////                return ob.getString("pic_id");
////            }
////        } catch (Exception e) {
////            e.printStackTrace();
////        } finally {
////            httpclient.getConnectionManager().shutdown();
////        }
////        return null;
////    }
////
////    /**
////     * test
////     *
////     * @param args
////     * @throws
////     */
////    public static void main(String[] args) {
////        long starttime = System.currentTimeMillis();
////        System.out.println("开始：" + starttime);
////        try {
////            String originalImg = "http://xxxxxx.com/images/original.jpg";
////            String qrCodeImg = "http://xxxxxx.com/images/qrcode.jpg";
////            String shareDesc = "原价9999.99元";
////            String img = generateImg(originalImg, qrCodeImg, shareDesc);
////            System.out.println("生成完毕,url=" + img);
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////        System.out.println("结束：" + (System.currentTimeMillis() - starttime) / 1000);
////    }
//}
