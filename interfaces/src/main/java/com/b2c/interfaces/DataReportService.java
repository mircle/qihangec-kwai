package com.b2c.interfaces;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.DailyReportEntity;

public interface DataReportService {

    /**
     * 分页查询统计数据
     * @param pageIndex
     * @param pageSize
     * @return
     */
    public PagingResponse<DailyReportEntity> dailyReportPageList(int pageIndex, int pageSize, String startDate, String endDate);
}
