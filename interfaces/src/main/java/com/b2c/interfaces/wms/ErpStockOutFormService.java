
package com.b2c.interfaces.wms;

import com.b2c.entity.ErpOrderItemEntity;
import com.b2c.entity.StockDataVo;
import com.b2c.entity.ErpStockOutFormItemEntity;
import com.b2c.entity.ErpStockOutPickVo;

import com.b2c.entity.ErpStockOutFormEntity;
import com.b2c.entity.erp.vo.ErpStockOutGoodsListVo;
import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.vo.ErpStockOutFormDetailVo;

import java.util.List;

/**
 * 描述：
 * 出库商品清单Service
 *
 * @author qlp
 * @date 2019-06-21 13:43
 */
public interface ErpStockOutFormService {

    /**
     * 获取拣货单list
     *
     * @param pageIndex
     * @param pageSize
     * @param No
     * @return
     */
    PagingResponse<ErpStockOutFormEntity> getPickingList(Integer pageIndex, Integer pageSize, String No, Integer status, Long id, String sourceNo);

    /**
     * 根据主键获取表单
     *
     * @param id
     * @return
     */
    ErpStockOutFormEntity getStockOutFormById(Long id);

    /**
     * 根据单号获取表单
     *
     * @param stockOutNo
     * @return
     */
    ErpStockOutFormEntity getStockOutFormByNumber(String stockOutNo);

    /**
     * 根据单据id获取商品list
     *
     * @param stockOutId
     * @return
     */
    List<ErpStockOutGoodsListVo> getStockOutGoodsForOrderByFormId(Long stockOutId);
    List<ErpStockOutGoodsListVo> getStockOutFormItemListByFormId(Long stockOutId);


    /**
     * 打印拣货单，更新拣货单状态为已打印
     *
     * @param id
     * @return
     */
    Integer printPicking(Long id);

    /**
     * 按订单出库
     * @param orderId 订单ID
     * @param userId
     * @param userName
     * @return
     */
    ResultVo<Integer> orderGoodsStockOut(Long orderId,Integer userId, String userName);


    /**
     * 出库单列表
     *
     * @param pageIndex
     * @param pageSize
     * @param number
     * @param startDate
     * @param endDate
     * @return
     */
    public PagingResponse<ErpStockOutFormEntity> getStockOutFormList(Integer pageIndex, Integer pageSize,Integer outType, String number, String startDate, String endDate);

    /**
     * 出库单记录
     *
     * @param id
     * @return
     */
    public ErpStockOutFormDetailVo getErpStockOutFormDetailVo(Long id);



    /**
     * 取消拣货
     * @param stockOutFormItemId
     * @return
     */

    ResultVo<Integer> cancelStockOut(Long stockOutFormItemId,Integer userId, String userName);

    /**
     * 确认拣货单，更新拣货单状态为已拣货
     *
     * @param id   拣货单id
     * @param list 确认拣货明细list
     * @return
     */
    Integer completePicking(Long id, List<ErpStockOutPickVo> list, Integer userId, String userName);
    public List<ErpOrderItemEntity> outList (String startDate, String endDate);

    /**
     * 小红书店铺订单加入到仓库（2022-4-19 20：45）
     * @param sourceNo
     * @param
     * @param orderId
     * @param createBy
     * @param stockDataVos
     * @return
     */
    ResultVo<Long> joinStockOutQueueForXHS(String sourceNo, Long orderId, String createBy, List<StockDataVo> stockDataVos,String companyCode,String expressNo);


    /**
     * 获取待拣货商品信息
     * @param pageIndex
     * @param pageSize
     * @return
     */
    PagingResponse<ErpStockOutFormItemEntity> getStockOutFormItemWaitPickGoodsList(Integer pageIndex, Integer pageSize,String specNum);

    /**
     * 小红书发货
     * @param orderId
     * @param companyCode
     * @param expressNo
     * @return
     */
    ResultVo<Long> sendOrderXHS(Long orderId, String companyCode,String expressNo);
}
