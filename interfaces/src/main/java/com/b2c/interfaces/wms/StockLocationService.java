package com.b2c.interfaces.wms;

import com.b2c.entity.ErpOrderItemEntity;
import com.b2c.entity.ErpStockLocationEntity;
import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ErpStockLocationBatchAddVo;
import com.b2c.entity.result.ResultVo;

import java.util.List;

/**
 * 描述：
 * 仓库Service
 *
 * @author qlp
 * @date 2019-03-21 18:17
 */
public interface StockLocationService {
    /**
     * 获取仓库列表(根据上级)
     *
     * @return
     */
    List<ErpStockLocationEntity> getListByParentId(Integer parentId);
    public List<ErpOrderItemEntity> localtionNumber();

    /**
     * 根据层级获取仓库
     * @param depth
     * @return
     */
//    List<ErpStockLocationEntity> getListByDepth(Integer depth);

    /**
     * 根据层级获取仓库(分页)
     * @param pageIndex
     * @param pageSize
     * @param depth
     * @return
     */
    PagingResponse<ErpStockLocationEntity> getListByDepth(Integer pageIndex, Integer pageSize, Integer depth,String number);

    /**
     * 根据仓库名查询id
     *
     * @param name
     * @return
     */
    public Integer getIdByName(String name);

    /**
     * 根据仓库编码查询id
     *
     * @param number
     * @return
     */
    public Integer getIdByNumber(String number);

    ErpStockLocationEntity getEntityByNumber(String number);

    /**
     * 检查仓位是否被使用
     * @param id
     * @return
     */
    boolean checkStockLocationUsed(Integer id);

    /**
     * 添加仓库信息（一级）
     *
     * @param number
     * @param name
     * @return
     */
    public Integer addStockHouse(String number, String name);

    /**
     * 添加仓库库区
     * @param number
     * @param name
     * @param parentId1
     * @return
     */
    Integer addReservoir(String number, String name, Integer parentId1);

    /**
     * 添加仓库仓位
     * @param number
     * @param name
     * @param houseId
     * @param reservoirId
     * @return
     */
    Integer addShelf(String number, String name, Integer houseId, Integer reservoirId);

    /**
     * 更新仓位信息
     * @param id
     * @param number
     * @param name
     * @param houseId
     * @param reservoirId
     */
    void updateShelf(Integer id, String number, String name, Integer houseId, Integer reservoirId);

    /**
     * 更新库区
     * @param id
     * @param number
     * @param name
     * @param houseId
     */
    void updateReservoir(Integer id, String number, String name, Integer houseId);
    /**
     * 更新仓库信息
     *
     * @param stockId
     * @param number
     * @param name
     */
    public void updateStockHouse(Integer stockId, String number, String name);

    /**
     * 删除仓库
     *
     * @param id
     */
    public ResultVo<Integer> delStockHouse(Integer id);

    /**
     * 根据Id查询仓库名
     *
     * @param id
     * @return
     */
    public String getName(Integer id);

    ResultVo<Integer> batchAddShelf(ErpStockLocationBatchAddVo shelf);

    /**
     * 查询库存（没有被使用的）
     * @param reservoirId
     * @return
     */
//    List<ErpStockLocationEntity> getShelfByReservoirIdAndNotUsed(Integer reservoirId, Integer selfId);

    /**
     * 获取仓库list by 编码
     * @param number
     * @return
     */
    List<ErpStockLocationEntity> getListByNumber(String number);
}
