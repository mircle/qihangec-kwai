


package com.b2c.interfaces.wms;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ErpStockInFormEntity;
import com.b2c.entity.vo.finance.ErpStockInFormItemVo;
import com.b2c.entity.query.ErpStockInItemQuery;
import com.b2c.entity.vo.ErpStockInFormDetailVo;

import java.util.List;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-10-09 15:11
 */
public interface ErpStockInFormService {
    /**
     * 获取入库单详情
     * @param id
     * @return
     */
    ErpStockInFormDetailVo getById(Long id);

    /**
     * 根据验货单获取入库list
     * @param checkoutId
     * @return
     */
    List<ErpStockInFormEntity> getListByCheckoutId(Long checkoutId);

    /**
     * 查询所有入库单
     * @param pageIndex
     * @param pageSize
     * @param inType 入库类型1采购入库2退货入库3盘点入库
     * @param number 单号
     * @param startDate 开始日期
     * @param endDate 结束日期
     * @return
     */
    PagingResponse<ErpStockInFormEntity> getErpStockInList(Integer pageIndex, Integer pageSize,Integer inType, String number, Integer startDate, Integer endDate,Long invoiceId);

    /**
     * 获取入库list
     * @param pageIndex
     * @param pageSize
     * @param billNo 采购单号
     * @param contractNo 合同好
     * @param startTime
     * @param endTime
     * @return
     */
    PagingResponse<ErpStockInFormItemVo> getErpStockInItemListByPurchase(Integer pageIndex, Integer pageSize, String billNo, String contractNo, String checkoutNumber,String specNumber, Integer startTime, Integer endTime);

    /**
     * 获取入库数据excel
     * @param billNo
     * @param contractNo
     * @param startTime
     * @param endTime
     * @return
     */
    List<ErpStockInFormItemVo> getErpStockInItemListByPurchaseForExcel(String billNo, String contractNo, String stockInNumber, Integer startTime, Integer endTime);

    /**
     * 获取退货入库数据
     * @param pageIndex
     * @param pageSize
     * @param refundNo
     * @param startTime
     * @param endTime
     * @return
     */
    PagingResponse<ErpStockInFormItemVo> getErpStockInItemListByRefund(Integer pageIndex, Integer pageSize, String refundNo,Integer startTime, Integer endTime);

    List<ErpStockInFormItemVo> getErpStockInItemListByRefundForExcel(String refundNo, Integer startTime, Integer endTime);

    /**
     * 查询仓库采购入库数据统计
     * @return
     */
    public PagingResponse<ErpStockInFormItemVo> purchase_in_list_count(ErpStockInItemQuery query);
    public List<ErpStockInFormItemVo> purchase_in_list_count_export(ErpStockInItemQuery query);
    public  List<ErpStockInFormItemVo> purchase_in_ht_export(Long iid);
}
