package com.b2c.interfaces.pdd;

import java.util.List;

import com.b2c.entity.pdd.FinanceMonthReportPddVo;
import com.b2c.entity.pdd.SalesAndRefundReportPddVo;
import com.b2c.entity.pdd.SalesReportPddVo;

public interface PddSalesReportService {
    List<SalesReportPddVo> getSalesReport(Integer shopId,String startDate,String endDate);

    List<FinanceMonthReportPddVo> getFinanceMonthReport(Integer shopId,String month);

    List<SalesReportPddVo> getSendReport(String date,Integer shopId);

    /**
     * 销量与退货数据
     * @param shopId
     * @return
     */
    List<SalesAndRefundReportPddVo> getSalesAndRefundReport(Integer shopId);
}
