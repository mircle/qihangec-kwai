package com.b2c.interfaces.ecom;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.KeyWordEntity;

import java.util.List;

public interface KeyWordService {
    void addKeyWord(KeyWordEntity entity);
    PagingResponse<KeyWordEntity> getList(Integer pageIndex, Integer pageSize,String source,String year,String keyword,Long parentId,Integer categoryId);
    List<KeyWordEntity> getParentList(Long parentId);
    KeyWordEntity getById(Long id);
}
