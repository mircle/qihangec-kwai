package com.b2c.interfaces;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.ManageGroupEntity;
import com.b2c.entity.ManageUserEntity;
import com.b2c.entity.WmsManagePermissionEntity;
import com.b2c.entity.vo.WmsManageUserMenuVo;

import java.util.List;

/**
 * @Description: pbd add 2019/9/28 10:01
 */
public interface WmsUserService {
    /**
     * 查询管理员分组
     * @param pageIndex
     * @param pageSize
     * @param name
     * @param mobile
     * @return
     */
    PagingResponse<ManageUserEntity> getWmsManageUsers(int pageIndex, int pageSize, String name, String mobile);

    /**
     * 查询管理员信息
     * @param userId
     * @return
     */
    ManageUserEntity getWmsManageUser(Integer userId);

    /**
     * 查询所有管理员分组
     *
     * @return
     */
    List<ManageGroupEntity> getManageGroups();

    /**
     * 添加管理员
     *
     * @param name    姓名
     * @param mobile  手机号
     * @param pwd     密码
     * @param groupId 分组id
     * @param state   状态
     */
    ResultVo<Integer> executeWmsManageUser(Integer userId,String userName, String name, String mobile, String pwd, String groupId, String state);
    /**
     * 删除管理员
     *
     * @param userId
     */
    void delWmsManageUser(Integer userId);
    /**
     * 管理员登录
     *
     * @param userName
     * @param userPwd
     * @return
     */
    ResultVo<ManageUserEntity> userLogin(String userName, String userPwd);

    /**
     * 查询用户已授权菜单,查询用户已授权菜单,存在返回true
     * @param userId
     * @return
     */
     boolean checkUserPermissionMenu(Integer userId, String permissionKey);

    /**
     * 查询用户已授权菜单访问地址,已授权返回true
     * @param userId
     * @return
     */
     boolean checkUserPermissionMenuByUrl(Integer userId, String url);

    /**
     * 获取权限点实体
     * @param permissionKey
     * @return
     */
     WmsManagePermissionEntity getEntityByKey(String permissionKey);

    /**
     * 初始化用户权限菜单
     * @param userId
     */
     void initUserPermissionMenu(Integer userId);

    /**
     * 查询用户权限菜单列表
     * @param userId
     * @param type 0 所有菜单 1授权菜单
     * @return
     */
    List<WmsManageUserMenuVo> getUserPerMissionMenuByWMS(Integer userId, Integer type);


    /**
     * 设置用户权限菜单
     * @param userId 用户id
     * @param menuIds 菜单列表
     */
    void setManageUserMenu(Integer userId,String[] menuIds);

    /**
     * 修改用户登录密码
     *
     * @param userId
     * @param oldPwd
     * @param newPwd
     * @return
     */
    ResultVo<ManageUserEntity> updWmsUserPwd(int userId, String oldPwd, String newPwd);
}
