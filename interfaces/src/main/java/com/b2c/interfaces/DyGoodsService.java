package com.b2c.interfaces;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.douyin.DyGoodsEntity;
import com.b2c.entity.result.ResultVo;

public interface DyGoodsService {
    ResultVo<Long> addGoods(DyGoodsEntity goodsEntity);

     PagingResponse<DyGoodsEntity> getGoodsList(Integer shopId,Integer pageIndex, Integer pageSize, String num);
}
