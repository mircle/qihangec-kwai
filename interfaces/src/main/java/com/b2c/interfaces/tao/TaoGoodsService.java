package com.b2c.interfaces.tao;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.tao.TaoGoodsEntity;

public interface TaoGoodsService {
    ResultVo<Long> addGoods(TaoGoodsEntity goodsEntity);

     PagingResponse<TaoGoodsEntity> getGoodsList(Integer shopId,Integer pageIndex, Integer pageSize, String num);
}
