package com.b2c.interfaces.erp;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.GoodsCategoryAttributeEntity;
import com.b2c.entity.GoodsSpecAttrEntity;
import com.b2c.entity.erp.ErpGoodsBrandEntity;
import com.b2c.entity.erp.ErpGoodsEntity;
import com.b2c.entity.erp.ErpGoodsSpecEntity;
import com.b2c.entity.erp.GoodsCategoryEntity;
import com.b2c.entity.erp.vo.*;
import com.b2c.entity.result.ResultVo;

import java.util.List;

/**
 * 描述：
 * 商品Service
 *
 * @author qlp
 * @date 2019-03-21 16:59
 */
public interface ErpGoodsService {

    PagingResponse<ErpGoodsEntity> getList(Integer pageIndex, Integer pageSize, String goodsNumber,Integer isDelete);
    /**
     * 查询抖音商品销量统计
     * @param shopId
     * @param pageIndex
     * @param pageSize
     * @param goodsNumber
     * @param startTime
     * @param endTime
     * @return
     */
    public PagingResponse<GoodsSearchShowVo> getDySalesList(Long shopId, Integer pageIndex, Integer pageSize, String goodsNumber, Integer startTime, Integer endTime);

    /**
     * 最近7天上新
     * @return
     */
    List<ErpGoodsEntity> getListFor7Day();
    /**
     * 关键词搜索商品（采购）
     *
     * @param number
     * @return
     */
    List<GoodsSearchShowVo> getGoodsSpecByNumberForPurchase(String number, int limit);



    /**
     * 商品编号搜索商品
     *
     * @param number
     * @return
     */
    List<GoodsSearchByNumberVo> getGoodsByNumber(String number);
    ErpGoodsEntity getGoodsEntityByNumber(String number);

    /**
     * 根据商品id查询商品规格
     *
     * @param goodsId
     * @return
     */
    List<ErpGoodsSpecListVo> getSpecByGoodsId(Integer goodsId);

    /**
     *
     * @param goodsId
     * @return
     */
    List<ErpGoodsSpecEntity> getSpecListByGoodsId(Integer goodsId);

    /**
     * 根据goodsNumber获取商品规格list
     *
     * @param goodsNumber
     * @return
     */
    PagingResponse<ErpGoodsSpecListVo> getSpecByGoodsNumber(Integer pageIndex,Integer pageSize,String goodsNumber);



    /**
     * excel批量导入商品
     *
     * @param goodsList
     * @return
     */
    ResultVo<Integer> goodsExcelBatchAdd(List<GoodsExcelVo> goodsList);


    GoodsSpecDetailVo getGoodsSpecDetailByNumber(String number);

    /**
     * 获取商品总数
     *
     * @return
     */
    int getTotalGoods();

    /**
     * 添加erp goods（单个）
     *
     * @param goodsAddVo
     * @return
     */
    ResultVo<Integer> goodsAdd(ErpGoodsAddVo goodsAddVo);

    public ResultVo<Integer> goodsAddSpec(ErpGoodsAddVo goodsAddVo);

    ErpGoodsEntity getById(Integer id);

    /**
     * 根据规格编码获取商品规格
     * @param specNumber
     * @return
     */
    ErpGoodsSpecEntity getSpecByNumber(String specNumber);

    ErpGoodsSpecEntity getSpecBySpecId(Integer specId);

    /**
     * @param type
     * @return
     */
    List<GoodsSpecAttrEntity> getSkuValueList(String type, Integer goodsId);

    /**
     * 修改erp goods (同步修改华衣云购商城商品编码)
     *
     * @param goodsId
     * @param goodsAddVo
     * @return
     */
    ResultVo<Integer> goodsBaseInfoEdit(Integer goodsId, ErpGoodsAddVo goodsAddVo);

    /**
     * 根据Id查询商品信息
     *
     * @param id
     * @return
     */
    public ErpGoodsEntity getErpGoodsEntity(Integer id);

    /**
     * 更新仓库商品到采购商品库
     */
//    void updErpGoodsByOGoods();


    void priceEdit(Integer goodsId, Float wholesalePrice, Float salePrice);

    List<GoodsCategoryAttributeEntity> getSpecByCategory(Integer categoryId);

    /**
     * 获取品牌列表
     * @return
     */
    List<ErpGoodsBrandEntity> getBrandList();

    public void updErpGoodSpecAttr(Integer specId,String attr1,String attr2);

    /**
     * 
     * 修改规格采购价
     * @param specId
     * @param purPrice
     */
    void updateSpecPurPrice(Integer specId,Double purPrice);

    /**
     * 获取商品库存数量
     * @param specId
     * @return
     */
    Integer getGoodsSpecStockById(Integer specId);

    List<GoodsCategoryEntity> getCategoryListByParentId(Integer parendId);
}
