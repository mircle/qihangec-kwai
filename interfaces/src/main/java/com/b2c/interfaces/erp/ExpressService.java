package com.b2c.interfaces.erp;

import com.alibaba.fastjson.JSONObject;
import com.b2c.entity.ExpressEntity;
import com.b2c.entity.erp.vo.PrintImgVo;
import com.b2c.entity.result.ResultVo;

/**
 * @Description: pbd add 2019/6/17 10:56
 */
public interface ExpressService {
    /**
     * 生成快递打印图片
     *
     * @param print
     */
//    String printImg(PrintImgVo print);

    /**
     * 打印快递面单
     * @param orderId
     * @param kuaidicom
     * @param kuaidinum
     * @param imgBase64
     * @return
     */
    ResultVo<Integer> printExpress(Long orderId,String kuaidicomName, String kuaidicom, String kuaidinum, String imgBase64);

    /**
     * 查询订单发货物流信息
     *
     * @param orderNum
     * @return
     */
    ExpressEntity getExpress(String orderNum);

    /**
     * 更新快递信息到平台
     *
     * @param obj
     */
    void updExpressInfo(JSONObject obj);
}
