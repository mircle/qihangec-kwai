package com.b2c.interfaces.erp;

import com.b2c.entity.UserEntity;

import java.util.List;

public interface ErpSalesUserService {
    /**
     * 查询业务员列表
     * @return
     */
     List<UserEntity> getDeveloperUserList();
    /**
     * 根据业务员id查询客户列表
     * @param developerId
     * @return
     */
    List<UserEntity> getUserListByDeveloperId(Integer developerId);
    /**
     * 查询客户列表
     * @return
     */
    List<UserEntity> getUserList();


}
