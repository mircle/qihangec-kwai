package com.b2c.interfaces;

import com.b2c.entity.vo.GoodsCategoryAnalyseVo;

import java.util.List;

public interface GoodsCategoryAnalyseService {

    List<GoodsCategoryAnalyseVo> getAnalyseReport();
}
