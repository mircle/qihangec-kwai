package com.b2c.interfaces.dou;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.DataRow;
import com.b2c.entity.douyin.*;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.vo.DyOrderImportOrderVo;

import java.util.ArrayList;
import java.util.List;

public interface DouyinOrderService {
    /**
     * 新增/修改订单数据
     * @param order
     * @param flag 0修改新增 1：新增
     * @return
     */
    ResultVo<Integer> editDouYinOrder(DcDouyinOrdersEntity order, Integer flag);
    /**
     * 分页查询商品列表
     * @param pageIndex
     * @param pageSize
     * @param orderNum
     * @param startTime
     * @param endTime
     * @param state
     * @return
     */
    PagingResponse<DcDouyinOrdersListVo> getDouyinOrders(Long shopId,Integer pageIndex, Integer pageSize, String orderNum, Integer startTime, Integer endTime, Integer state,Integer auditStatus,String logisticsCode);

    List<DcDouyinOrdersListVo> getDouyinOrders(Integer shopId,Integer startTime, Integer endTime,Integer status);

    public List<DcDouyinOrdersItemsEntity> getDouyinOrdersExport(Integer startTime, Integer endTime,Integer status);
    /**
     * 查询订单信息
     * @param orderId
     * @return
     */
     DcDouyinOrdersListVo getOderDetailByOrderId(Long orderId);
     DcDouyinOrdersListVo getOderDetailByOrderIdForSend(Long orderId);

    public List<DcDouyinOrdersItemsEntity> getOderDetailByPrint(String logisticsCode, Integer print);
    /**
     * 订单确认
     * @return
     */
     ResultVo<Integer> douyinOrderAffirm(Long dcDouyinOrdersId, Integer clientId, String receiver, String mobile, String address, String sellerMemo);

    /**
     * 修改订单item商品
     * @param orderItemId
     * @param erpGoodSpecId
     * @param quantity
     * @return
     */
    ResultVo<Long> updOrderSpec(Long orderItemId, Integer erpGoodSpecId, Integer quantity);
    /**
     * 订单发货
     * @param orderId
     */
    void updDouyinOrderStatus(String orderId,Integer orderStatus);

    /**
     * 添加订单赠品
     * @param orderId
     * @param erpGoodsId
     * @param erpGoodSpecId
     * @param quantity
     * @return
     */
    ResultVo<Long> addOrderGift(Long orderId, Integer erpGoodsId, Integer erpGoodSpecId, Integer quantity);
    /**
     * 更新退货订单
     * @param order
     * @return
     */
    ResultVo<Integer> editDouYinRefundOrder(DcDouyinOrdersRefundEntity order);
    /**
     * 分页查退货订单列表
     *
     * @param pageIndex
     * @param pageSize
     * @param state
     * @return
     */
     PagingResponse<DouyinOrdersRefundEntity> getDouyinRefundOrders(Integer shopId,Integer pageIndex, Integer pageSize, String refundNum, String logisticsCode, Integer startTime, Integer endTime, Integer state,Integer aftersaleType);
    /**
     * 退货订单审核到仓库
     * @param id
     * @return
     */
    ResultVo<Integer> confirmRefund(Long id, String logisticsCompany, String logisticsCode);
    /**
     * 获取售后订单详情
     * @param refundId
     * @return
     */
     DouyinOrdersRefundEntity getDouYinRefundOrderDetail(Long refundId);
    /**
     *
     * @param orderId
     * @param logisticsId
     * @param logisticsCode
     */
    public void updOrderLogisticsCode(Long orderId,String logisticsId,String logisticsCode,Integer print);

    public PagingResponse<DcDouyinOrdersItemsEntity> getDouyinPrintOrders(Integer dyShopId,Integer pageIndex, Integer pageSize, String orderNum, Integer startTime, Integer endTime, String goodsNum, String skuNum,Integer print,String code,String startDate,String endDate,Integer isBz);
    public void updOrderPrint(Long orderId,String result,Integer orderStatus,Integer print);

    /**
     * 更新订单状态
     * @param orderId
     * @param status
     */
    void dyOrderCancelNotify(Long orderId, Integer status, String cancelReason);

    public ResultVo<List<DcDouyinOrdersItemsEntity>>  getPrintOrderList(Long orderId, Integer isHebing, Integer print);

    /**
     * 订单发货
     * @param order
     * @return
     */
    public ResultVo<Integer> orderSend(DcDouyinOrdersListVo order);

    /**
     * 
     * 订单手动发货
     * @param id
     * @param company
     * @param code
     * @return
     */
    ResultVo<Integer> orderHandSend(Long id,String company,String code);

    public ResultVo<Integer> cancelOrderPrint(Long orderId);
    public void updRefundOrderAuditStatus(Long orderId,Integer auditStatus);

    public List<DcDouyinOrdersListVo> getDouyinOrderHebing(Integer dyShopId);

    public void updOrderPrintByCode(String logisticsCode,Integer orderStatus,String result,Integer print);
    /**
     * 订单批量修改sku
     * @param orderIds
     * @param oldSku
     * @param newSku
     * @return
     */
    public ResultVo<Integer> ordersUpdSku(ArrayList orderIds, String oldSku, String newSku);
    public ResultVo<Integer> ordersUpdSkuById(Long id,String newSku);
    public void zhuBoOrderSettle(List<Long> ids,Integer isSettle);
    public List<DouyinOrderStatis> douyinOrderStatis(String startTime, String endTime);
    public void test(List<DataRow> datas);
    public Long getDyOrderTime(Integer shopId);

    /**
     * 查询直播间销售数据
     * @param pageIndex
     * @param pageSize
     * @param orderDate 下单日期
     * @param shopId 店铺id
     * @param authorId 直播间作者id
     * @return
     */
    PagingResponse<DouyinOrderStatisticsEntity> getOrderStatisticsList(Integer pageIndex, Integer pageSize, String orderDate, Integer shopId, Long authorId);

    /**
     * 导出直播间销售数据
     * @param orderDate
     * @param shopId
     * @param authorId
     * @return
     */
    List<DouyinOrderStatisticsEntity> getOrderStatisticsListExport(String orderDate,Integer shopId,Long authorId);

    ResultVo<String> importExcelOrder(List<DyOrderImportOrderVo> orderList,Integer shopId);

    /**
     * 修改收货地址
     * @param id 主键id
     * @param receiverName
     * @param receiverPhone
     * @param receiverAddress
     * @return
     */
    ResultVo<Integer> updOrderAddress(Long id,String receiverName,String receiverPhone,String receiverAddress);

    /**
     * 手动添加退货
     * @param orderItemId
     * @param refundId
     * @return
     */
     ResultVo<Integer> handAddRefund(Long orderItemId,Long refundId);


    /**
      * 修改退货物流
      * @param orderId
      * @param company
      * @param code
      */
      void editRefundLogisticsCode(Long refundId,String company,String code,String logisticsTime);
}
